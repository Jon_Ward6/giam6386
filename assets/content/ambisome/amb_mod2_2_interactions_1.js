/**
 * Ambisome eDetailer Page Mod2_2_Interactions_1
 *
 * Used with the com.ward6.DragDrop library to allow for the drag-and-drop
 * functionality requird on this page.
 *
 * GIAM6386 - Ward6
 * 
 * @author Kashi Samaraweera
 * @copyright Ward6
 */

$(document).ready(function() {
	if (typeof(com.ward6.EDetailerPage) === "undefined")
        return false;

    var EDetailerPage = com.ward6.EDetailerPage;
    EDetailerPage.setModuleXML('references.xml');

    document.touchmove = function(event){
        console.log("Cancelling drag");
        return false;
    }

    EDetailerPage.addActivateFunction(thisEdetailerPage.activate);

    thisEdetailerPage.init();
    
});

var thisEdetailerPage = (function() {
    /*
     * Private fields
     */
    var DEBUG_MODE = true && com.ward6.EDetailerPage.DEBUG_MODE, // The EDetailerPage's DEBUG_MODE property affects DEBUG_MODE globally.
        PAGE_ID = "mod2_2_interactions_1";
    /*
     * Fields
     */
    var thisEdetailerPageAPI = {},
        _controls = {
            daggables   : false,
            dropAreas   : false
        },
        _submitted = false;

    /**
     * Initialises our page to react to touch/mouse input and movement.
     */
    thisEdetailerPageAPI.init = function() {
        var EDetailerPage = com.ward6.EDetailerPage,
            DragDrop = com.ward6.DragDrop;

        // Set the page ID (otherwise the get/setPageData methods won't work.
        EDetailerPage.setPageId(PAGE_ID);

        _controls = {
            draggables      : $('.draggable'),
            dropAreas       : $('.dropArea'),
        };

        var pageData = EDetailerPage.getPageData();
        if (pageData === false
            || !pageData.hasOwnProperty('allCorrect')) {
            //_toggleCompleteButton(true);
        } else {
            //_toggleCompleteButton(false);
            //_toggleActivity(false);
            thisEdetailerPageAPI.completeActivity();
        }

        var dropAreas = DragDrop.getDropAreas();
        for (var dragId = _controls.draggables.length; dragId--;) {
            var thisDropArea = _controls.dropAreas.eq(dragId),
                dropAreaCoords = [thisDropArea.position().left,thisDropArea.position().top];

            _controls.draggables.eq(dragId).css(
                {
                    left: dropAreaCoords[0] + 'px',
                    top: dropAreaCoords[1] + 'px'
                }
            );
        }

    }

    /**
     * Activates or adjusts the page.
     */
    thisEdetailerPageAPI.activate = function() {
        var DragDrop = com.ward6.DragDrop;

        DragDrop.dockDraggables();
    }

    return thisEdetailerPageAPI;
})();