/**
 * Ambisome eDetailer Page Mod2_3_tissue_penetration_2
 *
 * Drives the second of three sections that interactively illustrate the effects
 * of AmBisome (diagrammatic purposes only - does not indicate actual clinical
 * effects).
 *
 *
 * GIAM6386 - Ward6
 * 
 * @authors Dan Pacey (amateurish, verbose code) | Kashi Samaraweera (foreword)
 * @copyright Ward6
 */

$(document).ready(function() {
	EDetailerPage.addActivateFunction(thisEdetailerPage.activate);
});

var thisEdetailerPage = (function() {
	var thisEdetailerPageAPI = {};
	
	thisEdetailerPageAPI.activate = function() {
			/*
		 
		 This might all need a fiddle to get the timing right on iPad.
		 Fingers crossed!
		 
		*/
		
		
		
		/*
		
			REMOVED - probably not really necessary!
		
		// let's draw some candida cultures
		var candidaElements = '0';
		
		drawCandida();
		
		function drawCandida() {
			if(candidaElements < 50) {
				var candidaRandom = Math.floor(Math.random() * (13 - 5 + 1)) + 5;
				var candidaDots = '<span style="width:' + candidaRandom + 'px; height:' + candidaRandom + 'px; border-radius:' + candidaRandom + 'px;">Candida</span>';		
				console.log(candidaRandom);
				$(candidaDots).appendTo(".candida");
				candidaElements++
				drawCandida();
			}
		};
		
		*/
		
		
		
		// fill up the drip bag
		
		$("#dripBag .bagFluid")
			.animate({
				easing: 'easeInOutQuad',
				top: '0px'
			}, 1500, function() {
				beginAnimation(); // start the animation proper
				}
			);
		
		
		// scroll the "teleprompter"
		// animations are explicitly set for granular control
		// so timings can be adjusted as required
		// waits to complete previous function before continuing
		
		
		$("#teleprompter div")
			.delay(300)
			.animate(
				{
					top: '0px'
				}, 800, function() {
					// animate the next one in.. (2)
					$(this)
						.delay(2000) // length of time to show PREVIOUS line of text
						.animate(
							{
								top: '-60px'
							}, 800, function() {
								// animate the next one in.. (3)
								$(this)
									.delay(2600)
									.animate(
										{
											top: '-120px'
										}, 800
									);
							}
						);
				}
			); 
		
		
		/*************** 
		
		
		Core animation	
		
		
		****************/
		
		
		// this function is called to begin the animation once the drip has filled.
		
		function beginAnimation() {
			console.log('start the main animation');
			
			
			// empty the drip bag. 
			//no other animations are tied to this so we can just adjust the timing for iPad
			
			$("#dripBag .bagFluid")
				.delay(2000)
				.animate(
					{
						easing: 'easeInOutQuad',
						top: '115px'
					}, 3000
				);
			
			
			// Show first Petri-dish	
			$("div#hidden1")
				.delay(4000)
				.animate(
					{
						opacity: '1'
					},1500
				);
			
			// Show second Petri-dish
			$("div#hidden2")
				.delay(4500)
				.animate(
					{
						opacity: '1'
					},1500
				);
				
				
			// turn lungs orange
			$("#figaro .figaroOrgans")
				.delay(2000)
				.animate(
					{
						easing: 'easeInOutQuad',
						opacity: '0'
					}, 3500
				);
				
			$("#figaro .orangeOrgans")
				.delay(2200)
				.animate(
					{
						easing: 'easeInOutQuad',
						opacity: '1'
					}, 3500, function() {
							flashPetriDishes();
						}
				);
				
		};
		
		function flashPetriDishes() {
			
			// flash petri-dish to orange, flash white(ish) then fade away.
			
			$(".petriDish div.orangeGlow")
				.delay(500)
				.animate(
					{
					easing: 'easeInBack',
					opacity: '0.9',
					backgroundColor: '#f3d9c0'
					}, 200, function() {
						$(this)
							.animate(
								{
								easing: 'easeOutQuad',
								opacity: '0',
								backgroundColor: '#d37216'
								},500, function() {
										animatePetriDishes();
									}
							);
					}
				);
		};
		
		function animatePetriDishes() {
			
			// animate the cultures in those suckers
			
			
			$(".petriDish div.aspergillus").delay(1000).addClass('reduced'); // we do this in CSS with css3 transforms.
			
			$(".petriDish div.candida .candida1")
				.delay(1000)
				.animate(
					{
						opacity: '0'
					}, 5000
				);
				
			$(".petriDish div.candida .candida2")
				.delay(1000)
				.animate(
					{
						opacity: '1'
					}, 5000, function() {
							// finally, enable the "Continue" button
							$(".progressBtn").removeClass("disabled");
						}
				);
			
		};
	}
	return thisEdetailerPageAPI;
})();