/**
 * Ambisome eDetailer Case Review 0-A
 *
 * GIAM5571 - Ward6
 * 
 * @author Kashi Samaraweera
 * @copyright Ward6
 * @since May 9th 2012
 */

$(document).ready(function() {
	if (typeof(com.ward6.EDetailerPage) === "undefined")
        return false;
    
    var EDetailerPage = com.ward6.EDetailerPage;
    EDetailerPage.setModuleXML('references.xml');
    thisEdetailerPage.init();
});

var thisEdetailerPage = {
    DEBUG_MODE      : true && com.ward6.EDetailerPage.DEBUG_MODE,
    PAGE_ID         : "mod1_case0_a",
    NEXT_PAGE_URL   : 'amb_mod1_case0_b.html',
    ANIMATION_DURATION    : 1000,
    ANIMATION_EASING      : "easeOutSine",
    controls        : {
        barGraphs   : false,
        barLabels   : false
    },
    tally           : [
        // DEFAULT DATA... overwritten if real data exists / saved if no real data exists.
        0,
        0,
        0,
        0
    ],
    tallyTotal      : 0,
    selectedIndex   : -1,
    currentGraphPercents    :   [0, 0, 0, 0],
    graphsActivated : [false, false, false, false],
    init            : function() {
        var EDetailerPage = com.ward6.EDetailerPage;

        EDetailerPage.setPageId(this.PAGE_ID);

        this.controls = {
            barGraphs       : $("div.graph100").find("div.bar"),
            barLabels       : $("div.graph100").find("div.percentValue").children('meter'),
            barLabelSelect  : $("div.graph100").find("div.percentValue").children('span'),
            submitBtn       : $("input[type='button']")
        };

        EDetailerPage.addActivateFunction(this.activate, { context : this });
        this.controls.barGraphs.css({width:'0px'});
        this.controls.barLabels.html('0%');

        this.controls.barLabelSelect.css({ opacity : 0 });
        this.controls.submitBtn.bind(EDetailerPage.UIEvents.CLICK, { thisEdetailerPage : this }, this.nextPage);        
    },
    activate        : function() {
        var EDetailerPage = com.ward6.EDetailerPage,
            storedTallyPage = EDetailerPage.getPageData(),
            storedSelectionPageId = this.latestSelectionPageData(["mod1_4_yes", "mod1_7_yes", "mod1_11_no", "mod1_12"]),
            newlastTallyTime = new Date().getTime(),
            lastTallyTime = 0;
        
        if (storedTallyPage) {
            if (typeof(storedTallyPage.lastTallyTime) !== "undefined")
                this.tally = storedTallyPage.tally;
                lastTallyTime = storedTallyPage.lastTallyTime;        
        }
        
        if (storedSelectionPageId) {
            var preExitPageData = EDetailerPage.getPageData(storedSelectionPageId);
            if (typeof(preExitPageData.lastSaveTime) !== "undefined"
                && preExitPageData.lastSaveTime > lastTallyTime) {

                switch(storedSelectionPageId) {
                    case "mod1_4_yes":
                        this.tally[0]++;
                        this.selectedIndex = 0;
                        break;
                    case "mod1_7_yes":
                        this.tally[1]++;
                        this.selectedIndex = 1;
                        break;
                    case "mod1_11_no":
                        this.tally[2]++;
                        this.selectedIndex = 2;
                        break;
                    case "mod1_12":
                        this.tally[3]++;
                        this.selectedIndex = 3;
                        break;
                }
            }

            EDetailerPage.setPageData(
                {
                    tally : this.tally,
                    lastTallyTime : newlastTallyTime
                },
                true
            );
        }

        this.updateGraphs(this.tally);
        return 
    },
    latestSelectionPageData  : function(selectionPageIds) {
        var EDetailerPage = com.ward6.EDetailerPage,
            selectedPage = false,
            latestSelectionTime = 0,
            pageCount = 0;
            
        if (typeof(selectionPageIds.length) !== "number")
            return false
        
        pageCount = selectionPageIds.length;
        while (pageCount--) {
            var thisPageId = selectionPageIds[pageCount],
                thisPage = EDetailerPage.getPageData(thisPageId);
            
            if (typeof(thisPage) === "object")
                if (typeof(thisPage.lastSaveTime) !== "undefined")
                    if (thisPage.lastSaveTime >= latestSelectionTime) {
                        selectedPage = thisPageId;
                        latestSelectionTime = thisPage.lastSaveTime;
                    }
        }
        
        return selectedPage;
    },
    updateGraphs    : function(tally) {
        var currentGraphPercents = [],
            finalGraphPercents = [],
            tallyTotal = 0;
        
        // Get the current values of the graph.
        this.controls.barLabels.each(
            function (index, barLabel) {
                currentGraphPercents.push(parseInt($(barLabel).text().replace(/[^0-9.]/g,'')));
            }
        );
        this.currentGraphPercents = currentGraphPercents;

        if (this.DEBUG_MODE) console.log("Measuring current bar amounts: ", currentGraphPercents);
        if (this.DEBUG_MODE) console.log("Tally: ", tally);
        
        for (graphValues in tally) {
            tallyTotal += parseInt(tally[graphValues]);
        }
        
        // Prevent divide by zero for an empty tally set.
        this.tallyTotal = (tallyTotal === 0)? 1 : tallyTotal;
        
        if (this.DEBUG_MODE) console.log("Total tally amount: " + tallyTotal);

        this.controls.barLabelSelect.stop(true, false).animate( { opacity : 0 }, this.ANIMATION_DURATION / 2);
        this.animateGraph(3);
    },
    animateGraph    : function(graphIndex) {
        var barGraph = this.controls.barGraphs.eq(graphIndex),
            barWrapper = barGraph.parents('div.barWrapper'),
            _this = this;
        if (this.DEBUG_MODE) console.log("current bar: ", barGraph);
        if (this.DEBUG_MODE) console.log("Animating graph: ", graphIndex);
        this.graphsActivated[graphIndex] = true;

        if (this.DEBUG_MODE) console.log("Selected: ", graphIndex === this.selectedIndex);

        if (graphIndex === this.selectedIndex) {
            barWrapper.addClass('userBar');
        } else {
            barWrapper.removeClass('userBar');
        }

        barGraph.stop(true, false).animate(
            { width   :   ((this.tally[graphIndex] / this.tallyTotal) * 500) + 'px' },
            {
                step        : function(stepValue, stepData) {
                    var stepPercent = stepData.pos,
                        barPercent = (_this.tally[graphIndex] / _this.tallyTotal) * 100,
                        initialPercentText = _this.currentGraphPercents[graphIndex],
                        percentText = Math.round(initialPercentText + (barPercent - initialPercentText) * stepPercent),
                        barLabel = _this.controls.barLabels.eq(graphIndex);

                    barLabel.html(percentText + '%');

                    if (stepPercent > .3
                        && typeof(_this.graphsActivated[graphIndex - 1]) !== "undefined"
                        && _this.graphsActivated[graphIndex - 1] === false)
                        _this.animateGraph(graphIndex - 1);
                },
                complete    : function() {
                    _this.graphsActivated[graphIndex] = false;
                    if (graphIndex === _this.graphsActivated.length - 1)
                        if (_this.selectedIndex >= 0)
                            _this.controls.barLabelSelect.eq(_this.selectedIndex).stop(true, false).animate(
                                { opacity : "1" },
                                { 
                                    duration : _this.ANIMATION_DURATION / 2,
                                    easing   : _this.ANIMATION_EASING
                                }
                            );
                },
                easing      : this.ANIMATION_EASING,
                duration    : this.ANIMATION_DURATION
            }
        );
    },
    nextPage        : function(nextBtnClick) {
        var _this = nextBtnClick.data.thisEdetailerPage,
            nextPageURL = _this.NEXT_PAGE_URL;

        if (com.ward6.EDetailerPage.isApp)
            nextPageURL = "app://page//" + nextPageURL;

        window.location = nextPageURL;
    }
};