/**
 * Ambisome eDetailer Page 2
 *
 * GIAM5571 - Ward6
 * 
 * @author Kashi Samaraweera
 * @copyright Ward6
 * @since May 7th 2012
 */

$(document).ready(function() {
	if (typeof(com.ward6.EDetailerPage) === "undefined")
        return false;
    
    var EDetailerPage = com.ward6.EDetailerPage;
    EDetailerPage.setModuleXML('references.xml');
    
    thisEdetailerPage.init();
});

window.onload = function() {
    thisEdetailerPage.init();
}

var thisEdetailerPage = {
    DEBUG_MODE      : false && com.ward6.EDetailerPage.DEBUG_MODE,
    PAGE_ID         : "mod1_2",
    O               : {
        x   : 0,
        y   : 0
    },
    alpha           : 0,
    beta            : 0,
    selections      : [
        -Math.PI/2,
        -Math.PI/4,
        0,
        Math.PI/4,
        Math.PI/2
    ],
    controls        : {
        knobArea        : false,
        knobFace        : false,
        knobList        : false
    },
    init            : function() {
        var EDetailerPage = com.ward6.EDetailerPage;
        
        EDetailerPage.setPageId(this.PAGE_ID);
        this.controls = {
            knobArea    : $("div#knob"),
            knobFace    : $("img#knobFace"),
            knobList    : $("ul#knobValues").children("li")
        };
        
        this.O.x = this.controls.knobFace.offset().left + (this.controls.knobFace.width() / 2);
        this.O.y = this.controls.knobFace.offset().top + (this.controls.knobFace.height() / 2);
        
        this.controls.knobFace.css( { 'WebkitTransform'    :   'rotate(' + 0 + 'rad)' } );
        
        this.controls.knobArea.bind(EDetailerPage.UIEvents.DOWN, { thisEdetailerPage : this}, this.registerInput);
        this.controls.knobArea.bind(EDetailerPage.UIEvents.UP, { thisEdetailerPage : this}, this.deregisterInput);
        
        com.ward6.EDetailerPage.setPageData(
            {
                selected : 2,
                lastSaveTime : new Date().getTime()
            }
        );
    },
    registerInput   : function(downEvent) {
        var _this = downEvent.data.thisEdetailerPage,
            cursorMoveEvent = ('ontouchmove' in document.documentElement)? 'touchmove' : 'mousemove',
            x = downEvent.pageX - _this.O.x,
            y = _this.O.y - downEvent.pageY,
            alpha = _this.getAngle(x, y);

        _this.alpha = alpha;

        if (_this.DEBUG_MODE) console.log("Mouse / finger down\nAttaching move event handler");
        if (_this.DEBUG_MODE) console.log("X: " + x + "\nY: " + y + "\nangle: " + alpha * (180/Math.PI) + " degrees");
        _this.controls.knobArea.bind(cursorMoveEvent, { thisEdetailerPage : _this}, _this.trackMovement);
    },
    trackMovement   : function(moveEvent) {
        var _this = moveEvent.data.thisEdetailerPage,
            x = moveEvent.pageX - _this.O.x,
            y = _this.O.y - moveEvent.pageY,
            beta = _this.beta,
            gamma = _this.getAngle(x, y),
            delta = beta + (gamma - _this.alpha);

        moveEvent.preventDefault();
        
        if ((0-Math.PI/2) <= delta && delta <= (Math.PI/2))
            _this.controls.knobFace.css( { 'WebkitTransform'    :   'rotate(' + delta + 'rad)' } );


        if (_this.DEBUG_MODE && false) console.log(
            "Touch              (alpha): " + (_this.alpha * (180/Math.PI)) + " deg"+
            "\nKnob              (beta): " + (beta * (180/Math.PI)) + " deg" +
            "\nMovement         (gamma): " + (gamma * (180/Math.PI)) + " deg" +
            "\nChange   (gamma - alpha): " + ((gamma - _this.alpha) * (180/Math.PI)) + " deg" +
            "\nTotal            (delta): " + (delta * (180/Math.PI)) + " deg"
        );
        
        _this.alpha = gamma;
        if ((0-Math.PI/2) <= delta && delta <= (Math.PI/2))
            _this.beta = delta;

    },
    deregisterInput : function(upEvent) {
        var _this = upEvent.data.thisEdetailerPage,
            cursorMoveEvent = ('ontouchmove' in document.documentElement)? 'touchmove' : 'mousemove',
            snapAngle = _this.snapAngle(_this.beta),
            listIndex = 0;

        
        if (_this.DEBUG_MODE) console.log("Mouse / finger up\ndetaching move event handler");
        if (_this.DEBUG_MODE) console.log("snapAngle: " + snapAngle);

        _this.controls.knobArea.unbind(cursorMoveEvent, _this.trackMovement);
        _this.controls.knobFace.css( { 'WebkitTransform'    :   'rotate(' + snapAngle + 'rad)' } );
        _this.lightUpSelected(snapAngle);

        _this.beta = snapAngle;
    },
    getAngle        : function(x, y) {
        var hyp = Math.sqrt(x*x + y*y),
            alpha = Math.asin(y/hyp);
        
        alpha = (Math.PI / 2) - alpha;
        if (x <= 0)
            alpha = -alpha;
            
        return alpha;
    },
    snapAngle      : function(inputAngle) {
        var snapAngle = (Math.PI / 4) * (inputAngle / (Math.PI/4)).toFixed(0);
        if (this.DEBUG_MODE) console.log(
            "inputAngle: " + (inputAngle * (180/Math.PI)) + "deg" +
            "\nsnapAngle: " + (snapAngle * (180/Math.PI)) + "deg"
        );
        return snapAngle;
    },
    lightUpSelected : function(inputAngleSnapped) {
        var selected = false;

        this.controls.knobList.removeClass('active');

        for (selectionIndex in this.selections)
            if (this.selections[selectionIndex] === inputAngleSnapped)
                selected = selectionIndex;

        if (selected === false)
            return false;

        this.controls.knobList.eq(selected).addClass('active');
        com.ward6.EDetailerPage.setPageData(
            {
                selected : selected,
                lastSaveTime : new Date().getTime()
            }
        );
    },
    nextPage                : function(nextBtnClick) {
        var _this = nextBtnClick.data.thisEdetailerPage,
            nextPageURL = _this.NEXT_PAGE_URL;

        if (com.ward6.EDetailerPage.isApp)
            nextPageURL = "app://page//" + nextPageURL;

        window.location = nextPageURL;
    }
};