/**
 * Ambisome eDetailer Page X
 *
 * GIAM5571 - Ward6
 * 
 * @author Rudi Vandisario
 * @copyright Ward6
 * @since May 7th 2012 [<-- The last time this file was edited.]
 */

$(document).ready(function() {
	if (typeof(com.ward6.EDetailerPage) === "undefined")
        return false;

    var EDetailerPage = com.ward6.EDetailerPage;
    EDetailerPage.setModuleXML('references.xml');

    thisEdetailerPage.init();
});

var thisEdetailerPage = {
    DEBUG_MODE      : false && com.ward6.EDetailerPage.DEBUG_MODE, // The EDetailerPage's DEBUG_MODE property affects DEBUG_MODE globally.
    PAGE_ID         : "mod1_xxx",
    NEXT_PAGE_URL   : "_2.html",
    ANIMATION_DURATION    : 1000,
    ANIMATION_EASING      : "easeOutSine",
    controls        : {
        controlName     : false, // Controls aren't initialised here.
        controlName2    : false, // Merely stated in a handy list.
        controlName3    : false, // Each of them should equal false.
        submitBtn       : false
    },
    init                    : function() {
        var EDetailerPage = com.ward6.EDetailerPage;
        
        // Set the page ID (otherwise the get/setPageData methods won't work.
        EDetailerPage.setPageId(this.PAGE_ID);

        this.controls = {
            controlName     : $('#control1'),  // Controls aren't initialised here.
            controlName2    : $('#control1'), // Merely stated in a handy list.
            controlName3    : $('#control1'), // Each of them should equal false.
            submitBtn       : $('a.submitBtn')
        };

        // Example method binding ("method" = function that belongs to an object, i.e. thisEdetailerPage).
        this.controls.controlName.bind(EDetailerPage.UIEvents.DOWN, { thisEdetailerPage : this}, this.controlMethod);

        // And of course, our submit button.
        this.controls.controlName.bind(EDetailerPage.UIEvents.CLICK, { thisEdetailerPage : this}, this.nextPage);
    },
    controlMethod           : function(clickEvent) {
        var _this = clickEvent.data.thisEdetailerPage;
        
        // Do something that the control will trigger.
        _this.controls.controlName2.css({ width : '50px'});
    },
    nextPage                : function(nextBtnClick) {
        var _this = nextBtnClick.data.thisEdetailerPage,
            nextPageURL = _this.NEXT_PAGE_URL;

        if (com.ward6.EDetailerPage.isApp)
            nextPageURL = "app://page//" + nextPageURL;

        window.location = nextPageURL;
    }
};