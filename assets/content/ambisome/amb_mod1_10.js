/**
 * Ambisome eDetailer Page 10
 *
 * GIAM5571 - Ward6
 * 
 * @author Kashi Samarweera
 * @copyright Ward6
 * @since May 7th 2012
 */

$(document).ready(function() {
	if (typeof(com.ward6.EDetailerPage) === "undefined")
        return false;

    var EDetailerPage = com.ward6.EDetailerPage;
    EDetailerPage.setModuleXML('references.xml');

    thisEdetailerPage.init();
});

var thisEdetailerPage = {
    DEBUG_MODE      : true && com.ward6.EDetailerPage.DEBUG_MODE, // The EDetailerPage's DEBUG_MODE property affects DEBUG_MODE globally.
    PAGE_ID         : "mod1_10",
    ANIMATION_DURATION    : 1000,
    ANIMATION_EASING      : "easeOutSine",
    pageToggle      : 1,
    controls        : {
            yesNoToggle     : false,
            submitBtn       : false
    },
    init                    : function() {
        var EDetailerPage = com.ward6.EDetailerPage;
        
        // Set the page ID (otherwise the get/setPageData methods won't work.
        EDetailerPage.setPageId(this.PAGE_ID);

        this.controls = {
            yesNoToggle     : $('span.switch'),
            submitBtn       : $('input[type="button"]')
        };

        this.controls.yesNoToggle.bind(EDetailerPage.UIEvents.CLICK, { thisEdetailerPage : this}, this.toggleYesNo);
        this.controls.submitBtn.bind(EDetailerPage.UIEvents.CLICK, { thisEdetailerPage : this }, this.nextPage);
        
        for (i = 2; i--;) this.controls.yesNoToggle.trigger(EDetailerPage.UIEvents.CLICK);
    },
    toggleYesNo     : function(yesNoToggle) {
        var EDetailerPage = com.ward6.EDetailerPage,
            _this = yesNoToggle.data.thisEdetailerPage,
            toggle = $(this),
            yesNo = (toggle.hasClass("noPosition"))? 0 : 1,
            saveTime = new Date().getTime();
        
        _this.pageToggle = yesNo;
        EDetailerPage.setPageData(
            {
                selection       : yesNo,
                lastSaveTime    : saveTime
            }
        );
        if (_this.DEBUG_MODE) console.log("Toggle: ", yesNo);
    },
    nextPage        : function(clickEvent) {
        var _this = clickEvent.data.thisEdetailerPage,
            isApp = com.ward6.EDetailerPage.isApp,
            nextPageURL = '';
        
        if (_this.pageToggle) // If the user's response is 'yes'
            nextPageURL = 'amb_mod1_11_yes.html';
        else
            nextPageURL = 'amb_mod1_11_no.html';

        if (isApp)
            nextPageURL = 'app://page//' + nextPageURL;
            
        window.location = nextPageURL;
    }
};