/*
 * Copyright (c) Ward6 November 2012.
 * Ward6 Drag Drop API for multi-touch drag-and-drop functionality in iOS.
 *
 * @author Kashi Samaraweera
 * @version 0.8
 * @since November 22, 2012
 */

var com = com || {};
com.ward6 = com.ward6 || {};

/**
 * Ward6 Drag and Drop Library
 *
 * This drag-drop library was built to provide drag-and-drop functionality to
 * Ward6 HTML productions. It is designed to minimise the inconsistencies of
 * iOS's conformity to the HTML5 spec. related to drag-and-drop functionality.
 *
 * This library is optimised for both performance and multi-touch however
 * HTML must be implemented in a particular way in order to work effectively.
 * In order to ensure the best possible performance, user input tracking is
 * done on a single HTML element called the dragContainer. Tracking methods
 * will subscribe to the dragContainer's input events and these movements will
 * be delegated to the draggable elements.
 *
 * As this is a multi-touch aware scenario, touches are indexed and coordinated
 * to the draggable objects based on the order in which they're initiated for
 * dragging.
 *
 * Requires the com.ward6.EDetailerPage library to provide environmental detail
 * such as input type, event binding and UI availability states.
 *
 * Requires jQuery. Also required is jQuery's window.$ overriding (although it
 * can easily be internalised within DragDrop if you're handy with JavaScript).
 *
 * @module DragDrop
 * @namespace com.ward6
 * @static
 * @class DragDrop
 */
com.ward6.DragDrop = (function(){

    /*
     * Constants
     */
    var DEBUG_MODE = true && com.ward6.EDetailerPage.DEBUG_MODE,
        DROP_STATES = {
            DROPPED     : 0,
            UNDROPPED   : 1
        };

    /*
     * Member variables
     */
    var DragDropAPI = { init : _init },
        _dragEnabled = false,
        _controls = {
            dragContainer   : false,
            draggables      : false,
            dropAreas       : false
        },
        globalOffset = [0, 0],
        _dragging = [],
        _dropAreas = [],
        _dropNotifySubscribers = [];

    /*
     * Public methods
     */

    /**
     * Sets the dragContainer object. This object will be used to trace all
     * mouse or touch movement. This is preferred to tracking each object
     * individually for performance reasons. The consequences of this are the
     * bounds in which the user's input is tracked.
     */
    DragDropAPI.setDragContainer = function(jqObject) {
        if (jqObject.length === 0)
            return (DEBUG_MODE === true)? console.warn("DragDrop: The drag" +
                " container supplied did not have .") : false;

        _controls.dragContainer = jqObject;
        _init();
    }

    /**
     * Sets the item(s) that can be dragged by the user's mouse or touch. This
     * method expects a jQuery object that contains one or more HTML elements.
     * If an object with zero elements is provided, then an error is issued in
     * debug mode.
     * @method setDraggables
     * @param {jqElement} jqObject   A jQuery-encapsulated object or group of
     *                              objects. These will be made draggable.
     */
    DragDropAPI.setDraggables = function(jqObject) {
        if (jqObject.length === 0)
            return (DEBUG_MODE === true)? console.warn("DragDrop: The" +
                " draggable object supplied had zero-length (i.e. no" +
                " elements).") : false;

        _controls.draggables = jqObject;
        _init();
    }

    /**
     * Sets the item(s) that should act as dropAreas.
     * @method setDropAreas
     * @param {jqElement} jqObject   The jQuery-encapsulated object or group of
     *                              objects that will act as drop areas for the
     *                              draggables.
     */
    DragDropAPI.setDropAreas = function(jqObject) {
        if (jqObject.length === 0)
            return (DEBUG_MODE === true)? console.warn("DragDrop:" +
                " The drop area object supplied had zero-length (i.e. no" +
                " elements).") : false;

        _controls.dropAreas = jqObject;
        _init();
    }

    /**
     * Returns the current dropArea object that contains information about
     * which draggable is docked to which dropArea.
     * @return {jqElement[]}
     */
    DragDropAPI.getDropAreas = function() {
        return _dropAreas;
    }

    /**
     * Enables or disables drag-and-drop behaviour. When used without any
     * parameters then the status of the functionality is returned.
     * @param {Boolean} enabled (Optional) Enables or disables the drag and
     *                          drop functionality. If no parameter is supplied
     *                          then there is no change effected.
     * @return {Boolean}    The current state of the
     */
    DragDropAPI.toggleActivity = function(enabled) {
        var UIEvents = com.ward6.EDetailerPage.UIEvents;

        if (typeof(enabled) === "boolean") {
            if (enabled) {
                _controls.draggables.bind(UIEvents.DOWN, _addToDragging);
                _controls.dragContainer.bind(UIEvents.MOVE, _moveDragging);
                _controls.draggables.bind(UIEvents.UP, _removeFromDragging);
            } else {
                _controls.draggables.unbind(UIEvents.DOWN, _addToDragging);
                _controls.dragContainer.unbind(UIEvents.MOVE, _moveDragging);
                _controls.draggables.unbind(UIEvents.UP, _removeFromDragging);
            }
            _dragEnabled = enabled;
        }
        return _dragEnabled;
    }

    /**
     * Drop events can notify external methods by subscribing using the
     * addDropSubcriber method. Drop events are supplied a single argument with
     * the following properties:
     *
     * {
     *  dropState:  {Number}    An enumerated integer representing the nature
     *                          of the drop event. (See DROP_STATES constant
     *                          declared near the top of this object).
     *  dropArea:   {jqElement} A jQuery-encapsulated object that references
     *                          the dropArea that has been effected.
     *  draggable:  {jqElement} (optional) a jQuery-encapsulated object that
     *                          references the draggable object that has been
     *                          newly placed on the dropArea. This will only be
     *                          a non-false value if the dropState is DROPPED.
     *  docked:     {jqElement} (optional) a jQuery-encapsulated object that
     *                          references the draggable currently occupying
     *                          the dropArea. This will only be a non-false
     *                          value if there is a draggable docked onto the
     *                          dropArea and the dropState is DROPPED.
     * }
     * @param {function} functionToNotify   The external function that will be
     *                                      notified of a drop/undrop event.
     */
    DragDropAPI.addDropSubscriber = function(functionToNotify) {
        _dropNotifySubscribers.push(functionToNotify);
    }

    /**
     * Assesses the positions of each draggable item. If a draggable is
     * situated over a dropArea then it is docked onto the dropArea which it's
     * placed over.
     */
    DragDropAPI.dockDraggables = function() {
        var dragCounter = 0,
            draggables = _controls.draggables,
            dropAreas = _controls.dropAreas,
            totalDraggables = draggables.length,
            totalDropAreas = dropAreas.length;

        while (dragCounter < totalDraggables) {
            var draggable = draggables.eq(dragCounter),
                draggablePosition = draggable.position(),
                draggablePos = [draggablePosition.left, draggablePosition.top],
                dropAreaCounter = 0,
                activeDropArea = false;

            activeDropArea = _draggableOverDropArea(draggable);
            if (activeDropArea !== false)
                _dropAreas[activeDropArea] = draggable;

            dragCounter++;
        }
    }

    /**
     * Animates the object to a set of coordinates specified by xyArray. If the
     * object is not currently being manipulated by the user then it is sent to
     * xyArray coordinates defined.
     * @method sendElementToCoordinates
     * @param {jqElement} jqObject   the object to move.
     * @param {Number[]} xyArray    The coordinates to move the item to.
     */
    DragDropAPI.sendElementToCoordinates = function(jqObject, xyArray) {
        var element = (typeof(jqObject.get) !== "undefined")? jqObject.get(0) : false,
            elementPosition,
            dX,
            dY,
            distance;

        if (typeof(jqObject.animate) === "undefined")
            return (DEBUG_MODE)? console.warn("com.ward6.DragDrop: " +
                "sendElementToCoordinates argument jqObject was not a jQuery " +
                "object.") : false;

        elementPosition = [jqObject.offset().left, jqObject.offset().top];
        dX = elementPosition[0] - (xyArray[0] + globalOffset[0]);
        dY = elementPosition[1] - (xyArray[1] + globalOffset[1]);

        // Please note that this assumes pixels are square (which they are
        // typically not). Incidently, (and on a completely unrelated note), why
        // do mathematicians make poor bakers?
        // ...
        // Because they think PI r ^2
        // Ha!
        distance = Math.sqrt(Math.pow(dX, 2) + Math.pow(dY, 2));

        for (var draggableIndex = _dragging.length; draggableIndex > 0;
             draggableIndex--)
            if (_dragging[draggableIndex] === element)
                return true;

        jqObject.animate(
            {
                left: xyArray[0],
                top: xyArray[1]
            },
            {
                duration: Math.round(distance * 1.5),
                specialEasing: {top: 'easeOutSine', left: 'easeInOutSine'}
            }
        );
    }

    /*
     * Private methods
     */

    /**
     * Initialises the drag-and-drop functionality so that objects within the
     * HTML page can be manipulated with mouse or touch input.
     * @method _init
     * @private
     */
    function _init() {
        if (_controls.dragContainer === false
            || _controls.dropAreas === false
            || _controls.draggables === false)
            return (DEBUG_MODE)? console.log("Cannot initialise while vital" +
                " controls are missing.") : false;

        var EDetailerPage = com.ward6.EDetailerPage,
            containerPosition = _controls.dragContainer.offset();

        globalOffset = [containerPosition.left, containerPosition.top];

        for (var i = 0; i < _controls.draggables.length; i++)
            _anchorDraggable(_controls.draggables.eq(i),
                _controls.draggables.eq(i), true);

    }

    /**
     * Anchors a draggable item to an element. Doing so will save the draggable
     * item's anchor point to the element supplied.
     * @method _anchorDraggable
     * @private
     * @param {jqElement}    draggable   The draggable object wrapped by jQuery.
     * @param {jqElement}    anchor The object to which the draggable object
     *                              will snap.
     * @param {Boolean} homeLocation    If true, then this anchor will be used
                                        as the last-resort "home" location.
     */
    function _anchorDraggable(draggable, anchor, homeLocation) {
        var anchorPosition = [anchor.position().left,
                anchor.position().top],
            homeLocation = (typeof(homeLocation) === "undefined")? false
                : (homeLocation === true)? true : false;

        if (homeLocation)
            draggable.data("anchor_home", anchorPosition);

        draggable.data("anchor", anchorPosition);
    }

    /**
     * Adds an element to the list of elements being dragged. When an element
     * is added to the queue any movement of the cursor will dictate the
     * position of the element.
     * @method _addToDragging
     * @private
     * @param {Object} jqEvent  The jQuery event that triggers an attachment.
     *                          This will contain the element being dragged in
     *                          the target attribute.
     */
    function _addToDragging(jqEvent) {
        var jqElement = $(this),
            element = jqElement.get(0),
            objectPos = jqElement.position(),
            draggingCounter = _dragging.length,
            pageTouchPos = [jqEvent.pageX
                || jqEvent.originalEvent.touches[draggingCounter].pageX,
                jqEvent.pageY
                || jqEvent.originalEvent.touches[draggingCounter].pageY],
            objectTouchPos = [
                pageTouchPos[0] - objectPos.left - globalOffset[0],
                pageTouchPos[1] - objectPos.top - globalOffset[1]
            ],
            dropAreaIndex = _draggableOverDropArea(jqElement);

        jqEvent.preventDefault();
        jqEvent.stopPropagation();

        jqElement.removeClass("dragging");

        jqElement.stop(true, false);
        jqElement.data("touchPos" , objectTouchPos);
        jqElement.css(
            {
                left: objectPos[0] - globalOffset[0] + 'px',
                top:  objectPos[1] - globalOffset[1] + 'px',
                zIndex: '9999999'
            }
        );

        _dragging[draggingCounter] = element;

        jqElement.bind(com.ward6.EDetailerPage.UIEvents.UP, _removeFromDragging);

        if (dragIndex !== false) {
            _notifyDropArea(_controls.dropAreas[dragIndex], jqElement, DROP_STATES.UNDROPPED);
        }

        return false;
    }

    /**
     * Tracks the user's movements whilst interacting with the screen. Objects
     * contained in the _dragging array will be moved with each sucessful call
     * of the _moveDragging method.
     * @param {Object} jqEvent  The jQuery event that triggers upon moving the
     *                          cursor (touch-dragging or mouse-moving).
     */
    function _moveDragging(jqEvent) {
        var dragging = _dragging,
            draggingCounter = dragging.length;

        jqEvent.preventDefault();
        jqEvent.stopPropagation();

        _controls.dropAreas.removeClass("draggableOverDroppable");
        while (draggingCounter--) {
            if (typeof(dragging[draggingCounter]) === "undefined")
                continue;

            var jqElement = $(dragging[draggingCounter]),
                element = jqElement.get(draggingCounter),
                pageTouchPos = [jqEvent.pageX || jqEvent.originalEvent.touches[draggingCounter].pageX, jqEvent.pageY || jqEvent.originalEvent.touches[draggingCounter].pageY],
                objectPos = jqElement.data("touchPos"),
                TEMP_elementText = jqElement.text();

            _toggleActiveDropAreas(true);
            jqElement.css(
                {
                    left: pageTouchPos[0] - objectPos[0] - globalOffset[0] + 'px',
                    top:  pageTouchPos[1] - objectPos[1] - globalOffset[1] + 'px'
                }
            );
            _draggableOverDropArea(jqElement);
        }

    }

    /**
     * Toggles the state of the drop areas between their default (dormant)
     * state and their active (attention-grabbing) state. Used when the user is
     * actively dragging a draggable item.
     * @param   {Boolean}   dropAreaState   The toggle which will determine if
     *                                      the dropAreas should be active
     *                                      (true) or inactive (false).
     */
    function _toggleActiveDropAreas(dropAreaState) {
        if (dropAreaState === true) {
            _controls.dropAreas.addClass('indicateDroppable');
        } else {
            _controls.dropAreas.removeClass('indicateDroppable');
            _controls.dropAreas.removeClass("draggableOverDroppable");
        }
    }

    /**
     * Removes an object from the _dragging array so that it will stop being
     * moved when the user moves her cursor on the screen.
     * @method _removeFromDragging
     * @private
     * @param {Object} jqEvent  The jQuery event triggered when a user stops
     *                          dragging an object.
     */
    function _removeFromDragging(jqEvent) {
        var jqElement = $(jqEvent.target),
            element = jqElement.get(0),
            dragging = _dragging,
            draggingCounter = dragging.length;

        jqElement.removeClass("dragging");

        jqElement.css(
            {
                zIndex: "6000"
            }
        );
        while (draggingCounter--) {
            if (dragging[draggingCounter] === element) {
                var dropAreaIndex = _draggableOverDropArea(jqElement),
                    anchorPos = [jqElement.data("anchor_home")[0], jqElement.data("anchor_home")[1]];

                if (dragIndex !== false) {
                    _anchorDraggable(jqElement, _controls.dropAreas.eq(dragIndex));
                    anchorPos = [jqElement.data("anchor")[0], jqElement.data("anchor")[1]];
                }

                jqElement.removeClass('dragging');
                dragging.splice(draggingCounter, 1);
                DragDropAPI.sendElementToCoordinates(jqElement, anchorPos);

                _notifyDropArea(_controls.dropAreas[dragIndex], jqElement, DROP_STATES.DROPPED);
            }
        }

        if (dragging.length === 0)
            _toggleActiveDropAreas(false);

        return false;
    }

    /**
     * Called when the dropArea has had its status changed. This method will
     * notify subscribed (including external) subscribers of the drop event.
     * @method _notifyDropArea
     * @private
     * @param {jqElement} dropArea   The jQuery object encapsulating the dropArea
     *                              that is being effected.
     * @param {jqElement} dropped    The item being dropped (or undropped into
     *                              the dropArea).
     * @param {Number}  dropState   The drop state, which defines the state such
     *                              as dropped or undropped. This is enumerated
     *                              in the DROP_STATES constant.
     */
    function _notifyDropArea(dropArea, dropped, dropState) {
        var DragDrop = com.ward6.DragDrop,
            drop = (dropState === DROP_STATES.DROPPED)? true : false,
            dropAreas = _dropAreas,
            dropAreaIndex = _controls.dropAreas.index(dropArea),
            dropAreaOldDraggble = (typeof(dropAreas[dragIndex]) === "undefined")? false : dropAreas[dragIndex],
            previousDropAreaIndex = false,
            previousDropArea = false;

        if (dropState == DROP_STATES.DROPPED) {
            // Get the dropArea from which our draggable was lifted.
            for (var dIndex = 0; dIndex < dropAreas.length; dIndex++) {
                if (dropAreas[dIndex].get(0) === dropped.get(0)) {
                    previousDropAreaIndex = dIndex;
                    previousDropArea = _controls.dropAreas.eq(dIndex);
                }
            }

            if (dropAreaOldDraggble !== false
                && previousDropArea !== false) {
                var prevDropAreaCoords = [previousDropArea.position().left, previousDropArea.position().top];
                _anchorDraggable(dropAreaOldDraggble, previousDropArea, false);
                DragDropAPI.sendElementToCoordinates(dropAreaOldDraggble, prevDropAreaCoords);
                dropAreas[previousDropAreaIndex] = dropAreaOldDraggble;
            }

            dropAreas[dragIndex] = dropped;
        }
    }

    /**
     * Detects whether the draggable item is being dropped onto a dropArea
     * target. Once the assessment is made the draggable item being dropped will
     * either snap to the correct coordinates of the dropArea or return to the
     * the original location.
     *
     * As dropAreas can be in close proximity to each other, we need a method
     * to assess which dropArea the user is intent on selecting. We can do this
     * qualitatively (which requires a fair bit of engineering) or we can do it
     * quantitatively, as we're doing here.
     *
     * For each dropArea that the draggable item is hovering over we figure out
     * the area (in pixels) that the draggable item and dropArea share. The
     * highest of all the dropAreas is taken to be the intended dropArea.
     *
     *  ____________
     * |            |           Please excuse the poor ASCII art. The area
     * |   A  ______|______     that is shaded represents the conjunction
     * |    |///C\\\|      |    between the dropArea (rectangle B) and the
     *  ------------| B    |    draggable (rectangle A) object. The larger this
     *      |              |    area (rectangle C), the higher priority this
     *       --------------     dropArea is given for selection.
     *
     * @method
     * @private
     * @param   {jqElement}  draggable  The jQuery object being dragged.
     * @return  {Number}    The index of the dropArea over which the draggable
     *                      is afloat.
     */
    function _draggableOverDropArea(draggable) {
        var dropAreas = _controls.dropAreas,
            dropAreasArray = [],
            draggablePosition = [draggable.offset().left, draggable.offset().top],
            draggableSize = [draggable.width(), draggable.height()],
            dropAreaIndex = false,
            conjunctionArea = false;

        dropAreas.each(
            function(index, dropArea) {
                var dropAreaElement = $(dropArea),
                    dropAreaPosition = [dropAreaElement.offset().left, dropAreaElement.offset().top],
                    dropAreaSize = [dropAreaElement.width(), dropAreaElement.height()],
                    /** The area of the dropArea with +/- the width and height. **/
                    dropArea = [
                        [dropAreaPosition[0] - dropAreaSize[0], dropAreaPosition[0] + dropAreaSize[0]],
                        [dropAreaPosition[1] - dropAreaSize[1], dropAreaPosition[1] + dropAreaSize[1]]
                    ],
                    coverageDelta = 0,
                    conjunctionWidth = 0,
                    conjunctionHeight = 0,
                    thisConjunctionArea = 0;

                if (draggablePosition[0] > dropArea[0][0]
                    && draggablePosition[0] < dropArea[0][1]
                    && draggablePosition[1] > dropArea[1][0]
                    && draggablePosition[1] < dropArea[1][1]) {

                    if (draggablePosition[0] > dropAreaPosition[0])
                        conjunctionWidth = (dropAreaPosition[0] + dropAreaSize[0]) - draggablePosition[0];
                    else
                        conjunctionWidth = (draggablePosition[0] + draggableSize[0]) - dropAreaPosition[0];

                    if (draggablePosition[1] > dropAreaPosition[1])
                        conjunctionHeight = (dropAreaPosition[1] + dropAreaSize[1]) - draggablePosition[1];
                    else
                        conjunctionHeight = (draggablePosition[1] + draggableSize[1]) - dropAreaPosition[1];

                    thisConjunctionArea = conjunctionWidth * conjunctionHeight;

                    if (conjunctionArea === false
                        || thisConjunctionArea > conjunctionArea) {
                        conjunctionArea = thisConjunctionArea;
                        dragIndex = index;
                    }
                }
            }
        );

        if (dragIndex !== false) {
            _controls.dropAreas.eq(dragIndex).addClass("draggableOverDroppable");
        }

        return dragIndex;
    }

    /**
     * Takes a draggable or dropArea jQuery object and examines it for evidence
     * of a draggable identifier attributed to its class. This is identified by
     * "dragN", where N is any integer number > 0.
     * @param {jqElement} dragOrDrop The object to examine. This will be
     *                               retrieved by a jQuery.eq(0) statement to
     *                               observe a single HTML element.
     * @throws Error    Throws an error if no draggable or dropArea reference is
     *                  not found.
     */
    function _getDragDropClassId(dragOrDrop) {
        var classesString = dragOrDrop.eq(0).attr('class'),
            classes = classesString.split(' '),
            classCounter = classes.length,
            draggableTest = new RegExp(/^drag[0-9]{1,3}$/i),
            dropAreaTest = new RegExp(/^drop[0-9]{1,3}$/i),
            dragOrDropId = -1;

        while (classCounter--) {
            var thisClass = classes[classCounter];

            if (draggableTest.test(thisClass)
                || dropAreaTest.test(thisClass))
                dragOrDropId = thisClass.replace(/[a-z\s]+/ig, "");
        }

        dragOrDropId = parseInt(dragOrDropId);
        if (!isNaN(dragOrDropId)
            && dragOrDropId !== -1)
            return dragOrDropId;

        throw new Error("Class ID not found.");
    }


    return DragDropAPI;

} ());