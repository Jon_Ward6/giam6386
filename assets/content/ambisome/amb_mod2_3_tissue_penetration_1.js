/**
 * Ambisome eDetailer Page Mod2_3_tissue_penetration_1
 *
 * Drives the first of three sections that interactively illustrate the effects
 * of AmBisome (diagrammatic purposes only - does not indicate actual clinical
 * effects).
 *
 * This section features an automatic animation of a drip bag being filled and
 * then emptied. Text changes as we reach different water-marks in the drip bag
 * which will be a user initiated step for each animation.
 *
 * GIAM6386 - Ward6
 * 
 * @authors Dan Pacey (amateurish, verbose code) | Kashi Samaraweera (foreword)
 * @copyright Ward6
 */

$(document).ready(function() {
	EDetailerPage.addActivateFunction(thisEdetailerPage.activate);
});

var thisEdetailerPage = (function() {
	var thisEdetailerPageAPI = {};
	
	thisEdetailerPageAPI.activate = function() {
		/*
	 
	 This might all need a fiddle to get the timing right on iPad.
	 Fingers crossed!
	 
	*/
	
	
	// fill up the drip bag
	
	$("#dripBag .bagFluid")
		.animate({
			easing: 'easeInOutQuad',
			top: '0px'
		}, 7800, function() {
			beginAnimation(); // start the animation proper
			}
		);
	
	
	// scroll the "teleprompter"
	// animations are explicitly set for granular control
	// so timings can be adjusted as required
	// waits to complete previous function before continuing
	
	
	$("#teleprompter div")
		.delay(300)
		.animate(
			{
				top: '0px'
			}, 800, function() {
				// animate the next one in.. (2)
				$(this)
					.delay(2200) // length of time to show PREVIOUS line of text
					.animate(
						{
							top: '-60px'
						}, 800, function() {
							// animate the next one in.. (3)
							$(this)
								.delay(3600)
								.animate(
									{
										top: '-120px'
									}, 800, function() {
										// animate the next one in.. (4)
										$(this)
											.delay(8000)
											.animate(
												{
													top: '-180px'
												}, 800, function() {
													// animate the next one in.. (5)
													$(this)
														.delay(5000)
														.animate(
															{
																top: '-240px'
															}, 800, function() {
																// animate the next one in.. (6)
																$(this)
																	.delay(5000)
																	.animate(
																		{
																			top: '-300px'
																		}, 800
																	);
															}
														);
												}
											);
									}
								);
						}
					);
			}
		); 
		
		//phew
		
		
	// fade in labels
	
	$("#figaro .figaroLabels").delay(300).fadeIn(3000);
	
	
	
	/*************** 
	
	
	Core animation	
	
	
	****************/
	
	
	// this function is called to begin the animation once the drip has filled.
	
	function beginAnimation() {
		console.log('start the main animation');
		
		
		// empty the drip bag. 
		//no other animations are tied to this so we can just adjust the timing for iPad
		
		$("#dripBag .bagFluid")
			.delay(3000)
			.animate({
				easing: 'easeInOutQuad',
				top: '275px'
			}, 25000
			);
		
		
		//
		// Graphs
		//
		
		
		// First graph. Fade in the axis first	
		$("div#hidden1")
			.delay(0)
			.animate(
				{
					opacity: '1'
				},1500, function(){
					// draw in the dots
					$("#penGraph1 .penDots")
						.delay(5000)
						.animate(
							{
								width: '100%'
							}, 10000
						);
					// draw in the line
					$("#penGraph1 .penLines")
						.delay(17000)
						.animate(
							{
								width: '100%'
							}, 10000
						);
						
				}
			);
		
		// Second graph. Fade in the axis first
		$("div#hidden2")
			.delay(500)
			.animate(
				{
					opacity: '1'
				},1500, function(){
					// draw in the dots
					$("#penGraph2 .penDots")
						.delay(5000)
						.animate(
							{
								width: '100%'
							}, 10000
						);
					// draw in the line
					$("#penGraph2 .penLines")
						.delay(17000)
						.animate(
							{
								width: '100%'
							}, 10000
						);
						
				}
			);
		
		
		//
		// Veins and Organs
		//
		// here we animate the veins from 'white' to orange
		// different .png images are used for each
		//
		
		
		// fading out of white veins and white organs
		$("#figaro .figaroVeins")
			.delay(3000)
			.animate(
				{
					opacity: '0'
				}, 15000, function() {
					// then fade the white veins back in simultaneously with orange fading out
					$(this)
						.animate(
							{
								opacity: '1'
							}, 10000
						);
					// whilst fading out the white organs
					$("#figaro .figaroOrgans")
						.animate(
							{
							opacity: '0'
							}, 10000
						);
					}
			);
			
		// fading in of orange veins
		$("#figaro .orangeVeins")
			.delay(3000)
			.animate(
				{
					opacity: '1'
				}, 15000, function() {
					// then fade them out as the white veins fade back in
					$(this)
						.animate(
							{
								opacity: '0'
							}, 10000
						);
					// and concurrently fade in the orange organs
					$("#figaro .orangeOrgans")
						.animate(
							{
							opacity: '1'
							}, 10000, function() {
								// finally, enable the "Progress" button
								$(".progressBtn").removeClass("disabled");
							}
						);
					}
			);		
		
		
		};
	}
	return thisEdetailerPageAPI;
})();