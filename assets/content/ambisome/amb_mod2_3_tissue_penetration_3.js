/**
 * Ambisome eDetailer Page Mod2_3_tissue_penetration_3
 *
 * Drives the second of three sections that interactively illustrate the effects
 * of AmBisome (diagrammatic purposes only - does not indicate actual clinical
 * effects).
 *
 *
 * GIAM6386 - Ward6
 * 
 * @authors Dan Pacey (amateurish, verbose code) | Kashi Samaraweera (foreword)
 * @copyright Ward6
 */

$(document).ready(function() {
	EDetailerPage.addActivateFunction(thisEdetailerPage.activate);
});

var thisEdetailerPage = (function() {
	var thisEdetailerPageAPI = {};
	
	thisEdetailerPageAPI.activate = function() {	
		// fill up the drip bag
		
		$("#dripBag .bagFluid")
			.animate({
				easing: 'easeInOutQuad',
				top: '0px'
			}, 1500, function() {
				beginSlider(); // start the animation proper
				}
			);
		
		
		// scroll the "teleprompter" in
		$("#teleprompter div")
			.delay(300)
			.animate(
				{
					top: '0px'
				}, 800
			);
			
		
		
		// fade in labels
		
		$("#figaro .figaroLabels").delay(300).fadeIn(3000);
		
		
		
		// scale dots
		
		$(".labelDot").delay(500).transition({ scale: 0.364 }, 1000);
		
		
		/*************** 
		
		
		Core animation	
		
		
		****************/
		
		
		// this function is called to enable the dragger/slider.
		
		function beginSlider() {
			console.log('enable slider');
			
			$( ".adjustHandle" )
				.draggable(
					{ 
						axis: "y", 
						scroll: false, 
						containment: [0,206,1024,480],
						drag: function()
							{
								var position = $(this).position();
								var yPos = position.top;
								console.log(yPos);
								$("#dripBag .bagFluid").css('top', yPos - 91 + 'px');
								$(".labelDot").transition({ scale: yPos / 250 }, 0);
								
								if(yPos < 149) {
								
									// show message 1
									console.log('message 1');
									$("#teleprompter div")
										.animate(
											{
												top: '0px'
											}, 0
										);
								} else if(yPos > 150 && yPos < 250) {
								
									// show message 2
									console.log('message 2');
									$("#teleprompter div")
										.animate(
											{
												top: '-60px'
											}, 0
										);
								} else if(yPos > 250) {
								
									// show message 3
									console.log('message 3');
									$("#teleprompter div")
										.animate(
											{
												top: '-120px'
											}, 0
										);
								}
							}
						
					}
				);
			
			
		};
	}
	return thisEdetailerPageAPI;
})();