/**
 * Ambisome eDetailer Page Mod2_1_Targeted_2
 *
 * Retrieves the data stored in the previous page and visualises it in the form
 * of a pie chart.
 *
 * GIAM6386 - Ward6
 * 
 * @author Kashi Samaraweera
 * @copyright Ward6
 */

$(document).ready(function() {
	if (typeof(com.ward6.EDetailerPage) === "undefined")
        return false;

    var EDetailerPage = com.ward6.EDetailerPage;
    EDetailerPage.setModuleXML('references.xml');

    thisEdetailerPage.init();
    com.ward6.EDetailerPage.addActivateFunction(thisEdetailerPage.activate, {context : thisEdetailerPage});
    
});

var thisEdetailerPage = (function() {
    /*
     * Private fields
     */
    var DEBUG_MODE = true && com.ward6.EDetailerPage.DEBUG_MODE, // The EDetailerPage's DEBUG_MODE property affects DEBUG_MODE globally.
        PAGE_ID = "mod2_1_targeted_2",
        SUBMISSION_PAGE = "mod2_1_targeted_1";
        
    /*
     * Fields
     */
    var thisEdetailerPageAPI = {};
        _controls = {
            pieArea : false
        },
        tally = [0,0],
        pie = false;

    /**
     * Initialises our page
     */
    thisEdetailerPageAPI.init = function() {
        var EDetailerPage = com.ward6.EDetailerPage;
        
        // Set the page ID (otherwise the get/setPageData methods won't work.
        EDetailerPage.setPageId(PAGE_ID);
        EDetailerPage.overrideActivateOnlyOnce(true);

        _controls = {
            pieArea         : $('#pieArea'),
            percentText     : $('#percentText')
        };
    }
    
    thisEdetailerPageAPI.activate = function() {
        var storedSelectionPage = EDetailerPage.getPageData(SUBMISSION_PAGE),
            storedTallyPage = EDetailerPage.getPageData(),
            newlastTallyTime = new Date().getTime();

        /*
         * Copied from amb_mod_1_6.js and modified to suit this context.
         */
        if (DEBUG_MODE) console.log("storedTally: ", storedTallyPage, "\nstoredSelectionPage: ", storedSelectionPage);
        if (storedTallyPage)
            tally = storedTallyPage.tally;
        
        if (storedSelectionPage) {
            selectedIndex = (storedSelectionPage.allCorrect)? 1 : 0;

            // Add to the tally IFF the lastSaveTime > lastTallyTime
            if (typeof(storedTallyPage.lastTallyTime) === "undefined"
                && typeof(storedSelectionPage.lastSaveTime) !== "undefined") {
                // First time this page is loaded before localStorage is cleared.
                if (DEBUG_MODE) console.log("First time");
                tally[selectedIndex] = tally[selectedIndex] + 1;

            } else if ((typeof(storedSelectionPage.lastSaveTime) !== "undefined"
                && typeof(storedTallyPage.lastTallyTime) !== "undefined")
                && storedSelectionPage.lastSaveTime > storedTallyPage.lastTallyTime) {
                if (DEBUG_MODE) console.log("Adding to tally");
                // Typical run - new estimate submitted from previous page.
                if (DEBUG_MODE) console.log("Updating tally with new submission");
                    tally[selectedIndex] = tally[selectedIndex] + 1;
                    
            } else if ((typeof(storedSelectionPage.lastSaveTime) !== "undefined"
                && typeof(storedTallyPage.lastTallyTime) !== "undefined")
                && storedSelectionPage.lastSaveTime <= storedTallyPage.lastTallyTime) {
                // This page has been loaded again after a typical run.
                if (DEBUG_MODE) console.log("Repeat tally");
                newlastTallyTime = storedTallyPage.lastTallyTime;
            }
            
            EDetailerPage.setPageData(
                {
                    tally : tally,
                    lastTallyTime : newlastTallyTime
                },
                true
            );
        }

        pie = _bakePie();
        thisEdetailerPageAPI.pie = pie;
    }

    /**
     * Creates the pie chart if it is not already present. If it has already
     * been created then it will be replaced with an updated version.
     **/
    function _bakePie() {
        var correct = tally[1],
            incorrect = tally[0],
            total = correct + incorrect,
            percentCorrect = correct/total,
            currentPercentText =   _controls.percentText.text(),
            currentPercentNumber = parseFloat(currentPercentText),
            currentPercent = (isNaN(currentPercentNumber) || currentPercentNumber === 0)? 0 : currentPercentNumber;

        _controls.percentText.stop(true, false).animate(
            {
                opacity : 1 // Fake transformation.
            },
            {
                step: function(stepPos, data) {
                    var percentComplete = data.pos,
                        thisStep = 0;

                    if (percentComplete > 0)
                        thisStep = (percentCorrect*percentComplete)*100;

                    _controls.percentText.text(thisStep.toFixed(1) + '%');
                },
                duration: 1000
            }
        );
                
        return new Highcharts.Chart({
            chart: {
                renderTo: _controls.pieArea[0],
                backgroundColor: 'rgba(255,255,255,0)',
                plotBorderWidth: null,
                plotShadow: false,
                margin: [0, 0, 0, 0]
            },
            title: {
                text: null
            },
            colors: [
                "rgb(195, 0, 75)",
                "rgba(104, 115, 124, 0.2)"
            ],
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.percentage}%</b>',
                percentageDecimals: 1
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>: ' + this.percentage + ' %</b>';
                        }
                    },
                    borderWidth: 0
                }
            },
            series: [{
                type: 'pie',
                name: 'Correct on the first attempt',
                data: [
                    ["Correct", tally[1]],
                    ["Incorrect", tally[0]]
                ]
            }]
        });
    }
    
    thisEdetailerPageAPI.nextPage = function(nextBtnClick) {
        var _this = nextBtnClick.data.thisEdetailerPage,
            nextPageURL = _this.NEXT_PAGE_URL;

        if (com.ward6.EDetailerPage.isApp)
            nextPageURL = "app://page//" + nextPageURL;

        window.location = nextPageURL;
    }
    
    return thisEdetailerPageAPI;
})();