/**
 * Ambisome eDetailer Page Mod2_3_tissue_penetration_0
 *
 * This page allows users to rank items in an arbitrary order and compare their
 * selection to those of previous users.
 *
 * Requires the com.ward6.DragDrop library.
 *
 * GIAM6386 - Ward6
 * 
 * @author Kashi Samaraweera
 * @copyright Ward6
 */

$(document).ready(function() {
	if (typeof(com.ward6.EDetailerPage) === "undefined")
        return false;

    var EDetailerPage = com.ward6.EDetailerPage;
    EDetailerPage.setModuleXML('references.xml');

    document.touchmove = function(event){
        console.log("Cancelling drag");
        return false;
    }

    EDetailerPage.addActivateFunction(thisEdetailerPage.activate);

    thisEdetailerPage.init();
    
});

var thisEdetailerPage = (function() {
    /*
     * Constants
     */
    var DEBUG_MODE = true && com.ward6.EDetailerPage.DEBUG_MODE, // The EDetailerPage's DEBUG_MODE property affects DEBUG_MODE globally.
        PAGE_ID = "mod2_3_tissue_penetration_0",
        TALLY_SIZE = 6;
    /*
     * Fields
     */
    var thisEdetailerPageAPI = {},
        _dropAreas = [],
        _controls = {
            draggables: false,
            submitBtn: false,
            resultsBtn: false,
            nextPageBtn: false
        },
        _pageData = false;

    /**
     * Initialises our page.
     */
    thisEdetailerPageAPI.init = function() {
        var EDetailerPage = com.ward6.EDetailerPage,
            DragDrop = com.ward6.DragDrop,
            pageData = EDetailerPage.getPageData();

        // Set the page ID (otherwise the get/setPageData methods won't work).
        EDetailerPage.setPageId(PAGE_ID);

        // Assign some meaningful controls to our _controls.
        _controls = {
            dragSpace:      $('div#dragSpace'),
            draggables:     $('div.draggable'),
            dropAreas:      $('div.dropArea'),
            submitBtn:      $('a.submitBtn'),
            resultsBtn:     $('a.resultsBtn'),
            nextPageBtn:    $('a.nextPageBtn'),
            results:        $('div#averageSelections').children('div.button')
        };

        // Re-arrange the draggable options to reflect the order the user has
        // chosen (and saved to localStorage).

        DragDrop.setDragContainer($('div#dragSpace'));
        DragDrop.setDraggables($('div.draggable'));
        DragDrop.setDropAreas($('div.dropArea'));
        DragDrop.toggleActivity(true);

        _controls.resultsBtn.bind(EDetailerPage.UIEvents.UP, thisEdetailerPageAPI.revealAverage);
        _controls.nextPageBtn.bind(EDetailerPage.UIEvents.UP, _nextPage);

		// by Sam @ Nways
		// Clear session data when page loaded.
		if(pageData != false){
			EDetailerPage.clearSessionData();
		}
		
        // Allows the page to adjust off-screen so that it's ready before the
        // user navigates to it.
        thisEdetailerPageAPI.activate();
    };

    /**
     * Activates or adjusts the page. This will check the status of the
     * pageData and look for matching traits. If the pageData exists it will
     * disable the controls, if not, enable them so the user may use the
     * activity.
     */
    thisEdetailerPageAPI.activate = function() {
        var DragDrop = com.ward6.DragDrop,
            pageData = com.ward6.EDetailerPage.getPageData(),
            currentTime = new Date().getTime();

        for (var dragId = _controls.draggables.length; dragId--;) {
            var thisDropArea = _controls.dropAreas.eq(dragId),
                dropAreaCoords = [thisDropArea.position().left,thisDropArea.position().top];

            _controls.draggables.eq(dragId).css(
                {
                    left: dropAreaCoords[0] + 'px',
                    top: dropAreaCoords[1] + 'px'
                }
            );
        }

        DragDrop.dockDraggables();
		
        if (typeof(pageData) === false
            || typeof(pageData.ranking) === "undefined") {
            thisEdetailerPageAPI.togglePage(true);
        } else {
            thisEdetailerPageAPI.togglePage(false);
        }
    };

    /**
     * Enables or disables interactivity on the page. If the page is set to
     * enabled, then it will be manipulable by the user. If the page is set
     * to disabled, then the user may not effect any change to either the order
     * of the selections nor may she submit them.
     * @method togglePage
     * @param {Boolean} enabled
     */
    thisEdetailerPageAPI.togglePage = function(enabled) {
        var DragDrop = com.ward6.DragDrop,
            EDetailerPage = com.ward6.EDetailerPage;
        DragDrop.toggleActivity(enabled);
        if (enabled) {
            _controls.submitBtn.bind(EDetailerPage.UIEvents.UP, thisEdetailerPageAPI.submit);

            _controls.submitBtn.removeClass("disabled");
        } else {
            _controls.submitBtn.unbind(EDetailerPage.UIEvents.UP, thisEdetailerPageAPI.submit);

            _controls.submitBtn.addClass("disabled");
        }
    };

    /**
     * Calculates the correct order in which to display the options, arranges
     * them accordingly and then unveils them.
     * @method
     * @return {Array}
     */
    thisEdetailerPageAPI.revealAverage = function() {
        var totalsTally = thisEdetailerPageAPI.getCurrentData(),
            rankedResults = [],
            numberOfResults = totalsTally.length;

        // Sorting our array will forsake our index information, so we're going
        // to create an sortable array with objects storing both index & value.
        for (var totalIndex = 0; totalIndex < numberOfResults; totalIndex++) {
            rankedResults.push(
                {
                    index: totalIndex,
                    tally: totalsTally[totalIndex]
                }
            );
        }

        rankedResults.sort( function (a, b) { return (a.tally - b.tally) * -1; } );
        for (var rIndex = 0; rIndex < numberOfResults; rIndex++) {
            var thisAverageItem = _controls.results.eq(rankedResults[rIndex].index);
            thisAverageItem.css('top', parseInt(rIndex * 50) + 'px');
            thisAverageItem.addClass('reveal');
            thisAverageItem.addClass('colour' + rIndex);

            if (DEBUG_MODE) console.log(thisAverageItem.text() + " is number " + (rIndex+1)
                + " with " + rankedResults[rIndex].tally + " points.");

            thisAverageItem.stop(true, false).delay(rIndex * 100).animate(
                {
                    opacity:    1,
                    width:      210 + 'px'
                },
                {
                    duration: parseInt(Math.sin(Math.PI / 4 + (Math.PI * (rIndex / numberOfResults) / 4)) * 500, 10)
                }
            );
        }

        return rankedResults;
    };

    /**
     * Submits the list of items in its current format to the EDetailerPage to
     * save for retrieval on subsequent pages. This method will not check if
     * the user is eligible to submit.
     * @method
     * @param submitBtn
     */
    thisEdetailerPageAPI.submit = function(submitBtn) {
        var saveTime = new Date().getTime(),
            EDetailerPage = com.ward6.EDetailerPage,
            rankings = thisEdetailerPageAPI.getSelectedSites(),
            points = _assignPoints(rankings),
            tally = thisEdetailerPageAPI.getCurrentData();

        for (var pointsIndex = 0; pointsIndex < tally.length; pointsIndex++) {
            tally[pointsIndex] = tally[pointsIndex] + points[pointsIndex];
        }

        // Save our persistent tally (indicated by the second argument).
        EDetailerPage.setPageData(
            {
            tally:          tally
            },
            true
        );

        // Save our current attempt, which will be erased on clearSessionData()
        EDetailerPage.setPageData(
            {
                ranking:        rankings,
                lastSaveTime:   saveTime
            }
        );

        thisEdetailerPageAPI.togglePage(false);
    };

    /**
     * Retrieves any stored data for this page if available. If no data has
     * been created then it is intiailised here.
     *
     * This method is designed to create a numerical array, the size of which
     * is defined in a constant named TALLY_SIZE in the thisEdetailerPage
     * object.
     *
     * @return  {Number[]}  An array of the tally values that have been saved,
     *                      or a freshly-initialised set of numbers.
     */
    thisEdetailerPageAPI.getCurrentData = function() {
        var tallyData = [],
            thisPageData = com.ward6.EDetailerPage.getPageData();

        if (thisPageData !== false
            && typeof(thisPageData.tally) === "object") {
            tallyData = thisPageData.tally;
        } else {
            for (var i = 0; i < TALLY_SIZE; i++)
                tallyData.push(0);
        }

        return tallyData;
    };

    /**
     * Fetches an array of draggables in the order in which they have been
     * placed by the user. If there are any missing sites (i.e. they're still
     * being dragged, a Boolean false is returned instead.
     * @method getSelectedSites
     * @return {jqObject[]} An array of the draggable items that have been
     *                      arranged, representative of their chosen order or a
     *                      Boolean false if there are any missing values.
     */
    thisEdetailerPageAPI.getSelectedSites = function() {
        var DragDrop = com.ward6.DragDrop,
            selectedDraggables = DragDrop.getDropAreas(),
            orderedDragganbles = [];

        // Search through the selected draggables for a value that is missing.
        for (dragIndex in selectedDraggables) {
            var draggable = selectedDraggables[dragIndex],
                dragNumber = -1;
            try {
                dragNumber = _getDragDropClassId(draggable);
            } catch (e) { if (DEBUG_MODE) console.error(draggable, " did not exhibit a correctly formatted dragId"); }

            if (typeof(draggable) === "undefined"
                || typeof(draggable) === false) {
                return false;
            }

            orderedDragganbles.push(dragNumber);
        }

        return orderedDragganbles;
    };

    /*
     * Private Methods
     */

    /**
     * Takes an array of numbers representing the user's chosen order and then
     * assigns points to a new array based on their rankings.
     * @private
     * @param {Number[]} orderedDraggables  An array of numbers representing
     *                                      the order in which the user has
     *                                      selected their options.
     * @return {Number[]}   An array of numbers which assigns points to a total
     *                      tally. The number of points dedicated to each
     *                      element will reflect the rank the user has chosen.
     */
    function _assignPoints(orderedDraggables) {
        var odTotal = orderedDraggables.length,
            rankedPoints = new Array(odTotal);

        // Initialise our rankedPoints array.
        for (var rpIndex = 0; rpIndex < odTotal; rpIndex++)
            rankedPoints.push(0);

        // Assign points based on rank.
        for (var odIndex = 0; odIndex < odTotal; odIndex++) {
            var rankPoints = odTotal - odIndex,
                draggableIndex = orderedDraggables[odIndex];

            rankedPoints[draggableIndex] = rankPoints;
        }

        return rankedPoints;
    }

    function _nextPage(clickEvent) {

    }

    /**
     * Takes a draggable or dropArea jQuery object and examines it for evidence
     * of a draggable identifier attributed to its class. This is identified by
     * "dragN", where N is any integer number > 0.
     * @param {jqElement} dragOrDrop The object to examine. This will be
     *                               retrieved by a jQuery.eq(0) statement to
     *                               observe a single HTML element.
     * @throws Error    Throws an error if no draggable or dropArea reference is
     *                  not found.
     */
    function _getDragDropClassId(dragOrDrop) {
        var classesString = dragOrDrop.eq(0).attr('class'),
            classes = classesString.split(' '),
            classCounter = classes.length,
            draggableTest = new RegExp(/^drag[0-9]{1,3}$/i),
            dropAreaTest = new RegExp(/^drop[0-9]{1,3}$/i),
            dragOrDropId = -1;

        while (classCounter--) {
            var thisClass = classes[classCounter];

            if (draggableTest.test(thisClass)
                || dropAreaTest.test(thisClass))
                dragOrDropId = thisClass.replace(/[a-z\s]+/ig, "");
        }

        dragOrDropId = parseInt(dragOrDropId);
        if (!isNaN(dragOrDropId)
            && dragOrDropId !== -1)
            return dragOrDropId;

        throw new Error("Class ID not found.");
    }

    return thisEdetailerPageAPI;
})();