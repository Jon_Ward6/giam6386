/**
 * Ambisome eDetailer Page 5
 *
 * GIAM5571 - Ward6
 * 
 * @author Kashi Samaraweera
 * @copyright Ward6
 * @since May 7th 2012
 */

$(document).ready(function() {
	if (typeof(com.ward6.EDetailerPage) === "undefined")
        return false;
    
    var EDetailerPage = com.ward6.EDetailerPage;
    EDetailerPage.setModuleXML('references.xml');
    thisEdetailerPage.init();
});

var thisEdetailerPage = {
    DEBUG_MODE      : false,
    PAGE_ID         : "mod1_5",
    NEXT_PAGE_URL   : 'amb_mod1_6.html',
    controls        : {
        barGraphs   : false,
        barLabels   : false
    },
    selectedIndex   : 0,
    init            : function() {
        var EDetailerPage = com.ward6.EDetailerPage;

        EDetailerPage.setPageId(this.PAGE_ID);
        EDetailerPage.addActivateFunction(this.activate, { context : this });
        
        this.controls = {
            slider      : $('div.slider'),
            range       : $('input.range'),
            submitBtn   : $('a#submitBtn'),
            xRayBox     : $('div#xray_hype_container')
        };
        
        this.controls.slider.bind(EDetailerPage.UIEvents.MOVE, { thisEdetailerPage : this }, this.updateEstimate);
        this.controls.range.bind("change", { thisEdetailerPage : this }, this.updateEstimate);
        this.controls.submitBtn.bind(EDetailerPage.UIEvents.CLICK, { thisEdetailerPage : this }, this.nextPage);
    },
    activate        : function() {
        this.controls.xRayBox.css({ 'background-image' : 'url(page5_xrayOnly.png)' });
        // Switch on our x-ray backlight.
        HYPE.documents.xray.showNextScene();
        
        com.ward6.EDetailerPage.setPageData(
            {
                selected : this.selectedIndex,
                lastSaveTime : new Date().getTime()
            }
        );
    },
    updateEstimate      : function(slideEvent) {
        var _this = (typeof(slideEvent) !== "undefined")? slideEvent.data.thisEdetailerPage : this,
            selected = parseInt(_this.controls.range.val()) - 1;

        if (selected !== _this.selectedIndex)
            _this.saveEstimate(selected);
        
    },
    saveEstimate        : function(selection) {
        var EDetailerPage = com.ward6.EDetailerPage,
            lastSaveTime = new Date().getTime();

        EDetailerPage.setPageData(
            {
                selected : selection,
                lastSaveTime : lastSaveTime
            }            
        );

        if (this.DEBUG_MODE) console.log("Saved selection as " + selection);
        
        this.selectedIndex = selection;
    },
    nextPage        : function(nextBtnClick) {
        var _this = nextBtnClick.data.thisEdetailerPage,
            nextPageURL = _this.NEXT_PAGE_URL;

        if (com.ward6.EDetailerPage.isApp)
            nextPageURL = "app://page//" + nextPageURL;

        window.location = nextPageURL;
    }
};