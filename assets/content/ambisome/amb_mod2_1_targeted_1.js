/**
 * Ambisome eDetailer Page Mod2_1_Targeted_1
 *
 * This gem of a helper file contains our first attempt at a multi-touch compat-
 * ible JavaScript implementation. The code below is purposefully well
 * documented (unlike most of our other page helper files) which should make it
 * easy to refactor and extract into a library/component of com.ward6.
 *
 * Also, have a look in the _sendElementToCoordinates method for a cool joke!
 *
 * GIAM6386 - Ward6
 * 
 * @author Kashi Samaraweera
 * @copyright Ward6
 */

$(document).ready(function() {
	if (typeof(com.ward6.EDetailerPage) === "undefined")
        return false;

    var EDetailerPage = com.ward6.EDetailerPage;
    EDetailerPage.setModuleXML('references.xml');

    document.touchmove = function(event){
        console.log("Cancelling drag");
        return false;
    }
	
	EDetailerPage.clearSessionData();
	
    thisEdetailerPage.init();
    
});

var thisEdetailerPage = (function() {
    /*
     * Private fields
     */
    var DEBUG_MODE = true && com.ward6.EDetailerPage.DEBUG_MODE, // The EDetailerPage's DEBUG_MODE property affects DEBUG_MODE globally.
        PAGE_ID = "mod2_1_targeted_1",
        DROP_STATES = {
            DROPPED     : 0,
            UNDROPPED   : 1
        };

    /*
     * Fields
     */
    var thisEdetailerPageAPI = {
        dropAreas  :    [false, false, false]
    },
        _controls = {
            dragContainer   : false,
            daggables   : false,
            dropAreas   : false,
            submitBtn   : false,
            completeBtn : false
        },
        globalOffsetX = 0,
        globalOffsetY = 0,
        _dragging = [],
        _dragOrigins = [],
        _submitted = false;

    /**
     * Initialises our page to react to touch/mouse input and movement.
     */
    thisEdetailerPageAPI.init = function() {
        var EDetailerPage = com.ward6.EDetailerPage;
        
        // Set the page ID (otherwise the get/setPageData methods won't work.
        EDetailerPage.setPageId(PAGE_ID);

        _controls = {
            dragContainer   : $('div#dragSpace'),
            draggables      : $('.draggable'),
            dropAreas       : $('.dropArea'),
            submitBtn       : $('a.submitBtn'),
            completeBtn     : $('a.completeBtn')
        };

        var containerPosition = _controls.dragContainer.offset();
        
        globalOffsetX = containerPosition.left;
        globalOffsetY = containerPosition.top;

        for (var i = 0; i < _controls.draggables.length; i++)
            _anchorDraggable(_controls.draggables.eq(i), _controls.draggables.eq(i), true);

        var pageData = EDetailerPage.getPageData();
        if (pageData === false
            || !pageData.hasOwnProperty('allCorrect')) {
            _toggleCompleteButton(true);
        } else {
            _toggleCompleteButton(false);
            _toggleActivity(false);
            thisEdetailerPageAPI.completeActivity();
        }
    }
    
    /**
     * Anchors a draggable item to an element. Doing so will save the draggable
     * item's anchor point to the element supplied.
     * @param {jqObject}    draggable   The draggable object wrapped by jQuery.
     * @param {jqObject}    anchor  The object to which the draggable object
     *                              will snap.
     * @param {Boolean} homeLocation    If true, then this anchor will be used 
                                        as the last-resort "home" location.
     */
    function _anchorDraggable(draggable, anchor, homeLocation) {
        var anchorPosition = [anchor.offset().left - globalOffsetX, anchor.offset().top - globalOffsetY],
            homeLocation = (typeof(homeLocation) === "undefined")? false : (homeLocation === true)? true : false;

        if (homeLocation)
            draggable.data("anchor_home", anchorPosition);

        draggable.data("anchor", anchorPosition);
    }

    /**
     * Allows the whole exercise to be enabled or disabled. If the interface is
     * disabled then answers cannot be dragged and the confirm button will 
     * remain unclickable.
     * @param {Boolean} enabled True if the interface should be enabled.
     */
    function _toggleActivity(enabled) {
        if (enabled) {
            _controls.draggables.bind(EDetailerPage.UIEvents.DOWN, _addToDragging);
            _controls.dragContainer.bind(EDetailerPage.UIEvents.MOVE, _moveDragging);
            //_controls.draggables.bind(EDetailerPage.UIEvents.UP, _removeFromDragging);
        } else {
            _controls.draggables.unbind(EDetailerPage.UIEvents.DOWN, _addToDragging);
            _controls.dragContainer.unbind(EDetailerPage.UIEvents.MOVE, _moveDragging);
            _controls.draggables.unbind(EDetailerPage.UIEvents.UP, _removeFromDragging);        
        }
    }
    
    /**
     * Adds an element to the list of elements being dragged. When an element
     * is added to the queue any movement of the cursor will dictate the
     * position of the element. 
     * @param {Object} jqEvent  The jQuery event that triggers an attachment.
     *                          This will contain the element being dragged in
     *                          the target attribute.
     */
    function _addToDragging(jqEvent) {
        var jqElement = $(jqEvent.target),
            element = jqElement.get(0),
            objectPos = jqElement.position(),
            draggingCounter = _dragging.length,
            pageTouchPos = [jqEvent.pageX || jqEvent.originalEvent.touches[draggingCounter].pageX, jqEvent.pageY || jqEvent.originalEvent.touches[draggingCounter].pageY],
            objectTouchPos = [pageTouchPos[0] - objectPos.left - globalOffsetX, pageTouchPos[1] - objectPos.top - globalOffsetY],
            dropAreaIndex = _draggableOverDropArea(jqElement),
            dragIndex = _getDragDropClassId(jqElement);

        jqEvent.preventDefault();
        jqEvent.stopPropagation();
        
        jqElement.removeClass("dragging");
        
        jqElement.stop(true, false);
        jqElement.data("touchPos" , objectTouchPos);
        jqElement.css(
            {
                left: objectPos[0] - globalOffsetX + 'px',
                top:  objectPos[1] - globalOffsetY + 'px'
            }
        );
        _dragging[draggingCounter] = element;
		
        jqElement.bind(com.ward6.EDetailerPage.UIEvents.UP, _removeFromDragging);
		jqElement.css('z-index', 6000);
        if (dropAreaIndex !== false)
            _notifyDropArea(_controls.dropAreas[dropAreaIndex], jqElement, DROP_STATES.UNDROPPED);
    }
    
    /**
     * Tracks the user's movements whilst interacting with the screen. Objects
     * contained in the _dragging array will be moved with each sucessful call
     * of the _moveDragging method. 
     * @param {Object} jqEvent  The jQuery event that triggers upon moving the
     *                          cursor (touch-dragging or mouse-moving).
     */
    function _moveDragging(jqEvent) {
        var dragging = _dragging,
            draggingCounter = dragging.length;
        
        jqEvent.preventDefault();
        jqEvent.stopPropagation();

        _controls.dropAreas.removeClass("draggableOverDroppable");
        while (draggingCounter--) {
            if (typeof(dragging[draggingCounter]) === "undefined")
                continue;

            var jqElement = $(dragging[draggingCounter]),
                element = jqElement.get(draggingCounter),
                pageTouchPos = [jqEvent.pageX || jqEvent.originalEvent.touches[draggingCounter].pageX, jqEvent.pageY || jqEvent.originalEvent.touches[draggingCounter].pageY],
                objectPos = jqElement.data("touchPos"),
                TEMP_elementText = jqElement.text();
            
            _toggleActiveDropAreas(true);
            jqElement.css(
                {
                    left: pageTouchPos[0] - objectPos[0] - globalOffsetX + 'px',
                    top:  pageTouchPos[1] - objectPos[1] - globalOffsetY + 'px'
                } 
            );
            _draggableOverDropArea(jqElement);
        }
        
    }
    
    /**
     * Removes an object from the _dragging array so that it will stop being
     * moved when the user moves her cursor on the screen.
     * @param {Object} jqEvent  The jQuery event triggered when a user stops
     *                          dragging an object.
     */
    function _removeFromDragging(jqEvent) {
        var jqElement = $(jqEvent.target),
            element = jqElement.get(0),
            dragging = _dragging,
            draggingCounter = dragging.length;
		
        jqElement.removeClass("dragging");
		
        while (draggingCounter--) {
            if (dragging[draggingCounter] === element) {
                var dropAreaIndex = _draggableOverDropArea(jqElement),
                    anchorPos = [jqElement.data("anchor_home")[0], jqElement.data("anchor_home")[1]];
				
                if (dropAreaIndex !== false) {
                    _anchorDraggable(jqElement, _controls.dropAreas.eq(dropAreaIndex));
                    anchorPos = [jqElement.data("anchor")[0], jqElement.data("anchor")[1]];
                }

                jqElement.removeClass('dragging');
                dragging.splice(draggingCounter, 1);
                _sendElementToCoordinates(jqElement, anchorPos);
				jqElement.unbind(com.ward6.EDetailerPage.UIEvents.UP, _removeFromDragging);
				jqElement.css('z-index', 5000);
                _notifyDropArea(_controls.dropAreas[dropAreaIndex], jqElement, DROP_STATES.DROPPED);
            }
        }

        if (dragging.length === 0)
            _toggleActiveDropAreas(false);
            
        _dragging = dragging;
    }
    
    /**
     * Animates the object to a set of coordinates specified by xyArray. If the
     * object is not currently being manipulated by the user then it is sent to
     * xyArray coordinates defined.
     * @param {jqObject} jqObject   the object to move.
     * @param {Number[]} xyArray    The coordinates to move the item to.
     */
    function _sendElementToCoordinates(jqObject, xyArray) {
        var element = (typeof(jqObject.get) !== "undefined")? jqObject.get(0) : false,
            elementPosition,
            dX,
            dY,
            distance;

        if (typeof(jqObject.animate) === "undefined")
            return false;

        elementPosition = [jqObject.offset().left, jqObject.offset().top];
        dX = elementPosition[0] - (xyArray[0] + globalOffsetX);
        dY = elementPosition[1] - (xyArray[1] + globalOffsetY);

        // Please note that this assumes pixels are square (which they are
        // typically not). Incidently, (and on a completely unrelated note), why
        // do mathematicians make poor bakers?
        // ...
        // Because they think PI r ^2
        // Ha!
        distance = Math.sqrt(Math.pow(dX, 2) + Math.pow(dY, 2));
        
        for (var draggableIndex = _dragging.length; draggableIndex > 0; draggableIndex--)
            if (_dragging[draggableIndex] === element)
                return true;

        jqObject.animate(
            {
                left: xyArray[0],
                top: xyArray[1]
            },
            {
                duration: Math.round(distance * 1.5),
                specialEasing: {top: 'easeOutSine', left: 'easeInOutSine'}
            }
        );
    }
    
    /**
     * Called when the dropArea has had its status changed. This is indicated as
     * a true or false value of dropped.
     * @param {jqObject} dropArea   The jQuery object encapsulating the dropArea
     *                              that is being effected.
     * @param {jqObject} dropped    The item being dropped (or undropped into
     *                              the dropArea).
     * @param {Number}  dropState   The drop state, which defines the state such
     *                              as dropped or undropped. This is enumerated
     *                              in the DROP_STATES constant.
     */
    function _notifyDropArea(dropArea, dropped, dropState) {
        var drop = (dropState === DROP_STATES.DROPPED)? true : false,
            dropAreas = thisEdetailerPageAPI.dropAreas,
            dropAreaIndex = false,
            enableSubmit = true;
           
        // Find the index of the dropArea.
        for (areaIndex in dropAreas)
            if (dropArea === _controls.dropAreas[areaIndex]){
                dropAreaIndex = areaIndex;
			}

        if (drop) {
            //if (DEBUG_MODE) console.log("draggable '" + dropped.text() + "' dropped on dropArea " + dropAreaIndex);
            if (dropAreaIndex !== false) {
                // Remove the current resident.
                if (dropAreas[dropAreaIndex] !== false){
                    _sendElementToCoordinates(dropAreas[dropAreaIndex], dropAreas[dropAreaIndex].data('anchor_home'));
					console.log('send to home, rebind drop');
					dropAreas[dropAreaIndex].bind(com.ward6.EDetailerPage.UIEvents.UP, _removeFromDragging);
				}
                dropAreas[dropAreaIndex] = dropped;
            }
        } else {
            //if (DEBUG_MODE) console.log("draggable '" + dropped.text() + "' lifted from dropArea " + dropAreaIndex);
            if (dropAreaIndex !== false) {
                dropAreas[dropAreaIndex] = false;
            }
        }
        
        for (var dragIndex = 0; dragIndex < _controls.dropAreas.length; dragIndex++) {
            //if (DEBUG_MODE) console.log("Checking dropArea " + dragIndex);
            if (dropAreas[dragIndex] === false) {
                //if (DEBUG_MODE) console.log("Adding question mark to dropArea " + dragIndex);
                _controls.dropAreas.eq(dragIndex).text("?");
            } else {
                //if (DEBUG_MODE) console.log("Removing question mark from dropArea " + dragIndex);
                _controls.dropAreas.eq(dragIndex).text("");
            }
        }
		
        for (var dropAreaCount = 0; dropAreaCount < dropAreas.length; dropAreaCount++)
            enableSubmit = enableSubmit && (dropAreas[dropAreaCount] !== false)? true : false;

        if (enableSubmit)
            _toggleSubmit(true);
        else
            _toggleSubmit(false);        
    }
    
    /**
     * Allows the submit button to be clicked and appear active.
     * @param {Boolean} submitEnabled   Sets the state to enabled/disabled.
     */
    function _toggleSubmit(submitEnabled) {
        if (submitEnabled) {
            _controls.submitBtn.bind(com.ward6.EDetailerPage.UIEvents.CLICK, thisEdetailerPageAPI.submit);
            _controls.submitBtn.removeClass("disabled");
        } else {
            _controls.submitBtn.unbind(com.ward6.EDetailerPage.UIEvents.CLICK, thisEdetailerPageAPI.submit);
            _controls.submitBtn.addClass("disabled");
        }
    }
    
    /**
     * Detects whether the draggable item is being dropped onto a dropArea
     * target. Once the assessment is made the draggable item being dropped will
     * either snap to the correct coordinates of the dropArea or return to the
     * the original location.
     * @param   {jqObject}  jqObject    The jQuery object being dragged.
     */
    function _draggableOverDropArea(draggable) {
        var dropAreas = _controls.dropAreas,
            dropAreasArray = [],
            draggablePosition = [draggable.offset().left, draggable.offset().top],
            dropAreaIndex = false,
            dragIndex = false;
		
        dropAreas.each(
            function(index, dropArea) {
                var dropAreaElement = $(dropArea),
                    dropAreaPosition = [dropAreaElement.offset().left, dropAreaElement.offset().top],
                    dropAreaSize = [dropAreaElement.width(), dropAreaElement.height()],
                    dropArea = [
                        [dropAreaPosition[0] - dropAreaSize[0], dropAreaPosition[0] + dropAreaSize[0]],
                        [dropAreaPosition[1] - dropAreaSize[1], dropAreaPosition[1] + dropAreaSize[1]]
                    ];

                if (draggablePosition[0] > dropArea[0][0]
                    && draggablePosition[0] < dropArea[0][1]
                    && draggablePosition[1] > dropArea[1][0]
                    && draggablePosition[1] < dropArea[1][1]) {
                    dropAreaElement.addClass("draggableOverDroppable");
                    dragIndex = index;
                }
            }
        );
        return dragIndex;
    }

    /**
     * Toggles the state of the drop areas between their default (dormant)
     * state and their active (attention-grabbing) state. Used when the user is
     * actively dragging a draggable item.
     * @param   {Boolean}   dropAreaState   The toggle which will determine if
     *                                      the dropAreas should be active
     *                                      (true) or inactive (false).
     */
    function _toggleActiveDropAreas(dropAreaState) {
        if (dropAreaState === true) {
            _controls.dropAreas.addClass('indicateDroppable');
        } else {
            _controls.dropAreas.removeClass('indicateDroppable');
            _controls.dropAreas.removeClass("draggableOverDroppable");
        }
    }
    
    /**
     * Enables/disables the complete button's action.
     * @param {Boolean} completeEnabled True to enable, false otherwise.
     */
    function _toggleCompleteButton(completeEnabled) {
        if (completeEnabled) {
            _toggleActivity(true);
            _controls.completeBtn.bind(com.ward6.EDetailerPage.UIEvents.DOWN, thisEdetailerPageAPI.completeActivity);
            _controls.completeBtn.removeClass("disabled");
        } else {
            _controls.completeBtn.unbind(com.ward6.EDetailerPage.UIEvents.DOWN, thisEdetailerPageAPI.completeActivity);
            _toggleSubmit(false);
            _toggleActivity(false);
            _controls.completeBtn.addClass("disabled");        
        }
    }
    
    function _stopAllDragging(jqEvent) {
        _dragging = [];
        _toggleActiveDropAreas(false);
    }
    
    /**
     * Completes the event like magic before the user's very eyes.
     * @param {jqEvent} completeBtnEvent    The event encapsulating the event
     *                                      trigger.
     */
    thisEdetailerPageAPI.completeActivity = function(completeBtnEvent) {
        var draggables = _controls.draggables,
            dropAreas = _controls.dropAreas,
            pageData = com.ward6.EDetailerPage.getPageData(),
            answers = false;
        
        if (pageData !== false
            && pageData.hasOwnProperty('chosenAnswers'))
            answers = pageData.chosenAnswers;

        draggables.each(
            function (index, draggable) {
                var drag = $(draggable),
                    dragId = _getDragDropClassId(drag),
                    dropArea = false,
                    dropAreaXY = false,
                    correct = true;
                
                if (answers === false) {
                    dropArea = dropAreas.filter(".drop" + dragId);
                } else {
                    dropArea = dropAreas.filter(".drop" + answers[index]);
                    correct = index === answers[index];
                }
                
                if (dropArea.length < 1)
                    return;

                dropAreaXY = [dropArea.offset().left - globalOffsetX, dropArea.offset().top - globalOffsetY];
                
                _stopAllDragging();
                _toggleCompleteButton(false);
                _sendElementToCoordinates(drag, dropAreaXY);
                
                thisEdetailerPageAPI.dropAreas[index] = drag;
                console.log(correct);
                // Change the colour of our draggable once it's in place.
                drag.animate({ zIndex: 9999 }, { duration: 20, complete: function() { $(this).removeClass('incorrect').addClass('correct') } });
            }
        );
    }
    
    /**
     * Takes a draggable or dropArea jQuery object and examines it for evidence
     * of a draggable identifier attributed to its class. This is identified by
     * "dragN", where N is any integer number > 0.
     * @param {jqObject} dragOrDrop  The object to examine. This will be
     *                               retrieved by a jQuery.eq(0) statement to
     *                               observe a single HTML element.
     * @throws Error    Throws an error if no draggable or dropArea reference is
     *                  not found.
     */
    function _getDragDropClassId(dragOrDrop) {
        var classesString = dragOrDrop.eq(0).attr('class'),
            classes = classesString.split(' '),
            classCounter = classes.length,
            draggableTest = new RegExp(/^drag[0-9]{1,3}$/i),
            dropAreaTest = new RegExp(/^drop[0-9]{1,3}$/i),
            dragOrDropId = -1;

        while (classCounter--) {
            var thisClass = classes[classCounter];

            if (draggableTest.test(thisClass)
                || dropAreaTest.test(thisClass))
                dragOrDropId = thisClass.replace(/[a-z\s]+/ig, "");
        }
        
        dragOrDropId = parseInt(dragOrDropId);
        if (!isNaN(dragOrDropId)
            && dragOrDropId !== -1)
            return dragOrDropId;

        throw new Error("Class ID not found.");
    }

    /**
     * Works out which draggbles are in which position and marks accordingly.
     * Once the marking is done, the interface will be unlocked for viewing
     * results and auto-completion.
     * @param {jqEvent} submitBtnClick  The event that encapsulates the click of
     *                                  our submit button.
     */
    thisEdetailerPageAPI.submit = function(submitBtn) {
        var draggables = _controls.draggables,
            dropAreas = _controls.dropAreas,
            dropped = thisEdetailerPageAPI.dropAreas,
            dropCounter = dropped.length,
            EDetailerPage = com.ward6.EDetailerPage,
            allCorrect = true,
            chosen = [],
            lastSaveTime = new Date().getTime();
        
        while (dropCounter--) {
            var thisDraggable = dropped[dropCounter],
                thisDropArea = dropAreas.eq(dropCounter),
                dragId = -1,
                dropId = -1;
            
            if (thisDraggable === false)
                return;

            try {
                dragId = _getDragDropClassId(thisDraggable);
                dropId = _getDragDropClassId(thisDropArea);
            } catch (except) {}
            
            if (dragId === -1
                || dropId === -1)
                return;
            
            chosen.push(2-dropCounter);
            allCorrect = allCorrect && (dragId === dropId);
            if (dragId === dropId)
                thisDraggable.addClass("correct");
            else
                thisDraggable.addClass("incorrect");
        }
        
        _toggleSubmit(false);
        _toggleActivity(false);
        
        if (DEBUG_MODE) console.log("allCorrect: ", allCorrect, "\nallCorrect === true:", (allCorrect===true)? "true" : "false");
        
        EDetailerPage.setPageData(
            {
                allCorrect : allCorrect,
                chosenAnswers : chosen,
                lastSaveTime : lastSaveTime
            }
        );
    }
    
    thisEdetailerPageAPI.nextPage = function(nextBtnClick) {
        var _this = nextBtnClick.data.thisEdetailerPage,
            nextPageURL = _this.NEXT_PAGE_URL;

        if (com.ward6.EDetailerPage.isApp)
            nextPageURL = "app://page//" + nextPageURL;

        window.location = nextPageURL;
    }
    
    return thisEdetailerPageAPI;
})();