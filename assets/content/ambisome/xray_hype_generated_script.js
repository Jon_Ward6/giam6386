//	HYPE.documents["xray"]

(function HYPE_DocumentLoader() {
	var resourcesFolderName = "xray_Resources";
	var documentName = "xray";
	var documentLoaderFilename = "xray_hype_generated_script.js";

	// find the URL for this script's absolute path and set as the resourceFolderName
	try {
		var scripts = document.getElementsByTagName('script');
		for(var i = 0; i < scripts.length; i++) {
			var scriptSrc = scripts[i].src;
			if(scriptSrc != null && scriptSrc.indexOf(documentLoaderFilename) != -1) {
				resourcesFolderName = scriptSrc.substr(0, scriptSrc.lastIndexOf("/"));
				break;
			}
		}
	} catch(err) {	}

	// Legacy support
	if (typeof window.HYPE_DocumentsToLoad == "undefined") {
		window.HYPE_DocumentsToLoad = new Array();
	}
 
	// load HYPE.js if it hasn't been loaded yet
	if(typeof HYPE_100 == "undefined") {
		if(typeof window.HYPE_100_DocumentsToLoad == "undefined") {
			window.HYPE_100_DocumentsToLoad = new Array();
			window.HYPE_100_DocumentsToLoad.push(HYPE_DocumentLoader);

			var headElement = document.getElementsByTagName('head')[0];
			var scriptElement = document.createElement('script');
			scriptElement.type= 'text/javascript';
			scriptElement.src = resourcesFolderName + '/' + 'HYPE.js?hype_version=100';
			headElement.appendChild(scriptElement);
		} else {
			window.HYPE_100_DocumentsToLoad.push(HYPE_DocumentLoader);
		}
		return;
	}
	
	var hypeDoc = new HYPE_100();
	
	var attributeTransformerMapping = {b:"i",c:"i",bC:"i",d:"i",aS:"i",M:"i",e:"f",N:"i",f:"d",aT:"i",O:"i",g:"c",aU:"i",P:"i",Q:"i",aV:"i",R:"c",aW:"f",aI:"i",S:"i",T:"i",l:"d",aX:"i",aJ:"i",m:"c",n:"c",aK:"i",X:"i",aZ:"i",A:"c",Y:"i",aL:"i",B:"c",C:"c",D:"c",t:"i",E:"i",G:"c",bA:"c",a:"i",bB:"i"};

var scenes = [{onSceneLoadAction:{type:0},timelines:{kTimelineDefaultIdentifier:{framesPerSecond:30,animations:[],identifier:"kTimelineDefaultIdentifier",name:"Main Timeline",duration:0}},sceneIndex:0,perspective:"600px",oid:"13",initialValues:{"14":{o:"content-box",h:"page5_xrayOff.png",x:"visible",a:0,q:"100% 100%",b:0,j:"absolute",r:"inline",c:695,k:"div",z:"1",d:415,aX:0}},backgroundColor:"#212121",name:"inactive"},{onSceneLoadAction:{type:0},initialValues:{"3":{o:"content-box",h:"page5_xrayOn.png",x:"visible",a:0,q:"100% 100%",b:0,j:"absolute",r:"inline",aX:0,c:695,k:"div",d:415,z:"5",e:"0.000000"},"11":{o:"content-box",h:"page5_xrayStarterOverexposed.png",x:"visible",a:-1,q:"100% 100%",b:-1,j:"absolute",r:"inline",c:695,k:"div",z:"6",d:415,e:"0.000000",aP:"pointer",bD:"none"},"4":{o:"content-box",h:"page5_xrayOnly.png",x:"visible",a:0,q:"100% 100%",b:0,j:"absolute",r:"inline",aX:0,c:695,k:"div",d:415,z:"4",e:"0.000000"},"2":{o:"content-box",h:"page5_xrayOff.png",x:"visible",a:0,q:"100% 100%",b:0,j:"absolute",r:"inline",c:695,k:"div",z:"1",d:415,aX:0},"7":{o:"content-box",h:"page5_xrayStarterRight.png",x:"visible",a:0,q:"100% 100%",b:0,j:"absolute",r:"inline",aX:0,c:695,k:"div",d:415,z:"2",e:"0.250000"},"5":{o:"content-box",h:"page5_xrayStarterLeft.png",x:"visible",a:0,q:"100% 100%",b:0,j:"absolute",r:"inline",aX:0,c:695,k:"div",d:415,z:"3",e:"0.000000"}},timelines:{kTimelineDefaultIdentifier:{framesPerSecond:30,animations:[{f:"3",t:0.40000001,d:0.033333331,i:"e",e:"0.750000",r:1,s:"0.000000",o:"5"},{f:"3",t:0.40000001,d:0.033333331,i:"e",e:"0.200000",r:1,s:"0.000000",o:"4"},{f:"3",t:0.43333334,d:0.033333331,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:0.43333334,d:0.16666669,i:"e",e:"0.200000",s:"0.200000",o:"4"},{f:"3",t:0.46666667,d:0.033333331,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:0.5,d:0.033333361,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:0.53333336,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:0.56666666,d:0.033333361,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:0.60000002,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:0.60000002,d:0.033333302,i:"e",e:"0.250000",s:"0.200000",o:"4"},{f:"3",t:0.63333333,d:0.033333361,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:0.63333333,d:0.033333361,i:"e",e:"0.750000",r:1,s:"0.250000",o:"7"},{f:"3",t:0.63333333,d:0.033333361,i:"e",e:"0.400000",s:"0.250000",o:"4"},{f:"3",t:0.66666669,d:0.033333302,i:"e",e:"0.500000",s:"0.750000",o:"7"},{f:"3",t:0.66666669,d:1.0666666,i:"e",e:"0.400000",s:"0.400000",o:"4"},{f:"3",t:0.66666669,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:0.69999999,d:0.033333361,i:"e",e:"0.750000",s:"0.500000",o:"7"},{f:"3",t:0.69999999,d:0.033333361,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:0.73333335,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:0.73333335,d:0.033333302,i:"e",e:"0.500000",s:"0.750000",o:"7"},{f:"3",t:0.76666665,d:0.033333361,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:0.76666665,d:0.033333361,i:"e",e:"0.750000",s:"0.500000",o:"7"},{f:"3",t:0.80000001,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:0.80000001,d:0.033333302,i:"e",e:"0.500000",s:"0.750000",o:"7"},{f:"3",t:0.83333331,d:0.033333361,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:0.83333331,d:0.033333361,i:"e",e:"0.750000",s:"0.500000",o:"7"},{f:"3",t:0.86666667,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:0.86666667,d:0.033333302,i:"e",e:"0.500000",s:"0.750000",o:"7"},{f:"3",t:0.89999998,d:0.033333361,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:0.89999998,d:0.033333361,i:"e",e:"0.750000",s:"0.500000",o:"7"},{f:"3",t:0.93333334,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:0.93333334,d:0.033333302,i:"e",e:"0.500000",s:"0.750000",o:"7"},{f:"3",t:0.96666664,d:0.033333361,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:0.96666664,d:0.033333361,i:"e",e:"0.750000",s:"0.500000",o:"7"},{f:"3",t:1,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:1,d:0.033333302,i:"e",e:"0.500000",s:"0.750000",o:"7"},{f:"3",t:1.0333333,d:0.033333421,i:"e",e:"0.600000",s:"0.500000",o:"7"},{f:"3",t:1.0333333,d:0.033333421,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:1.0666667,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:1.0666667,d:0.033333302,i:"e",e:"0.700000",s:"0.600000",o:"7"},{f:"3",t:1.1,d:0.033333302,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:1.1,d:0.033333302,i:"e",e:"0.650000",s:"0.700000",o:"7"},{f:"3",t:1.1333333,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:1.1333333,d:0.73333335,i:"e",e:"1.000000",s:"0.650000",o:"7"},{f:"3",t:1.1666666,d:0.033333421,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:1.2,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:1.2333333,d:0.033333302,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:1.2666667,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:1.3,d:0.033333421,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:1.3333334,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:1.3666667,d:0.033333302,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:1.4,d:0.033333421,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:1.4333334,d:0.033333302,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:1.4666667,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:1.5,d:0.033333302,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:1.5333333,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:1.5666666,d:0.033333421,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:1.6,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:1.6333333,d:0.033333421,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:1.6666667,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:1.7,d:0.033333302,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:1.7333333,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:1.7333333,d:0.033333302,i:"e",e:"1.000000",r:1,s:"0.000000",o:"3"},{f:"3",t:1.7333333,d:0.033333302,i:"e",e:"0.800000",s:"0.400000",o:"4"},{f:"3",t:1.7666667,d:0.033333302,i:"e",e:"0.500000",s:"0.750000",o:"5"},{f:"3",t:1.7666667,d:0.033333302,i:"e",e:"0.000000",s:"1.000000",o:"3"},{f:"3",t:1.7666667,d:0.033333302,i:"e",e:"0.000000",s:"0.800000",o:"4"},{f:"3",t:1.8,d:0.033333302,i:"e",e:"0.750000",s:"0.500000",o:"5"},{f:"3",t:1.8,d:0.033333302,i:"e",e:"1.000000",s:"0.000000",o:"3"},{f:"3",t:1.8333333,d:0.033333421,i:"e",e:"1.000000",s:"0.750000",o:"5"},{f:"3",t:1.8333333,d:0.033333421,i:"e",e:"0.000000",s:"1.000000",o:"3"},{f:"3",t:1.8666667,d:0.033333302,i:"e",e:"1.000000",s:"1.000000",o:"5"},{f:"3",t:1.8666667,d:0.033333302,i:"e",e:"0.000000",s:"0.000000",o:"3"},{f:"3",t:1.9,d:0.033333421,i:"e",e:"1.000000",s:"1.000000",o:"5"},{f:"3",t:1.9,d:0.033333421,i:"e",e:"1.000000",s:"0.000000",o:"3"},{f:"3",t:1.9333334,d:0.033333302,i:"e",e:"0.750000",s:"1.000000",o:"5"},{f:"3",t:1.9333334,d:0.066666603,i:"e",e:"0.000000",s:"1.000000",o:"3"},{f:"3",t:2,d:0.13333344,i:"e",e:"0.150000",r:1,s:"0.000000",o:"11"},{f:"3",t:2,d:0.13333344,i:"e",e:"1.000000",s:"0.000000",o:"3"},{f:"3",t:2.1333334,d:0.93333316,i:"e",e:"0.000000",s:"0.150000",o:"11"}],identifier:"kTimelineDefaultIdentifier",name:"Main Timeline",duration:3.0666666}},sceneIndex:1,perspective:"600px",oid:"1",onSceneAnimationCompleteAction:{type:0},backgroundColor:"#212121",name:"active"}];


	
	var javascripts = [];


	
	var Custom = {};
	var javascriptMapping = {};
	for(var i = 0; i < javascripts.length; i++) {
		try {
			javascriptMapping[javascripts[i].identifier] = javascripts[i].name;
			eval("Custom." + javascripts[i].name + " = " + javascripts[i].source);
		} catch (e) {
			hypeDoc.log(e);
			Custom[javascripts[i].name] = (function () {});
		}
	}
	
	hypeDoc.setAttributeTransformerMapping(attributeTransformerMapping);
	hypeDoc.setScenes(scenes);
	hypeDoc.setJavascriptMapping(javascriptMapping);
	hypeDoc.Custom = Custom;
	hypeDoc.setCurrentSceneIndex(0);
	hypeDoc.setMainContentContainerID("xray_hype_container");
	hypeDoc.setResourcesFolderName(resourcesFolderName);
	hypeDoc.setShowHypeBuiltWatermark(0);
	hypeDoc.setShowLoadingPage(false);
	hypeDoc.setDrawSceneBackgrounds(false);
	hypeDoc.setDocumentName(documentName);

	HYPE.documents[documentName] = hypeDoc.API;

	hypeDoc.documentLoad(this.body);
}());

