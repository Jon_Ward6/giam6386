/**
 * Ambisome eDetailer Page 4-NO
 *
 * GIAM5571 - Ward6
 * 
 * @author Kashi Samarwaeera
 * @copyright Ward6
 * @since May 11th 2012
 */

$(document).ready(function() {
	if (typeof(com.ward6.EDetailerPage) === "undefined")
        return false;

    var EDetailerPage = com.ward6.EDetailerPage;
    EDetailerPage.setModuleXML('references.xml');
    
    thisEdetailerPage.init();
});

var thisEdetailerPage = {
    DEBUG_MODE      : true && com.ward6.EDetailerPage.DEBUG_MODE, // The EDetailerPage's DEBUG_MODE property affects DEBUG_MODE globally.
    PAGE_ID         : "mod1_4_no",
    NEXT_PAGE_URL   : "amb_mod1_timeline0.html",
    ANIMATION_DURATION    : 2000,
    ANIMATION_EASING      : "easeOutSine",
    selectedIndex   : 1,
    controls        : {
        graphPeople     : false,
        submitBtn       : false
    },
    init                    : function() {
        var EDetailerPage = com.ward6.EDetailerPage;
        
        // Set the page ID (otherwise the get/setPageData methods won't work.
        EDetailerPage.setPageId(this.PAGE_ID);

        this.controls = {
            graphBar        : $('div#comparisonBar'),
            submitBtn       : $('div.advanceToTimeline a.button'),
            peopleNoText    : $('hgroup.graphPeopleValues').find("h1:not(h1.notChosen)"),
            peopleYesText   : $('hgroup.graphPeopleValues').find("h1.notChosen"),
            yesNo           : $("span.switch")
        };
        
        this.controls.yesNo.bind(EDetailerPage.UIEvents.CLICK, { thisEdetailerPage : this }, this.toggleYesNo);

        // And of course, our submit button.
        this.controls.submitBtn.bind(EDetailerPage.UIEvents.CLICK, { thisEdetailerPage : this}, this.nextPage);

        EDetailerPage.addActivateFunction(this.activate, { context : this });
    },
    activate                : function() {
        var _this = this,
            EDetailerPage = com.ward6.EDetailerPage,
            storedNoCount = EDetailerPage.getPageData(),
            storedYesCount = EDetailerPage.getPageData("mod1_4_yes"),
            storedAnswer = EDetailerPage.getPageData("mod1_3"),
            tally = [
                0,
                0
            ], // tally[0] = # of nos; tally[1] = # of yeses
            tallyTotal = 1,
            tallyTime = new Date().getTime();

        if (storedNoCount
            && typeof(storedNoCount.nos) !== "undefined")
            tally[0] = parseInt(storedNoCount.nos);

        if (storedYesCount
            && typeof(storedYesCount.yeses) !== "undefined")
            tally[1] = parseInt(storedYesCount.yeses);
        
        if (storedAnswer
            && typeof(storedAnswer.selection) !== "undefined") {
            var answer = parseInt(storedAnswer.selection),
                lastSaveTime = (typeof(storedAnswer.lastSaveTime) !== "undefined")? storedAnswer.lastSaveTime : 0,
                lastTallyTime = (typeof(storedNoCount.lastTallyTime) !== "undefined")? storedNoCount.lastTallyTime : 0;
            
            if (answer === 0
                && lastSaveTime > lastTallyTime) {
                tally[0]++;
                EDetailerPage.setPageData(
                    {
                        nos : tally[0],
                        lastTallyTime : tallyTime
                    },
                    true
                );
            }
        }

        tallyTotal = (tally[0] + tally[1] > 0)? tally[0] + tally[1] : 1;
        
        if (this.DEBUG_MODE) console.log("tally: ", tally, "\ntallyTotal: ", tallyTotal);

        this.controls.graphBar.stop(true, false).animate(
            { width : (tally[0]/tallyTotal)*100 + '%' },
            {
                step        : function(percent, stepData) {
                    var existingNoValue = _this.controls.peopleNoText.html(),
                        newNoValue = stepData.pos * (tally[0]/tallyTotal)*100,
                        existingYesValue = _this.controls.peopleYesText.html(),
                        newYesValue = 100 - newNoValue;

                    _this.controls.peopleNoText.html(existingNoValue.replace(/[0-9]+/, Math.round(newNoValue)));
                    _this.controls.peopleYesText.html(existingYesValue.replace(/[0-9]+/, Math.round(newYesValue)));
                },
                duration    : this.ANIMATION_DURATION,
                easing      : this.ANIMATION_EASING
            }
        );
        
    },
    toggleYesNo     : function(yesNoToggle) {
        var EDetailerPage = com.ward6.EDetailerPage,
            _this = yesNoToggle.data.thisEdetailerPage,
            toggle = $(this),
            yesNo = (toggle.hasClass("noPosition"))? 0 : 1,
            saveTime = new Date().getTime();
        
        console.log("Saving data...");
        _this.pageToggle = yesNo;
        EDetailerPage.setPageData(
            {
                selection       : yesNo,
                lastSaveTime    : saveTime
            }
        );
        if (_this.DEBUG_MODE) console.log("Toggle: ", yesNo);
    },
    nextPage                : function(nextBtnClick) {
        var _this = nextBtnClick.data.thisEdetailerPage,
            nextPageURL = _this.NEXT_PAGE_URL;

        if (com.ward6.EDetailerPage.isApp)
            nextPageURL = "app://page//" + nextPageURL;

        window.location = nextPageURL;
    }
};