(function(){
    
    var DebugAPI = {},
        DEBUG_MODE = true,
        INDENT_STRING = '    ',
        DEBUG_LINE_PREFIX = '<p>',
        DEBUG_LINE_SUFFIX = '</p>',
        debugWindow = false,
        lineNumber = 1,
        debugWindowString = '';

    DebugAPI.showWindow = function() {
        var debugContainer = document.createElement('div');
        debugContainer.setAttribute('id', 'w6Debug');
        $(debugContainer).load('_debugWindow.html',
            function () {
                // loaded. place the debugWindowString contents inside.
                debugContainer.innerHTML += debugWindowString;
            }
        );
        
        $("body").append(debugContainer);
        debugWindow = $(debugContainer);
        //window.console.log = DebugAPI.log;
    }
    
    function _runOnce() {
        if (typeof($) === "undefined")
            return new Error("Ward6Debug cannot function as jQuery was not found");

        window.com = window.com || {};
        window.com.ward6 = window.com.ward6 || {};
        window.com.ward6.Debug = DebugAPI;
        
        $(document).ready(function() {
            DebugAPI.showWindow();
        });
    }
    
    DebugAPI.log = function() {
        var addition = '';

        for (argIndex in arguments) {
            var arg = arguments[argIndex],
                contents = '';
        
            if (typeof(arg) === "string") {
                addition += DEBUG_LINE_PREFIX + '<span class="lineNumber">' + lineNumber + '</span>' + '<span style="color: #339900">"' + arg + '"</span>' + DEBUG_LINE_SUFFIX;
            } else if (typeof(arg) === "number") {
                addition +=  DEBUG_LINE_PREFIX + '<span class="lineNumber">' + lineNumber + '</span>' + '<span style="color: #333333">' + arg + '</span>' + DEBUG_LINE_SUFFIX;
            } else if (typeof(arg) === "object") {
                addition += DEBUG_LINE_PREFIX + '<span class="lineNumber">' + lineNumber + '</span>' + "Object: {" + DEBUG_LINE_SUFFIX;
                addition += _formatObjectAsString(arg, 2);
                addition += DEBUG_LINE_PREFIX + '<span class="lineNumber">&nbsp;</span>' + "}" + DEBUG_LINE_SUFFIX;
            } else if (typeof(arg) === "undefined") {
                addition += DEBUG_LINE_PREFIX + '<span class="lineNumber">' + lineNumber + '</span><span color="#666666">undefined</span>' + DEBUG_LINE_SUFFIX;
            } else if (arg == null) {
                addition += DEBUG_LINE_PREFIX + '<span class="lineNumber">' + lineNumber + '</span><span color="#666666">null</span>' + DEBUG_LINE_SUFFIX;
            }
            lineNumber++;
        }
        debugWindowString = addition + debugWindowString;
        debugWindow.prepend(addition);
    }
    
    /**
     * This method will take an object and splay it as a well-formatted list
     * of fields.
     * @param {Object} object   The object to be disassembled.
     * @param {Object} indent   (Optional) the current indentation index.
     */
    function _formatObjectAsString(object, indent) {
        var indent = (typeof(indent) === "undefined")? 1 : indent,
            addition = "";

        for (objIndex in object) {
            var currentObject = object[objIndex],
                itemString = currentObject,
                indentation = "";
            
            console.log("Processing (" + indent + ")" + objIndex);
            if (!object.hasOwnProperty(objIndex)) {
                console.log("Skipping " + objIndex);
                continue;
            }

            for (var i = 0; i < indent; i++) {
                indentation += '<span class="lineNumber">&nbsp;</span>';
            }

            switch (typeof(currentObject)) {
                case "function":
                    itemString = 'function()';
                    break;
                case "boolean":
                    itemString = (itemString)? "true" : "false";
                    break;
                case "string":
                    itemString = '<span style="color: #339900">"' + itemString + '"</span>';
                    break;
                case "number":
                    itemString = itemString;
                    break;
                case "object":
                    itemString = DEBUG_LINE_PREFIX + indentation + objIndex + ": {" + DEBUG_LINE_SUFFIX;
                    itemString += _formatObjectAsString(currentObject, indent + 1);
                    itemString += DEBUG_LINE_PREFIX + indentation + "}" + DEBUG_LINE_SUFFIX;
                    break;
                default:
                    itemString = "other";
            }
            
            addition += DEBUG_LINE_PREFIX + indentation + '<span class="objectName">' + objIndex +'</span>: ' + itemString + DEBUG_LINE_SUFFIX;
        }
        
        return addition;
    }

    _runOnce();
})();