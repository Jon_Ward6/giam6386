/**
 * Ambisome eDetailer Page timeline0
 *
 * GIAM5571 - Ward6
 * 
 * @author Kashi Samaraweera
 * @copyright Ward6
 * @since May 14th 2012
 */

$(document).ready(function() {
	if (typeof(com.ward6.EDetailerPage) === "undefined")
        return false;
    
    var EDetailerPage = com.ward6.EDetailerPage;
    EDetailerPage.setModuleXML('references.xml');
    thisEdetailerPage.init();
});

var thisEdetailerPage = {
    DEBUG_MODE      : true && com.ward6.EDetailerPage.DEBUG_MODE,
    PAGE_ID         : "timeline0",
    NEXT_PAGE_URL   : "amb_mod1_10.html",
    controls        : {
        timeline        : false,
        ekgBackground   : false
    },
    originPage      : "mod1_4_no",
    ANIMATION_DURATION    : 5000,
    ANIMATION_EASING      : "easeInOutQuad",
    landmarks       : {
        day1    :   [0, 500],
        day4    :   [-2890, 3350],
        day6    :   [-4810, 4900],
        day13   :   [-11530, 11720]
    },
    init            : function() {
        var EDetailerPage = com.ward6.EDetailerPage,
            origin = [0,0];

        this.controls = {
            timeline        : $('div#timeline'),
            ekgBackground   : $('div#ekgBackground'),
            xRayButton      : $('div#day6_event1').find('a.button'),
            continueButton  : $('div#day13_event1').find('a.button')
        };
        this.originPage = this.landmarksFromLatestSelectionPageData(["mod1_4_no", "mod1_7_no", "mod1_9_no", "mod1_9_yes"]);
        
        EDetailerPage.setPageId(this.PAGE_ID);
        this.snapTimeline(this.originPage);
        
        EDetailerPage.addActivateFunction(this.activate, { context : this });
        this.controls.xRayButton.bind(EDetailerPage.UIEvents.CLICK, { eDetailerPage : this }, this.nextPage);
        this.controls.continueButton.bind(EDetailerPage.UIEvents.CLICK, { eDetailerPage : this }, this.nextPage);
    },
    activate        : function() {
        var snapToPage = this.landmarksFromLatestSelectionPageData(["mod1_4_no", "mod1_7_no", "mod1_9_no", "mod1_9_yes"]);
        this.originPage = snapToPage;
        setTimeout(function() { thisEdetailerPage.animateTimeline(snapToPage); }, 1000);
    },
    snapTimeline      : function(originPage) {
        var originPage = originPage || this.originPage,
            timeline = this.controls.timeline,
            ekgBackground = this.controls.ekgBackground,
            snapCoordinates = [0,0];

        switch (originPage) {
            case "mod1_4_no":
                snapCoordinates = this.landmarks.day1;
                break;
            case "mod1_7_no":
                snapCoordinates = this.landmarks.day6;
                break;
            case "mod1_9_no":
                snapCoordinates = this.landmarks.day6;
                break;
            case "mod1_9_yes":
                snapCoordinates = this.landmarks.day6;
                break;
        }

        timeline.css({ left : snapCoordinates[0] + 'px' });
        ekgBackground.css({ width : snapCoordinates[1] + 'px' });
    },
    animateTimeline   : function(originPage) {
        var originPage = originPage || false,
            timeline = this.controls.timeline,
            ekgBackground = this.controls.ekgBackground,
            snapCoordinates = [0,0];

        switch (originPage) {
            case "mod1_4_no":
                snapCoordinates = this.landmarks.day4;
                break;
            case "mod1_7_no":
                snapCoordinates = this.landmarks.day13;
                break;
            case "mod1_9_no":
                snapCoordinates = this.landmarks.day13;
                break;
            case "mod1_9_yes":
                snapCoordinates = this.landmarks.day13;
                break;
        }

        timeline.animate(
            { left : snapCoordinates[0] + 'px' },
            {
                easeing: this.ANIMATION_EASING,
                duration: this.ANIMATION_DURATION
            }
        );
        ekgBackground.animate(
            { width : snapCoordinates[1] + 'px' },
            {
                easeing: this.ANIMATION_EASING,
                duration: this.ANIMATION_DURATION
            }
        );
        
        if (originPage === "mod1_4_no") {
            timeline.animate(
                { left : this.landmarks.day6[0] + 'px' },
                {
                    easeing: this.ANIMATION_EASING,
                    duration: this.ANIMATION_DURATION
                }
            );
            ekgBackground.animate(
                { width : this.landmarks.day6[1] + 'px' },
                {
                    easeing: this.ANIMATION_EASING,
                    duration: this.ANIMATION_DURATION
                }
            );
        }
    },
    landmarksFromLatestSelectionPageData  : function(selectionPageIds) {
        var EDetailerPage = com.ward6.EDetailerPage,
            selectedPage = false,
            latestSelectionTime = 0,
            pageCount = 0;
            
        if (typeof(selectionPageIds.length) !== "number")
            return false
        
        pageCount = selectionPageIds.length;
        while (pageCount--) {
            var thisPageId = selectionPageIds[pageCount],
                thisPage = EDetailerPage.getPageData(thisPageId);
            
            if (typeof(thisPage) === "object")
                if (typeof(thisPage.lastTallyTime) !== "undefined")
                    if (thisPage.lastTallyTime >= latestSelectionTime) {
                        selectedPage = thisPageId;
                        latestSelectionTime = thisPage.lastTallyTime;
                    }
        }

        return selectedPage;
    },
    nextPage        : function(clickEvent) {
        var _this = clickEvent.data.eDetailerPage;
            nextPageUrl = _this.NEXT_PAGE_URL;

        if (_this.originPage === "mod1_4_no")
            nextPageUrl = "amb_mod1_5.html";
            
        if (com.ward6.EDetailerPage.isApp)
            nextPageUrl = 'app://page//' + nextPageUrl;
            
        window.location = nextPageUrl;
    }
};