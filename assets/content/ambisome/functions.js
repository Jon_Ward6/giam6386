
$(document).ready(function() {

	var bindMoveEvent = ("ontouchmove" in document.documentElement)? "touchmove" : "mousemove",
        bindClickEvent = ("ontouchend" in document.documentElement)? "touchend" : "mouseup",
        profileBar = false;

    // Awesome one-liner for turning it on (the switch).
	$(".switch").bind(bindClickEvent, function(clickEvent) { $(this).toggleClass("noPosition"); });
	
	$(".disabled").bind(bindClickEvent, function(clickEvent) { return false; });
	
	window.toggleGuidelines = function(isVisible) {
        var EDetailerPage = com.ward6.EDetailerPage || { UIEvents : { CLICK : "click" } },
            guidelinesClose = ($(this).hasClass('.closeButton'))? $(this) : $('.guideLines .closeButton'),
        	guidelinesButton = $(".referGuidelines"),
        	guidelines = (typeof(isVisible) === "boolean")? $('.guideLines') :
        	   (typeof(isVisible.data.targetPane) !== "undefined")? isVisible.data.targetPane
        	       : ($(this).hasClass('closeButton'))? $(this).parents('div.guideLines')
        	           : $('.guideLines'),
            isVisible = (typeof(isVisible) === "boolean")? isVisible :
                isVisible.data.visibility || false;

		// Animate the guidleines button;
        guidelinesButton.css('display', 'block');
		guidelinesButton.stop(true,false);
    	guidelinesButton.animate(
            {
                top: (isVisible)? '768px' : '700px',
                easing: 'easeInOutQuad'
            },
            {
                duration: 800,
                complete: function() {
                    if (isVisible) $(this).css('display', 'none');
                }
            }
        );

        // Animate the guidelines themselves.
        guidelines.stop(true, false);
        if (isVisible) {
            guidelines.css('display', 'block');
            guidelines.animate(
                { top: '0px' },
                1000
            );
            guidelinesButton.unbind(EDetailerPage.UIEvents.CLICK, window.toggleGuidelines);
        } else {
            guidelines.animate(
                { top: '768px' },
                {
                    duration : 1000,
                    complete : function() {
                        guidelines.css('display', 'none');
                    }
                }
            );
            guidelinesButton.bind(EDetailerPage.UIEvents.CLICK, { visibility : true }, window.toggleGuidelines);
        }
    };

    toggleGuidelines(false);
    $('.guideLines .closeButton').bind(com.ward6.EDetailerPage.UIEvents.CLICK, { visibility : false }, window.toggleGuidelines);

	// pie chart from case review 2-b

	$('#aspergillusLink, #aspergillus').bind(bindClickEvent, function() { 
		//$('.guideLines#aspergillusInfo').addClass('active');
	});
	
	$('#aspergillusLink, #aspergillus').bind(bindClickEvent,
	   {
	       visibility : true,
	       targetPane : $('#aspergillusInfo')
	   },
	   toggleGuidelines
    );
        	
    $('#candidaLink, #candida').bind(bindClickEvent,
	   {
	       visibility : true,
	       targetPane : $('#candidaInfo')
	   },
	   toggleGuidelines
    );

	$('#zygoLink, #zygo').bind(bindClickEvent,
	   {
	       visibility : true,
	       targetPane : $('#zygoInfo')
	   },
	   toggleGuidelines
    );

	$('#candidaLink, #candida').bind(bindClickEvent, function() { 
		$('.guideLines#candidaInfo').addClass('active');
	});
	
	$('#zygoLink, #zygo').bind(bindClickEvent, function() { 
		$('.guideLines#zygoInfo').addClass('active');
	});
	
	
	
		
	function updateSlider() {
		activeIncrement = $(".range").val();
		$(".rangeValue > div").removeClass('active');
		$(".rangeValue > div:nth-child(" + activeIncrement + ")")
			.addClass('active');
	}
	
	$(".slider").bind(bindMoveEvent, function() { updateSlider(); });	
	$(".slider").bind(bindClickEvent, function() { updateSlider(); });

	
	
	// show profile bar
	// showing on click/touch for now, maybe change later to allow sliding
	
	$('#profileBar').bind(bindClickEvent, function() {
	   var _profileBar = $(this),
	       active = _profileBar.hasClass('active')? true : false;

		_profileBar.toggleClass('active');
		profileBar = !active;
	});
	
	$('#profileBar').find('a.button').bind(bindClickEvent,
	   function(e) {
	       if (profileBar)
	           $('#profileBar').trigger(bindClickEvent);
	   }
	);
	
	// Testing (Disables the x-ray button in the profile).
	// $('#profileBar').find('a.button').click(function(e){console.log("clicked xray button"); return false;});

			
	// for showing and hdiing definitions (glossary)
	
		
	$('dt').bind(bindClickEvent, function() {
		
		var thisDefinition = $(this).text();
		
		
		function removeH3() {
			$('dd h3').remove();
		}
			
		
		$(this).siblings('dt').removeClass('active');
		$(this).addClass('active');
		
			
		$('dd').stop(true,true).slideUp(400);
		setTimeout(removeH3, 400);
		
		$(this).next('dd').stop(true,true).delay(600).slideDown(800);
		
		setTimeout(setH3, 410);
		
		function setH3() {
			$('dt.active').next('dd').prepend('<h3 class="caseReview">' + thisDefinition + '</h3>');
		}
		
	});



	$('.selecTable tr').bind(bindClickEvent, function() {
		$(this).siblings().removeClass('activeRow');
		$(this).toggleClass('activeRow');		
	});
	

    // Added 19/11/2012 
    $('div#toggleBar').bind(EDetailerPage.UIEvents.UP,
        function(jqEvent) 
        {
            var toggleBar = $(this),
                basePanel = $('#baseInfoPanel');
            
            //basePanel.toggleClass('open');
            toggleBar.toggleClass('open');
            
            if (toggleBar.hasClass('open')){
                toggleBar.html("Close");
				$('video').removeAttr('controls');
				$('#baseInfoPanel #contentBar').slideDown(500);
			}else{
                toggleBar.html("Show Information");
				$('#baseInfoPanel #contentBar').slideUp(500);
				$('video').attr('controls', true);
			}
        }
    );
	
});