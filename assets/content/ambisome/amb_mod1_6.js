/**
 * Ambisome eDetailer Page 6
 *
 * GIAM5571 - Ward6
 * 
 * @author Kashi Samaraweera
 * @copyright Ward6
 * @since May 7th 2012
 */

$(document).ready(function() {
	if (typeof(com.ward6.EDetailerPage) === "undefined")
        return false;
    
    var EDetailerPage = com.ward6.EDetailerPage;
    EDetailerPage.setModuleXML('references.xml');
    thisEdetailerPage.init();
});

var thisEdetailerPage = {
    DEBUG_MODE      : true && com.ward6.EDetailerPage.DEBUG_MODE,
    PAGE_ID         : "mod1_6",
    ANIMATION_DURATION    : 1000,
    ANIMATION_EASING      : "easeOutSine",
    controls        : {
        barGraphs   : false,
        barLabels   : false
    },
    tally           : [
        // SAMPLE DATA... overwritten if real data exists (or saved as the basis if no data exists).
        0,
        0,
        0
    ],
    pageToggle      : true,
    tallyTotal      : 0,
    selectedIndex   : 0,
    currentGraphPercents    :   [0, 0, 0],
    graphsActivated : [false, false, false],
    init            : function() {
        var EDetailerPage = com.ward6.EDetailerPage;

        EDetailerPage.setPageId(this.PAGE_ID);

        this.controls = {
            barGraphs       : $("div.graph100").find("div.bar"),
            barLabels       : $("div.graph100").find("div.percentValue").children('meter'),
            barLabelSelect  : $("div.graph100").find("div.percentValue").children('span'),
            yesNo           : $("span.switch"),
            submitBtn       : $("input[type='button']")
        };

        EDetailerPage.addActivateFunction(this.activate, { context : this });
        this.controls.barGraphs.css({width:'0px'});
        this.controls.barLabels.html('0%');

        this.controls.barLabelSelect.css({ opacity : 0 });
        this.controls.yesNo.bind(EDetailerPage.UIEvents.CLICK, { thisEdetailerPage : this }, this.toggleYesNo);
        this.controls.submitBtn.bind(EDetailerPage.UIEvents.CLICK, { thisEdetailerPage : this }, this.nextPage);
    },
    activate        : function() {
        var EDetailerPage = com.ward6.EDetailerPage,
            storedTallyPage = EDetailerPage.getPageData("mod1_6"),
            storedSelectionPage = EDetailerPage.getPageData("mod1_5"),
            newlastTallyTime = new Date().getTime();

        for (i = 2; i--;) this.controls.yesNo.trigger(EDetailerPage.UIEvents.CLICK);

        if (this.DEBUG_MODE) console.log("storedTally: ", storedTallyPage, "\nstoredSelectionPage: ", storedSelectionPage);
        if (storedTallyPage)
            this.tally = storedTallyPage.tally;
        
        if (storedSelectionPage) {
            this.selectedIndex = parseInt(storedSelectionPage.selected);

            // Add to the tally IFF the lastSaveTime > lastTallyTime
            if (typeof(storedTallyPage.lastTallyTime) === "undefined"
                && typeof(storedSelectionPage.lastSaveTime) !== "undefined") {
                // First time this page is loaded before localStorage is cleared.
                if (this.DEBUG_MODE) console.log("First time");
                this.tally[this.selectedIndex] = this.tally[this.selectedIndex] + 1;

            } else if ((typeof(storedSelectionPage.lastSaveTime) !== "undefined"
                && typeof(storedTallyPage.lastTallyTime) !== "undefined")
                && storedSelectionPage.lastSaveTime > storedTallyPage.lastTallyTime) {
                if (this.DEBUG_MODE) console.log("Adding to tally");
                // Typical run - new estimate submitted from previous page.
                if (this.DEBUG_MODE) console.log("Updating tally with new submission");
                    this.tally[this.selectedIndex] = this.tally[this.selectedIndex] + 1;
                    
            } else if ((typeof(storedSelectionPage.lastSaveTime) !== "undefined"
                && typeof(storedTallyPage.lastTallyTime) !== "undefined")
                && storedSelectionPage.lastSaveTime <= storedTallyPage.lastTallyTime) {
                // This page has been loaded again after a typical run.
                if (this.DEBUG_MODE) console.log("Repeat tally");
                newlastTallyTime = storedTallyPage.lastTallyTime;
            }
            
            EDetailerPage.setPageData(
                {
                    tally : this.tally,
                    lastTallyTime : newlastTallyTime
                },
                true
            );
        }

        this.updateGraphs(this.tally);
    },

    updateGraphs    : function(tally) {
        var currentGraphPercents = [],
            finalGraphPercents = [],
            tallyTotal = 0;
        
        // Get the current values of the graph.
        this.controls.barLabels.each(
            function (index, barLabel) {
                currentGraphPercents.push(parseInt($(barLabel).text().replace(/[^0-9.]/g,'')));
            }
        );
        this.currentGraphPercents = currentGraphPercents;

        if (this.DEBUG_MODE) console.log("Measuring current bar amounts: ", currentGraphPercents);
        if (this.DEBUG_MODE) console.log("Tally: ", tally);
        
        for (graphValues in tally) {
            tallyTotal += parseInt(tally[graphValues]);
        }
        
        // Prevent divide by zero for an empty tally set.
        this.tallyTotal = (tallyTotal === 0)? 1 : tallyTotal;
        
        if (this.DEBUG_MODE) console.log("Total tally amount: " + tallyTotal);

        this.controls.barLabelSelect.stop(true, false).animate( { opacity : 0 }, this.ANIMATION_DURATION / 2);        
        this.animateGraph(0);
    },
    animateGraph    : function(graphIndex) {
        var barGraph = this.controls.barGraphs.eq(graphIndex),
            barWrapper = barGraph.parents('div.barWrapper'),
            _this = this;

        if (this.DEBUG_MODE) console.log("Animating graph: ", graphIndex);
        this.graphsActivated[graphIndex] = true;

        if (this.DEBUG_MODE) console.log("Selected: ", graphIndex === this.selectedIndex);

        if (graphIndex === this.selectedIndex) {
            barWrapper.addClass('userBar');
        } else {
            barWrapper.removeClass('userBar');
        }

        barGraph.stop(true, false).animate(
            { width   :   ((this.tally[graphIndex] / this.tallyTotal) * 500) + 'px' },
            {
                step        : function(stepValue, stepData) {
                    var stepPercent = stepData.pos,
                        barPercent = (_this.tally[graphIndex] / _this.tallyTotal) * 100,
                        initialPercentText = _this.currentGraphPercents[graphIndex],
                        percentText = Math.round(initialPercentText + (barPercent - initialPercentText) * stepPercent),
                        barLabel = _this.controls.barLabels.eq(graphIndex);

                    barLabel.html(percentText + '%');

                    if (stepPercent > .3
                        && _this.graphsActivated[graphIndex + 1] !== "undefined"
                        && _this.graphsActivated[graphIndex + 1] === false)
                        _this.animateGraph(graphIndex + 1);
                },
                complete    : function() {
                    _this.graphsActivated[graphIndex] = false;
                    if (graphIndex === _this.graphsActivated.length - 1)
                        _this.controls.barLabelSelect.eq(_this.selectedIndex).stop(true, false).animate(
                            { opacity : "1" },
                            { 
                                duration : _this.ANIMATION_DURATION / 2,
                                easing   : _this.ANIMATION_EASING
                            }
                        );
                },
                easing      : this.ANIMATION_EASING,
                duration    : this.ANIMATION_DURATION
            }
        );
    },
    toggleYesNo     : function(yesNoToggle) {
        var EDetailerPage = com.ward6.EDetailerPage,
            _this = yesNoToggle.data.thisEdetailerPage,
            toggle = $(this),
            yesNo = (toggle.hasClass("noPosition"))? 0 : 1,
            saveTime = new Date().getTime();
        
        _this.pageToggle = yesNo;
        EDetailerPage.setPageData(
            {
                selection       : yesNo,
                lastSaveTime    : saveTime
            }
        );
        if (_this.DEBUG_MODE) console.log("Toggle: ", yesNo);
    },
    nextPage        : function(clickEvent) {
        var _this = clickEvent.data.thisEdetailerPage,
            isApp = com.ward6.EDetailerPage.isApp,
            nextPageURL = '';
        
        if (_this.pageToggle) // If the user's response is 'yes'
            nextPageURL = 'amb_mod1_7_yes.html';
        else
            nextPageURL = 'amb_mod1_7_no.html';
            
        if (isApp)
            nextPageURL = 'app://page//' + nextPageURL;
            
        window.location = nextPageURL;
    }
};