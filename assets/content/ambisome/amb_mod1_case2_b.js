/**
 * Ambisome eDetailer Page case2-b
 *
 * GIAM5571 - Ward6
 * 
 * @author Kashi Samaraweera
 * @copyright Ward6
 * @since May 1st 2012
 */

$(document).ready(function() {
	if (typeof(com.ward6.EDetailerPage) === "undefined")
        return false;
    
    var EDetailerPage = com.ward6.EDetailerPage;
    EDetailerPage.setPageId(thisEdetailerPage.PAGE_ID);    
    EDetailerPage.setModuleXML('references.xml');
});

var thisEdetailerPage = {
    DEBUG_MODE      : true && com.ward6.EDetailerPage.DEBUG_MODE,
    PAGE_ID         : 'mod1_case2_b'
};