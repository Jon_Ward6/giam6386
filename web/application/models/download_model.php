<?php

class Download_model extends CI_Model {
	
	private $TBL_USER;
	private $TBL_PRACTICE;
	private $TBL_CALLS;
	private $TBL_CALLMODULE;
	private $TBL_MODULE;
	private $TBL_REP;

    function __construct()
    {
        parent::__construct();
        
        $this->TBL_PRACTICE = 'practice';
        $this->TBL_USER = 'user';
        $this->TBL_CALLS = 'calls';
		$this->TBL_CALLMODULE = 'call_module';
		$this->TBL_MODULE = 'module';
		$this->TBL_REP = 'rep';
    }
	

	public function users(){
		$header = "Client Name,Practice Name,Address,Email,Date Creation, ";
		$header .= "\r\n";
		
		
		//$sql  = sprintf("SELECT * FROM $this->TBL_USER, $this->TBL_PRACTICE WHERE $this->TBL_USER.practice_uid=$this->TBL_PRACTICE.practice_uid");
		
		session_start();
		$access_level = $_SESSION['access_level']>0? $_SESSION['access_level'] : 0;
			
		$sql  = sprintf("SELECT * FROM $this->TBL_USER, $this->TBL_PRACTICE, $this->TBL_REP  
						WHERE $this->TBL_USER.practice_uid=$this->TBL_PRACTICE.practice_uid AND $this->TBL_USER.rep_uid=$this->TBL_REP.rep_uid AND $this->TBL_REP.access_level<=$access_level");
		$query = $this->db->query($sql);
		
		$msg = '';
		foreach ($query->result() as $row)
		{
			$clientname = $row->firstname . ' ' . $row->lastname;
			$email = $row->email;
			$name = $row->name;
			$date_creation = $row->date_creation;
			$date_update = $row->date_update;
		
			$msg .= '"' . $clientname . '",';
			$msg .= '"' . $name . '",';
			$msg .= '"' . "{$row->street}, {$row->suburb}, {$row->postcode}" . '",';
			$msg .= '"' . $email . '",';
			$msg .= '"' . $date_creation . '",';
			$msg .= "\r\n";
		}
	
		return  $header . $msg;
	}
	
	
	public function calls(){
		$header = "Rep Name,Client Name,Practice Name,Modules,Time Spent,Call Time, ";
		$header .= "\r\n";
		
		//$sql  = sprintf("SELECT * FROM $this->TBL_CALLS WHERE id>0 ORDER By rep_uid");	
		session_start();
		$access_level = $_SESSION['access_level']>0? $_SESSION['access_level'] : 0;
		$sql  = sprintf("SELECT * FROM $this->TBL_CALLS, $this->TBL_REP WHERE $this->TBL_CALLS.rep_uid=$this->TBL_REP.rep_uid AND $this->TBL_REP.access_level<=$access_level ORDER By $this->TBL_CALLS.rep_uid");
		$query = $this->db->query($sql);
		
		$userArr = array();
		$msg = '';
		foreach ($query->result() as $row)
		{
			$userArr = $this->getCallUser($row->user_uid);
			$modules = $this->getCallModules($row->call_uid);
			$modules = strlen($modules) > 0? $modules : '&nbsp;';
			$rep = $this->getCallRep($row->rep_uid);
			
			if(isset($userArr['firstname']) && $userArr['lastname'] && $userArr['name']){
				$fullname = $userArr['firstname'] . ' '. $userArr['lastname'] .'",';
				$name = $userArr['name'];
			}
			$msg .= '"' . $rep . '",';
			$msg .= '"' . $fullname;
			$msg .= '"' . $name . '",';
			$msg .= '"' . $modules . '",';
			$msg .= '"' . date('i:s', $row->time_spent) . '",';
			$msg .= '"' . $row->end_time . '",';
			$msg .= "\r\n";
		}
	
		return  $header . $msg;
	}
	
	
	private function getCallUser($user_uid){
		//$userArr = array();
	
		$sql  = sprintf("SELECT * FROM $this->TBL_USER, $this->TBL_PRACTICE WHERE $this->TBL_USER.practice_uid=$this->TBL_PRACTICE.practice_uid AND $this->TBL_USER.user_uid='$user_uid' LIMIT 1");	
		$query = $this->db->query($sql);
		/*
		foreach ($query->result() as $row){
			$userArr['firstname'] = $row->firstname;
			$userArr['lastname'] = $row->lastname;
			$userArr['name'] = $row->name;
		
		}
		*/
		//$row = $query->row_array();
		//print_r($row['firstname']);
	
		return $query->row_array();
		
		
	}
	
	
	private function getCallRep($rep_uid){
		
		//rewr-bgtr-wert-5hjo-qwww
		$sql = sprintf("SELECT * FROM $this->TBL_REP WHERE rep_uid='$rep_uid'");	
	
		$query = $this->db->query($sql);
		foreach ($query->result() as $row){
			return $row->username;
		}
		
		return '';
	}
	
	
	private function getCallModules($call_uid){
		
		$sql  = sprintf("SELECT * FROM $this->TBL_CALLMODULE,$this->TBL_MODULE WHERE $this->TBL_CALLMODULE.call_uid='$call_uid' AND $this->TBL_CALLMODULE.module_uid=$this->TBL_MODULE.module_uid");	
		$query = $this->db->query($sql);
		
		$modules = '';
		foreach ($query->result() as $row){
			$time_spent = date('i:s', $row->time_spent);
			$modules .= $row->title . "($time_spent), ";
		}
		
		return $modules;
	}


	
		
		
	
}