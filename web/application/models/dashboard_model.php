<?php

class Dashboard_model extends CI_Model {
	
	private $TBL_USER;
	private $TBL_PRACTICE;
	private $TBL_CALLS;
	private $TBL_CALLMODULE;
	private $TBL_MODULE;
	private $TBL_REP;
	private $TBL_CALLPAGE;
	private $TBL_KEYCODE;

    function __construct()
    {
        parent::__construct();
        
        $this->TBL_PRACTICE = 'practice';
        $this->TBL_USER = 'user';
        $this->TBL_CALLS = 'calls';
		$this->TBL_CALLMODULE = 'call_module';
		$this->TBL_MODULE = 'module';
		$this->TBL_REP = 'rep';
		$this->TBL_CALLPAGE = 'call_page';
		$this->TBL_KEYCODE = 'admin_keycode';
    }
	

	public function users(){

		//$query = $this->db->query("SELECT * 
				//FROM $this->TBL_USER, $this->TBL_PRACTICE
 				//WHERE $this->TBL_USER.practice_uid=$this->TBL_PRACTICE.practice_uid");
				
		//CHANGED:: enforce the access level.	
		$access_level = $_SESSION['access_level']>0? $_SESSION['access_level'] : 0;
		
		/*	
		$query = $this->db->query("SELECT * 
				FROM $this->TBL_USER, $this->TBL_PRACTICE, $this->TBL_REP 
 				WHERE $this->TBL_USER.practice_uid=$this->TBL_PRACTICE.practice_uid 
				AND $this->TBL_USER.rep_uid= $this->TBL_REP.rep_uid AND $this->TBL_REP.access_level<=$access_level ORDER BY $this->TBL_USER.date_creation DESC");
		*/
		
		$query = $this->db->query("SELECT $this->TBL_USER.firstname, $this->TBL_USER.lastname, $this->TBL_PRACTICE.name, $this->TBL_PRACTICE.street,$this->TBL_PRACTICE.suburb,$this->TBL_PRACTICE.postcode, $this->TBL_USER.email,$this->TBL_USER.date_creation 
				FROM $this->TBL_USER, $this->TBL_PRACTICE, $this->TBL_REP 
 				WHERE $this->TBL_USER.practice_uid=$this->TBL_PRACTICE.practice_uid 
				AND $this->TBL_USER.rep_uid= $this->TBL_REP.rep_uid AND $this->TBL_REP.access_level<=$access_level ORDER BY $this->TBL_USER.date_creation DESC");
		
		
		$dataArr = Array();
		
		
		if ($query->num_rows() > 0)
		{
		   foreach ($query->result() as $row)
		   {
			   
			$email = $this->filterEmail($row->email);
			   
			   
		   	$arr = Array($row->firstname . ' '. $row->lastname, $row->name, "{$row->street} {$row->suburb} {$row->postcode}", $email, $row->date_creation);
		   	array_push($dataArr, $arr);
		   	/*
		   		$arr = Array();
		      	$arr['client_name'] = $row->firstname . ' '. $row->lastname;
		   		$arr['practice_name'] = $row->name;
		    	$arr['address'] = "{$row->street} {$row->suburb} {$row->postcode}";
		    	$arr['email'] = $row->email;
		    	$arr['date_creation'] = $row->date_creation;
		    	
		    	array_push($dataArr, $arr);
		    */
		   }
		}
		
		
		
		
		return $dataArr;
	}
	
	
	public function calls(){
		$dataArr = Array();
		
		$TBL_CALLS = $this->TBL_CALLS;
		$TBL_REP = $this->TBL_REP;
		$access_level = $_SESSION['access_level']>0? $_SESSION['access_level'] : 0;
		
		//$query = $this->db->query("SELECT * FROM {$this->TBL_CALLS} WHERE id>0 ORDER BY end_time DESC");
		
		//CHANGED:: enforce the access level.
		$query = $this->db->query("SELECT * FROM $TBL_CALLS, $TBL_REP WHERE $TBL_REP.rep_uid=$TBL_CALLS.rep_uid AND $TBL_REP.access_level<=$access_level ORDER BY $TBL_CALLS.end_time DESC");
		
		if ($query->num_rows() > 0)
		{
		   foreach ($query->result() as $row)
		   {
		   		$userArr = $this->getCallUser($row->user_uid);
		   		$pages = $this->callPageNum($row->call_uid);
		   		
		   		$client_name = (isset($userArr['firstname']))? $userArr['firstname'] . ' ' . $userArr['lastname']  : '';
		   		$practice_name = (isset($userArr['name']))? $userArr['name'] : '';
	
		   		
		   		$arr = Array();
		   		$arr[0] = $this->getCallRep($row->rep_uid);
		   		$arr[1] = $row->groupcall_uid? 'Group Call': '';
		   		$arr[2] = $client_name;
		   		$arr[3] = $practice_name;
		   		$arr[4] = $this->getCallModules($row->call_uid);
		   		$arr[5] = $pages>0? "<a href='". $this->config->item('base_url').'/index.php/dashboard/pages/'.$row->call_uid ."'>" . $pages . "</a>" : $pages;
		   		$arr[6] = date('i:s', $row->time_spent);
		   		$arr[7] = $row->end_time;
		   		
		   		array_push($dataArr, $arr);
		   		
		   		//echo "group:".$row->groupcall_uid;
		   }
		   
		}
		
		
		return $dataArr;
	
	}
	
	public function reps(){
		
		$access_level = $_SESSION['access_level']>0? $_SESSION['access_level'] : 0;
		
		$query = $this->db->query("SELECT * FROM $this->TBL_REP WHERE access_level<=$access_level ORDER BY id DESC");
		
		$dataArr = Array();
		
		
		if ($query->num_rows() > 0)
		{
		   foreach ($query->result() as $row)
		   {
		   	/*
		   		$arr = Array();
		      	$arr['username'] = $row->username;
		   		$arr['email'] = $row->email;
		    	$arr['access_level'] = $row->access_level;
		    	
		    	array_push($dataArr, $arr);
		    */
			
			
			$email = $this->filterEmail($row->email);
		   	
		   	$arr = Array();
		      	$arr[0] = $row->username;
		   		$arr[1] = $email;
		    	$arr[2] = $row->access_level;
		    	$arr[3] = '<a class="center" href="'.$this->config->item('base_url').'/index.php/dashboard/reps_edit/'.$row->id.'">Edit</a>';
		    	
		    	array_push($dataArr, $arr);
		   	
		   }
		}
		
		
		return $dataArr;
		
	}
	
	
	public function rep_by_id($id){
		$query = $this->db->query("SELECT * FROM $this->TBL_REP WHERE id=$id LIMIT 1");
		
		return $query->row();
		
	}
	
	public function rep_update($id, $username, $email, $access_level){
		$data = array(
               'username' => $username,
               'email' => $email,
			   'access_level' => $access_level
            );

		$this->db->where('id', $id);
		$this->db->update($this->TBL_REP, $data);
	}
	
	public function rep_add($username, $email, $access_level){
		$this->db->set('username', $username);
		$this->db->set('email', $email);
		$this->db->set('access_level', $access_level);
		//
		$this->db->insert($this->TBL_REP); 
	}
	
	public function rep_delete($id){
		$this->db->delete($this->TBL_REP, array('id' => $id)); 
	}
	
	
	//Pages
	public function pages($call_uid){
		$dataArr = Array();
		
		
		$sql = sprintf("SELECT * FROM {$this->TBL_CALLPAGE} WHERE call_uid='$call_uid' GROUP BY page_uid");
		$query = $this->db->query($sql);
		
		
		foreach ($query->result() as $row)
		{
		   		
		   		$arr = Array();
				$arr[0] = $row->module_uid;
		   		$arr[1] = $row->page_uid;
		   		$arr[2] = date('i:s', $row->time_spent);
				
				
				
				//$pagePos = strrpos($row->module_uid, "casestudy");
				//$qickPagePos = strrpos($row->module_uid, "quickaccess");
				/*
				if($pagePos===0){
					$sql2 = sprintf("SELECT * FROM page WHERE page_uid='{$row->page_uid}' LIMIT 1");
					$query2 = $this->db->query($sql2);
					if ($query2->num_rows() > 0){
						$row2 = $query2->row(0);
						$arr[0] = stripslashes($row2->page_name);
					}
					
				}
				*/
				if($row->module_uid=="quickaccess"){
					$sql3 = sprintf("SELECT * FROM page_quickaccess WHERE page_uid='{$row->page_uid}' LIMIT 1");
					$query3 = $this->db->query($sql3);
					if ($query3->num_rows() > 0){
						$row3 = $query3->row(0);
						$arr[1] = stripslashes($row3->page_name);
					}
					
				}else{
					
					$sql2 = sprintf("SELECT * FROM page WHERE page_uid='{$row->page_uid}' LIMIT 1");
					$query2 = $this->db->query($sql2);
					if ($query2->num_rows() > 0){
						$row2 = $query2->row(0);
						$arr[1] = stripslashes($row2->page_name);
					}
					
				}
				
		   		
		   		array_push($dataArr, $arr);
		   		
		}
		
		return $dataArr;
	}
	
	
	//keycode ------------------------------------------------------------------------------------------------------------------------
	
	public function getKeyCode(){
		//CHANGED:: just one record only. update it everyday with new code.
		$newCode = $this->genRandomString();
		
		//check any record
		$query1 = $this->db->query("SELECT * FROM {$this->TBL_KEYCODE} WHERE status='yes' LIMIT 1");
		if (!$query1->num_rows() > 0)
		{
			$query2 = $this->db->query("INSERT INTO {$this->TBL_KEYCODE} (keycode,status,ts_created) VALUES ('$newCode', 'yes', NOW())");
		}else{
			
			$query3 = $this->db->query("UPDATE {$this->TBL_KEYCODE} SET keycode='$newCode', status='yes', ts_created=NOW() WHERE ts_created < NOW() - INTERVAL 1 DAY");
		
		}
		
		
		
		//get the updated data now
		$data = array();
	
		$query4 = $this->db->query("SELECT * FROM {$this->TBL_KEYCODE} WHERE status='yes' LIMIT 1");
		if ($query4->num_rows() > 0)
		{
		   foreach ($query4->result() as $row)
		   {
			   $data['keycode'] = $row->keycode;
			   $data['valid'] = date("Y-m-d H:i:s", strtotime($row->ts_created) + 24 * (60 * 60));
			   return $data;
		   }
		}
		
		return $data;
		
		/*
		//1. test the 24 hrs keycode validation
		$query = $this->db->query("UPDATE {$this->TBL_KEYCODE} SET status='no' WHERE ts_created < NOW() - INTERVAL 1 DAY");
		$data = array();
		
		//2. check db for any valid keycode
		$query2 = $this->db->query("SELECT * FROM {$this->TBL_KEYCODE} WHERE status='yes' LIMIT 1");
		if ($query2->num_rows() > 0)
		{
		   foreach ($query2->result() as $row)
		   {
			   $data['keycode'] = $row->keycode;
			   $data['valid'] = date("Y-m-d H:i:s", strtotime($row->ts_created) + 24 * (60 * 60));
			   return $data;
			   //return $row->keycode;
		   }
		}
		
		//3. or get the valid code
		$newCode = $this->genRandomString();
		$query = $this->db->query("INSERT INTO {$this->TBL_KEYCODE} (keycode,status,ts_created) VALUES ('$newCode','yes',NOW())");
		
		$data['keycode'] = $newCode;
	    $data['valid'] = date("Y-m-d H:i:s", strtotime(time()) + 24 * (60 * 60));
		return $data;   
		*/
	}
	
	
	
	public function emailKeycode($email, $from, $subject, $name,$keycode, $valid){
		//testing - should change back to $email
		$to = 'amBisome@w6digital.com.au';//$email;
		$subject = $subject;
		$headers = "From: AmBisome<$from>\n";
		$headers .= "Reply-To: AmBisome<$from>\n";
		$headers .= "Return-Path: AmBisome<$from>\n";
		$headers .= "Content-type: text/html\n";
		
		$message = <<<EOF
<html>
	<body>
		Hi {$name},
		<br/><br/>
		Please use the following keycode to re-activate the 'AmBisome' App:
		<br/><br/>
		<b>keycode:</b> {$keycode} <br/>
		<b>valid to:</b> {$valid} <br/>
		<br/>
		
		<br/><br/>
		The Administrator<br/>
		AmBisome
		
	</body>
</html>
EOF;
		

    	// now lets send the email.
    	mail($to, $subject, $message, $headers);
	
	}

	

	//get the reps name from email
	public function searchRepArray($arr, $email){
		foreach ($arr as $obj) {
    		if($obj[1]==$email){
				return $obj[0];
			}
		}
		return false;
	}
	
	
	
	//PRIVATE FUNCTIONS ----------------------------------------------------------------------------------------------------------------
	private function getCallUser($user_uid){
		
		$userArr = array();
		
		$sql  = sprintf("SELECT * FROM $this->TBL_USER, $this->TBL_PRACTICE 
						WHERE $this->TBL_USER.practice_uid=$this->TBL_PRACTICE.practice_uid 
						AND $this->TBL_USER.user_uid='$user_uid' LIMIT 1");	
		
		$query = $this->db->query($sql);
		
		foreach ($query->result() as $row)
		{
			$userArr['firstname'] = $row->firstname;
			$userArr['lastname'] = $row->lastname;
			$userArr['name'] = $row->name;
			
			//return $userArr;
		}
		return $userArr;
	}
	
	
	private function getCallModules($call_uid){
		
		$sql  = sprintf("SELECT * FROM $this->TBL_CALLMODULE,$this->TBL_MODULE 
						 WHERE $this->TBL_CALLMODULE.call_uid='$call_uid' 
						 AND $this->TBL_CALLMODULE.module_uid=$this->TBL_MODULE.module_uid");
			
		$query = $this->db->query($sql);
		
		$modules = '';
		foreach ($query->result() as $row)
		{
			$time_spent = date('i:s', $row->time_spent);
			$modules .= $row->title . "($time_spent), ";
		}
		
		return $modules;
	}
	
	
	private function getCallRep($rep_uid){
		
		//rewr-bgtr-wert-5hjo-qwww
		$sql = sprintf("SELECT * FROM {$this->TBL_REP} WHERE rep_uid='$rep_uid'");
		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0){
			foreach ($query->result() as $row){
				return $row->username;
			}
		
		}
		
		return '';
	}

	private function filterEmail($email){
		$myEmail = $email;
		if(strlen($email)==0) $myEmail = '';
		if(strrpos($email, 'dummyemail')>0) $myEmail = '';
		if(!strrpos($email, '@')) $myEmail = '';
		
		//
		return $myEmail;
		
	}
	
	
	private function callPageNum($call_uid){
		$sql = sprintf("SELECT * FROM {$this->TBL_CALLPAGE} WHERE call_uid='$call_uid'");
		$query = $this->db->query($sql);
		
		return $query->num_rows();
	}
	
	
	private function genRandomString() {
		$length = 10;
		$characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWZYZ"; //"0123456789abcdefghijklmnopqrstuvwzyz";//
		$real_string_legnth = strlen($characters) - 1;
		$string="";
	
		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, $real_string_legnth)];
		}
		
		return $string;
	}
		
		
	
}