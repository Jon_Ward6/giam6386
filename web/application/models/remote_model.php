<?php

class Remote_model extends CI_Model {
	
	private $TBL_USER;
	private $TBL_PRACTICE;
	private $TBL_CALLS;
	private $TBL_CALLMODULE;
	private $TBL_MODULE;
	private $TBL_REP;
	private $TBL_CALLPAGE;
	private $TBL_KEYCODE;

    function __construct()
    {
        parent::__construct();
        
        $this->TBL_PRACTICE = 'practice';
        $this->TBL_USER = 'user';
        $this->TBL_CALLS = 'calls';
		$this->TBL_CALLMODULE = 'call_module';
		$this->TBL_MODULE = 'module';
		$this->TBL_REP = 'rep';
		$this->TBL_CALLPAGE = 'call_page';
		$this->TBL_KEYCODE = 'admin_keycode';
    }
    
    
	//user --------------------------------------------------------------------------------------------
	public function adduser()
	{	
		$new_data = utf8_encode(stripslashes($this->input->post('data')));
		$dataArr = json_decode($new_data, true);
	
			
		$sql='';
			
			
		if(is_array($dataArr) && count($dataArr)>0){
			foreach ($dataArr as $data) {
				//test dupe
				$test_sql = sprintf("SELECT * FROM {$this->TBL_USER} WHERE user_uid='%s'", $data['user_uid']);
				$query = $this->db->query($test_sql);
				if ($query->num_rows() > 0) continue;
					
					
				$sql = sprintf("INSERT INTO {$this->TBL_USER} 
				(user_uid, rep_uid, practice_uid,firstname, lastname, email, status, date_creation, date_update) 
				VALUES ('%s','%s','%s','%s','%s','%s','%s',NOW(),NOW())", 
				$data['user_uid'],$data['rep_uid'],$data['practice_uid'],$data['firstname'],$data['lastname'],$data['email'],'local');
				//
				$this->db->query($sql);
			}
				
		}else{
			return "NO"; 
		}
			
			
		return "YES"; 
	}
	
	
	
	
	
	public function getusers()
	{
		$userArr = array();
			
		$new_data = utf8_encode(stripslashes($this->input->post('data')));
		$dataArr = json_decode($new_data, true);
			
			
		$sql = sprintf("SELECT * FROM {$this->TBL_USER} WHERE rep_uid ='%s'", $dataArr['rep_uid']);
		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0)
		{
   			foreach ($query->result() as $row)
   			{
   				array_push($userArr, array("user_uid"=>$row->user_uid, 
								   		"firstname"=>$row->firstname,
								   		"lastname"=>$row->lastname,
								   		"email"=>$row->email,
								   		"rep_uid"=>$row->rep_uid,
								   		"practice_uid"=>$row->practice_uid,
								   		"status"=>$row->status,
								   		"date_creation"=>$row->date_creation,
								   		"date_update"=>$row->date_update,
								   		"practice_arr"=>$this->get_practice_by_id($row->practice_uid)
				));	
   	
   			}
   			
		}
		
			
		$returnArr = array();
		$returnArr['type'] = 'getusers';
		$returnArr['data'] = $userArr;
			
		return json_encode($returnArr);
	}
	
	
	//practice --------------------------------------------------------------------------------------------
	public function addpractice()
	{
		$new_data = utf8_encode(stripslashes($this->input->post('data')));
		$dataArr = json_decode($new_data, true);
	
		$sql='';
			
			
		if(is_array($dataArr) && count($dataArr)>0){
			foreach ($dataArr as $data) {
				//test
				$test_sql = sprintf("SELECT * FROM {$this->TBL_PRACTICE} WHERE practice_uid='%s'", $data['practice_uid']);
				$query = $this->db->query($test_sql);
				if ($query->num_rows() > 0) continue;
					
					
				$sql = sprintf("INSERT INTO {$this->TBL_PRACTICE} 
				(practice_uid, name, street,suburb, postcode, state, region_code) 
				VALUES ('%s','%s','%s','%s','%s','%s','%s')", 
				$data['practice_uid'],$data['name'],$data['street'],$data['suburb'],$data['postcode'],$data['state'],$data['region_code']);
				//
				$this->db->query($sql);
			}
				
		}else{
			return "NO"; 
		}

			return "YES";
	}
	
	
	public function get_practice_by_id($practice_uid)
	{
		$arr = array();
			
			
		$sql = sprintf("SELECT * FROM {$this->TBL_PRACTICE} WHERE practice_uid ='%s'", $practice_uid);
		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0)
		{
   			foreach ($query->result() as $row)
   			{
				array_push($arr, array("practice_uid"=>$row->practice_uid, 
										   "name"=>$row->name,
										   "street"=>$row->street,
										   "suburb"=>$row->suburb,
										   "postcode"=>$row->postcode,
										   "state"=>$row->state,
										   "region_code"=>$row->region_code
				));
   			}	
		}
			
		return $arr;
	}
	
	
	
	
	//calls --------------------------------------------------------------------------------------------
	public function getcalls()
	{
		$callArr = array();
			
		$new_data = utf8_encode(stripslashes($this->input->post('data')));
		$dataArr = json_decode($new_data, true);
			
			
		$sql = sprintf("SELECT * FROM {$this->TBL_CALLS} WHERE rep_uid ='%s'", $dataArr['rep_uid']);
		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0)
		{
   			foreach ($query->result() as $row)
   			{
				//TODO: need to do the dateTime to string conversion
   				array_push($callArr, array("call_uid"=>$row->call_uid, 
								   		"user_uid"=>$row->user_uid,
								   		"rep_uid"=>$row->rep_uid,
   										"groupcall_uid"=>$row->groupcall_uid,
								   		"start_time"=>$row->start_time,
								   		"end_time"=>$row->end_time,
								   		"time_spent"=>$row->time_spent,
										"callmodules"=>$this->getcallmodules($row->call_uid)
				));	
   	
   			}
   			
		}
		
			
		$returnArr = array();
		$returnArr['type'] = 'getcalls';
		$returnArr['data'] = $callArr;
			
		return json_encode($returnArr);
		
	}
	
	
	public function addcall()
	{
		$new_data = utf8_encode(stripslashes($this->input->post('data')));
		$dataArr = json_decode($new_data, true);
	
		$sql='';
			
			
		if(is_array($dataArr) && count($dataArr)>0){
			foreach ($dataArr as $data) {
				//test
				$test_sql = sprintf("SELECT * FROM {$this->TBL_CALLS} WHERE call_uid='%s' AND user_uid='%s'", $data['call_uid'], $data['user_uid']);
				$query = $this->db->query($test_sql);
				if ($query->num_rows() > 0) continue;
				
				$start_time_str = strtotime($data['start_time']);
				$start_time = date("Y-m-d H:i:s", $start_time_str);
				$end_time_str = strtotime($data['end_time']);
				$end_time = date("Y-m-d H:i:s", $end_time_str);
				
				
				$sql = sprintf("INSERT INTO {$this->TBL_CALLS} 
				(call_uid, user_uid, rep_uid, groupcall_uid, start_time, end_time, time_spent) 
				VALUES ('%s','%s','%s','%s','%s','%s','%d')", 
				$data['call_uid'],$data['user_uid'],$data['rep_uid'],$data['groupcall_uid'],$start_time,$end_time,$data['time_spent']);
				//
				$this->db->query($sql);
			}
				
		}else{
			return "NO"; 
		}
			
			
		return "YES";
		
	}
	
	
	//callmodule - call this internally from 'getcalls'
	public function getcallmodules($call_uid){
		$callModuleArr = array();
			
		$new_data = utf8_encode(stripslashes($this->input->post('data')));
		$dataArr = json_decode($new_data, true);
			
			
		$sql = sprintf("SELECT * FROM {$this->TBL_CALLMODULE} WHERE call_uid ='%s'", $call_uid);
		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0)
		{
   			foreach ($query->result() as $row)
   			{
				//call_uid, module_uid, user_uid, time_spent
   				array_push($callModuleArr, array("call_uid"=>$row->call_uid, 
								   		"module_uid"=>$row->module_uid,
								   		"user_uid"=>$row->user_uid,
								   		"time_spent"=>$row->time_spent
				));	
   	
   			}
   			
		}
		
			
		return $callModuleArr;//$returnArr;
		
	}
	
	
	
	public function addcallmodule()
	{
		$new_data = utf8_encode(stripslashes($this->input->post('data')));
		$dataArr = json_decode($new_data, true);
	
		$sql='';
			
			
		if(is_array($dataArr) && count($dataArr)>0){
			foreach ($dataArr as $data) {
				//test dupe
				$test_sql = sprintf("SELECT * FROM {$this->TBL_CALLMODULE} WHERE module_uid='%s' AND call_uid='%s'", $data['module_uid'], $data['call_uid']);
				$query = $this->db->query($test_sql);
				if ($query->num_rows() > 0) continue;
					
				$sql = sprintf("INSERT INTO {$this->TBL_CALLMODULE} 
				(call_uid, module_uid, user_uid, time_spent) 
				VALUES ('%s','%s','%s','%d')", 
				$data['call_uid'],$data['module_uid'],$data['user_uid'],$data['time_spent']);
				//
				$this->db->query($sql);
			}
				
		}else{
			return "NO"; 
		}
			
			
		return "YES";
	}
	
	//Call Pages ---------------------------------------------------------------------------------------------
	public function getcallpages()//$call_uid
	{
		
		$callPageArr = array();
			
		$new_data = utf8_encode(stripslashes($this->input->post('data')));
		$dataArr = json_decode($new_data, true);
			
		$TBL_CALLS =$this->TBL_CALLS;
		$TBL_CALLPAGE =$this->TBL_CALLPAGE;
		
			
		//$sql = sprintf("SELECT * FROM {$this->TBL_CALLPAGE} WHERE rep_uid ='%s'", $dataArr['rep_uid']);
		$sql = sprintf("SELECT $TBL_CALLPAGE.call_uid, $TBL_CALLPAGE.module_uid, $TBL_CALLPAGE.user_uid, $TBL_CALLPAGE.page_uid,$TBL_CALLPAGE.page_name, $TBL_CALLPAGE.time_spent  
						FROM $TBL_CALLPAGE,$TBL_CALLS 
						WHERE $TBL_CALLS.rep_uid ='%s' AND $TBL_CALLPAGE.call_uid=$TBL_CALLS.call_uid", $dataArr['rep_uid']);
						
		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0)
		{
   			foreach ($query->result() as $row)
   			{
				//call_uid, module_uid, user_uid, time_spent,page_uid
   				array_push($callPageArr, array("call_uid"=>$row->call_uid, 
								   		"module_uid"=>$row->module_uid,
								   		"user_uid"=>$row->user_uid,
								   		"time_spent"=>$row->time_spent,
										"page_name"=>$row->page_name,
   										"page_uid"=>$row->page_uid
				));	
   	
   			}
   			
		}
		$returnArr = array();
		$returnArr['type'] = 'getcallpages';
		$returnArr['data'] = $callPageArr;
			
		return json_encode($returnArr);
			
		//return $callPageArr;
	}
	
	
	public function addcallpage()
	{
		//$new_data = utf8_encode(stripslashes($this->input->post('data')));
		$new_data = utf8_encode($this->input->post('data'));
		$dataArr = json_decode($new_data, true);
	
		$sql='';
			
			
		if(is_array($dataArr) && count($dataArr)>0){
			foreach ($dataArr as $data) {
				//
				if(!$data['page_name']) $data['page_name'] = "";
				$page_name = str_replace("'", "\''", $data['page_name']);
				$page_name = str_replace("\\'", "\'", $data['page_name']);
				
				//test dupe
				$test_sql = sprintf("SELECT * FROM {$this->TBL_CALLPAGE} WHERE call_uid='%s' AND page_uid='%s'", $data['call_uid'], $data['page_uid']);
				$query = $this->db->query($test_sql);
				if ($query->num_rows() > 0) continue;
				

					
				$sql = sprintf("INSERT INTO {$this->TBL_CALLPAGE} 
				(call_uid, module_uid, user_uid, time_spent, page_uid, page_name) 
				VALUES ('%s','%s','%s','%d','%s','%s')", 
				$data['call_uid'],$data['module_uid'],$data['user_uid'],$data['time_spent'], $data['page_uid'], $page_name);
				//
				$this->db->query($sql);
			}
				
		}else{
			return "NO1:".count($dataArr); 
		}
			
			
		return "YES";
		
	}
	
	
	
	
	
	
	
	//Reps ---------------------------------------------------------------------------------------------
	// get one!
	public function getrep()
	{
		$repArr = array();
			
		$new_data = utf8_encode(stripslashes($this->input->post('data')));
		$dataArr = json_decode($new_data, true);
			
			
		$sql = sprintf("SELECT * FROM {$this->TBL_REP} WHERE username ='%s' AND password='%s' LIMIT 1", 
		$dataArr['username'], $dataArr['password']);
			
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
   			foreach ($query->result() as $row)
   			{	
				$repArr['rep_uid'] = $row->rep_uid;
				$repArr['username'] = $row->username;
				$repArr['password'] = $row->password;
				$repArr['email'] = $row->email;
				$repArr['access_level'] = $row->access_level;
   			}
				
		}
			
		$returnArr = array();
		$returnArr['type'] = 'getrep';
		$returnArr['data'] = $repArr;
			
		return json_encode($returnArr);
	}
	
	
	//get all reps
	public function getallrep()
	{
		$repArr = array();
		
		
		$sql = sprintf("SELECT * FROM {$this->TBL_REP} WHERE id>0");
			
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
   			foreach ($query->result() as $row)
   			{	
			
			
				array_push($repArr, array("rep_uid"=>$row->rep_uid, 
								   		"username"=>$row->username,
								   		"password"=>$row->password,
								   		"email"=>$row->email,
								   		"access_level"=>$row->access_level
								   		
				));
			
   			}
				
		}
			
		$returnArr = array();
		$returnArr['type'] = 'getallrep';
		$returnArr['data'] = $repArr;
			
		return json_encode($returnArr);
		
	}
	
	//KeyCode ----------------------------------------------------------------------------------------
	public function getkeycode(){
		
		$new_data = utf8_encode(stripslashes($this->input->post('data')));
		$dataArr = json_decode($new_data, true);
		$returnArr = array();
		$returnArr['type'] = 'getkeycode';
		$returnArr['data'] = 'NO';
		
		//don't care the overdue codes - let the admin to check
		//0. get the data from db
		$query = $this->db->query("SELECT * FROM {$this->TBL_KEYCODE} WHERE keycode='{$dataArr['keycode']}' AND status='yes' LIMIT 1");
		//
		if ($query->num_rows() > 0){
			//1.check the 24 hours validation rule
			foreach ($query->result() as $row)
   			{	
				if($this->datePassedBy(24, $row->ts_created)){
					$returnArr['data'] = 'NO';
					return json_encode($returnArr);
				}
			}
			//2. Passed!
			$returnArr['data'] = 'YES';
			return json_encode($returnArr);
		}
		
		$returnArr['data'] = 'NO';
		return json_encode($returnArr);
		
		
		
	}
	
	//notification for failed attempts logins --------------------------------------------------------------------------------------------
	public function alertEmail(){
		
		$new_data = utf8_encode(stripslashes($this->input->post('data')));
		$dataArr = json_decode($new_data, true);
		
		$name = $dataArr['username'];
		
		//this email address will forward all emails to: jason.armstrong@ward6.com.au
		$from = 'admin@w6digital.com.au';
	
		$to = 'beyondhiv@w6digital.com.au';
		$subject = 'AmBisome - login failure alert';
		$headers = "From: AmBisome<$from>\n";
		$headers .= "Reply-To: AmBisome<$from>\n";
		$headers .= "Return-Path: AmBisome<$from>\n";
		$headers .= "Content-type: text/html\n";
		
		$message = <<<EOF
<html>
	<body>
		Attention BeyondHIV Admin,
		<br/><br/>
		An iPad running the BeyondHIV app has logged five (5) unsuccessful login attempts, locking out all user access to the app on that particular iPad. No data has been lost, 
		however the application will remain suspended until it is manually unlocked. To unlock the iPad, log in to the admin control panel at:
		<br/><br/>
		http://amBisome.w6digital.com.au/admin/
		<br/><br/>
		Click on the KEYCODE link in the main menu upon logging in.<br/>
		You will be supplied with a 10-character keycode which should be supplied to the effected representative to manually type into the unlock screen.<br/><br/>
		User Information:<br/>
		Last logged in by: {$name} <br/>
		
		<br/><br/>
		This e-mail was automatically generated by the BeyondHIV system.<br/>
		
	</body>
</html>
EOF;
		

    	// now lets send the email.
    	mail($to, $subject, $message, $headers);
	
	}
	
	
	
	
	
	
	//private functions --------------------------------------------------------------------------------------------
	//e.g. (24, "2012-01-8 11:24:00")
	private function datePassedBy($hours, $from){
		
		$utime=strtotime($from); 
		$now=time();
	
		$minisecond = $hours * (60 * 60);
	
		return ($now > $utime+$minisecond);
	}
	
	
	
	//test --------------------------------------------------------------------------------------------
	public function test()
	{
		
	}
    
}