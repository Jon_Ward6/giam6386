<?php

class Admin_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	

	public function login($username, $passwd){	
		$this->db->select('id,username,access_level');
		$this->db->where('username',$username);
		$this->db->where('password', $passwd);
		$this->db->where('access_level >', 0);
		$this->db->limit(1);
		$q = $this->db->get('rep');
		
		
		if ($q->num_rows() > 0){
			$row = $q->row_array();
			$_SESSION['userid'] = $row['id'];
			$_SESSION['username'] = $row['username'];
			$_SESSION['access_level'] = $row['access_level'];
			
		}else{
			$this->session->set_flashdata('error', 'Sorry, your username or password is incorrect!');
		}
	}

		
		
	
}