<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

require_once "jqgrid/jqGrid.php";

class call_datagrid {

    public function call_datagrid($rows)
    {

    	$grid = new jqGridRender();
		$grid->dataType = 'json';
		

		$model = array(
		    array("name"=>"rep_name"),
		    array("name"=>"client_name"),
		    array("name"=>"practice_name"),
		    array("name"=>"modules"),
		    array("name"=>"time_spent"),
			array("name"=>"call_time")
		);

		$grid->setColModel($model);
		
		
		// Set grid caption using the option caption
		
		$grid->setGridOptions(array(
		    "caption"=>"eDetailer Calls:",
			"width"=>950,
			"height"=>280,
		    "rowNum"=>10,
		    "sortname"=>"call_time",
		    "hoverrows"=>true,
		    "rowList"=>array(10,20,50),
			"datatype"=>"local"
		    ));
		    
		$grid->setColProperty("rep_name", array("label"=>"Rep Name", "width"=>80));
		$grid->setColProperty("client_name", array("label"=>"Client Name", "width"=>90));
		$grid->setColProperty("practice_name", array("label"=>"Practice Name", "width"=>120));
		$grid->setColProperty("modules", array("label"=>"Modules", "width"=>120));
		$grid->setColProperty("time_spent", array("label"=>"Time Spent", "width"=>70));
		$grid->setColProperty("call_time", array("label"=>"Call Time", "width"=>70));
		
		
		$grid->toolbarfilter = true;
		$grid->setFilterOptions(array("stringResult"=>true));

		//print_r($rows);
		$grid->callGridMethod("#grid", 'addRowData', array("dcall_time",$rows));
		$grid->renderGrid('#grid','#pager',true, null, null, true,true);
		
    }
    
}