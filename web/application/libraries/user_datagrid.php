<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

require_once "jqgrid/jqGrid.php";

class user_datagrid {

    public function user_datagrid($rows)
    {
    	
    	$grid = new jqGridRender(NULL);
		$grid->dataType = 'json';

		$model = array(
		    array("name"=>"client_name"),
		    array("name"=>"practice_name"),
		    array("name"=>"address"),
		    array("name"=>"email"),
			array("name"=>"date_creation")
		);

		$grid->setColModel($model);
		
		// Set grid caption using the option caption
		
		$grid->setGridOptions(array(
		    "caption"=>"eDetailer Users:",
			"width"=>950,
			"height"=>280,
		    "rowNum"=>10,
		    "sortname"=>"firstname",
		    "hoverrows"=>true,
		    "rowList"=>array(10,20,50),
			"datatype"=>"local"
		    ));
		    
		$grid->setColProperty("client_name", array("label"=>"Client Name", "width"=>80));
		$grid->setColProperty("practice_name", array("label"=>"Practice Name", "width"=>90));
		$grid->setColProperty("address", array("label"=>"Address", "width"=>120));
		$grid->setColProperty("email", array("label"=>"Email", "width"=>90));
		$grid->setColProperty("date_creation", array("label"=>"Date Creation", "width"=>70));

		//print_r($rows);
		$grid->callGridMethod("#grid", 'addRowData', array("date_creation",$rows));
		$grid->renderGrid('#grid','#pager',true, null, null, true,true);
    }
    
}

