<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

require_once "jqgrid/jqGrid.php";

class reps_datagrid {

    public function reps_datagrid($rows)
    {

    	$grid = new jqGridRender();
		$grid->dataType = 'json';
		

		$model = array(
		    array("name"=>"username"),
		    array("name"=>"email"),
			array("name"=>"access_level")
		);

		$grid->setColModel($model);
		
		
		// Set grid caption using the option caption
		
		$grid->setGridOptions(array(
		    "caption"=>"eDetailer Calls:",
			"width"=>950,
			"height"=>280,
		    "rowNum"=>10,
		    "sortname"=>"call_time",
		    "hoverrows"=>true,
		    "rowList"=>array(10,20,50),
			"datatype"=>"local"
		    ));
		    
		$grid->setColProperty("username", array("label"=>"User Name", "width"=>180));
		$grid->setColProperty("email", array("label"=>"Email", "width"=>190));
		$grid->setColProperty("access_level", array("label"=>"Access Level", "width"=>80));
		
		

		//print_r($rows);
		$grid->callGridMethod("#grid", 'addRowData', array("dcall_time",$rows));
		$grid->renderGrid('#grid','#pager',true, null, null, true,true);
		
    }
    
}