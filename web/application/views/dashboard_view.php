<!DOCTYPE html>
<html lang="en">
<head>
	<title>eDetailer - Dashboard</title>
	
    <base href="<?php echo $this->config->item('base_url') ?>www/" />
	<link rel="stylesheet" href="css/site.css" type="text/css" media="screen" />
	<script src="js/jquery.js" type="text/javascript"></script>
    
</head>


<body>


<div id="wrapper" >
	<div id='header' >
	<?php $this->load->view('admin_header_view'); ?>
	</div >
	
	<div id='main'>
	<?php $this->load->view($main);?>
	</div>
	
	
</div >
<div id='footer'>
	<?php $this->load->view('admin_footer_view'); ?>
</div >




	<script src="js/main.js" type="text/javascript"></script>
</body>
</html>