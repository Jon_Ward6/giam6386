	<script src="js/jquery.dataTables.js" type="text/javascript"></script>
	
	
	<script type="text/javascript" charset="utf-8">
		
	
		$(document).ready(function()
  		{
			var oTable;
			var giRedraw = false; 
			
			/* Add a click handler to the rows - this could be used as a callback */
			$("#users_grid tbody").click(function(event) {
				$(oTable.fnSettings().aoData).each(function (){
					$(this.nTr).removeClass('row_selected');
				});
				
				$(event.target.parentNode).addClass('row_selected');
				
			});
			
			/* Add a click handler for the delete row */
			$('#delete').click( function() {
				var anSelected = fnGetSelected( oTable );
				oTable.fnDeleteRow( anSelected[0] );
			} ); 
			
	  		
			oTable = $('#users_grid').dataTable( {
	  			"aaData": <?php echo json_encode($rows); ?>,

	  			"aoColumnDefs": [
									{ "sWidth": "20%", "aTargets": [0] },
									{ "sWidth": "10%", "sClass": 'center', "aTargets": [2] },
									{ "sWidth": "10%", "sClass": 'center', "aTargets": [3] }

								],
								"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        						"iDisplayLength": 25,
								"sPaginationType": "full_numbers",
	  		 					"oLanguage": {
	  		 						"sSearch": "Search:",
	  		 						"oPaginate": {
	  		 							"sFirst": "&laquo; ",
	  		 							"sLast": "&raquo; ",
	  		 							"sNext": " &rsaquo; ",
	  		 							"sPrevious": " &lsaquo; "
	  		 						}
	  		 					}
	  					
			} );




	  		/* Get the rows which are currently selected */
	  		function fnGetSelected( oTableLocal )
	  		{
	  			var aReturn = new Array();
	  			var aTrs = oTableLocal.fnGetNodes();
	  			for ( var i=0 ; i<aTrs.length ; i++ )
	  			{
	  				if ( $(aTrs[i]).hasClass('row_selected') )
	  			{
	  			aReturn.push( aTrs[i] );
	  			}
	  		}
	  					


    	}});

    	

    
	</script>
	
	
	
	
	<h1>Reps</h1>
	
	<div id='add_rep'>	
		<input class="add_rep addButton" type="button" value="+"  onclick="window.location.href='<?php echo $this->config->item('base_url') ?>index.php/dashboard/reps_add'; " > 
	</div>

	<div id='download'>	
		<form>
        	<input type="button" class="download formButton" value="Export data" onclick="javascript: openPage('report_download_users.php')" />
        </form>  
	</div>
	
	<?php 
		//$this->load->library('reps_datagrid', $rows);
	?>
	
	  <table id="users_grid">
	    <thead>
	      <tr>
	        <th>User Name</th>
	        <th>Email</th>
	        <th>Access level</th>
	        <th>Edit</th>
	      </tr>
	    </thead>
	    <tbody>
	      <tr>
	        <td>loading...</td>
	      </tr>
	    </tbody>
	  </table>

