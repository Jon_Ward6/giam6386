
	<script src="js/jquery.dataTables.js" type="text/javascript"></script>
	
	
	<script type="text/javascript" charset="utf-8">
		
	
		$(document).ready(function()
  		{
	  		
	  		$('#users_grid').dataTable( {
	  			"aaData": <?php echo json_encode($rows); ?>,
	  			"aoColumnDefs": [
									{ "sWidth": "15%", "aTargets": [0] },
	  		 						{ "sWidth": "75%", "aTargets": [1] },
	  		 						{ "sWidth": "10%", "sClass": 'center', "aTargets": [2] }
									
	  		 						
	  		 					],
								"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        						"iDisplayLength": 25,
	  		 					"sPaginationType": "full_numbers",
	  		 					"oLanguage": {
	  		 						"oPaginate": {
	  		 							"sFirst": "&laquo; ",
	  		 							"sLast": "&raquo; ",
	  		 							"sNext": " &rsaquo; ",
	  		 							"sPrevious": " &lsaquo; "
	  		 						}
	  		 					}
		  					
			} );
    	});

    
	</script>

	


	<h1>Pages</h1>
	
	
	  <table id="users_grid">
	    <thead>
	      <tr>
            <th>Module Name</th>
	        <th>Page Name</th>
	        <th>Time Spent</th>
            
	      </tr>
	    </thead>
	    <tbody>
	      <tr>
	        <td>loading...</td>
	      </tr>
	    </tbody>
	  </table>
