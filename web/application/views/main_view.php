
	<ul id="dashNav">
		
		
		<li class="dash1 clickable">
		
			<h3><?php echo anchor("dashboard/calls","View Calls");?></h3>
			<p/>
			View records of calls.
			<p/>
		</li>
		
		<li class="dash2 clickable">
			<h3><?php echo anchor("dashboard/users","View Doctors");?></h3>
			<p/>
			Manage your clients details.
			<p/>
		</li>
		
		
		<li class="dash3 clickable">
			<h3><?php echo anchor("dashboard/reps","View Reps");?></h3>
			<p/>
			Manage Rep account details.
			<p/>
		</li>
		
	</ul>

