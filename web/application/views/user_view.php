

	<script src="js/jquery.dataTables.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
		
	
		$(document).ready(function()
  		{
	  		
	  		$('#users_grid').dataTable( {
	  			"aaData": <?php echo json_encode($rows); ?>,
				"aaSorting": [[4, 'desc']],
	  			"aoColumnDefs": [
	 							{ "sWidth": "15%", "aTargets": [0] },
	 							{ "sWidth": "15%", "sClass": 'center', "aTargets": [4] }

	 							],
								"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        						"iDisplayLength": 25,
	 							"sPaginationType": "full_numbers",
	  		 					"oLanguage": {
	  		 						"sSearch": "Search:",
	  		 						"oPaginate": {
	  		 							"sFirst": "&laquo; ",
	  		 							"sLast": "&raquo; ",
	  		 							"sNext": " &rsaquo; ",
	  		 							"sPrevious": " &lsaquo; "
	  		 						}
	  		 					}	  			
	  					
			} );
    	});
    
	</script>


	<script type="text/javascript">
		function openPage(page)
		{
		    testwindow = window.open(page, "mywindow", "location=1,status=1,scrollbars=1,width=100,height=100");
		    testwindow.moveTo(0, 0);
		}
   </script>


	<h1>Doctors</h1>

	<div id='download'>
		<form>
        	<input type="button" class="download formButton" value="Export data" onclick="javascript: openPage('<?php echo $this->config->item('base_url') ?>/index.php/download/users')" />
        </form>  
	</div>

	  <table id="users_grid">
	    <thead>
	      <tr>
	        <th>Client Name</th>
	        <th>Practice Name</th>
	        <th>Address</th>
	        <th>Email</th>
	        <th>Date Creation</th>
	      </tr>
	    </thead>
	    <tbody>
	      <tr>
	        <td>loading...</td>
	      </tr>
	    </tbody>
	  </table>
