<div id='main' >

		<h1>Reps - edit</h1>
		
		<?php
		
			$username_data = array('name'=> 'username','id'=> 'username','size'=> 85, 'value'=>$rows->username);
			$email_data = array('name'=> 'email','id'=> 'email','size'=> 85, 'value'=>$rows->email);
			$access_data = array('name'=> 'access_level','id'=> 'access_level','size'=> 85, 'value'=>$rows->access_level);
			
			$hidden = array('id'=>$rows->id);
			
			echo form_open("dashboard/reps_edit_submit", "", $hidden);
			
			echo "<p> <label for='username'> Username </label>";
			echo form_input($username_data) . " </p> ";
			
			echo "<p> <label for='email' > Email </label>";
			echo form_input($email_data) . " </p> ";
			
			echo "<p> <label for='access_level' > Access </label>";
			echo form_input($access_data) . " </p> ";
			
			echo "<input type='submit' value='Update' name='submit' class='formButton login'>";
			echo "<input type='submit' value='Delete' name='delete' class='formButton formReoveButton remove'>";
			echo form_close();
		?>
		
		<?php
			if ($this->session->flashdata('error')){
			echo "<div class='message'>";
			echo $this->session->flashdata('error');
			echo "</div> ";
			}
		?>
		
</div>