<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>eDetailer - main</title>

	<base href="<?php echo $this->config->item('base_url') ?>www/" />
	<link rel="stylesheet" href="css/site.css" type="text/css" media="screen" />

</head>


<body>

<div id="wrapper">
	<div id="header">
		<div id='logo'>
			<h1>Ward 6</h1>
		</div>
	</div>
	
	<div id='main' >
		<h3>Welcome to the iPad app download section:</h3>
        <br/><br/>
        
		<?php
			$udata = array('name'=> 'username','id'=> 'u','size'=> 45);
			$pdata = array('name'=> 'password','id'=> 'p','size'=> 45);
			echo form_open("ipad/verify");
			echo "<p> <label for='u'> Username </label>";
			echo form_input($udata) . " </p> ";
			echo "<p> <label for='p' > Password </label>";
			echo form_password($pdata) . " </p> ";
			echo "<input type='submit' value='Login' name='submit' class='formButton login'>";
			echo form_close();
		?>
		
		<?php
			if ($this->session->flashdata('error')){
			echo "<div class='message'>";
			echo $this->session->flashdata('error');
			echo "</div> ";
			}
		?>
		
	</div>
</div>
<div id='footer'>
	<?php $this->load->view('admin_footer_view'); ?>
</div >


</body>
</html>