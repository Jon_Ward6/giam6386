<div id='main' >

		<h1>Reps - add</h1>
		
		<?php
		
			$username_data = array('name'=> 'username','id'=> 'username','size'=> 85, 'value'=>'');
			$email_data = array('name'=> 'email','id'=> 'email','size'=> 85, 'value'=>'');
			$access_data = array('name'=> 'access_level','id'=> 'access_level','size'=> 85, 'value'=>'');
			
			
			echo form_open("dashboard/reps_edit_submit");
			
			echo "<p> <label for='username'> Username </label>";
			echo form_input($username_data) . " </p> ";
			
			echo "<p> <label for='email' > Email </label>";
			echo form_input($email_data) . " </p> ";
			
			echo "<p> <label for='access_level' > Access </label>";
			echo form_input($access_data) . " </p> ";
			
			echo "<input type='submit' value='Add' name='submit' class='formButton login'>";

			echo form_close();
		?>
		
		<?php
			if ($this->session->flashdata('error')){
			echo "<div class='message'>";
			echo $this->session->flashdata('error');
			echo "</div> ";
			}
		?>
		
</div>