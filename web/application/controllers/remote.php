<?php
class Remote extends CI_Controller {
	
	public function Remote()
  	{  
    	parent::__construct();
    	
    	$this->load->model('Remote_model');
    	
  		//if($this->input->get('key')!='rep-data-2011-03'){
  		
    	//let's say the key='BeyondHIV@Atripla'
  		if($this->input->post('key')!=md5('BeyondHIV+Atripla')){
			redirect('admin');
		}
		//echo md5('BeyondHIV+Atripla');
  	}
	
	public function index()
	{
		
		//echo md5('test');
	}
	
	//user --------------------------------------------------------------------------------------------
	public function adduser()
	{
		echo $this->Remote_model->adduser();
	}
	
	public function getusers()
	{
		echo $this->Remote_model->getusers();
	}
	
	
	//practice --------------------------------------------------------------------------------------------
	public function addpractice()
	{
		echo $this->Remote_model->addpractice();
	}
	
	
	//public function get_practice_by_id($practice_uid)
	//{
		
	//}
	
	
	//calls --------------------------------------------------------------------------------------------
	public function addcall()
	{
		echo $this->Remote_model->addcall();	
	}
	
	public function addcallmodule()
	{
		echo $this->Remote_model->addcallmodule();
	}
	
	public function addcallpage()
	{
		echo $this->Remote_model->addcallpage();
	}
	
	public function getcallpages()
	{
		echo $this->Remote_model->getcallpages();
	}
	
	public function getcalls()
	{
		echo $this->Remote_model->getcalls();
	}
	
	
	//Reps ---------------------------------------------------------------------------------------------
	public function getrep()
	{
		echo $this->Remote_model->getrep();
	}
	
	
	public function getallrep()
	{
		echo $this->Remote_model->getallrep();
	}
	
	
	//KeyCode -----------------------------------------------------------------------------------------
	public function getkeycode()
	{
		echo $this->Remote_model->getkeycode();
	}
	
	
	//notification for failed attempts logins ---------------------------------------------------------
	public function alertEmail(){
		$this->Remote_model->alertEmail();
	}
	
	
	
	//Test --------------------------------------------------------------------------------------------
	public function test()
	{
		
	}
	
}