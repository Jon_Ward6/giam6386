<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	public function Admin()
  	{  
      parent::__construct();
      session_start();
  	}
	
	public function index()
	{
		
		$this->load->model('Admin_model');
		$this->load->view('admin_login_view');
		
	}
	
	public function verify(){
		$_SESSION['userid'] = 0;
		
		//check
		if ($this->input->post('username')){
			$u = $this->input-> post('username');
			$pw = $this->input->post('password');
			$this->Admin_model->login($u,$pw);
			
			if ($_SESSION['userid'] > 0){
				redirect('dashboard','refresh');
				
				//return;
			}
		}
		//nope, go back!
		$this->session->set_flashdata('error', 'Sorry, you entered an incorrect username or password.');
		$this->load->view('admin_login_view');
	}
}



