<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	public function Dashboard(){
		parent::__construct();
		
		session_start();
		//
		if(!isset($_SESSION['userid']) || !$_SESSION['userid'] > 0){
			redirect('admin');
			return;
		}
		
		
		
		$this->load->model('Dashboard_model');
      	
		
	}
	
	
	public function index()
	{
		$data['main'] = 'main_view';
		$this->load->vars($data);
		$this->load->view('dashboard_view');
	}
	
	public function calls()
	{
		$data['rows'] = $this->Dashboard_model->calls();
		
		$data['main'] = 'call_view';
		$data['title'] = 'eDetailer - calls';
		
		$this->load->view('dashboard_view', $data);
	}
	
	
	public function users()
	{
		$data['rows'] = $this->Dashboard_model->users();
		
		$data['main'] = 'user_view';
		$data['title'] = 'eDetailer - users';
		
		$this->load->view('dashboard_view', $data);
	}
	
	
	public function reps()
	{
		$data['rows'] = $this->Dashboard_model->reps();
		$data['main'] = 'rep_view';
		$data['title'] = 'eDetailer - reps';
		
		$this->load->view('dashboard_view', $data);
	}
	
	public function reps_edit($id)
	{
		$data['rows'] = $this->Dashboard_model->rep_by_id($id);
		$data['main'] = 'rep_edit_view';
		$data['title'] = 'eDetailer - reps edit';
		$this->load->view('dashboard_view', $data);

	}
	
	public function reps_add()
	{
		$data['main'] = 'rep_add_view';
		$data['title'] = 'eDetailer - reps add';
		$this->load->view('dashboard_view', $data);

	}
	
	public function reps_edit_submit()
	{
		//update
		if($this->input->post('submit')=='Update'){
			$id = $this->input-> post('id');
			$username = $this->input-> post('username');
			$email = $this->input-> post('email');
			$access_level = $this->input-> post('access_level');
			$this->Dashboard_model->rep_update($id, $username, $email, $access_level);
		}
		
		//add
		if($this->input->post('submit')=='Add'){
			$username = $this->input-> post('username');
			$email = $this->input-> post('email');
			$access_level = $this->input-> post('access_level');
			$this->Dashboard_model->rep_add($username, $email, $access_level);
		}
		
		//delete
		if($this->input->post('delete')=='Delete'){
			$id = $this->input-> post('id');
			$this->Dashboard_model->rep_delete($id);
		}
		
		
		//reload
		$this->reps();
	}
	
	public function logout()
	{
		unset($_SESSION['userid']);
		unset($_SESSION['username']);
		$this->session->set_flashdata('error',"You were successfully logged out.");
		redirect('admin/verify','refresh');
	}
	
	
	
	public function pages($call_uid)
	{
		$data['rows'] = $this->Dashboard_model->pages($call_uid);
		
		$data['main'] = 'page_view';
		$data['title'] = 'eDetailer - Pages';
		
		$this->load->view('dashboard_view', $data);
		
	
	}
	
	public function keycode()
	{
		$submit_email = $this->input-> post('email');
		
		$data = $this->Dashboard_model->getKeyCode();
		$reps = $this->Dashboard_model->reps();
		
		$data['main'] = 'keycode_view';
		$data['title'] = 'eDetailer - Keycode';
		$data['data'] = $data;
		$data['reps'] = $reps;
		$data['msg'] = '';
		
		if(!$submit_email){
			$this->load->view('dashboard_view', $data);
		
		}else{

			//email out here	
			$this->load->helper('email');

			//check Email. 
			if(!valid_email($this->input-> post('reps_email'))) {
				$data['msg'] = 'Invalid email address';
				$this->load->view('dashboard_view', $data);
				return;
			//check keycode
			}else if(!$this->input-> post('keycode')){
				$data['msg'] = 'missed keycode.';
				$this->load->view('dashboard_view', $data);
				return;
			}else{
				//email
				$email = $this->input-> post('reps_email');
				$from = 'admin@w6digital.com.au';
				$subject = 'AmBisome - new keycode';
				$keycode = $this->input-> post('keycode');
				$valid = $this->input-> post('valid');
				$name = $this->Dashboard_model->searchRepArray($reps, $email);
				$name = ucfirst($name);
				
				$this->Dashboard_model->emailKeycode($email, $from, $subject, $name,$keycode, $valid);
			}
			
			redirect('dashboard/emailkeycode', 'refresh');
		}
			
	}
	
	public function emailkeycode(){
		
		$data['main'] = 'keycode_email_view';
		$data['title'] = 'eDetailer - Email';
		
		$this->load->view('dashboard_view', $data);
	}
	
	
	
	
}
