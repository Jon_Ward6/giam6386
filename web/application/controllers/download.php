<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download extends CI_Controller {
	
	public function Download(){
		parent::__construct();

		$this->load->model('Download_model');
	}
	
	
	public function index()
	{
		
	}
	
	public function users()
	{
		$data['rows'] = $this->Download_model->users();
		$data['type'] = 'users';
		$this->load->view('download_view', $data);
	}
	
	
	public function calls()
	{
		$data['rows'] = $this->Download_model->calls();
		$data['type'] = 'calls';
		$this->load->view('download_view', $data);
	
	}
	
}