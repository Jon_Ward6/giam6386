<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class IPad extends CI_Controller {
	
	public function IPad(){
		parent::__construct();
		session_start();
		
		$this->load->model('Ipad_model');
	}
	
	
	public function index()
	{
		
		//$this->load->model('Ipad_model');
		$this->load->view('ipad_login_view');
		
	}
	
	public function verify(){
		$_SESSION['userid'] = 0;
		
		//check
		if ($this->input->post('username')){
			$u = $this->input-> post('username');
			$pw = $this->input->post('password');
			$this->Ipad_model->login($u,$pw);
			
			if ($_SESSION['userid'] > 0){
				redirect('ipad_download','refresh');
				
				//return;
			}
		}
		//nope, go back!
		$this->session->set_flashdata('error', 'Sorry, you entered an incorrect username or password.');
		$this->load->view('admin_login_view');
	}
	
	

	
}