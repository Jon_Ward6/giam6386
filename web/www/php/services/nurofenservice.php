<?php
	//
	require_once("db_mysql.inc.php");
	//
	class NurofenService
	{
		private $db;
		private $tbl_report;
		private $tbl_user;
		private $tbl_practice;
		private $tbl_callmodule;
		private $tbl_call;
		private $tbl_rep;
		
		//constructor
		public function __construct()
		{
			$this->db = new ps_DB;
			$this->tbl_report = 'report';
			$this->tbl_user = 'user';
			$this->tbl_practice = 'practice';
			$this->tbl_callmodule = 'call_module';
			$this->tbl_call = 'calls';
			$this->tbl_rep = 'rep';
			
			//security check (should be a md5 key)
			if($_REQUEST['key']!='rep-data-2011-03') return;
			//if(!isset($_REQUEST['data'])) return;
			//
			if($_REQUEST['action']=='report') 	$this->setReport();	
			if($_REQUEST['action']=='adduser')  $this->addUser();
			if($_REQUEST['action']=='test')     $this->test();
			if($_REQUEST['action']=='getusers') $this->getUsers();
			if($_REQUEST['action']=='addpractice') $this->addPractice();
			if($_REQUEST['action']=='addcall') $this->addCall();
			if($_REQUEST['action']=='addcallmodule') $this->addCallModule();
			if($_REQUEST['action']=='getrep') $this->getRep();
		}
		
		
		
		//add user from clients (iPads)
		public function addUser()
		{
			$new_data = utf8_encode(stripslashes($_REQUEST['data']));
			$dataArr = json_decode($new_data, true);
	
			//$ts_created = date("F j, Y, g:i a");
			$sql='';
			
			
			if(is_array($dataArr) && count($dataArr)>0){
				foreach ($dataArr as $data) {
					//test
					$test_sql = sprintf("SELECT email FROM {$this->tbl_user} WHERE email='%s'", $data['email']);
					$this->db->query($test_sql);
					if($this->db->next_record()) continue;
					
					//time stamp
					//$date_creation = strtotime(date('Y-m-d H:i:s', $data['date_creation']));
					//$date_update = strtotime(date('Y-m-d H:i:s', $data['date_update']));
					//
					$sql = sprintf("INSERT INTO {$this->tbl_user} 
					(user_uid, rep_uid, practice_uid,firstname, lastname, email, status, date_creation, date_update) 
					VALUES ('%s','%s','%s','%s','%s','%s','%s',NOW(),NOW())", 
					$data['user_uid'],$data['rep_uid'],$data['practice_uid'],$data['firstname'],$data['lastname'],$data['email'],'local');
					//
					$this->db->query($sql);
				}
				
			}else{
				echo "NO"; 
			}
			
			
			echo "YES"; 
			//echo (json_encode($dataArr));
	
		}
		
		
		public function getUsers(){
			$userArr = array();
			
			$new_data = utf8_encode(stripslashes($_REQUEST['data']));
			$dataArr = json_decode($new_data, true);
			
			
			$sql = sprintf("SELECT * FROM {$this->tbl_user} WHERE rep_uid ='%s'", $dataArr['rep_uid']);
			$this->db->query($sql);
			while($this->db->next_record()){
				
				array_push($userArr, array("user_uid"=>$this->db->f("user_uid"), 
										   "firstname"=>$this->db->f("firstname"),
										   "lastname"=>$this->db->f("lastname"),
										   "email"=>$this->db->f("email"),
										   "rep_uid"=>$this->db->f("rep_uid"),
										   "practice_uid"=>$this->db->f("practice_uid"),
										   "status"=>$this->db->f("status"),
										   "date_creation"=>$this->db->f("date_creation"),
										   "date_update"=>$this->db->f("date_update"),
										   "practice_arr"=>$this->getPracticeById($this->db->f("practice_uid"))
				));	
			}
			
			$returnArr = array();
			$returnArr['type'] = 'getusers';
			$returnArr['data'] = $userArr;//json_encode($userArr);
			
			echo json_encode($returnArr);
			
		}
		
		
		
		//add user from clients (iPads)
		public function addPractice(){
			$new_data = utf8_encode(stripslashes($_REQUEST['data']));
			$dataArr = json_decode($new_data, true);
	
			$sql='';
			
			
			if(is_array($dataArr) && count($dataArr)>0){
				foreach ($dataArr as $data) {
					//test
					$test_sql = sprintf("SELECT * FROM {$this->tbl_practice} WHERE practice_uid='%s'", $data['practice_uid']);
					$this->db->query($test_sql);
					if($this->db->next_record()) continue;
					
					
					$sql = sprintf("INSERT INTO {$this->tbl_practice} 
					(practice_uid, name, street,suburb, postcode, state, region_code) 
					VALUES ('%s','%s','%s','%s','%s','%s','%s')", 
					$data['practice_uid'],$data['name'],$data['street'],$data['suburb'],$data['postcode'],$data['state'],$data['region_code']);
					//
					$this->db->query($sql);
				}
				
			}else{
				echo "NO"; 
			}
			
			
			echo "YES";
			
		}
		
		
		public function getPracticeById($practice_uid){
			$arr = array();
			
			
			$sql = sprintf("SELECT * FROM {$this->tbl_practice} WHERE practice_uid ='%s'", $practice_uid);
			$this->db->query($sql);
			while($this->db->next_record()){
				//practice_uid, name, street,suburb, postcode, state, region_code
				array_push($arr, array("practice_uid"=>$this->db->f("practice_uid"), 
										   "name"=>$this->db->f("name"),
										   "street"=>$this->db->f("street"),
										   "suburb"=>$this->db->f("suburb"),
										   "postcode"=>$this->db->f("postcode"),
										   "state"=>$this->db->f("state"),
										   "region_code"=>$this->db->f("region_code")
				));	
			}
			
			return $arr;
			
		}
		
		
		//CALLS ---------------------------------------------------------------------------------
		
		public function addCall(){
			$new_data = utf8_encode(stripslashes($_REQUEST['data']));
			$dataArr = json_decode($new_data, true);
	
			$sql='';
			
			
			if(is_array($dataArr) && count($dataArr)>0){
				foreach ($dataArr as $data) {
					//test
					$test_sql = sprintf("SELECT * FROM {$this->tbl_call} WHERE call_uid='%s'", $data['call_uid']);
					$this->db->query($test_sql);
					if($this->db->next_record()) continue;
					
					$sql = sprintf("INSERT INTO {$this->tbl_call} 
					(call_uid, user_uid, rep_uid,start_time, end_time, time_spent) 
					VALUES ('%s','%s','%s',NOW(),NOW(),'%d')", 
					$data['call_uid'],$data['user_uid'],$data['rep_uid'],$data['time_spent']);
					//
					$this->db->query($sql);
				}
				
			}else{
				echo "NO"; 
			}
			
			
			echo "YES";
			
		}
		
		
		public function addCallModule(){
			$new_data = utf8_encode(stripslashes($_REQUEST['data']));
			$dataArr = json_decode($new_data, true);
	
			$sql='';
			
			
			if(is_array($dataArr) && count($dataArr)>0){
				foreach ($dataArr as $data) {
					
					$sql = sprintf("INSERT INTO {$this->tbl_callmodule} 
					(call_uid, module_uid, user_uid, time_spent) 
					VALUES ('%s','%s','%s','%d')", 
					$data['call_uid'],$data['module_uid'],$data['user_uid'],$data['time_spent']);
					//
					$this->db->query($sql);
				}
				
			}else{
				echo "NO"; 
			}
			
			
			echo "YES";
			
		}
		
		
		// Rep ---------------------------------------------------------------------------
		
		public function getRep(){
			$repArr = array();
			
			$new_data = utf8_encode(stripslashes($_REQUEST['data']));
			$dataArr = json_decode($new_data, true);
			
			
			$sql = sprintf("SELECT * FROM {$this->tbl_rep} WHERE username ='%s' AND password='%s' LIMIT 1", 
			$dataArr['username'], $dataArr['password']);
			
			$this->db->query($sql);
			if($this->db->next_record()){	
				$repArr['rep_uid'] = $this->db->f("rep_uid");
				$repArr['username'] = $this->db->f("username");
				$repArr['password'] = $this->db->f("password");
				$repArr['email'] = $this->db->f("email");
				$repArr['access_level'] = $this->db->f("access_level");
				
			}
			
			$returnArr = array();
			$returnArr['type'] = 'getrep';
			$returnArr['data'] = $repArr;
			
			echo json_encode($returnArr);
			
		}
		
		
		
		
		///////////////////////////////////////////////////////////////////////////////////
		public function test()
		{
			//echo $this->getCase(1);
		}
		
		
		
	}

	$app = new NurofenService();


?>