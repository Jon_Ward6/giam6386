<?php
include("db_mysql.inc.php");

$dbq = new ps_DB;
$dbx = new ps_DB;

$TBL_USER = 'user';
$TBL_PRACTICE = 'practice';
$TBL_CALLS = 'calls';
$TBL_CALLMODULE = 'call_module';
$TBL_MODULE = 'module';
$TBL_REP = 'rep';
$today = date('d/m/Y');


$TRS = '';
$TRS_CALL = '';

if($_REQUEST['action']=='report'){
	getReport();
}



//
function getReport(){
	global $_REQUEST, $dbq, $dbx, $TRS, $TRS_CALL;
	global $TBL_USER, $TBL_PRACTICE, $TBL_CALLS, $TBL_CALLMODULE, $TBL_MODULE;
	
	//USERS
	$sql  = sprintf("SELECT * FROM $TBL_USER, $TBL_PRACTICE WHERE $TBL_USER.practice_uid=$TBL_PRACTICE.practice_uid");	

	$query = $dbq->query($sql);
	while($dbq->next_record()){
		$TRS .= "<tr align='center'>";
		//
		$firstname = $dbq->f('firstname');
		$lastname = $dbq->f('lastname');
		$email = $dbq->f('email');
		$name = $dbq->f('name');
		$date_creation = $dbq->f('date_creation');
		$date_update = $dbq->f('date_update');
		
		
		$TRS .= "<td>$firstname $lastname</td>";
		$TRS .= "<td>$name</td>";
		$TRS .= "<td>{$dbq->f('street')}, {$dbq->f('suburb')}, {$dbq->f('postcode')}</td>";
		$TRS .= "<td>$email</td>";
		$TRS .= "<td>$date_creation</td>";	
		//
		$TRS .= "</tr>";
	}
	
	//CALLS
	//$sql_calls  = sprintf("SELECT * FROM $TBL_CALLS WHERE id>0 ORDER By rep_uid DESC");
	$sql_calls  = sprintf("SELECT * FROM $TBL_CALLS WHERE id>0 ORDER By end_time DESC LIMIT 40");

	$query_calls = $dbq->query($sql_calls);
	while($dbq->next_record()){
		$userArr = getCallUser($dbq->f('user_uid'));
		$modules = getCallModules($dbq->f('call_uid'));
		$modules = strlen($modules) > 0? $modules : '&nbsp;';
		$rep = getCallRep($dbq->f('rep_uid'));
		
		$TRS_CALL .= "<tr align='center'>";
		
		$TRS_CALL .= "<td>". $rep . "</td>";
		$TRS_CALL .= "<td>". $userArr['firstname'] . ' ' . $userArr['lastname']. "</td>";
		$TRS_CALL .= "<td>". $userArr['name'] . "</td>";
		$TRS_CALL .= "<td>". $modules . "</td>";
		$TRS_CALL .= "<td>". date('i:s', $dbq->f('time_spent')) . "</td>";
		$TRS_CALL .= "<td>". $dbq->f('end_time') . "</td>";
		
		$TRS_CALL .= "</tr>";
	}
	
}


function getCallUser($user_uid){
	global $dbx;
	global $TBL_USER,$TBL_PRACTICE;
	
	$userArr = array();
	
	$sql  = sprintf("SELECT * FROM $TBL_USER, $TBL_PRACTICE WHERE $TBL_USER.practice_uid=$TBL_PRACTICE.practice_uid AND $TBL_USER.user_uid='$user_uid'");	

	$query = $dbx->query($sql);
	if($dbx->next_record()){
		$userArr['firstname'] = $dbx->f('firstname');
		$userArr['lastname'] = $dbx->f('lastname');
		$userArr['name'] = $dbx->f('name');
		
	}
	
	return $userArr;
}

function getCallRep($rep_uid){
	global $dbx;
	global $TBL_REP;
	
	//rewr-bgtr-wert-5hjo-qwww
	$sql = sprintf("SELECT * FROM $TBL_REP WHERE rep_uid='$rep_uid'");	

	$query = $dbx->query($sql);
	if($dbx->next_record()){
		return $dbx->f('username');
	}
	
	return '';
}

function getCallModules($call_uid){
	global $dbx;
	global $TBL_CALLMODULE,$TBL_MODULE;
	
	$sql  = sprintf("SELECT * FROM $TBL_CALLMODULE,$TBL_MODULE WHERE $TBL_CALLMODULE.call_uid='$call_uid' AND $TBL_CALLMODULE.module_uid=$TBL_MODULE.module_uid");	
	$query = $dbx->query($sql);
	
	$modules = '';
	while($dbx->next_record()){
		$time_spent = date('i:s', $dbx->f('time_spent'));//formatTime($dbx->f('time_spent'));
		$modules .= $dbx->f('title') . "($time_spent), ";
	}
	
	return $modules;
}

//date('H:i:s', $secs)
//mm:ss
function formatTime($secs) {
   $times = array(60, 1);
   $time = '';
   $tmp = '';
   for($i = 0; $i < 2; $i++) {
      $tmp = floor($secs / $times[$i]);
      if($tmp < 1) {
         $tmp = '00';
      }
      elseif($tmp < 10) {
         $tmp = '0' . $tmp;
      }
      $time .= $tmp;
      if($i < 1) {
         $time .= ':';
      }
      $secs = $secs % $times[$i];
   }
   return $time;
}

//date('H:i:s', $secs)
//hh:mm:ss
function formatTime3($secs) {
   $times = array(3600, 60, 1);
   $time = '';
   $tmp = '';
   for($i = 0; $i < 3; $i++) {
      $tmp = floor($secs / $times[$i]);
      if($tmp < 1) {
         $tmp = '00';
      }
      elseif($tmp < 10) {
         $tmp = '0' . $tmp;
      }
      $time .= $tmp;
      if($i < 2) {
         $time .= ':';
      }
      $secs = $secs % $times[$i];
   }
   return $time;
}


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Report</title>

<script type="text/javascript">
function openPage(page)
{
    testwindow = window.open(page+'?action=report', "mywindow", "location=1,status=1,scrollbars=1,width=100,height=100");
    testwindow.moveTo(0, 0);
}
</script>


<style type="text/css">
<!--
body,td,th {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 10px;
	
}
-->
</style>




</head>

<body>
<table width="1050" border="0" align="center" cellpadding="4">
  <tr>
    <td><img src="w6_logo_sm.png" width="165" height="22" /></td>
  </tr>
  <tr>
    <td bgcolor="#96c11f"><b>&nbsp;&nbsp;</b></td>
  </tr>
  <tr>
    <td width="626"><h3>&nbsp;</h3>
    <h3><strong>eDetailer > Data Report - generated on <?php echo $today; ?> </strong></h3></td>
  </tr>
  <tr>
    <td align="right">
    	<form>
        	<input type="button" value=" Download Users" onclick="javascript: openPage('report_download_users.php')" />
        	<!--<input type="button" value=" Print Report " onclick="window.print();return false;" />-->
        </form>   
    </td>
  </tr>
  <tr>
    <td><strong>Users:</strong></td>
  </tr>
  <tr>
    <td>
    
    <table width="1050" border="1">
      <tr align="center">
        <td width="80" bgcolor="#CCCCCC">Client Name</td>
        <td width="80" bgcolor="#CCCCCC">Practice Name</td>
        <td width="120" bgcolor="#CCCCCC">Address</td>
        <td width="80" bgcolor="#CCCCCC">Email</td>
        <td width="60" bgcolor="#CCCCCC">Date Creation</td>
      </tr>
      
      
 
      <?php echo $TRS; ?>
    </table>
    
    </td>
  </tr>
  
  <tr>
    <td align="right" height="60">
    	<p/><p/>
    	<form>
        	<input type="button" value=" Download Calls" onclick="javascript: openPage('report_download_calls.php')" />
        	<!--<input type="button" value=" Print Report " onclick="window.print();return false;" />-->
        </form>   
    </td>
  </tr>
  <tr>
    <td><strong>Calls:</strong></td>
  </tr>
  <tr>
    <td>
    
    <table width="1050" border="1">
      <tr align="center">
      	<td width="60" bgcolor="#CCCCCC">Rep Name</td>
        <td width="80" bgcolor="#CCCCCC">Client Name</td>
        <td width="80" bgcolor="#CCCCCC">Practice Name</td>
        <td width="120" bgcolor="#CCCCCC">Modules</td>
        <td width="60" bgcolor="#CCCCCC">Time Spent</td>
        <td width="60" bgcolor="#CCCCCC">Call Time</td>
      </tr>
      
      
 
      <?php echo $TRS_CALL; ?>
    </table>
    
    </td>
  </tr>
  
  
  
</table>
</body>
</html>