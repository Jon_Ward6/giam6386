<?php
include("db_mysql.inc.php");

header("Content-Disposition: filename=edetailer_report_calls.csv");
header("Content-type: application/octetstream");
header("Pragma: no-cache");
header("Expires: 0");

$dbq = new ps_DB;
$dbx = new ps_DB;

$TBL_USER = 'user';
$TBL_PRACTICE = 'practice';
$TBL_CALLS = 'calls';
$TBL_CALLMODULE = 'call_module';
$TBL_MODULE = 'module';
$TBL_REP = 'rep';

if($_REQUEST['action']=='report'){
	getReport();
}
	
//
function getReport(){
	global $_REQUEST, $dbq, $dbx, $TBL_CALLS;
	
	
	$header = "Rep Name,Client Name,Practice Name,Modules,Time Spent,Call Time, ";
	$header .= "\r\n";
	echo $header;

	
	$msg = '';
	
	//CALLS
	$sql_calls  = sprintf("SELECT * FROM $TBL_CALLS WHERE id>0 ORDER By rep_uid");	

	$query_calls = $dbq->query($sql_calls);
	while($dbq->next_record()){
		$userArr = getCallUser($dbq->f('user_uid'));
		$modules = getCallModules($dbq->f('call_uid'));
		$modules = strlen($modules) > 0? $modules : '&nbsp;';
		$rep = getCallRep($dbq->f('rep_uid'));
		
		
		$msg .= '"' . $rep . '",';
		$msg .= '"' . $userArr['firstname'] . ' '. $userArr['lastname'] .'",';
		$msg .= '"' . $userArr['name'] . '",';
		$msg .= '"' . $modules . '",';
		$msg .= '"' . date('i:s', $dbq->f('time_spent')) . '",';
		$msg .= '"' . $dbq->f('end_time') . '",';
		$msg .= "\r\n";
		
	}
	
	
	echo $msg;
	
}



//
function getCallUser($user_uid){
	global $dbx;
	global $TBL_USER,$TBL_PRACTICE;
	
	$userArr = array();
	
	$sql  = sprintf("SELECT * FROM $TBL_USER, $TBL_PRACTICE WHERE $TBL_USER.practice_uid=$TBL_PRACTICE.practice_uid AND $TBL_USER.user_uid='$user_uid'");	

	$query = $dbx->query($sql);
	if($dbx->next_record()){
		$userArr['firstname'] = $dbx->f('firstname');
		$userArr['name'] = $dbx->f('name');
		
	}
	
	return $userArr;
}

function getCallRep($rep_uid){
	global $dbx;
	global $TBL_REP;
	
	//rewr-bgtr-wert-5hjo-qwww
	$sql = sprintf("SELECT * FROM $TBL_REP WHERE rep_uid='$rep_uid'");	

	$query = $dbx->query($sql);
	if($dbx->next_record()){
		return $dbx->f('username');
	}
	
	return '';
}

function getCallModules($call_uid){
	global $dbx;
	global $TBL_CALLMODULE,$TBL_MODULE;
	
	$sql  = sprintf("SELECT * FROM $TBL_CALLMODULE,$TBL_MODULE WHERE $TBL_CALLMODULE.call_uid='$call_uid' AND $TBL_CALLMODULE.module_uid=$TBL_MODULE.module_uid");	
	$query = $dbx->query($sql);
	
	$modules = '';
	while($dbx->next_record()){
		$time_spent = date('i:s', $dbx->f('time_spent'));
		$modules .= $dbx->f('title') . "($time_spent), ";
	}
	
	return $modules;
}




?>