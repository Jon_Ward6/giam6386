<?php

require_once 'jq-config.php';
require_once "jqgrid/jqGrid.php";
require_once "jqgrid/jqGridPdo.php";


// Connection to the server

$conn = new PDO(DB_DSN,DB_USER,DB_PASSWORD);

// Tell the db that we use utf-8

$conn->query("SET NAMES utf8");


// Create the jqGrid instance

$grid = new jqGridRender($conn);


// Write the SQL Query
$TBL_CALLS = 'calls';
$sql  = sprintf("SELECT * FROM $TBL_CALLS WHERE id>0");	




$grid->SelectCommand =  $sql;

// set the ouput format to json

$grid->dataType = 'json';

// Let the grid create the model

$grid->setColModel();

// Set the url from where we obtain the data

$grid->setUrl('php/call-grid.php');

// Set grid caption using the option caption

$grid->setGridOptions(array(

    "caption"=>"eDetailer Calls:",
	
	"width"=>800,
	
	"height"=>240,

    "rowNum"=>10,

    "sortname"=>"firstname",

    "hoverrows"=>true,

    "rowList"=>array(10,20,50),

    ));

// Change some property of the field(s)
/*
$grid->setColProperty("OrderID", array("label"=>"ID", "width"=>60));

$grid->setColProperty("OrderDate", array(

    "formatter"=>"date",

    "formatoptions"=>array("srcformat"=>"Y-m-d H:i:s","newformat"=>"m/d/Y")

    )

);
*/
// Enjoy

$grid->renderGrid('#grid','#pager',true, null, null, true,true);
//$grid->renderGrid('#grid','#pager',true, null, null, false,false);

$conn = null;

?>