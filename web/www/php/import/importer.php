<?php
include("db_mysql.inc.php");

$dbq = new ps_DB;
$dbx = new ps_DB;
$dbw = new ps_DB;

$TBL_PRACTICE = 'xdata_practice';
$TBL_REP = 'xdata_rep';
$TBL_USER = 'xdata_user';

//target
$TBL_NSW = 'xdata_MKIANI_nsw';
$TBL_SANT = 'xdata_HPAPA_sant';
$TBL_VT = 'xdata_GSAAD_victas';
$TBL_WA = 'xdata_MMCILWAIN_wa';
$TBL_QLD = 'xdata_CMCCALL_qld';
$TBL_QLD2 = 'xdata_APEASE_qld2';
$TBL_NZ = 'xdata_JBETHELL_nz';


if($_REQUEST['action']=='import'){
	importData();
}



//
function importData(){
	global $_REQUEST, $dbq, $dbx, $dbw;
	global $TBL_REP, $TBL_PRACTICE, $TBL_USER;
	global $TBL_NSW, $TBL_QLD, $TBL_QLD2, $TBL_SANT, $TBL_VT, $TBL_WA, $TBL_NZ;
	
	$state = $_REQUEST['state'];
	
	$TBL = '';
	
	if($state=='nsw') $TBL = $TBL_NSW;
	if($state=='qld') $TBL = $TBL_QLD;
	if($state=='qld2') $TBL = $TBL_QLD2;
	if($state=='sant') $TBL = $TBL_SANT;
	if($state=='vt') $TBL = $TBL_VT;
	if($state=='wa') $TBL = $TBL_WA;
	if($state=='nz') $TBL = $TBL_NZ;
	
	$rep_uid = '';
	if($state=='nsw') $rep_uid = 'Ambisome-NSW-MaggieKiani';
	if($state=='qld') $rep_uid = 'Ambisome-QLD-CandiceMcCall';
	if($state=='qld2') $rep_uid = 'Ambisome-Qld-AmandaPease';
	if($state=='sant') $rep_uid = 'Ambisome-SaNt-HelenPapazaharoudakis';
	if($state=='vt') $rep_uid = 'Ambisome-VicTas-GeorgeSaad';
	if($state=='wa') $rep_uid = 'Ambisome-WA-MargotMcIlwain';
	if($state=='nz') $rep_uid = 'Ambisome-NZ-JillBethell';
	
	
	if($TBL=='') return;
	if($rep_uid=='') return;
	if(!isset($state)) return;
	
	
	
	$sql  = sprintf("SELECT * FROM $TBL WHERE lastname !=''");
	
	$query = $dbq->query($sql);
	while($dbq->next_record()){
		//practice
		$practice_uid = randUID();//random
		$name = $dbq->f('account');
		$street = $dbq->f('address');
		$suburb = $dbq->f('town');
		$postcode = $dbq->f('postcode');
		$state = $dbq->f('state');
		$region_code = '';//empty
		
		$sql_practice  = sprintf("INSERT INTO $TBL_PRACTICE (practice_uid,name,street,suburb,postcode,state,region_code) 
							  VALUES ('%s','%s','%s','%s','%s','%s','%s')", 
							  $practice_uid,$name,$street,$suburb,$postcode,$state,$region_code);
		
		$dbx->query($sql_practice);
						  
		//user
		$user_uid = randUID();//random
		//$rep_uid = '';
		//$practice_uid = '';
		$firstname = $dbq->f('firstname');
		$lastname = $dbq->f('lastname');
		$email = $dbq->f('email');
		$status = '';//empty
		
		$sql_user  = sprintf("INSERT INTO $TBL_USER (user_uid,rep_uid,practice_uid,firstname,lastname,email,status,date_creation,date_update) 
							  VALUES ('%s','%s','%s','%s','%s','%s','%s',NOW(),NOW())", 
							  $user_uid,$rep_uid,$practice_uid,$firstname,$lastname,$email,$status);
		
		$dbw->query($sql_user);
	}
	//
	echo "success! for " . $state;
}

function randUID() { 
    $s = strtoupper(md5(uniqid(rand(),true))); 
    $guidText = 
        //substr($s,0,8) . '-' . 
		'default' . '-' .
        substr($s,8,4) . '-' . 
        substr($s,12,4). '-' . 
        substr($s,16,4). '-' . 
        substr($s,20); 
    return $guidText;
}

?>
