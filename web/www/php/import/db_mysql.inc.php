<?php

$global_vars = array(
//Database connection Details
"DB_HOST" => "localhost",
"DB_USER" => "ambisome_ward6",
"DB_PWD" => "ward6666Six",
"DB_NAME" => "ambisome_edetailer"
);

    // Globalize everything for later use
while (list($key, $value) = each($global_vars)) {
  define($key, $value);
}


/***********************************************************************

mySQL Database Access Class

Methods in the class are:

query($q) - Established connection to database and runs the query returning
            a query ID if successfull.

next_record() - Returns the next row in the RecordSet for the last query run.
                Returns False if RecordSet is empty or at the end.

num_rows()  -Returns the number of rows in the RecordSet from a query.

f($field_name) - Returns the value of the given field name for the current
                 record in the RecordSet.

sf($field_name) - Returns the value of the field name from the $vars variable
                  if it is set, otherwise returns the value of the current
                  record in the RecordSet.  Useful for handling forms that have
                  been submitted with errors.  This way, fields retain the values
                  sent in the $vars variable (user input) instead of the database
                  values.

p($field_name) - Prints the value of the given field name for the current
                 record in the RecordSet.

sp($field_name) - Prints the value of the field name from the $vars variable
                  if it is set, otherwise prints the value of the current
                  record in the RecordSet.  Useful for handling forms that have
                  been submitted with errors.  This way, fields retain the values
                  sent in the $vars variable (user input) instead of the database
                  values.


************************************************************************/

class ps_DB {

	//Database connection Details
  	var $DB_HOST;
  	var $DB_USER;
  	var $DB_PWD;
  	var $DB_NAME;
  	var $lid = 0;           // Link ID for database connection
  	var $qid = 0;           // Query ID for current query
  	var $row;               // Current row in query result set
  	var $record = array();  // Current row record data
  	var $error = "";        // Error Message
  	var $errno = "";        // Error Number
  	var $last_q = "";		// Last Query (for debugging)
  	var $affected = 0;
  	var $obj;

	//constructor
	function ps_DB()
	{
		$this->DB_HOST = DB_HOST;
		$this->DB_USER = DB_USER;
		$this->DB_PWD = DB_PWD;
		$this->DB_NAME = DB_NAME;
	}
	//
	function set($host,$db,$user,$pwd)
	{
		$this->DB_HOST = $host;
		$this->DB_USER = $user;
		$this->DB_PWD = $pwd;
		$this->DB_NAME = $db;
	}
	//
	function test()
	{
	    if (!$this->connect()) {
	    	return false;
	    }
		return true;

	}
	
  	// PRIVATE
  	// Connects to DB and returns DB lid
  	function connect() 
  	{
	    if ($this->lid == 0) {
	    	$this->lid = mysql_pconnect($this->DB_HOST,$this->DB_USER,$this->DB_PWD);
	      	if (!$this->lid) {
	        	$this->halt("connect(" . $this->DB_HOST . "," . $this->DB_USER . ",PASSWORD)  failed.");
	      	}
	
	      	if (!@mysql_select_db($this->DB_NAME,$this->lid)) {
	        	$this->halt("Cannot connect to database ".$this->DB_NAME);
	        	return 0;
	      	}
	    }
	    return $this->lid;
  	}


    // PUBLIC
    // Runs query and sets up the query id for the class.
    function query($q) {

	    if (empty($q)) return 0;
	
	    if (!$this->connect()) return 0;
	
	    if ($this->qid) {
	    	@mysql_free_result($this->qid);
	      	$this->qid = 0;
	    }
	
	    $this->qid   = @mysql_query($q, $this->lid);
	    $this->row   = 0;
	    $this->errno = mysql_errno();
	    $this->error = mysql_error();
		$this->affected = mysql_affected_rows();
		
	    if (!$this->qid) {
	    	$this->halt("Invalid SQL: ".$q);
	    }
		$this->last_q = $q;
	
	    return $this->qid;
    }


    // PUBLIC
    // Return next record in result set
    function next_record() {

	    if (!$this->qid) {
	    	$this->halt("next_record called with no query pending.");
	      	return 0;
	    }
	
	    $this->record = @mysql_fetch_array($this->qid);
	    $this->row   += 1;
	    $this->errno  = mysql_errno();
	    $this->error  = mysql_error();
	
	    $stat = is_array($this->record);
	    return $stat;
    }
    
    //
    function next_record_object() {

	    if (!$this->qid) {
	    	$this->halt("next_record called with no query pending.");
	      	return 0;
	    }
	
	    $this->obj = @mysql_fetch_object($this->qid);
	    $this->row   += 1;
	    $this->errno  = mysql_errno();
	    $this->error  = mysql_error();
	
	    //$stat = is_array($this->obj);
	    return $this->obj;
    }

  	// PUBLIC
  	// Field Value
    function f($field_name) {
    	return $this->record[$field_name];// REMOVED 'stripslashes'
    }

    // PUBLIC
    // Selective field value
    function sf($field_name) {
	    global $vars, $default;
	
	    if ($vars["error"] and $vars["$field_name"]) {
	    	return stripslashes($vars["$field_name"]);
	    } elseif ($default["$field_name"]) {
	    	return stripslashes($default["$field_name"]);
	    } else {
	    	return stripslashes($this->record[$field_name]);
	    }
    }

    // PUBLIC
    // Print field
    function p($field_name) {
    	print stripslashes($this->record[$field_name]);
  	}

  	// Selective print field
  	// PUBLIC
  	function sp($field_name) {
	    global $vars, $default;
	
	    if ($vars["error"] and $vars["$field_name"]) {
	    	print stripslashes($vars["$field_name"]);
	    } elseif ($default["$field_name"]) {
	    	print stripslashes($default["$field_name"]);
	    } else {
	    	print stripslashes($this->record[$field_name]);
	    }
  	}

  	// Returns the number of rows in query
  	function num_rows() {
	    if ($this->lid) {
	    	return @mysql_numrows($this->qid);
	    }else{
	    	return 0;
	    }
  	}
	//
   	function last_id() {
	    if ($this->lid) {
	    	return @mysql_insert_id($this->lid);
	    }else{
	    	return 0;
	    }
  	}
  	//
   	function last_q() {
     	return $this->last_q;
  	}

    // PRIVATE
    // Halt and display error message
    function halt($msg) {
	    $this->error = @mysql_error($this->lid);
	    $this->errno = @mysql_errno($this->lid);
		error_log("MySQL error #" . $this->errno . " : " . $this->error,0);
	  
	    //exit;

    }
    
    //
  	function get_fields($table) {
  		
	    if (!$this->connect()) {
	      return false;
	    }
	
	  	$fields = @mysql_list_fields($this->DB_NAME, $table, $this->lid);
	    $columns = @mysql_num_fields($fields);
		$rec = array();
	    for ($i = 0; $i < $columns; $i++) {
			$rec[@mysql_field_name($fields, $i)] = @mysql_field_type($fields, $i);
	    }
		return $rec;
  	}
  	
	// must be called BEFORE next_record()
  	function get_result_fields(){
	    if ($this->lid) {
	    	$i = 0;
		  	$rec = array();
		 	while ($i < mysql_num_fields($this->qid)) {
				//echo "Information for column $i:<br />\n";
				$meta = mysql_fetch_field($this->qid);
			    if (!$meta) {
				    //echo "No information available<br />\n";
					return 0;
			    }
				$rec[$i]=$meta->name;
				$i++;
		  	} 
		  	return $rec;
		}
	    return 0;
  	}
  	
	// must be called BEFORE next_record()
  	function get_result_fields_keys(){
	    if ($this->lid) {
	    	$i = 0;
		  	$rec = array();
		  	while ($i < mysql_num_fields($this->qid)) {
				//echo "Information for column $i:<br />\n";
				$meta = mysql_fetch_field($this->qid);
			    if (!$meta) {
				    //echo "No information available<br />\n";
					return 0;
			    }
				
				$rec[$meta->name]=$i;
				$i++;
		  	} 
		  	return $rec;
		}
	    return 0;
  	}
  
  	// Field Value
  	// PUBLIC
    function r() {
    	return $this->record;// REMOVED 'stripslashes'
  	}
  	
  	//
  	function affected() {
		return $this->affected;
  	}
  	
  	//
  	function error(){
		return $this->error;
  	}
	
  	//
	function insert($table,$arr) {
		$q = "insert into $table (`" . implode("`,`",array_keys($arr)) . "`) values ('" . implode("','",array_values($arr)) . "')";
		if(!$this->query($q)){
			return false;
		}
		if($this->affected() != 1){
			return false;
		}
		return $this->last_id();
	}
	
	//
	function update($table,$arr,$where) {
		$q = "update $table set `" . $this->implode_with_keys("',`",$arr,"` = '") . "'" . " where $where";
		if(!$this->query($q))
		{
			return false;
		}
		return true;
	}
	
	// eg. implode_with_keys("',",$arr," = '") . "'"
	// creates key1 = 'val1', key2 = 'val2' etc
	function implode_with_keys($glue, $array, $padding = '') {
	       $output = array();
	       foreach( $array as $key => $item )
	               $output[] = $key . $padding . $item;
	
	       return implode($glue, $output);
	}

};

?>
