<?php

require_once 'jq-config.php';

// include the jqGrid Class

require_once "jqgrid/jqGrid.php";

// include the PDO driver class

require_once "jqgrid/jqGridPdo.php";




// Connection to the server

$conn = new PDO(DB_DSN,DB_USER,DB_PASSWORD);

// Tell the db that we use utf-8

$conn->query("SET NAMES utf8");


// Create the jqGrid instance

$grid = new jqGridRender(NULL);

/*
// Write the SQL Query
$TBL_USER = 'user';
$TBL_PRACTICE = 'practice'; 
$sql  = sprintf("SELECT firstname,lastname, name 
				FROM $TBL_USER, $TBL_PRACTICE
 				WHERE $TBL_USER.practice_uid=$TBL_PRACTICE.practice_uid");	




$grid->SelectCommand =  $sql;
*/
// set the ouput format to json

$grid->dataType = 'json';

// Let the grid create the model

$model = array(
    array("name"=>"firstname","width"=>80),
    array("name"=>"lastname","width"=>80),
	array("name"=>"name","width"=>80)
);

$grid->setColModel($model);
// Set the url from where we obtain the data

//$grid->setUrl('php/user-grid.php');

// Set grid caption using the option caption

$grid->setGridOptions(array(

    "caption"=>"eDetailer Users:",
	
	"width"=>800,
	
	"height"=>240,

    "rowNum"=>10,

    "sortname"=>"firstname",

    "hoverrows"=>true,

    "rowList"=>array(10,20,50),
	
	"datatype"=>"local"

    ));


// Enjoy
//print_r($rows);
$grid->callGridMethod("#grid", 'addRowData', array("firstname",$rows));
$grid->renderGrid('#grid','#pager',true, null, null, true,true);

$conn = null;

?>