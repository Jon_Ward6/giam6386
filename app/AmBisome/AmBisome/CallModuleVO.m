//
//  CallModuleVO.m
//  eDetailer
//
//  Created by Jonathan Xie on 19/04/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "CallModuleVO.h"


@implementation CallModuleVO

@synthesize rowid, call_uid, module_uid, user_uid, time_spent;


- (void)dealloc {
    [module_uid release];
    [user_uid release];
    [call_uid release];
    [super dealloc];
}


@end
