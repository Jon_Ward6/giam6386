//
//  UserVO.m
//  Demo
//
//  Created by Jonathan Xie on 29/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "UserVO.h"


@implementation UserVO

@synthesize rowid, user_uid, rep_uid, practice_uid, firstname, lastname, email, phone, status;

- (void)dealloc {
    [practice_uid release];
    [rep_uid release];
    [user_uid release];
    [status release];
    [firstname release];
    [lastname release];
    [email release];
    [phone release];
    [super dealloc];
}


-(UserVO *)dataSet:(NSDictionary*)dict{
    firstname = [dict valueForKey:@"firstname"];
    lastname = [dict valueForKey:@"lastname"];
    email = [dict valueForKey:@"email"];
    phone = [dict valueForKey:@"phone"];
    //status = [dict valueForKey:@"status"];
    
    return self;
}


@end
