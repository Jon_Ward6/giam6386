//
//  CallModuleVO.h
//  eDetailer
//
//  Created by Jonathan Xie on 19/04/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CallModuleVO : NSObject {
    NSInteger rowid;
    NSString * call_uid;
    NSString * module_uid;
    NSString * user_uid;
    double time_spent;
}

@property (nonatomic, assign) NSInteger rowid;
@property (nonatomic, retain) NSString * call_uid;
@property (nonatomic, retain) NSString * module_uid;
@property (nonatomic, retain) NSString * user_uid;
@property (nonatomic, assign) double time_spent;



@end
