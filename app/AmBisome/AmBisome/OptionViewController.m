//
//  OptionViewController.m
//  AtriplaED
//
//  Created by Jonathan Xie on 8/06/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import "OptionViewController.h"
#import "NewCallViewController.h"
#import "StatsViewController.h"
#import "GoogleAnalytices.h"
#import "Model.h"
#import "DataModel.h"
#import "RepVO.h"
#import "JSON.h"
#import "SearchClientViewController.h"

@implementation OptionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [model release], model=nil;
    [dataModel release],dataModel=nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    model = [Model sharedModel];
    dataModel = [DataModel sharedDataModel];
    
    //Google Analytices
    [GoogleAnalytices trackPageView:@"/app_login_option"];
    
    //sync data here to make sure we have enough time to have the callback from server!
    //and not interfering with the UI drawing
    //should move this action to 'Sync' button!
    //[self performSelector:@selector(syncData) withObject:NO afterDelay:1.0];
    //[dataModel downloadAllData:model.repVO.rep_uid];
}

-(void)syncData{
    //NSLog(@"called");
    //[dataModel downloadAllData:model.repVO.rep_uid];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

-(IBAction)onDemo:sender{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.60];
    
    CGRect frame = self.navigationController.view.frame;
    frame.origin.y = -(768+30);
    self.navigationController.view.frame = frame;
    
    
    [UIView commitAnimations];
    
    //Google Analytices
    [GoogleAnalytices trackPageView:@"/app_login_demo"];

    
}

-(IBAction)onCall:sender{
    
    SearchClientViewController * nextView = [[SearchClientViewController alloc] init]; 
    [self.navigationController pushViewController:nextView animated:YES];
    [nextView release];
    
    
    
}


-(IBAction)onStats:sender{
    StatsViewController * nextView = [[StatsViewController alloc] initWithNibName:@"StatsViewController" bundle:nil];
    
    [self.navigationController pushViewController:nextView animated:YES];
    
    [nextView release];
}





@end
