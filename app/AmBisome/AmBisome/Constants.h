//
//  Constants.h
//
//  Created by Jono Xie on 15/06/10.
//  Copyright 2010 MarchDigital. All rights reserved.
//
extern NSString * const	APP_NAME;
extern NSString * const	APP_VERSION;
extern NSString * const WEBSERVICE_URL;
extern NSString * const SERVER_NAME;
extern NSString * const GANTrackerID;
//
extern int const SCREEN_WIDTH;
extern int const SCREEN_HEIGHT;

extern int const MODULE_NUMBER;

extern int const kGANDispatchPeriodSec;

