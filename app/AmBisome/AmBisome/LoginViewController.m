//
//  LoginViewController.m
//  AtriplaED
//
//  Created by Jonathan Xie on 8/06/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import "LoginViewController.h"
#import "OptionViewController.h"
#import "UserVO.h"
#import "Model.h"
#import "DataModel.h"
#import "RepVO.h"
#import "Constants.h"
#import "JSON.h"
#import "UIView+Extensions.h"
#import "GoogleAnalytices.h"
#import "KeyCodeLoginViewController.h"


@implementation LoginViewController

@synthesize usernameText,passwordText;
@synthesize currentControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    dataModel.delegate = nil;
    [model release], model=nil;
    [dataModel release],dataModel=nil;
    [usernameText release], usernameText=nil;
    [passwordText release], passwordText=nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    model = [Model sharedModel];
    dataModel = [DataModel sharedDataModel];
    dataModel.delegate = self;

    //launch the block out
    if([self getSecurityCount]>=5){
        [self openKeycodeScreen];
    }
    
    
    

    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //clean up - if timeout!
    usernameText.text = @"";
    passwordText.text = @"";
    [usernameText resignFirstResponder];
    [passwordText resignFirstResponder];
    currentControl = nil;
    //
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    self.view.frame = frame;
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}


-(IBAction)onNext:sender{
    
    
    RepVO* rep = [dataModel repSelectOne:usernameText.text passwd:passwordText.text];
    
    
    if(rep.access_level>0){
        
        //cache
        model.repVO = rep;
        
        //save username
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setValue:rep.username forKey:@"username"];
        [prefs synchronize];
        
        //reset the count to 0
        [self setSecurityCount:YES];
        
        
        //If it's new installation, we need to sync back to data from server
        //Changed: move the next screen!
        NSArray * users = [dataModel userSelectAll:model.repVO.rep_uid];
        if([users count]==0){
            
            //[dataModel downloadAllData:model.repVO.rep_uid];
            
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
            
            dispatch_async(queue, ^{
                [dataModel downloadAllData:model.repVO.rep_uid];
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    //don't have to be anything to sync
                });
            });
            
            
            
            
            
        }
        
        
        OptionViewController * optionView = [[OptionViewController alloc] init]; 
        [self.navigationController pushViewController:optionView animated:YES];
        [optionView release];
        
        
    }else{
        
        
        //If can't find the rep locally, we should try remote server
        if([model isWebSiteReachable:SERVER_NAME]){
            NSMutableDictionary *repDict = [[[NSMutableDictionary alloc] init]autorelease];
            [repDict setValue:usernameText.text forKey:@"username"];
            [repDict setValue:passwordText.text forKey:@"password"];
        
            [dataModel serverRequestWithAction:@"getrep" andData:[repDict JSONRepresentation]];
            
            return;
        }else{
            //make a delay call here to wait for the server callback
            //[self performSelector:@selector(loginErrorMessage) withObject:NO afterDelay:0.5];
            //[self loginErrorMessage];
        }
        
        [self loginErrorMessage];
    }
    
    
    
    //Google Analytices
    [GoogleAnalytices trackCustomVariable:@"logged_in_by" value:usernameText.text index:1];
    [GoogleAnalytices trackPageView:@"/app_login"];
    
}


//DataModelDelegate
- (void)dataModelDidFinishLoading:(DataModel*)dataModel data:(id)data{
    
     
    if([data isKindOfClass:[RepVO class]]){
        //if we have already login
        if(model.repVO) return;
        //
        RepVO * rep = (RepVO*)data;
        
        //reset the count to 0
        [self setSecurityCount:YES];
        
        //save the user name
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setValue:rep.username forKey:@"username"];
        [prefs synchronize];
        

        
        if(rep.access_level>0){
            //cache
            model.repVO = rep;

            
            //empty! lets sync data back from server
            [self syncUserData:rep];
            
            //go next view
            OptionViewController * optionView = [[OptionViewController alloc] init]; 
            [self.navigationController pushViewController:optionView animated:YES];
            [optionView release];
            
        }else{
            //NSLog(@"no permission to access");
            
            [self loginErrorMessage];
        }
    }
}


// delegate function
- (void)dataModel:(DataModel*)model didFailWithError:(NSError *)error{
    /*
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please check your internet connection!" 
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [alert release];
     */
    
    [self loginErrorMessage];
    
}


-(void)syncUserData:(RepVO*)repVO{
    NSArray * users = [dataModel userSelectAll:repVO.rep_uid];
    if([users count]==0){
        [dataModel downloadAllData:model.repVO.rep_uid];
    }
}


-(void)loginErrorMessage{
    if(model.repVO==nil){
        

        NSString * msg = @"Wrong username or password";
        
        //the counter ++
        [self setSecurityCount:NO];
        
        NSInteger failCount = [self getSecurityCount];
        if(failCount==3) msg = @"Wrong username or password! \nPlease note: \nThe app will lock up after 5 failed login attempts.";
        if(failCount==4) msg = @"Wrong username or password! \nThe app will lock up after one more failed login attempt.";
        
        if(failCount>=5){
            //launch the block out screen with keycode login
            [self openKeycodeScreen];
            
            //notify to admin
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSInteger failEmail = [prefs integerForKey:@"logInFailEmail"];
            
            if(failEmail!=1){
                
                NSMutableDictionary *repDict = [[[NSMutableDictionary alloc] init]autorelease];
                [repDict setValue:[prefs stringForKey:@"username"] forKey:@"username"];
                
                [dataModel serverRequestWithAction:@"alertEmail" andData:[repDict JSONRepresentation]];
                
                [prefs setInteger:1 forKey:@"logInFailEmail"];
                [prefs synchronize];
            }
            
            
            
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg 
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        
    }
}

-(void)openKeycodeScreen
{
    KeyCodeLoginViewController * keyCodeViewController = [[KeyCodeLoginViewController alloc] initWithNibName:@"KeyCodeLoginViewController" 
                                                                                                      bundle:nil];
    [self.navigationController pushViewController:keyCodeViewController animated:NO];
    [keyCodeViewController release];
}


-(NSInteger)getSecurityCount{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    return [prefs integerForKey:@"logInFailCount"];
    
}

-(void)setSecurityCount:(BOOL)success{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSInteger currentFailCount = [prefs integerForKey:@"logInFailCount"];
    
    //
    if(success==YES){
        
        [prefs setInteger:0 forKey:@"logInFailCount"];
        [prefs synchronize];
        //currentFailCount = 0;
    }else{
        currentFailCount +=1;
        [prefs setInteger:currentFailCount forKey:@"logInFailCount"];
        [prefs synchronize];
    }
    
    [prefs synchronize];
    
}







-(IBAction)onBackgroundButton:sender{
    [usernameText resignFirstResponder];
    [passwordText resignFirstResponder];
}






-(BOOL) textFieldShouldReturn:(UITextField*) textField {
    //
    //[textField resignFirstResponder]; 
    //[self onNext:nil];
    //return YES;
    
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
        [self onNext:nil];
    }
    return NO; 
}


- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
	currentControl = textField;
	return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField { 
	[self.view maintainVisibityOfControl:textField toView:self.view offset:0.0f];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	if (textField == currentControl) {
		//If the textfield is still the same one, we can reset the view animated
		[self.view resetViewToIdentityTransform];			
	}else {
        
	}
}



@end
