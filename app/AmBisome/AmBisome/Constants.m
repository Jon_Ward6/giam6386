//
//  Constants.m
//
//  Created by Jono Xie on 15/06/10.
//  Copyright 2010 MarchDigital. All rights reserved.
//

#import "Constants.h"

NSString * const APP_NAME = @"Ambisome";
NSString * const APP_VERSION = @"1.0.0";
NSString * const WEBSERVICE_URL = @"http://ambisome.w6digital.com.au/admin/index.php/remote/";
NSString * const SERVER_NAME = @"ambisome.w6digital.com.au";
NSString * const GANTrackerID = @"";//@"UA-10174902-15";
//
int const SCREEN_WIDTH = 1024;
int const SCREEN_HEIGHT = 768;
int const MODULE_NUMBER = 2;

int const kGANDispatchPeriodSec = 10;

