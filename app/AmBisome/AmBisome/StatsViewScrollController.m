//
//  StatsViewScrollController.m
//  eDetailer
//
//  Created by Jonathan Xie on 28/04/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "StatsViewScrollController.h"
#import "StatsViewSingleController.h"
#import "StatsViewController.h"
#import "DataModel.h"
#import "CallVO.h"
#import "PracticeVO.h"
#import "Model.h"

@implementation StatsViewScrollController

@synthesize scrollView, userCallArray, user_uid, callLabel;
@synthesize userName, practiceName, address, email,phone;
@synthesize statsViewController=statsViewController_;

- (id)initWithUserId:(NSString*)uid
{
    self = [super init];
    if (self) {
        self.user_uid = uid;
    }
    return self;
}



- (void)dealloc
{
    dataModel = nil;
    [statsViewController_ release], statsViewController_=nil;
    [userName release];
    [practiceName release];
    [address release];
    [email release];
    [phone release];
    
    [user_uid release];
    [userCallArray release];
    [scrollView release];
    [callLabel release];

    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    dataModel = [DataModel sharedDataModel];
    
    if(self.user_uid>0){
        self.userCallArray = [[DataModel sharedDataModel] callSelectByUserId:self.user_uid];
        self.userCallArray = (NSMutableArray*)[[self.userCallArray reverseObjectEnumerator] allObjects];//reverse!
    }
    
    int count = [self.userCallArray count];
    
    
    
    self.scrollView.pagingEnabled = YES;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * count, 1);
    self.scrollView.showsHorizontalScrollIndicator = NO;
    //scrollView.showsVerticalScrollIndicator = NO;
    //scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
	self.scrollView.contentMode = UIViewContentModeScaleAspectFit;
	self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.scrollView.minimumZoomScale = (self.scrollView.frame.size.width / self.scrollView.contentSize.width) / 0.99;
    //
    self.scrollView.delaysContentTouches = YES;
    self.scrollView.canCancelContentTouches = NO;
    
    
    
    //add views
    StatsViewSingleController * statsController = nil;
    
    int i;
    for(i=0;i<count;i++){
        //still can't release this!
        statsController = [[StatsViewSingleController alloc] initWithCallObject:[self.userCallArray objectAtIndex:i] 
                                                                          label:[NSString stringWithFormat:@"Call %d of %d", i+1, count]];
        statsController.view.frame = CGRectMake(self.scrollView.frame.size.width * i, 0.0,self.scrollView.frame.size.width, self.scrollView.frame.size.height);
        
        statsController.view.tag = i+1;
        [self.scrollView addSubview:statsController.view];
        
        
        //[statsController release];
        
    }
    
    
    //display the first item from the array
    [self loadCallData:0];
    
}


//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView {
    
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) /  pageWidth) + 2;
    
    //
    [self loadCallData:page-1];
}


-(void)loadCallData:(NSInteger)index
{
    //NOTE:: better to use the 'self.user_id' directly!

    callLabel.text = [NSString stringWithFormat:@"Call %d of %d", index+1, [self.userCallArray count]];
    
    UserVO * userVO = [dataModel userSelectOne:self.user_uid];
    PracticeVO * practiceVO = [dataModel practiceSelectOne:userVO.practice_uid];
    
    
    userName.text = [NSString stringWithFormat:@"%@ %@", userVO.firstname, userVO.lastname];
    practiceName.text = practiceVO.name;
    address.text = [NSString stringWithFormat:@"%@ %@", practiceVO.street, practiceVO.suburb];
    phone.text = userVO.phone;
    
    email.text = [[Model sharedModel] validateEmail:userVO.email]? userVO.email : @"";
    
}

-(IBAction)backBtn:(id)sender{

    [self.statsViewController showAll:nil];
    
}

@end

