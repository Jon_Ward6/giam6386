//
//  MainViewQuickAccessController.m
//  AtriplaED
//
//  Created by Jonathan Xie on 27/06/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import "MainViewQuickAccessController.h"
#import "MainViewQuickAccessMenuController.h"
#import "Model.h"
#import "MainViewPageController.h"
#import "MainViewContentController.h"
#import "MainViewController.h"
#import "PageVO.h"
#import "ModuleVO.h"
#import "MainViewQuickAccessMenuController.h"

@implementation MainViewQuickAccessController
@synthesize scrollView, contentArr, pageControllers;
@synthesize menuViewController,closeBtn;
@synthesize currentPageIndex;


- (id)initWithContent:(NSArray *)anArray scrollEnabled:(BOOL)scroll
{
    self = [super init];
    if (self) {
        isScrollableContent = scroll;
        self.contentArr = anArray;
        
        //reset the scrollview first
        [self resetScrollView];
        
        //load new contents and configure the scrollview
        [self configScrollView];
    }
    return self;
}




- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"launchQuickAccessView" object:nil];
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    //[closeBtn release];
    closeBtn = nil;
    [menuViewController release], menuViewController=nil;
    [pageControllers release], pageControllers=nil;
    [scrollView release],scrollView=nil;
    [contentArr release],contentArr=nil;
    //[model release]; // can't release here cos we need model as the reference to the views!
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    model = [Model sharedModel];
    model.mainViewQuickAccessController = self;
    
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-module.png"]];
    
    menuViewController = [[MainViewQuickAccessMenuController alloc] initWithNibName:@"MainViewQuickAccessMenuController" bundle:nil];
    [self.view addSubview:menuViewController.view];
    //[menuViewController release];
    
    //
    closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(1000-22, -10, 40, 70);
    [closeBtn setImage:[UIImage imageNamed:@"btn-close-quickacess.png"] forState:UIControlStateNormal];
    // add targets and actions
    [closeBtn addTarget:self action:@selector(closeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:closeBtn];
    
    
    //add listener
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationListener:) name:@"launchQuickAccessView" object:nil];
    
}

-(void)notificationListener:(NSNotification *)notification{
    NSDictionary *userInfo = [notification userInfo];
    NSArray * arr = [userInfo objectForKey:@"pageArray"];
    NSString * action = [userInfo objectForKey:@"action"];

    if([action isEqualToString:@"quickaccess"] && [arr count]>0){
        [self showContent:arr scrollEnabled:YES];
    }
    
    if([action isEqualToString:@"nextPage"]){
        [self nextPage];
    }
    
    if([action isEqualToString:@"prevPage"]){
        [self prevPage];
    }
}






-(void)closeBtnClicked:sender
{
    if([self.menuViewController.view alpha]==0){
        
        //go back to menu instead
        [self showMenu:YES animated:NO];
    }else {
        //close the whole quickaccess screen itself!
        if(model.mainViewContentController)[model.mainViewContentController showQuickAccessView:NO];
        if(model.mainViewController)[model.mainViewController showQuickAccessView:NO];
    }
}


#pragma mark - logics

-(void)showMenu:(BOOL)flag animated:(BOOL)animated{
    model.mainViewQuickAccessController = self;
    //if(self.view.tag==200) model.mainViewQuickAccessController = self;
    //if(self.view.tag==201) model.mainViewContentQuickAccessController = self;
    
    [self resetScrollView];
    
    
    if(animated){
        if(!flag){
            menuViewController.view.alpha = 1.0;
            [UIView animateWithDuration:0.50
                             animations:^{ 
                                 //dashboard
                                 CGRect navFrame = menuViewController.view.frame;
                                 navFrame.origin.y = -menuViewController.view.frame.size.height;
                                 menuViewController.view.frame = navFrame;
                                 
                             }
                             completion:^(BOOL finished){
                                 menuViewController.view.alpha = 0.0;
                             }];
            
        }else{
            
            menuViewController.view.alpha = 1.0;
            [UIView animateWithDuration:0.50
                             animations:^{ 
                                 //dashboard
                                 CGRect navFrame = menuViewController.view.frame;
                                 navFrame.origin.y = 0;
                                 menuViewController.view.frame = navFrame;

                             }];
        }
    }else{
        CGRect navFrame = menuViewController.view.frame;
        navFrame.origin.y = 0;
        menuViewController.view.frame = navFrame;
        
        [menuViewController.view setAlpha:flag];
    }
}

-(void)showContent:(NSArray *)anArray scrollEnabled:(BOOL)scroll{
    
    //hide menu
    [self showMenu:NO animated:YES];
    
    if(model.mainViewContentController!=nil) [model.mainViewContentController showDashBoard:NO];
    if(model.mainViewController!=nil)        [model.mainViewController showDashBoard:NO];
    
    //data
    isScrollableContent = scroll;
    self.contentArr = anArray;
    
    
    //make sure we scroll back to the beginning!
    [scrollView scrollRectToVisible:CGRectMake(0.0f, 0.0f, scrollView.frame.size.width, scrollView.frame.size.height) animated:NO];
    
    //reset the scrollview first
    [self resetScrollView];
    
   
    model.mainViewQuickAccessController = self;
    
    
    //load new contents and configure the scrollview
    [self configScrollView];
}


-(void)resetScrollView{
    
    NSArray *subviews = [[scrollView subviews] copy];
    for (UIView *subview in subviews) {
        //if([subview isKindOfClass:[MainViewPageController class]]) {
        [subview removeFromSuperview];
        //}
    }
    [subviews release];
    //
    self.pageControllers = nil;
}


-(void)configScrollView{
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    NSUInteger count = [self.contentArr count];
    
    
    for (unsigned i = 0; i < count; i++) {
        [controllers addObject:[NSNull null]];
    }
    self.pageControllers = controllers;
    [controllers release];
    //
    scrollView.pagingEnabled = YES;
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * count, scrollView.frame.size.height);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = NO;
    scrollView.delegate = self;
	scrollView.contentMode = UIViewContentModeScaleAspectFit;
	scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	scrollView.minimumZoomScale = (scrollView.frame.size.width / scrollView.contentSize.width) / 0.99;
    scrollView.bounces = NO;
    
    
    //custom
    scrollView.scrollEnabled = isScrollableContent;

    
    //
    [self loadScrollViewWithPage:0];
	[self loadScrollViewWithPage:1];
    
    //start tracking
    [self trackPage:0];
    
}

- (void)loadScrollViewWithPage:(int)page
{
    
    if(page < 0) return;
	if(page >=[self.contentArr count]) return;
    
	
    //this is for static(web) content page
    MainViewPageController *controller = [pageControllers objectAtIndex:page];
	
    if ((NSNull *)controller == [NSNull null]) {
        
		controller = [[MainViewPageController alloc] initWithPage:[self.contentArr objectAtIndex:page]];
        [pageControllers replaceObjectAtIndex:page withObject:controller];
		
        [controller release];
    }
    
    
	
    // add the controller's view to the scroll view
    if (nil == controller.view.superview) {
        CGRect frame = scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        
		
        [scrollView addSubview:controller.view];
		
    }
    
	controller.view.tag = page+100;
    
    
    //force update on every page! - MainViewPageController
    if(page==currentPageIndex){
        NSString * action = (page==currentPageIndex)? @"onScreen" : @"offScreen";
        [controller notifyPageAction:action];
    }
    
}







#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    //[closeBtn setAlpha:0];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
    //[closeBtn setAlpha:0.2];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView
{
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    [self trackPage:page];
    
    currentPageIndex = page;
    
	
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    
}




//ADD::page time tracking
- (void)trackPage:(NSInteger)page {
    //0. filter
    if(page <0) return;//out of bound
	if(page >=[self.contentArr count]) return;//out of bound
    //if(page == currentPageIndex && page !=0) return;//same page!
    
    
    //1. end the time for last open page
    if(model.quickAccessCurrentPageVO !=nil && model.quickAccessModuleVO != nil){
        
        model.quickAccessCurrentPageVO.endTime = [NSDate date];
        [model.quickAccessModuleVO addPage:model.quickAccessCurrentPageVO];
        //
        //[model.quickAccessCurrentPageVO release];
        
    }

    
    //2. start the new time for current page
    
    if(model.userVO){
        model.quickAccessCurrentPageVO =nil;
        PageVO * myPageVO = (PageVO*)[self.contentArr objectAtIndex:page];
    
        model.quickAccessCurrentPageVO = [[[PageVO alloc] init] autorelease];
        //should add the uid here!
        model.quickAccessCurrentPageVO.module_uid = @"quickaccess";//myPageVO.module_uid;
        model.quickAccessCurrentPageVO.page_uid = myPageVO.page_uid;
        model.quickAccessCurrentPageVO.filename = myPageVO.filename;
        model.quickAccessCurrentPageVO.type = myPageVO.type;
        model.quickAccessCurrentPageVO.startTime = [NSDate date];
        model.quickAccessCurrentPageVO.page_name = myPageVO.page_name;
    }
   
}







//Public navigaton: nextPage / prevPage
-(void)nextPage
{
    [self gotoPage:currentPageIndex + 1];
}

-(void)prevPage
{   
    [self gotoPage:currentPageIndex - 1];
}

-(void)gotoPage:(int)page
{
    
    if(page<[self.pageControllers count] && page>=0){
        
        [self loadScrollViewWithPage:page - 1];
        [self loadScrollViewWithPage:page];
        [self loadScrollViewWithPage:page + 1];
        
        CGRect frame = scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        
        [scrollView scrollRectToVisible:frame animated:YES];
        
        currentPageIndex = page;
    }
    
}


@end
