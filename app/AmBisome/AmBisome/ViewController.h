//
//  ViewController.h
//  AmBisome
//
//  Created by Jonathan Xie on 3/04/12.
//  Copyright (c) 2012 Ward6. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@class MainViewController;
@class LoginViewController;

@interface ViewController : UIViewController {
    UINavigationController * navController_;
    UINavigationController * loginNavController_;
    
    MainViewController * mainView;
    LoginViewController * loginView;
    
	Model * model;
    
}

@property(nonatomic, retain) UINavigationController * navController;
@property(nonatomic, retain) UINavigationController * loginNavController;

@property(nonatomic, retain) MainViewController * mainView;
@property(nonatomic, retain) LoginViewController * loginView;

@end
