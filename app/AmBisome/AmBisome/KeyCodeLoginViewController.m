//
//  KeyCodeLoginViewController.m
//  BeyondHIV
//
//  Created by Jonathan Xie on 6/01/12.
//  Copyright (c) 2012 Ward6. All rights reserved.
//

#import "KeyCodeLoginViewController.h"
#import "UIView+Extensions.h"
#import "DataModel.h"
#import "Model.h"
#import "Constants.h"

@implementation KeyCodeLoginViewController

@synthesize keycodeText;
@synthesize currentControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}
- (void)viewDidUnload
{
    [super viewDidUnload];
    
}

- (void)dealloc
{
    dataModel.delegate = nil;
    dataModel = nil;
    
    [keycodeText release], keycodeText=nil;
    [super dealloc];
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    dataModel = [DataModel sharedDataModel];
    dataModel.delegate = self;

}


-(IBAction)loginAction:(id)sender{
    
    if([[Model sharedModel] isWebSiteReachable:SERVER_NAME]){
        [dataModel keycodeLogin:keycodeText.text];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please check your internet connection!" 
                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        [keycodeText resignFirstResponder];
    }
    
}


//DataModelDelegate
- (void)dataModelDidFinishLoading:(DataModel*)dataModel data:(id)data{
    
    if([data isEqualToString:@"YES"]){
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setInteger:0 forKey:@"logInFailCount"];
        [prefs setInteger:0 forKey:@"logInFailEmail"];
        [prefs synchronize];
        
        [self.navigationController popToRootViewControllerAnimated:NO]; 
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter correct keycode!" 
                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void)dataModel:(DataModel*)model didFailWithError:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter correct keycode!" 
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [alert release];
}








-(IBAction)onBackgroundButton:sender{
    [keycodeText resignFirstResponder];
}



-(BOOL) textFieldShouldReturn:(UITextField*) textField {
    //
    //[textField resignFirstResponder]; 
    //[self onNext:nil];
    //return YES;
    
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
        //[self onNext:nil];
    }
    return NO; 
}


- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
	currentControl = textField;
	return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField { 
	[self.view maintainVisibityOfControl:textField toView:self.view offset:0.0f];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	if (textField == currentControl) {
		//If the textfield is still the same one, we can reset the view animated
		[self.view resetViewToIdentityTransform];			
	}else {
        
	}
}



@end
