//
//  ModuleVO.m
//  Demo
//
//  Created by Jonathan Xie on 29/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "ModuleVO.h"
#import "PageVO.h"


@implementation ModuleVO
@synthesize rowid, title, description, module_uid, access_level, content_url, downloaded;
@synthesize startTime=startTime_, endTime=endTime_, time_spent, pageArray=pageArray_;

- (void)dealloc {
    [content_url release];
    [downloaded release];
    [module_uid release];
    [title release];
    [description release];
    [startTime_ release];
    [endTime_ release];
    [pageArray_ release];
    time_spent = 0;
    [super dealloc];
}

-(void)setEndTime:(NSDate *)aEndTime{
    //endTime_ = aEndTime;
    
    if (endTime_ != aEndTime)
    {
        [endTime_ release];
        endTime_ = [aEndTime retain];
    }
    
    self.time_spent = [endTime_ timeIntervalSinceDate:startTime_];
}


- (void)addPage:(PageVO*)pageVO{
    if(self.pageArray==nil){
        self.pageArray = [[[NSMutableArray alloc] init] autorelease]; 
    }
    //
    for (PageVO* page in self.pageArray){
        if([pageVO.filename isEqualToString:page.filename]){
            if(pageVO.time_spent>0){
                page.time_spent += pageVO.time_spent;
            }
            return;
        }
    }
    //if it's new page
    [self.pageArray addObject:pageVO];
}


@end
