//
//  Model.m
//  MVC
//
//  Created by Keith A Peters on 2/19/09.
//  Copyright 2009 BIT-101. All rights reserved.
//

#import "Model.h"
#import "DataModel.h"
#import "SynthesizeSingleton.h"
#import "Constants.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "JSON.h"
#import "Reachability.h"
#import "CallVO.h"
#import "CallModuleVO.h"
#import "ModuleVO.h"
#import "UserVO.h"
#import "RepVO.h"
#import "PageVO.h"
#import "MainViewQuickAccessController.h"

@implementation Model

@synthesize appView, moduleArr,databasePath;
@synthesize callVO;
@synthesize moduleVO;
@synthesize userVO;
@synthesize repVO;
@synthesize callModuleVO;

@synthesize mainViewContentController, mainViewController;
@synthesize mainViewQuickAccessController;
@synthesize moduleSessionData;
@synthesize clearSessionData;
@synthesize currentPageVO;
@synthesize usersArr;
@synthesize quickAccessModuleVO,quickAccessCurrentPageVO; 

SYNTHESIZE_SINGLETON_FOR_CLASS(Model);


- (id) init
{
	self = [super init]; 
	if (self != nil) {
        //
        dataModel = [DataModel sharedDataModel];
        
        //views
		appView = nil;
        mainViewContentController = nil;
        mainViewController = nil;
        mainViewQuickAccessController = nil;
        
        //stats (shouldn't init here!)
        callVO = [[CallVO alloc] init];
        //moduleVO = nil;//[[ModuleVO alloc] init];
        userVO = [[UserVO alloc] init];
        //repVO = [[RepVO alloc] init];
        quickAccessModuleVO = [[ModuleVO alloc] init];
        
        currentPageVO = nil;
        //quickAccessModuleVO = nil;
        clearSessionData = NO;
        
        moduleArr = [[dataModel moduleSelectAll] retain];
        
        moduleSessionData = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:0], @"casestudy", nil];
        
         
	}
	
	return self;
}



-(NSString *)timeBetween:(NSDate *)startDate andEndTime:(NSDate*)endDate{
	
	NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
	[df setDateFormat:@"HH:mm"];
	//NSDate *date1 = [df dateFromString:@"14:10"];
	//NSDate *date2 = [df dateFromString:@"18:09"];
	NSTimeInterval interval = [startDate timeIntervalSinceDate:endDate];
	int hours = (int)interval / 3600;             // integer division to get the hours part
	int minutes = (interval - (hours*3600)) / 60; // interval minus hours part (in seconds) divided by 60 yields minutes
	NSString *timeDiff = [NSString stringWithFormat:@"%d:%02d", hours, minutes];
	
	return timeDiff;
	
}



// Helper 

- (NSString *)uniqueString
{
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef uuidStr = CFUUIDCreateString(NULL, uuid);
    CFRelease(uuid);
    [(NSString *)uuidStr autorelease];
    return (NSString *)uuidStr;
}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
    
    return [emailTest evaluateWithObject:candidate];
}



// REACHABILITY ---------------------------------------------------------------------------------------------------------

-(BOOL) isInternetReachable {
	return ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable);
}

-(BOOL) isWebSiteReachable: (NSString *)host {
	return ([[Reachability reachabilityWithHostName: host] currentReachabilityStatus] != NotReachable);
}





- (void) dealloc
{
    [quickAccessModuleVO release], quickAccessModuleVO=nil;
    [quickAccessCurrentPageVO release],quickAccessCurrentPageVO=nil; 
    [usersArr release], usersArr=nil;
    [moduleSessionData release], moduleSessionData=nil;
    [dataModel release];
    [repVO release];
    [userVO release];
    [moduleVO release];
    [callModuleVO release];
    [callVO release];
    [currentPageVO release];
    [databasePath release];
    [moduleArr release];
	[appView release];
    [mainViewContentController release];
    [mainViewQuickAccessController release];
	[super dealloc];
}

@end
