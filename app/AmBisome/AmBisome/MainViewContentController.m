//
//  MainViewContentController.m
//  Demo
//
//  Created by Jonathan Xie on 11/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "MainViewContentController.h"
#import "MainViewPageController.h"
#import "MainViewNavController.h"
#import "MainViewQuickAccessController.h"
#import "AppScrollView.h"
#import "Model.h"
#import "DataModel.h"
#import "PageVO.h"
#import "ModuleVO.h"
#import "DataModel.h"
#import "MainViewPopUpController.h"
#import "MainViewContentScrollController.h"

#define MODULE1_FIRSTVIEW @"amb_mod1_0.html"
#define MODULE2_FIRSTVIEW @"amb_mod2_menu.html"

@implementation MainViewContentController


@synthesize pageControllers;
@synthesize navView, quickAccessView, currentPageIndex;
@synthesize pageArr, pageName;
@synthesize tapGR, swipeUpGR, swipeDownGR;
@synthesize popup=popup_;
@synthesize contentNavController=contentNavController_;

- (id)initWithDataArray:(NSArray *)array
{
    self = [super init];
    if (self) {
        self.pageArr = array;
        pageControllers = [[NSMutableArray alloc] init];
        //currentPageIndex = 1;
    }
    return self;
}

- (id)initWithModuleIndex:(NSInteger)index
{
    self = [super init];
    if (self) {
        if(index==1) self.pageName = MODULE1_FIRSTVIEW;
        if(index==2) self.pageName = MODULE2_FIRSTVIEW;
    }
    return self;
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [popup_ release], popup_=nil;
    
    tapGR.delegate = nil;
    swipeUpGR.delegate = nil;
    swipeDownGR.delegate = nil;
    
    tapGR=nil;
    swipeUpGR=nil;
    swipeDownGR=nil;
    
    [model release], model = nil;
    [navView release], navView = nil;
    [pageControllers release], pageControllers = nil;
    [pageArr release], pageArr = nil;
    [pageName release],pageName=nil;
    [quickAccessView release], quickAccessView = nil;
    //
    [contentNavController_ release],contentNavController_=nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    model.mainViewQuickAccessController = self.quickAccessView;
    
    //[self addGestureRecognizer];
    
    //Bad idea!!! hardcode the first page tracking! change this if we have other module!
    [self trackPageByFileName:self.pageName];
    
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    model = [Model sharedModel];
    model.mainViewContentController = self;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-module.png"]];
    self.navigationController.navigationBarHidden = YES;
    
    
    [self addGestureRecognizer];
    
    //UIWebView should sit at the bottom of stack
    MainViewPageController * webViewController = [[[MainViewPageController alloc] initWithFileName:self.pageName] autorelease];
    
    
    self.contentNavController = [[[UINavigationController alloc] initWithRootViewController:webViewController] autorelease];
	self.contentNavController.navigationBarHidden = YES;
    self.contentNavController.view.frame = CGRectMake(0.0, 0.0, 1024.0, 768);
    [self.view addSubview:self.contentNavController.view];
    
    
    //quickAccess view
    self.quickAccessView = [[[MainViewQuickAccessController alloc] initWithContent:nil scrollEnabled:YES] autorelease];
    self.quickAccessView.view.tag = 121;
    [self.view addSubview:self.quickAccessView.view];

    
    //Need delay to work!
    [self performSelector:@selector(showQuickAccessView:) withObject:nil afterDelay:0.0];
    
    //nav view
    self.navView = [[[MainViewNavController alloc] init] autorelease];
    [self.view addSubview:self.navView.view];
    self.navView.view.tag = 101;
    
    
    
    [self showDashBoard:NO];
    
    
    
    //add notification for popup
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(popupWebView:) 
                                                 name:@"popupWebView"
                                               object:nil];
    
}

-(void)popupWebView:(NSNotification *)notification
{    
    NSDictionary *dict = [notification userInfo];
    //
    self.popup = [[[MainViewPopUpController alloc] initWithPage:[dict valueForKey:@"file"]] autorelease];

    [self.view addSubview:self.popup.view];
    

}


- (void)addGestureRecognizer
{
    tapGR=nil;
    swipeUpGR=nil;
    swipeDownGR=nil;
    
    
    UIGestureRecognizer *recognizer;
    //Gestures
    recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGR = (UITapGestureRecognizer *)recognizer;
    tapGR.numberOfTapsRequired = 2;
    tapGR.numberOfTouchesRequired = 2;
    tapGR.delegate = self;
    [self.view addGestureRecognizer:tapGR];
    //[self.scrollView addGestureRecognizer:tapGR];
    
    [recognizer release];
    
    
    //swipe up
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeUp:)];
    swipeUpGR = (UISwipeGestureRecognizer *)recognizer;
    swipeUpGR.numberOfTouchesRequired = 2;
    swipeUpGR.direction = UISwipeGestureRecognizerDirectionUp;
    swipeUpGR.delegate = self;
    [self.view addGestureRecognizer:swipeUpGR];
    
    
    [recognizer release];
    
    
    //swipe down
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeDown:)];
    swipeDownGR = (UISwipeGestureRecognizer *)recognizer;
    swipeDownGR.numberOfTouchesRequired = 2;
    swipeDownGR.direction = UISwipeGestureRecognizerDirectionDown;
    swipeDownGR.delegate = self;
    [self.view addGestureRecognizer:swipeDownGR];
    
    [recognizer release];
}



- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}


- (void)pauseAllVideoAndAudio{
    for(id controller in pageControllers) {
        if(controller != [NSNull null]){
            //[(MainViewPageController *)controller notifyPageAction: @"pauseAllVideoAndAudio"];
        }
    }
}




//ADD::page time tracking
- (void)trackPage:(NSInteger)page {
    //0. filter
    if(page <0) return;//out of bound
	if(page >=[self.pageArr count]) return;//out of bound
    //if(page == currentPageIndex && page !=0) return;//same page!
    
    
    //1. end the time for last open page
    if(model.currentPageVO !=nil && model.moduleVO != nil){
        model.currentPageVO.endTime = [NSDate date];
        [model.moduleVO addPage:model.currentPageVO];
        
    }
    
    
    //2. start the new time for current page
    
    model.currentPageVO =nil;
    PageVO * myPageVO = (PageVO*)[self.pageArr objectAtIndex:page];
    
    
    model.currentPageVO = [[[PageVO alloc] init] autorelease];
    //should add the uid here!
    model.currentPageVO.module_uid = myPageVO.module_uid;
    model.currentPageVO.page_uid = myPageVO.page_uid;
    model.currentPageVO.filename = myPageVO.filename;
    model.currentPageVO.type = myPageVO.type;
    model.currentPageVO.startTime = [NSDate date];
    model.currentPageVO.page_name = myPageVO.page_name;
    
    
    //NSLog(@"currentPageVO: %@", model.currentPageVO);
}

- (void)trackPageByFileName:(NSString*)fileName {
    if(fileName==nil) return;
    if([fileName length]==0) return;
    
    //1. end the time for last open page
    if(model.currentPageVO !=nil && model.moduleVO != nil){
        model.currentPageVO.endTime = [NSDate date];
        [model.moduleVO addPage:model.currentPageVO];
        
    }
    
    //2. start the new time for current page
    
    model.currentPageVO =nil;
    PageVO * myPageVO = [[DataModel sharedDataModel] getPageByFileName:fileName];
    
    
    model.currentPageVO = [[[PageVO alloc] init] autorelease];
    //should add the uid here!
    model.currentPageVO.module_uid = myPageVO.module_uid;
    model.currentPageVO.page_uid = myPageVO.page_uid;
    model.currentPageVO.filename = myPageVO.filename;
    model.currentPageVO.type = myPageVO.type;
    model.currentPageVO.startTime = [NSDate date];
    model.currentPageVO.page_name = myPageVO.page_name;
    
}



//Public navigaton: nextPage / prevPage ---------------------------------------

-(void)gotoNextPage:(NSString*)file
{
    [self trackPageByFileName:file];
    
    //
    MainViewPageController * webViewController = [[[MainViewPageController alloc] initWithFileName:file] autorelease];
    [self.contentNavController pushViewController:webViewController animated:YES];
    
    
}

-(void)gotoScrollPages:(NSString*)moduleID{
    //if it is the case pages
    NSArray * pages = [[DataModel sharedDataModel] pageSelectByModuleId:moduleID];//@"module1-case" 
    
    
    MainViewContentScrollController* contentScrollViewcontroller = [[[MainViewContentScrollController alloc] initWithDataArray:pages moduleID:moduleID] autorelease];
    [self.contentNavController pushViewController:contentScrollViewcontroller animated:YES];
   
}




//TapGestureRecognizer delegate  ---------------------------------------
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


//handlers
-(void) handleTap:(UITapGestureRecognizer *)recognizer  {
    
    if(self.navView.demoview.alpha==1){
        [self.navView showDemoView:NO];
        //return;
    }
    
    if(self.quickAccessView.view.alpha==1){
        [self showQuickAccessView:NO];
        //return;
    }
    
    
    if(self.navView.view.alpha==1){
        [self showDashBoard:NO];
    }else{
        [self showDashBoard:YES];
    }
}

-(void) handleSwipeUp:(UISwipeGestureRecognizer *)recognizer  {
    
    if(self.navView.demoview.alpha==1){
        [self.navView showDemoView:NO];
        //return;
    }
    
    if(self.quickAccessView.view.alpha==1){
        [self showQuickAccessView:NO];
        //return;
    }
    
    
    [self showDashBoard:NO];
}

-(void) handleSwipeDown:(UISwipeGestureRecognizer *)recognizer  {
    
    [self showDashBoard:YES];
}



// Show /Hide over Layers  ---------------------------------------

-(void)showDashBoard:(BOOL)flag
{

    
    if(flag==YES){
        self.navView.view.alpha = 1.0;
        [UIView animateWithDuration:0.50
                         animations:^{ 
                             //dashboard
                             CGRect navFrame = self.navView.view.frame;
                             navFrame.origin.y = 0;
                             self.navView.view.frame = navFrame;
                             
                         } 
                         completion:^(BOOL finished){
                             
                             
                         }];
        
        
    }else{
        
        [UIView animateWithDuration:0.50
                         animations:^{ 
                             //dashboard
                             CGRect navFrame = self.navView.view.frame;
                             navFrame.origin.y = -self.navView.view.frame.size.height-20;//20 was to make sure it is off screen
                             self.navView.view.frame = navFrame;
                             
                         } 
                         completion:^(BOOL finished){
                             
                         }];
        
        
        
    }
}

-(void)showQuickAccessView:(BOOL)flag
{
    
    
    [navView showDemoView:NO];
    [self showDashBoard:NO];//hide the DashBoard!
    
    if(flag==YES){
        //close if already open
        if(self.quickAccessView.view.alpha==1){
            //[self showQuickAccessView:NO];
            //return;
        }
        
        
        [self.quickAccessView showMenu:YES animated:NO];
        self.quickAccessView.view.alpha = 1.0;
        
        [UIView animateWithDuration:0.80
                         animations:^{ 
                             CGRect helpFrame = self.quickAccessView.view.frame;
                             helpFrame.origin.y = 0;
                             self.quickAccessView.view.frame = helpFrame;
                             
                         }];
        
        
    }else{
        //end the time for any open tracking
        if(model.quickAccessCurrentPageVO!=nil){
            model.quickAccessCurrentPageVO.endTime = [NSDate date];
            [model.quickAccessModuleVO addPage:model.quickAccessCurrentPageVO];
            //
            model.quickAccessCurrentPageVO = nil;
        }
        
        [UIView animateWithDuration:0.80
                         animations:^{ 
                             CGRect helpFrame = self.quickAccessView.view.frame;
                             helpFrame.origin.y = -(self.quickAccessView.view.frame.size.height+160);
                             self.quickAccessView.view.frame = helpFrame;
                         }
                         completion:^(BOOL finished){
                             self.quickAccessView.view.alpha = 0.0;
                             
                             //@"quickAccessViewWillOffScreen"
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"quickAccessViewWillOffScreen" object:nil userInfo:nil];
                         }];
        
        
    }
    
    
}



@end
