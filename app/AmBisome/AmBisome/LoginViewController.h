//
//  LoginViewController.h
//  AtriplaED
//
//  Created by Jonathan Xie on 8/06/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataModel.h"

@class Model;


@interface LoginViewController : UIViewController<UITextFieldDelegate, DataModelDelegate>{
    Model * model;
    DataModel * dataModel;
    UITextField * usernameText;
    UITextField * passwordText;
    
    UIControl *currentControl;
    
}

@property(nonatomic, retain) IBOutlet UITextField * usernameText;
@property(nonatomic, retain) IBOutlet UITextField * passwordText;

@property (nonatomic, assign) UIControl *currentControl;

@end



@interface LoginViewController(private)

-(IBAction)onNext:sender;
-(void)loginErrorMessage;
-(void)syncUserData:(RepVO*)repVO;

-(NSInteger)getSecurityCount;
-(void)setSecurityCount:(BOOL)success;

-(void)openKeycodeScreen;

@end
