//
//  ModuleVO.h
//  Demo
//
//  Created by Jonathan Xie on 29/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PageVO;

@interface ModuleVO : NSObject {
    NSInteger rowid;
    NSString * module_uid;
    NSString * title;
    NSString * description;
    NSInteger access_level;
    NSString * content_url;
    NSString * downloaded;
    NSDate * startTime_;
    NSDate * endTime_;
    double time_spent;
    NSMutableArray * pageArray_;
}

@property (nonatomic, assign) NSInteger rowid;
@property (nonatomic, retain) NSString * module_uid;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * description;
@property (nonatomic, assign) NSInteger access_level;
@property (nonatomic, retain) NSString * content_url;
@property (nonatomic, retain) NSString * downloaded;
@property (nonatomic, retain) NSDate * startTime;
@property (nonatomic, retain) NSDate * endTime;
@property (nonatomic, assign) double time_spent;
@property (nonatomic, retain) NSMutableArray * pageArray;

- (void)addPage:(PageVO*)pageVO;

@end
