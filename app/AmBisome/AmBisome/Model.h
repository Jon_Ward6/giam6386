//
//  Model.h
//  MVC
//
//  Created by Jonathan Xie
//  Copyright 2010 XIEngine. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ViewController;
@class MainViewContentController;
@class MainViewController;
@class MainViewQuickAccessController;
@class CallVO;
@class CallModuleVO;
@class ModuleVO;
@class UserVO;
@class RepVO;
@class DataModel;
@class PageVO;


@interface Model : NSObject {
	
	ViewController * appView;
    MainViewContentController * mainViewContentController;
    MainViewController * mainViewController;
    MainViewQuickAccessController * mainViewQuickAccessController;
	
	NSString *databaseName;
	NSString *databasePath;
    
    NSMutableArray * moduleArr;
    
    CallVO * callVO;
    ModuleVO * moduleVO;
    CallModuleVO * callModuleVO;
    UserVO * userVO;
    RepVO * repVO;
    
    DataModel * dataModel;
    
    NSMutableDictionary * moduleSessionData;
    
    BOOL clearSessionData;
    
    //support multi-users
    NSMutableArray * usersArr;
    
    //time tracking
    PageVO * currentPageVO;
    
    //time tracking for quickaccess
    ModuleVO * quickAccessModuleVO;
    PageVO * quickAccessCurrentPageVO;
    
}

@property (nonatomic, retain) ViewController * appView;
@property (nonatomic, retain) MainViewContentController * mainViewContentController;
@property (nonatomic, retain) MainViewController * mainViewController;
@property (nonatomic, retain) MainViewQuickAccessController * mainViewQuickAccessController;

@property (nonatomic, retain) NSMutableArray * moduleArr;
@property (nonatomic, retain) NSString *databasePath;
@property (nonatomic, retain) CallVO * callVO;
@property (nonatomic, retain) CallModuleVO * callModuleVO;
@property (nonatomic, retain) ModuleVO * moduleVO;
@property (nonatomic, retain) UserVO * userVO;
@property (nonatomic, retain) RepVO * repVO;
@property (nonatomic, retain) PageVO * currentPageVO;
@property (nonatomic, retain) NSMutableDictionary * moduleSessionData;
@property (nonatomic, readwrite) BOOL clearSessionData;

@property (nonatomic, retain) NSMutableArray * usersArr;


@property (nonatomic, retain) ModuleVO * quickAccessModuleVO;
@property (nonatomic, retain) PageVO * quickAccessCurrentPageVO;



+ (Model *)sharedModel;

-(NSString *)timeBetween:(NSDate *)startDate andEndTime:(NSDate*)endDate;

-(BOOL) isInternetReachable;
-(BOOL) isWebSiteReachable: (NSString *)host;

- (NSString *)uniqueString;
- (BOOL) validateEmail: (NSString *) candidate;

@end
