//
//  BlueBadge.m
//
//  Copyright 2008 Stepcase Limited.
//

#import "BlueBadge.h"


@implementation BlueBadge

@synthesize count;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self!=nil) {
        // Initialization code
		[self setBackgroundColor: [UIColor clearColor]];
		[self setCount: 0];
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
	if (count == 0) return;
	NSString *countString = [NSString stringWithFormat: @"%d", count];
	UIFont *font = [UIFont boldSystemFontOfSize: 15];
	CGSize numberSize = [countString sizeWithFont: font];
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	float radius = 3.0f;//numberSize.height / 2.0;
	float startPoint = (rect.size.width - (numberSize.width + numberSize.height))/2.0;
	
    CGContextSetRGBFillColor(context, 123.0/255, 43.0/255, 131.0/255, 1.0);
	CGContextBeginPath(context);
	//CGContextAddArc(context, startPoint + radius, radius, radius, M_PI / 2 , 3 * M_PI / 2, NO);
	//CGContextAddArc(context, startPoint + radius + numberSize.width, radius, radius, 3 * M_PI / 2, M_PI /2, NO);
    
    
    //CGContextMoveToPoint(context, startPoint + radius, radius);
    CGContextAddArc(context, startPoint + (numberSize.width*2) -radius, radius, radius, 3 * M_PI / 2, 0, 0);
    CGContextAddArc(context, startPoint + (numberSize.width*2) -radius, numberSize.height - radius, radius, 0, M_PI / 2, 0);
    CGContextAddArc(context, startPoint + radius, numberSize.height - radius, radius, M_PI / 2, M_PI, 0);
    CGContextAddArc(context, startPoint + radius, radius, radius, M_PI, 3 * M_PI / 2, 0);
    
    
    
    
    
    
	CGContextClosePath(context);
	CGContextFillPath(context);
	
	[[UIColor whiteColor] set];	
	[countString drawInRect: CGRectMake(startPoint + radius, rect.origin.y, rect.size.width, rect.size.height) withFont: font];
}

- (void)drawWithCount:(NSInteger)i {
	self.count = i;
	[self setNeedsDisplay];
}

- (void)dealloc {
    [super dealloc];
}


@end
