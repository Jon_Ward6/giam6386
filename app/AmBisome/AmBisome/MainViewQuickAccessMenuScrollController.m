//
//  MainViewQuickAccessMenuScrollController.m
//  BeyondHIV
//
//  Created by Jonathan Xie on 8/02/12.
//  Copyright (c) 2012 Ward6. All rights reserved.
//

#import "MainViewQuickAccessMenuScrollController.h"
#import "MainViewQuickAccessMenuController.h"

@implementation MainViewQuickAccessMenuScrollController

@synthesize scrollView=scrollView_;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)dealloc
{
    [scrollView_ release],scrollView_=nil;
    [super dealloc];
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"quickaccess-bg.png"]];
    
    //menu view controller
    MainViewQuickAccessMenuController * menuViewController = [[[MainViewQuickAccessMenuController alloc] initWithNibName:@"MainViewQuickAccessMenuController" bundle:nil] autorelease];
    
    
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, menuViewController.view.frame.size.height);
    
    //self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = YES;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
	self.scrollView.contentMode = UIViewContentModeScaleAspectFit;
	self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.scrollView.minimumZoomScale = (self.scrollView.frame.size.width / self.scrollView.contentSize.width) / 1.00;//0.99;
    self.scrollView.bounces = YES;
    
    
    
    [self.scrollView addSubview:menuViewController.view];
 
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView {
    
}


@end
