//
//  PracticeVO.h
//  eDetailer
//
//  Created by Jonathan Xie on 15/04/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PracticeVO : NSObject {
    NSInteger rowid;
    NSString * practice_uid;
    NSString * name;
    NSString * street;
    NSString * suburb;
    NSString * postcode;
    NSString * state;
    NSString * region_code;
}

@property (nonatomic, assign) NSInteger rowid;
@property (nonatomic, retain) NSString * practice_uid;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * street;
@property (nonatomic, retain) NSString * suburb;
@property (nonatomic, retain) NSString * postcode;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * region_code;

@end
