//
//  GoogleAnalytices.h
//  eDetailer
//
//  Created by Jonathan Xie on 19/05/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GoogleAnalytices : NSObject {
    
}

+(void)trackPageView:(NSString*)view;
+(void)trackCustomVariable:(NSString *)name value:(id)value index:(NSInteger)index;

@end
