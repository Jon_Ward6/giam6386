    //
//  MainViewController.m
//  Nurofen
//
//  Created by Jonathan Xie on 4/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "MainViewController.h"
#import "MainViewContentController.h"
#import "Constants.h"
#import "Model.h"
#import "AppScrollView.h"
#import "ViewController.h"
#import "CallVO.h"
#import "ModuleVO.h"
#import "DataModel.h"
#import <QuartzCore/QuartzCore.h>
#import "GoogleAnalytices.h"
#import "MainViewPopUpController.h"
#import "MainViewNavController.h"
#import "MainViewQuickAccessController.h"
#import "PageVO.h"


@interface MainViewController(private)
    -(void)moduleButtons;
    -(void)addGestureRecognizer;
@end


@implementation MainViewController

@synthesize contentView;
@synthesize navView, quickAccessView;
@synthesize tapGR, swipeUpGR, swipeDownGR;
@synthesize popup=popup_;

- (void)dealloc {
    [model release];
    [dataModel release];
    [popup_ release], popup_=nil;
    
    tapGR.delegate = nil;
    swipeUpGR.delegate = nil;
    swipeDownGR.delegate = nil;
    
    [tapGR release], tapGR=nil;
    [swipeUpGR release], swipeUpGR=nil;
    [swipeDownGR release], swipeDownGR=nil;
    
    [navView release], navView = nil;
    [quickAccessView release], quickAccessView = nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //quick fix the popup issues here
    if(self.popup!=nil){
       //[self.popup.view removeFromSuperview];
       //self.popup=nil;
    }
    
    //add notification for popup
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"popupWebView" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(popupWebView:) 
                                                 name:@"popupWebView"
                                               object:nil];
    
    
    
    model.mainViewQuickAccessController = self.quickAccessView;
    //
    [self addGestureRecognizer];
    
    //reset the session back to default 0
    //[model.moduleSessionData setValue:[NSNumber numberWithInt:0] forKey:@"casestudy"];
    
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //remove notification for popup when getting out!
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"popupWebView" object:nil];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    model = [Model sharedModel];
    dataModel = [DataModel sharedDataModel];
    
    model.mainViewController = self;
    self.view.backgroundColor = [UIColor clearColor];
    
    
    
    //quickAccess view
    self.quickAccessView = [[[MainViewQuickAccessController alloc] initWithNibName:@"MainViewQuickAccessController" bundle:nil] autorelease];
    self.quickAccessView.view.tag = 120;
    [self.view addSubview:self.quickAccessView.view];
    
    //[self.quickAccessView release];
    
    //WTF! you have to make the delay call to work?
    [self performSelector:@selector(showQuickAccessView:) withObject:NO afterDelay:0.0];
    
    //nav view
    self.navView = [[[MainViewNavController alloc] init] autorelease];
    [self.view addSubview:self.navView.view];
    //[self.navView release];
    self.navView.view.tag = 100;    
    [self showDashBoard:NO];
    
    
    //[self addGestureRecognizer];

    //inital the quickaccess module
    
    if(model.callVO != nil){
        model.quickAccessModuleVO = nil;
        model.quickAccessModuleVO = [[[ModuleVO alloc] init] autorelease];
        model.quickAccessModuleVO.module_uid = @"quickaccess";
        model.quickAccessModuleVO.startTime = [NSDate date];
    }
    
    
}


-(void)popupWebView:(NSNotification *)notification
{
    
    NSDictionary *dict = [notification userInfo];
    
    //NSLog(@"popup1:: %@", dict);
    //NSLog(@"popup1:: %f", self.view.frame.origin.x);
    //
    self.popup = [[[MainViewPopUpController alloc] initWithPage:[dict valueForKey:@"file"]] autorelease];
    
    [self.view addSubview:self.popup.view];
    
    
}


-(void)addGestureRecognizer
{
    tapGR=nil;
    swipeUpGR=nil;
    swipeDownGR=nil;
    
    UIGestureRecognizer *recognizer;
    //Gestures
    recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGR = (UITapGestureRecognizer *)recognizer;
    tapGR.numberOfTapsRequired = 2;
    tapGR.numberOfTouchesRequired = 2;
    tapGR.delegate = self;
    [self.view addGestureRecognizer:tapGR];
    
    [recognizer release];
    
    
    //swipe up
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeUp:)];
    swipeUpGR = (UISwipeGestureRecognizer *)recognizer;
    swipeUpGR.numberOfTouchesRequired = 2;
    swipeUpGR.direction = UISwipeGestureRecognizerDirectionUp;
    swipeUpGR.delegate = self;
    [self.view addGestureRecognizer:swipeUpGR];
    
    [recognizer release];
    
    
    //swipe down
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeDown:)];
    swipeDownGR = (UISwipeGestureRecognizer *)recognizer;
    swipeDownGR.numberOfTouchesRequired = 2;
    swipeDownGR.direction = UISwipeGestureRecognizerDirectionDown;
    swipeDownGR.delegate = self;
    [self.view addGestureRecognizer:swipeDownGR];
    
    [recognizer release];
}




- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}





-(IBAction)module1ButtonAction:sender{
    [self btnAction:sender];
}

-(IBAction)module2ButtonAction:sender{
    [self btnAction:sender];
}




-(void)btnAction:sender{
    //reset the session
    model.clearSessionData = NO;
    
    int i = [(UIButton*)sender tag];
    if(i<0) return;
    
    self.contentView = nil; 
    [model.moduleSessionData setValue:[NSNumber numberWithInt:0] forKey:@"casestudy"];
    
    //NSArray * pages = [dataModel pageSelectByModuleId:[(ModuleVO*)[model.moduleArr objectAtIndex:i-1] module_uid]];
    //self.contentView = [[[MainViewContentController alloc] initWithDataArray:pages] autorelease];  
    //self.contentView = [[MainViewContentController alloc] initWithNibName:nil bundle:nil];
    
    
    self.contentView = [[MainViewContentController alloc] initWithModuleIndex:i];
    
    
    
    
    [UIView beginAnimations:@"animation" context:nil];
    [UIView setAnimationDuration:1.20];
    [UIView setAnimationDidStopSelector:@selector(pageTransitionDidStop:)];
    [self.navigationController pushViewController: self.contentView animated:NO];
    [self.contentView viewDidAppear:YES];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO]; 
    [UIView commitAnimations];
    
    
    
    //setup the new ModualVO - only if the model.callVO is initialize;
    if(model.callVO && model.callVO.call_uid){
        model.moduleVO = nil;
        model.moduleVO = [[[ModuleVO alloc] init] autorelease];
        model.moduleVO.module_uid = [(ModuleVO *)[model.moduleArr objectAtIndex:i-1] module_uid];
        model.moduleVO.rowid = [(ModuleVO *)[model.moduleArr objectAtIndex:i-1] rowid];
        model.moduleVO.startTime = [NSDate date];
        
        //Google Analytices
        [GoogleAnalytices trackPageView:[NSString stringWithFormat:@"/app_module_%@", [(ModuleVO *)[model.moduleArr objectAtIndex:i-1] module_uid]]];
    }
    
    
    
    
    //cleanup
    [navView showDemoView:NO];
    [self showQuickAccessView:NO];
    [self showDashBoard:NO];
    

}


- (void)pageTransitionDidStop:sender{
    //[self.contentView release];
}




//

//TapGestureRecognizer delegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


//handlers
-(void) handleTap:(UITapGestureRecognizer *)recognizer  {
    
    if(self.navView.demoview.alpha==1){
        [self.navView showDemoView:NO];
        //return;
    }
    
    if(self.quickAccessView.view.alpha==1){
        [self showQuickAccessView:NO];
        //return;
    }
    
    
    
    if(self.navView.view.alpha==1){
        [self showDashBoard:NO];
    }else{
        [self showDashBoard:YES];
    }
}

-(void) handleSwipeUp:(UISwipeGestureRecognizer *)recognizer  {
    
    if(self.navView.demoview.alpha==1){
        [navView showDemoView:NO];
        //return;
    }
    
    if(self.quickAccessView.view.alpha==1){
        [self showQuickAccessView:NO];
        //return;
    }
    
    
    [self showDashBoard:NO];
}

-(void) handleSwipeDown:(UISwipeGestureRecognizer *)recognizer  {
    
    [self showDashBoard:YES];
}





-(void)showDashBoard:(BOOL)flag
{
    
    if(flag==YES){
        self.navView.view.alpha = 1.0;
        [UIView animateWithDuration:0.50
                         animations:^{ 
                             //dashboard
                             CGRect navFrame = navView.view.frame;
                             navFrame.origin.y = 0;
                             self.navView.view.frame = navFrame;
                             
                         }];
        
        
    }else{
        
        [UIView animateWithDuration:0.50
                         animations:^{ 
                             //dashboard
                             CGRect navFrame = navView.view.frame;
                             navFrame.origin.y = -navView.view.frame.size.height;
                             self.navView.view.frame = navFrame;
                             self.navView.view.alpha = 0.0;
                         }
                         completion:^(BOOL finished){
                             //self.navView.view.alpha = 0.0;
                             
                         }];
        
        
        
    }
}


-(void)showQuickAccessView:(BOOL)flag
{
    [self.navView showDemoView:NO];
    [self showDashBoard:NO];//hide the DashBoard!
    
    if(flag==YES){
        //close if already open
        if(self.quickAccessView.view.alpha==1){
            [self showQuickAccessView:NO];
            return;
        }
        
        [self.quickAccessView showMenu:YES animated:NO];
        self.quickAccessView.view.alpha = 1.0;
        [UIView animateWithDuration:0.80
                         animations:^{ 
                             CGRect helpFrame = self.quickAccessView.view.frame;
                             helpFrame.origin.y = 0;
                             self.quickAccessView.view.frame = helpFrame;
                         }];
        
        
    }else{
        //end the time for any open tracking
        if(model.quickAccessCurrentPageVO!=nil){
            model.quickAccessCurrentPageVO.endTime = [NSDate date];
            [model.quickAccessModuleVO addPage:model.quickAccessCurrentPageVO];
            //
            model.quickAccessCurrentPageVO = nil;
        }
        
        [UIView animateWithDuration:0.80
                         animations:^{ 
                             CGRect helpFrame = self.quickAccessView.view.frame;
                             helpFrame.origin.y = -(self.quickAccessView.view.frame.size.height+160);
                             self.quickAccessView.view.frame = helpFrame;
                             self.quickAccessView.view.alpha = 0.0;
                         }
                         completion:^(BOOL finished){
                             //self.quickAccessView.view.alpha = 0.0;
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"quickAccessViewWillOffScreen" object:nil userInfo:nil];
                         }];

        
        
    }
    
}







@end
