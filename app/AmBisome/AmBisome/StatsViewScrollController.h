//
//  StatsViewScrollController.h
//  eDetailer
//
//  Created by Jonathan Xie on 28/04/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DataModel;
@class StatsViewController;

@interface StatsViewScrollController : UIViewController<UIScrollViewDelegate> {
    UIScrollView * scrollView;
    NSMutableArray * userCallArray;
    NSString* user_uid;
    DataModel * dataModel;
    
    UILabel * userName;
    UILabel * practiceName;
    UITextView * address;
    UILabel * email;
    UILabel * phone;
    
@private
    StatsViewController * statsViewController_;
}


@property(nonatomic, retain) IBOutlet UILabel * userName;

@property(nonatomic, retain) IBOutlet UILabel * practiceName;
@property(nonatomic, retain) IBOutlet UITextView * address;
@property(nonatomic, retain) IBOutlet UILabel * email;
@property(nonatomic, retain) IBOutlet UILabel * phone;


@property(nonatomic, retain) IBOutlet UIScrollView * scrollView;
@property(nonatomic, retain) IBOutlet UILabel * callLabel;
@property(nonatomic, retain) NSMutableArray * userCallArray;
@property(nonatomic, retain) NSString* user_uid;

@property(nonatomic, retain) StatsViewController * statsViewController;



- (id)initWithUserId:(NSString*)uid;
- (void)loadCallData:(NSInteger)index;

@end
