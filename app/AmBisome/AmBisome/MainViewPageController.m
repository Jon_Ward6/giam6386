//
//  MainViewPageController.m
//  Demo
//
//  Created by Jonathan Xie on 14/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "MainViewPageController.h"
#import "TouchableWebView.h"
#import "Model.h"
#import "JSON.h"
#import "DataModel.h"
#import "MainViewQuickAccessController.h"
#import "MainViewContentController.h"

const CGFloat VIEW_PADDING = 50.0;



/*
 Important Note:
    We have to remember that the host of this MainViewPageController can be either MainViewContentController's scrollview
    or MainViewQuickAccessController's scrollview!
 
 */



@implementation MainViewPageController

@synthesize aWebView=aWebView_;
@synthesize type, name;
@synthesize pageIndex, dataArray;
@synthesize indicator;



- (id)initWithPage:(NSDictionary *)pageDict
{
    self = [super init];
    if (self) {
        
        
        NSArray *fileArr = [[pageDict valueForKey:@"filename"] componentsSeparatedByString: @"."];
        if([fileArr count]<2) return self;
        
        self.type = [fileArr objectAtIndex:1];
        self.name = [fileArr objectAtIndex:0];
        
        
    }
    return self;
}


- (id)initWithFileName:(NSString *)fileName
{
    self = [super init];
    if (self) {
        
        
        NSArray *fileArr = [fileName componentsSeparatedByString: @"."];
        if([fileArr count]<2) return self;
        
        self.type = [fileArr objectAtIndex:1];
        self.name = [fileArr objectAtIndex:0];
        
        
    }
    return self;
}


- (void)update:(NSInteger)newPageIndex data:(NSArray*)anArray{
    
    
    pageIndex = newPageIndex;
    dataArray = anArray;
    
    if (pageIndex >= 0 && pageIndex < [anArray count]){
    
        NSDictionary * dict = [anArray objectAtIndex:newPageIndex];
    
        NSArray *fileArr = [[dict valueForKey:@"filename"] componentsSeparatedByString: @"."];
        if([fileArr count]<2) return;
    
        self.type = [fileArr objectAtIndex:1];
        self.name = [fileArr objectAtIndex:0];
    
        [self gotoPage];        
    }
    
    
}


- (void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [dataArray release], dataArray=nil;
    [name release], name=nil;
    [type release], type= nil;
    [indicator release], indicator = nil;
    
    //[aWebView_ stopLoading];
    //[aWebView_ release], aWebView_=nil;
    
    [super dealloc];
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-module.png"]];// [UIColor clearColor];
    
    
    
    model = [Model sharedModel];
    dataModel = [DataModel sharedDataModel];
    
    self.aWebView.opaque = NO;
    self.aWebView.backgroundColor = [UIColor clearColor];
    //self.aWebView.dataDetectorTypes = UIDataDetectorTypeNone;
    //self.aWebView.scalesPageToFit = YES;
    //self.aWebView.mediaPlaybackRequiresUserAction = NO;//allow the video autoplay
    

    
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	indicator.center = CGPointMake(1024/2.0, 760/2.0);
	indicator.hidesWhenStopped = YES;
    
    [self.view addSubview:indicator];
    
    
    [self gotoPage];
     
    
    //bounce bugs fixing 
    for (id subview in self.aWebView.subviews)
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            ((UIScrollView *)subview).bounces = NO;
    
    
    //notify the screen is off the screen
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onViewWillOffScreen:) 
                                                 name:@"quickAccessViewWillOffScreen"
                                               object:nil];
    
    
}

-(void)onViewWillOffScreen:(NSNotification*)notification{
    [self callJavascriptFunction:@"deactivate"];
}

-(void)viewWillDisappear:(BOOL)animated{
    if(self.aWebView!=nil)
    [self callJavascriptFunction:@"deactivate"];
    //[self.webContent loadRequest:NSURLRequestFromString(@"about:blank")];
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    
}

-(void)delayNotifyPageAction:(id)sender{
    NSString *jsCommand = @"com.ward6.EDetailerPage.activate();";
    [self.aWebView stringByEvaluatingJavaScriptFromString:jsCommand];

}


- (void)notifyPageAction:(NSString *)action {
    //NSString *jsCommand = @"com.ward6.EDetailerPage.activate(true);";
    //[self.aWebView stringByEvaluatingJavaScriptFromString:jsCommand];
    [self performSelector:@selector(delayNotifyPageAction:) withObject:nil afterDelay:1.0];
}




-(void)gotoPage{
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:self.name ofType:self.type];
    NSURL *url = [NSURL fileURLWithPath:htmlFile];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    
    [self.aWebView loadRequest:urlRequest];
    
    
    
}



#pragma mark -
#pragma mark webView


- (void)webViewDidStartLoad:(UIWebView *)webView {
	[indicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    if([self isPageType:@"quickaccess"]){
        
        
        NSString *jsCommand = [NSString stringWithFormat:@"EDetailerPage.setProfile(%d);", [[model.moduleSessionData objectForKey:@"casestudy"] intValue]];

        [self.aWebView stringByEvaluatingJavaScriptFromString:jsCommand];
    }
    
    [self performSelector:@selector(delayCallToWebViewJavaScript:) withObject:nil afterDelay:0.5];
    
    [indicator stopAnimating];
    
    //JS:: activate
    //[self notifyPageAction:nil];
}



- (BOOL)webView:(UIWebView *)webView2 shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
  
    
    NSURL *url = request.URL;
    NSString *urlString = url.absoluteString;
    
    //NSLog(@"URL::%@", urlString);
    
    NSRange range = [urlString rangeOfString: @"app://"];
    if (range.location == 0){
        NSString *requestString = [[request URL] absoluteString];
        NSArray *components = [requestString componentsSeparatedByString:@"//"];
        NSString * action = [components objectAtIndex:1];
        
        
        //show quick access
        if([action isEqualToString:@"quickaccess"]) 
        {
            int index = [[components objectAtIndex:2] intValue];
            
            
            if(index>=0){
                [model.mainViewContentController showQuickAccessView:YES];
                NSArray * pages = [dataModel pageQuickAccessSelectByModuleId:[NSString stringWithFormat:@"quickaccess%d", index]];
                
                //Use Notification not references
                //NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithObject:pages forKey:@"pageArray"];
                NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:pages,@"pageArray",@"quickaccess",@"action", nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"launchQuickAccessView" object:nil userInfo:dict];
            }
        }
        
        //navigate forward
        if([action isEqualToString:@"nextPage"]) 
        {
            
            //if([self isPageType:@"casestudy"]) [model.mainViewContentController nextPage];
            if([self isPageType:@"quickaccess"]){
                if([components count]==4){
                    NSString * dataName = [components objectAtIndex:2];
                    NSString * dataValue = [components objectAtIndex:3];
                    //
                    [model.moduleSessionData setValue:dataValue forKey:dataName];
                }
                
                //[model.mainViewQuickAccessController nextPage];
                
                NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithObject:@"nextPage" forKey:@"action"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"launchQuickAccessView" object:nil userInfo:dict];
            }else{
                //suppose the rest of them are on main scroll
                NSMutableDictionary* dict2 = [NSMutableDictionary dictionaryWithObject:@"nextPage" forKey:@"action"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"nextPageContentScroll" object:nil userInfo:dict2];
            }
        }
        
        //navigate backward
        if([action isEqualToString:@"prevPage"]) 
        {
            //if([self isPageType:@"casestudy"]) [model.mainViewContentController prevPage];
            if([self isPageType:@"quickaccess"]) {
                
                //[model.mainViewQuickAccessController prevPage];
                
                NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithObject:@"prevPage" forKey:@"action"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"launchQuickAccessView" object:nil userInfo:dict];
                
            }
        }
        
        /*
        //navigate to page
        if([action isEqualToString:@"gotoPage"]) 
        {
            int pageNum = [[components objectAtIndex:2] intValue];
            if([self isPageType:@"casestudy"]) [model.mainViewContentController gotoPage:pageNum];
            
        }
        */
        //data session
        if([action isEqualToString:@"saveSessionData"]) 
        {
            NSString * dataName = [components objectAtIndex:2];
            NSString * dataValue = [components objectAtIndex:3];
            //
            [model.moduleSessionData setValue:dataValue forKey:dataName];
        }
        
        //data session
        if([action isEqualToString:@"popup"]) 
        {
            
            NSString * fileName = [components objectAtIndex:2];
            fileName = [fileName stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            
            //
            NSDictionary * fileDictionary = [NSDictionary dictionaryWithObject:fileName forKey:@"file"];
            
            NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
            [notificationCenter postNotificationName:@"popupWebView" object:nil userInfo:fileDictionary];
            
            
        }
        
        //NEW:: for ambisome only - navigate to page
        if([action isEqualToString:@"page"] || [action isEqualToString:@"gotoPage"]) 
        {
            NSString * file = [components objectAtIndex:2];
            NSArray *fileArr = [file componentsSeparatedByString: @"."];
            if([fileArr count]>1){
                self.type = [fileArr objectAtIndex:1];
                self.name = [fileArr objectAtIndex:0];
                
                //NSLog(@"file::%@", file);
                
                //scroll view at last section!   amb_mod1_13_exit.html
                if([file isEqualToString:@"amb_mod1_case0_a.html"]){
                    [model.mainViewContentController gotoScrollPages:@"module1-case"];
                    return NO;
                }
                //module 2 has all scroll views!
                if([file isEqualToString:@"amb_mod2_1_targeted_1.html"]){
                    [model.mainViewContentController gotoScrollPages:@"module2-1"];
                    return NO;
                }
                if([file isEqualToString:@"amb_mod2_2_interactions_1.html"]){
                    [model.mainViewContentController gotoScrollPages:@"module2-2"];
                    return NO;
                }
                if([file isEqualToString:@"amb_mod2_3_tissue_penetration_0.html"]){
                    [model.mainViewContentController gotoScrollPages:@"module2-3"];
                    return NO;
                }
                
                if([file isEqualToString:@"amb_mod2_4_safety_1.html"]){
                    [model.mainViewContentController gotoScrollPages:@"module2-4"];
                    return NO;
                }
                
                //
                [model.mainViewContentController gotoNextPage:file];
            }
        }
        
        //restart the module - end of preso
        if([action isEqualToString:@"restart"]) 
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"restartModule" object:nil userInfo:nil];
            
        }
        
        return NO;
    }
    
    return YES; // Return YES to make sure regular navigation works as expected.
    
    
}



- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    
    if ( [error code] != 204 ) {
        
        // report the error inside the webview
        //NSString* errorString = [NSString stringWithFormat:@"Failed to load page: %@ - %@",error.localizedDescription, [error code]];
        //[self.aWebView loadHTMLString:errorString baseURL:nil];
        
        //NSLog(@"%@", error.localizedDescription);
        
    }
    
    
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.aWebView;
}



-(void)delayCallToWebViewJavaScript:(id)sender{
    if(!model.clearSessionData){
        [self.aWebView stringByEvaluatingJavaScriptFromString:@"com.ward6.EDetailerPage.clearSessionData();"];
        model.clearSessionData = YES;
    }
    
}


//PUBLIC FUNCTION TO DISABLE THE ANY VIDEO
//e.g. deactivate()
-(void)callJavascriptFunction:(NSString*)functionName{
    NSString * nameSpace = [NSString stringWithFormat:@"com.ward6.EDetailerPage.%@()",functionName];
    [self.aWebView stringByEvaluatingJavaScriptFromString:nameSpace];
}



// Helper functions 

-(BOOL)isPageType:(NSString*)pageType{
    NSRange range = [self.name rangeOfString: pageType];
    return (range.location != NSNotFound);
}




@end
