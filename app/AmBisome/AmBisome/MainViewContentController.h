//
//  MainViewContentController.h
//  Demo
//
//  Created by Jonathan Xie on 11/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppScrollView;
@class MainViewNavController;
@class MainViewPageController;
@class MainViewQuickAccessController;
@class Model;
@class PageVO;
@class MainViewPopUpController;

@interface MainViewContentController : UIViewController <UIGestureRecognizerDelegate>{
    Model * model;
    NSArray * pageArr;
    NSString * pageName;
    NSMutableArray *pageControllers;
    NSInteger currentPageIndex;
    
    MainViewNavController * navView;
    MainViewQuickAccessController * quickAccessView;
    
    //
    UITapGestureRecognizer *tapGR;
    UISwipeGestureRecognizer *swipeUpGR;
    UISwipeGestureRecognizer *swipeDownGR;
    
    //
    MainViewPopUpController * popup_;
    
    //
    UINavigationController * contentNavController;
}


@property(nonatomic, retain) NSArray * pageArr;
@property(nonatomic, retain) NSString * pageName;
@property(nonatomic, retain) NSMutableArray * pageControllers;
@property (nonatomic, assign) NSInteger currentPageIndex;
@property(nonatomic, retain) MainViewNavController * navView;
@property(nonatomic, retain) MainViewQuickAccessController * quickAccessView;

@property (nonatomic, retain) UITapGestureRecognizer *tapGR;
@property (nonatomic, retain) UISwipeGestureRecognizer *swipeUpGR;
@property (nonatomic, retain) UISwipeGestureRecognizer *swipeDownGR;
@property (nonatomic, retain) MainViewPopUpController * popup;

@property (nonatomic, retain) UINavigationController * contentNavController;


- (id)initWithDataArray:(NSArray *)array;
- (id)initWithModuleIndex:(NSInteger)index;
- (void)pauseAllVideoAndAudio;
-(void)showDashBoard:(BOOL)flag;
-(void)showQuickAccessView:(BOOL)flag;

//-(void)gotoPage:(int)page;
//-(void)nextPage;
//-(void)prevPage;
-(void)gotoNextPage:(NSString*)file;
-(void)gotoScrollPages:(NSString*)moduleID;

-(void)trackPage:(NSInteger)page;
-(void)trackPageByFileName:(NSString*)fileName;

@end


@interface MainViewContentController(private)
- (void) configScrollView;
- (void)loadScrollViewWithPage:(int)page;
- (void)addGestureRecognizer;
- (void)shwoCloseButton;

-(void)popupWebView:(NSNotification *)notification;
@end
