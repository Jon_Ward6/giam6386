//
//  DataTester.h
//  eDetailer
//
//  Created by Jonathan Xie on 19/05/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DataModel;
@class Model;

@interface DataTester : NSObject {
    Model * model;
    DataModel * dataModel;
}

-(void)test;

@end
