//
//  StatsViewController.m
//  Demo
//
//  Created by Jonathan Xie on 23/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "StatsViewController.h"
#import "DataModel.h"
#import "Model.h"
#import "CallVO.h"
#import "CallModuleVO.h"
#import "UserVO.h"
#import "PracticeVO.h"
#import "RepVO.h"
#import "StatsViewAllController.h"
#import "StatsViewSingleController.h"
#import "StatsViewScrollController.h"
#import "GANTracker.h"
#import <QuartzCore/QuartzCore.h>

@interface StatsViewController(private)
-(NSString *)timeFormat:(double)seconds;
@end

@implementation StatsViewController

@synthesize callArray, callModuleArray, userArray;
@synthesize aTableView, scrollView, allView;
@synthesize searchText;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)dealloc
{
    model = nil;
    [searchText release];
    [aTableView release];
    [scrollView release];
    [allView release];
    [userArray release];
    [callModuleArray release];
    [callArray release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}






#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-module.png"]];//[UIColor clearColor];
    
    model = [Model sharedModel];
    
    
    self.callArray = [[DataModel sharedDataModel] callSelectAll:model.repVO.rep_uid];
    self.callModuleArray = [[DataModel sharedDataModel] callModuleSelectAll];
    self.userArray = [[DataModel sharedDataModel] userSelectAll:model.repVO.rep_uid];
    
    //self.showAllBtn.alpha = 0;
    
    //table view
    
    self.aTableView.backgroundColor = [UIColor clearColor];
    self.aTableView.opaque = NO;
    self.aTableView.backgroundView = nil;
    
    //allView
    self.allView = [[[StatsViewAllController alloc] init] autorelease];
    self.allView.view.frame = CGRectMake(0.00, 530.00, 1024.00, 223.00);
    [self.view addSubview:self.allView.view];
    
    //[allView release];
    
    NSError * error;
    if (![[GANTracker sharedTracker] trackPageview:@"/app_stats"
                                         withError:&error]) {
        NSLog(@"error in trackPageview");
    }
    
    
    
}


-(IBAction)searchResetAction:(id)sender{
    searchText.text = @"";
    [searchText resignFirstResponder];
    
    self.userArray = [[DataModel sharedDataModel] userSelectAll:model.repVO.rep_uid];
    [self.aTableView reloadData];
}




-(IBAction)goBack:sender{
    [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)showAll:sender{
    
    if(self.scrollView){
        //animation
        [UIView animateWithDuration:0.6
                         animations:^{ 
                             CGPoint center = self.scrollView.view.center;
                             center.y += 696;
                             self.scrollView.view.center = center;
                         } 
                         completion:^(BOOL finished){
                             [self.scrollView.view removeFromSuperview];
                             self.scrollView = nil;
                             
                         }];
        
        //[self.scrollView.view removeFromSuperview];
        //self.scrollView = nil;
    }
    
    [self.allView.view setAlpha:1];
    
    //reload the all data (default)
    self.userArray = [[DataModel sharedDataModel] userSelectAll:model.repVO.rep_uid];
    [self.aTableView reloadData];
    
    
    searchText.text = @"";
}

-(IBAction)search:sender{
    NSMutableArray * searchArr = [[DataModel sharedDataModel] userSearch:searchText.text uid:model.repVO.rep_uid];
    
    
    //if([searchArr count]>0){
    self.userArray = searchArr;
    [self.aTableView reloadData];
    //}
    
    [searchText resignFirstResponder];
    
    //self.showAllBtn.alpha = 1;
}






#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.userArray count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    
    UIView * subview = [cell.contentView viewWithTag:99];
    if(subview){
        [subview removeFromSuperview];
    }
    
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	//cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.highlightedTextColor = [UIColor blackColor];
    
    //cell.textLabel.textColor = [UIColor colorWithRed:0/255.0 green:105/255.0 blue:158/255.0 alpha:1.0];
    cell.detailTextLabel.textColor = [UIColor darkGrayColor];
    cell.detailTextLabel.highlightedTextColor = [UIColor darkGrayColor];
    
    //steps the make the cell background with alpha image
    UIView* backgroundView = [ [ [ UIView alloc ] initWithFrame:CGRectZero ] autorelease ];
    backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tablecell-bg-stats-active.png"]];
    backgroundView.opaque = NO;
    cell.backgroundView = backgroundView;
    for ( UIView* view in cell.contentView.subviews ) 
    {
        view.backgroundColor = [UIColor clearColor];
    }
    
    UIView* selctedBackgroundView = [ [ [ UIView alloc ] initWithFrame:CGRectZero ] autorelease ];
    selctedBackgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"gfx-tablecell-bg.png"]];
    selctedBackgroundView.opaque = NO;
    cell.selectedBackgroundView = selctedBackgroundView;
    
    
    //self.userArray
    UserVO * userVO = (UserVO *)[self.userArray objectAtIndex:indexPath.row];
    //NSLog(@"userVO:%@", userVO.user_uid);
    
    PracticeVO * practiceVO = [[DataModel sharedDataModel] practiceSelectOne:userVO.practice_uid];
    NSString * practiceName = practiceVO.name;
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", userVO.firstname, userVO.lastname];    
    cell.detailTextLabel.text = practiceName;
    
    
    //Badge
    NSMutableArray * calls = [[[DataModel sharedDataModel] callSelectByUserId:userVO.user_uid] retain];
    
    //NSLog(@"calls:%@",calls);
    
    UILabel *callNum=[[UILabel alloc] initWithFrame:CGRectMake(self.aTableView.frame.size.width/3, 12, 28, 22)]; 
    callNum.textColor=[UIColor darkGrayColor];
    callNum.textAlignment = UITextAlignmentCenter;
    [callNum setFont:[UIFont systemFontOfSize:15]];
    callNum.text= [NSString stringWithFormat:@"%d",[calls count]];
    callNum.backgroundColor= [UIColor whiteColor];//[UIColor colorWithRed:(123.0/255.0) green:(43.0/255) blue:(131.0/255) alpha:1.0]; 
    callNum.tag = 99;
    [cell.contentView addSubview:callNum]; 
    [callNum release];
    [calls release];
    
    callNum.layer.cornerRadius = 5.0;
    
    
    
    
    return cell;
}



#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UserVO * userVO = (UserVO *)[self.userArray objectAtIndex:indexPath.row];
    [searchText resignFirstResponder];
    
    if(userVO.rowid>0){
        //if(self.singleView) [self.singleView  ];
        [self.allView.view setAlpha:0];
        
        if(self.scrollView){
            [self.scrollView.view removeFromSuperview];
            self.scrollView = nil;
        }
        
        
        self.scrollView = [[[StatsViewScrollController alloc] initWithUserId:userVO.user_uid] autorelease];
        self.scrollView.view.frame = CGRectMake(0.00, 768.00, 1024.00, 696.00);
        self.scrollView.statsViewController = self;
        [self.view addSubview:self.scrollView.view];
        
        //animation
        [UIView beginAnimations:@"animation" context:nil];
        [UIView setAnimationDuration:0.6];
        self.scrollView.view.frame = CGRectMake(0.00, 768-696, 1024.00, 696.00);
        
        
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.scrollView.view cache:NO]; 
        [UIView commitAnimations];
        
        
        
        
        
        //[scrollView release];
        
        //self.showAllBtn.alpha = 1;
    }
    
    //[tableView deselectRowAtIndexPath:indexPath animated:YES]; 
    
}


#pragma mark textField delegate


- (BOOL) textFieldShouldClear:(UITextField *)textField{
    //
    
    NSMutableArray * searchArr = [[DataModel sharedDataModel] userSearch:@"" uid:model.repVO.rep_uid];
    
    self.userArray = searchArr;
    [self.aTableView reloadData];
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSMutableArray * searchArr = [[DataModel sharedDataModel] userSearch:textField.text uid:model.repVO.rep_uid];
    
    self.userArray = searchArr;
    [self.aTableView reloadData];
    
    return YES;
}







//private help functions
-(NSString *)timeFormat:(double)seconds{
    NSString * timeStr = @"0";
    
    if(seconds>0){
        int secs = (int)seconds % 60;
        int mins = (int)seconds / 60;
        int hrs  = (int)mins / 60;
        
        timeStr = [NSString stringWithFormat:@"%02d : %02d : %02ds", hrs, mins, secs];
    }
    return timeStr;
}





@end
