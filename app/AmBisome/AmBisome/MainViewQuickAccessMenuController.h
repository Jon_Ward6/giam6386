//
//  MainViewQuickAccessMenuController.h
//  AtriplaED
//
//  Created by Jonathan Xie on 27/06/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"
#import "DataModel.h"

@interface MainViewQuickAccessMenuController : UIViewController<UITableViewDelegate, UITableViewDataSource> {
    Model * model;
    DataModel * dataModel;
    UITableView * tableView_;
    NSMutableArray * tableArr_;
}

@property(nonatomic, retain) UITableView * tableView;
@property(nonatomic, retain) NSMutableArray * tableArr;

@end
