//
//  SearchClientViewController.h
//  AtriplaED
//
//  Created by Jonathan Xie on 8/06/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Model;
@class DataModel;

@interface SearchClientViewController : UIViewController<UITableViewDelegate, UITextFieldDelegate> {
    Model * model;
    DataModel * dataModel;
    UITableView * sourceTableView_;
    UITableView * targetTableView_;
    NSMutableArray * dataArr_;
    UITextField * searchText_;
    
}

@property(nonatomic, retain) IBOutlet UITableView * sourceTableView;
@property(nonatomic, retain) IBOutlet UITableView * targetTableView;
@property(nonatomic, retain) IBOutlet UITextField * searchText;
@property(nonatomic, retain) NSMutableArray * dataArr;


@end


@interface SearchClientViewController()

//- (void) accessoryButtonTapped: (UIControl *) button withEvent: (UIEvent *) event;

@end