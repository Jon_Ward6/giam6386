//
//  StatsViewAllController.m
//  eDetailer
//
//  Created by Jonathan Xie on 27/04/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "StatsViewAllController.h"
#import "DataModel.h"
#import "CallVO.h"
#import "CallModuleVO.h"
#import "UserVO.h"
#import "PracticeVO.h"
#import "Model.h"
#import "RepVO.h"

@implementation StatsViewAllController

@synthesize callArray, callModuleArray, userArray;
@synthesize totalUser, totalCall, totalCallDuration, averageCallDuration; 
@synthesize longestCallDuration, shortestCallDuration;
@synthesize totalModuleView, averageModuleDuration;




- (void)dealloc
{
    [totalUser release];
    [totalCall release];
    [totalCallDuration release];
    [averageCallDuration release];
    [longestCallDuration release];
    [shortestCallDuration release];
    [totalModuleView release];
    [averageModuleDuration release];
    
    [userArray release];
    [callModuleArray release];
    [model release];
    [dataModel release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    dataModel = [DataModel sharedDataModel];
    model = [Model sharedModel];
    
    self.callArray = [[DataModel sharedDataModel] callSelectAll:model.repVO.rep_uid];//[dataModel callSelectAll];
    self.callModuleArray = [dataModel callModuleSelectAll];
    self.userArray = [dataModel userSelectAll:model.repVO.rep_uid];
    
    
    totalUser.text = [NSString stringWithFormat:@"%d", [self.userArray count]];
    totalCall.text = [NSString stringWithFormat:@"%d", [self.callArray count]];
    
    
    //totalCallDuration
    double totalCallTime = 0.00;    
    for(CallVO * callVO in callArray){
        totalCallTime += callVO.time_spent;
    }
    totalCallDuration.text = [NSString stringWithFormat:@"%@", [self timeFormat:totalCallTime]];
    
    
    //averageCallDuration
    averageCallDuration.text = [NSString stringWithFormat:@"%@", [self timeFormat:totalCallTime/[callArray count]]];
    
    
    //longestCallDuration
    double longestCallTime = 0.00;
    for(CallVO * callVO in callArray){
        longestCallTime = MAX(callVO.time_spent, longestCallTime);
    }
    longestCallDuration.text = [NSString stringWithFormat:@"%@", [self timeFormat:longestCallTime]];
    
    
    //shortestCallDuration
    double shortestCallTime = 1000000.00;
    for(CallVO * callVO in callArray){
        shortestCallTime = MIN(callVO.time_spent, shortestCallTime);
    }
    if(shortestCallTime == 1000000.00) shortestCallTime = 0.00;
    
    shortestCallDuration.text = [NSString stringWithFormat:@"%@", [self timeFormat:shortestCallTime]];
    
    
    //totalModuleView
    NSMutableArray * totalModuleViewArr = [dataModel callModuleSelectAll];
    totalModuleView.text = [NSString stringWithFormat:@"%d", [totalModuleViewArr count]];
    
    
    //averageModuleDuration
    double totalModuleTime = 0.00;    
    for(CallModuleVO * callModuleVO in totalModuleViewArr){
        totalModuleTime += callModuleVO.time_spent;
    }
    
    averageModuleDuration.text = [NSString stringWithFormat:@"%@", [self timeFormat:totalModuleTime/[totalModuleViewArr count]]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}


-(NSString *)timeFormat:(double)seconds{
    NSString * timeStr = @"0";

    
    if(seconds>0){
        //int secs = 0;//;(int)seconds % 60;
        //int mins = (int)seconds / 60;
        //int hrs  = (int)mins / 60;
        
        int hrs = (int)seconds / 3600; 
        int remainder = (int)seconds % 3600; 
        int mins = (int)remainder / 60; 
        int secs = (int)remainder % 60;
        
        
        timeStr = [NSString stringWithFormat:@"%02d : %02d : %02ds", hrs, mins, secs];
    }
    return timeStr;
}


@end
