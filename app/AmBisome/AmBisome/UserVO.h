//
//  UserVO.h
//  Demo
//
//  Created by Jonathan Xie on 29/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UserVO : NSObject {
    NSInteger rowid;
    NSString * user_uid;
    NSString * rep_uid;
    NSString * practice_uid;
    NSString * firstname;
    NSString * lastname;
    NSString * email;
    NSString * phone;
    NSString * status;
}

@property (nonatomic, assign) NSInteger rowid;
@property (nonatomic, retain) NSString * user_uid;
@property (nonatomic, retain) NSString * rep_uid;
@property (nonatomic, retain) NSString * practice_uid;
@property (nonatomic, retain) NSString * firstname;
@property (nonatomic, retain) NSString * lastname;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * status;


-(UserVO *)dataSet:(NSDictionary*)dict;

@end
