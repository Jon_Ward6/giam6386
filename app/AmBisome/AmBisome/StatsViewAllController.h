//
//  StatsViewAllController.h
//  eDetailer
//
//  Created by Jonathan Xie on 27/04/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DataModel;
@class Model;

@interface StatsViewAllController : UIViewController {
    DataModel * dataModel;
    Model * model;
    //
    NSMutableArray * callArray;
    NSMutableArray * callModuleArray;
    NSMutableArray * userArray;
    
    
    UILabel * totalUser;
    UILabel * totalCall;
    UILabel * totalCallDuration;
    UILabel * averageCallDuration;
    UILabel * longestCallDuration;
    UILabel * shortestCallDuration;
    
    UILabel * totalModuleView;
    UILabel * averageModuleDuration;
}


@property(nonatomic, retain) NSMutableArray * callArray;
@property(nonatomic, retain) NSMutableArray * callModuleArray;
@property(nonatomic, retain) NSMutableArray * userArray;

@property(nonatomic, retain) IBOutlet UILabel * totalUser;
@property(nonatomic, retain) IBOutlet UILabel * totalCall;
@property(nonatomic, retain) IBOutlet UILabel * totalCallDuration;
@property(nonatomic, retain) IBOutlet UILabel * averageCallDuration;
@property(nonatomic, retain) IBOutlet UILabel * longestCallDuration;
@property(nonatomic, retain) IBOutlet UILabel * shortestCallDuration;


@property(nonatomic, retain) IBOutlet UILabel * totalModuleView;
@property(nonatomic, retain) IBOutlet UILabel * averageModuleDuration;


@end


@interface StatsViewAllController(private)
-(NSString *)timeFormat:(double)seconds;
@end
