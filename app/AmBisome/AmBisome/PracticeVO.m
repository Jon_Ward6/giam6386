//
//  PracticeVO.m
//  eDetailer
//
//  Created by Jonathan Xie on 15/04/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "PracticeVO.h"


@implementation PracticeVO

@synthesize rowid, practice_uid, name, street, suburb, postcode, state, region_code;


- (void)dealloc {
    [practice_uid release];
    [name release];
    [street release];
    [suburb release];
    [postcode release];
    [state release];
    [region_code release];
    
    [super dealloc];
}

@end
