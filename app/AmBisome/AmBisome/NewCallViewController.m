//
//  NewCallViewController.m
//  AtriplaED
//
//  Created by Jonathan Xie on 8/06/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import "NewCallViewController.h"
#import "NewClientViewController.h"
#import "SearchClientViewController.h"
#import "GoogleAnalytices.h"

@implementation NewCallViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{

    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    //Google Analytices
    [GoogleAnalytices trackPageView:@"/app_login_newcall"];
}



-(IBAction)onCancel:sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)onNewClient:sender{
    
    NewClientViewController * nextView = [[NewClientViewController alloc] init]; 
    [self.navigationController pushViewController:nextView animated:YES];
    [nextView release];
}

-(IBAction)onSearchClient:sender{
    
    SearchClientViewController * nextView = [[SearchClientViewController alloc] init]; 
    [self.navigationController pushViewController:nextView animated:YES];
    [nextView release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

@end
