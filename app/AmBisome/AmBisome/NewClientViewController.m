//
//  NewClientViewController.m
//  AtriplaED
//
//  Created by Jonathan Xie on 8/06/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import "NewClientViewController.h"
#import "UIView+Extensions.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "Model.h"
#import "DataModel.h"
#import "UserVO.h"
#import "CallVO.h"
#import "RepVO.h"
#import "PracticeVO.h"
#import "UserVO.h"
#import "JSON.h"
#import "Constants.h"
#import "GoogleAnalytices.h"
#import "LaunchViewController.h"


@implementation NewClientViewController

@synthesize firstname, lastname, practiceName, address, suburb, postcode, phone, email;
@synthesize currentControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    //[currentControl release];
    [firstname release];
    [lastname release];
    [practiceName release];
    [address release];
    [suburb release];
    [postcode release];
    [phone release];
    [email release];
    
    [model release];
    [dataModel release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    model = [Model sharedModel];
    dataModel = [DataModel sharedDataModel];
    
    //Google Analytices
    [GoogleAnalytices trackPageView:@"/app_login_newclient"];
    
    [[self.view viewWithTag:21] setAlpha:0];
    [[self.view viewWithTag:22] setAlpha:0];
    [[self.view viewWithTag:23] setAlpha:0];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];

}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

//back to the menu screen
-(IBAction)onMenu:sender{
    
    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
}

//back to the search screen
-(IBAction)onCancel:sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)onSave:sender{
    
    //validation
    if(![firstname.text length]>0){
        //show warning image
        [[self.view viewWithTag:21] setAlpha:1];
        return;
    }
    
    if(![lastname.text length]>0){
        //show warning image
        [[self.view viewWithTag:22] setAlpha:1];
        return;
    }
    
    if(![practiceName.text length]>0){
        //show warning image
        [[self.view viewWithTag:23] setAlpha:1];
        return;
    }
    //passed!
    [[self.view viewWithTag:21] setAlpha:0];
    [[self.view viewWithTag:22] setAlpha:0];
    [[self.view viewWithTag:23] setAlpha:0];
    
    //testing interent connection
    BOOL isOnline = [model isWebSiteReachable:SERVER_NAME];
    
    // 1. create and save Practice locally and remotely
    
    PracticeVO * practiceVO = [[[PracticeVO alloc] init] autorelease];
    practiceVO.practice_uid = [model uniqueString];
    practiceVO.name = practiceName.text;
    practiceVO.street = address.text;
    practiceVO.suburb = suburb.text;
    practiceVO.postcode = postcode.text;
    
    //int pId = [dataModel practiceAdd:practiceVO];
    [dataModel practiceAdd:practiceVO];
    
    // 2. create and save User locally and remotely
    
    //if(!pId>0) pId = 1;
    
    
    UserVO * userVO = [[[UserVO alloc] init] autorelease];
    userVO.user_uid = [model uniqueString];
    userVO.rep_uid = model.repVO.rep_uid;
    userVO.practice_uid = practiceVO.practice_uid;//pId;
    userVO.firstname = firstname.text;
    userVO.lastname = lastname.text;
    userVO.email = [email.text length]>0? email.text : [NSString stringWithFormat:@"dummyemail_%@", [model uniqueString]];
    userVO.status = isOnline? @"local" : @"offline";
    
    [dataModel userAdd:userVO];
    //
    model.userVO = userVO;
    
    //Fixed: if we have already selected at least one doctor, we should treat it as multiusers
    if([model.usersArr indexOfObject:userVO]==NSNotFound){
        [model.usersArr addObject:userVO];
    }
    
    
    //
    [self resignAllResponder];
    
    [self.navigationController popViewControllerAnimated:YES];
    //}
    
    /*
    //syn the practice to the server
    if(isOnline){
        NSMutableDictionary *practiceDict = [[[NSMutableDictionary alloc] init] autorelease];
        [practiceDict setValue:practiceVO.practice_uid forKey:@"practice_uid"];
        [practiceDict setValue:practiceVO.name forKey:@"name"];
        [practiceDict setValue:practiceVO.street forKey:@"street"];
        [practiceDict setValue:practiceVO.suburb forKey:@"suburb"];
        [practiceDict setValue:practiceVO.postcode forKey:@"postcode"];
        
        //build the array
        NSMutableArray *practiceDataArr = [[[NSMutableArray alloc] initWithObjects:practiceDict, nil] autorelease];
        
        //jSON
        NSString* jsonPracticeString = [practiceDataArr JSONRepresentation];
        [dataModel serverRequestWithAction:@"addpractice" andData:jsonPracticeString];
        
    }
    
    //sync to server from here
    if(isOnline){
        NSMutableDictionary *userDict = [[[NSMutableDictionary alloc] init] autorelease];
        [userDict setValue:userVO.user_uid forKey:@"user_uid"];
        [userDict setValue:userVO.rep_uid forKey:@"rep_uid"];
        [userDict setValue:userVO.practice_uid forKey:@"practice_uid"];
        [userDict setValue:userVO.firstname forKey:@"firstname"];
        [userDict setValue:userVO.lastname forKey:@"lastname"];
        [userDict setValue:userVO.email forKey:@"email"];
        [userDict setValue:@"local" forKey:@"status"];
        
        //build the array
        NSMutableArray *dataArr = [[[NSMutableArray alloc] initWithObjects:userDict, nil] autorelease];
        
        //jSON
        NSString* jsonUserString = [dataArr JSONRepresentation];
        [dataModel serverRequestWithAction:@"adduser" andData:jsonUserString];
        
    }
    */
    
    //try to delay the animation to give more time for data saving
    //[self performSelector:@selector(delayCloseAnimation) withObject:nil afterDelay:0.5];
    
}



-(void)delayCloseAnimation{
    //Now go back to search view
    [self.navigationController popViewControllerAnimated:YES];
    
}




-(IBAction)onBackgroundTouch:sender{
    [self resignAllResponder];
}


-(void)resignAllResponder{
    [firstname resignFirstResponder];
    [lastname resignFirstResponder];
    [practiceName resignFirstResponder];
    [address resignFirstResponder];
    [suburb resignFirstResponder];
    [postcode resignFirstResponder];
    [phone resignFirstResponder];
    [email resignFirstResponder];
}



-(BOOL) textFieldShouldReturn:(UITextField*) textField {
    
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
    
    
    //return YES;
}


- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
	currentControl = textField;
	return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField { 
	[self.view maintainVisibityOfControl:textField toView:self.view offset:0.0f];
    
    /*
     CGPoint point = [textField.superview convertPoint:textField.frame.origin toView:self.view];
     NSLog(@"textField: %f", point.y);
     
     float shiftBy = -100;
     [UIView beginAnimations:nil context:nil];
     [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
     CGAffineTransform slideTransform = CGAffineTransformMakeTranslation(0.0, shiftBy);
     self.view.transform = slideTransform;			
     [UIView commitAnimations];
     */
    
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	if (textField == currentControl) {
		//If the textfield is still the same one, we can reset the view animated
		[self.view resetViewToIdentityTransform];			
	}else {
        
	}
}

@end
