//
//  MainViewPopUpController.h
//  W6eDetailer
//
//  Created by Jonathan Xie on 31/10/11.
//  Copyright (c) 2011 Ward6. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewPopUpController : UIViewController<UIWebViewDelegate>{
    UIWebView * aWebView_;
    
    NSString * type_;
    NSString * name_;
    NSString * fileStr_;
    
    UIActivityIndicatorView *indicator;
    

}

@property (nonatomic, retain) IBOutlet UIWebView * aWebView;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * fileStr;
@property (nonatomic, retain) UIActivityIndicatorView *indicator;


- (id)initWithPage:(NSString *)file;

@end
