//
//  CallVO.h
//  Demo
//
//  Created by Jonathan Xie on 23/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserVO.h"
#import "ModuleVO.h"

@interface CallVO : NSObject {
    NSInteger rowid;
    NSString * call_uid;
    NSString * user_uid;
    NSString * rep_uid;
    NSDate * startTime_;
    NSDate * endTime_;
    double time_spent;
    
    NSMutableArray * moduleArray_;
    
    //added: multi-users
    NSMutableArray * usersArr;
    NSString * groupcall_uid;
}

@property (nonatomic, assign) NSInteger rowid;
@property (nonatomic, retain) NSString * call_uid;
@property (nonatomic, retain) NSString * user_uid;
@property (nonatomic, retain) NSString * rep_uid;
@property (nonatomic, assign) double time_spent;
@property (nonatomic, retain) NSDate * startTime;
@property (nonatomic, retain) NSDate * endTime;
@property (nonatomic, retain) NSMutableArray * moduleArray;

@property (nonatomic, retain) NSMutableArray * usersArr;
@property (nonatomic, retain) NSString * groupcall_uid;

- (void)addModule:(ModuleVO*)moduleVO;


@end
