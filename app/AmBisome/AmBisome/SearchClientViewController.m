//
//  SearchClientViewController.m
//  AtriplaED
//
//  Created by Jonathan Xie on 8/06/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import "SearchClientViewController.h"
#import "Model.h"
#import "DataModel.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "UserVO.h"
#import "PracticeVO.h"
#import "CallVO.h"
#import "RepVO.h"
#import "JSON.h"
#import "Constants.h"
#import "GoogleAnalytices.h"
#import "LaunchViewController.h"
#import "NewClientViewController.h"

@implementation SearchClientViewController

@synthesize sourceTableView=sourceTableView_,targetTableView=targetTableView_, dataArr=dataArr_,searchText=searchText_;



- (void)dealloc
{
    [searchText_ release],searchText_=nil;
    [dataArr_ release],dataArr_=nil;
    [model release];
    [dataModel release];
    [sourceTableView_ release],sourceTableView_=nil;
    [targetTableView_ release],targetTableView_=nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    model = [Model sharedModel];
    dataModel = [DataModel sharedDataModel];
    
    self.sourceTableView.backgroundColor = [UIColor clearColor];
    self.sourceTableView.opaque = NO;
    self.sourceTableView.backgroundView = nil;
    
    self.targetTableView.backgroundColor = [UIColor clearColor];
    self.targetTableView.opaque = NO;
    self.targetTableView.backgroundView = nil;
    
    //load data
    //self.dataArr = [dataModel userSelectAll:model.repVO.rep_uid];
    //[self.sourceTableView reloadData];
    
    
    //
    model.usersArr = nil;
    model.usersArr = [[[NSMutableArray alloc] init] autorelease];
    
    
    //Google Analytices
    [GoogleAnalytices trackPageView:@"/app_login_searchclient"];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //load and refresh data
    self.dataArr = [dataModel userSelectAll:model.repVO.rep_uid];
    [self.sourceTableView reloadData];
    [self.targetTableView reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}



//back to previous screen
-(IBAction)onCancel:sender{
    [self.searchText resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}


//do search
-(IBAction)onSearch:sender{
    NSMutableArray * searchArr = [dataModel userSearch:self.searchText.text uid:model.repVO.rep_uid];

    self.dataArr = searchArr;
    [self.sourceTableView reloadData];

    
    [self.searchText resignFirstResponder];
}

//clear up the search and back to default
-(IBAction)onResetDefault:sender{
    self.searchText.text = @"";
    [self onSearch:nil];
}


-(IBAction)onAddNewCall:sender{
    NewClientViewController * nextView = [[NewClientViewController alloc] init];
    [self.navigationController pushViewController:nextView animated:YES];
    [nextView release];
    
}



// next screen
-(IBAction)goNext:sender{
    //if any selection
    if([model.usersArr count]>0){
        LaunchViewController * nextView = [[LaunchViewController alloc] init];
        nextView.userVO = [model.usersArr objectAtIndex:0];//(UserVO *)[self.dataArr objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:nextView animated:YES];
        [nextView release];
    }
}


-(IBAction)onBackgroundTouch:sender{
    [self.searchText resignFirstResponder];
}


#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView.tag==1) return [self.dataArr count];
    if(tableView.tag==2) return [model.usersArr count];
    
    return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        
    }
    
    // Configure the cell.
    cell.textLabel.textColor = [UIColor whiteColor];
    
    UserVO * userVO = [[[UserVO alloc] init] autorelease];
    
    if(tableView.tag==1 && [self.dataArr count]>0){
        userVO = (UserVO *)[self.dataArr objectAtIndex:indexPath.row];
    }
    
    if(tableView.tag==2 && [model.usersArr count]>0){
        userVO = (UserVO *)(UserVO *)[model.usersArr objectAtIndex:indexPath.row];
    }
    
    
    PracticeVO * practiceVO = [dataModel practiceSelectOne:userVO.practice_uid];
    NSString * practiceName = practiceVO.name;    
    
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", userVO.firstname, userVO.lastname];
    cell.detailTextLabel.text = practiceName;
    
    
    //assessory view
    if(tableView.tag==2){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(0.0, 0.0, 26.0, 26.0);
        button.frame = frame;
        [button setBackgroundImage:[UIImage imageNamed:@"btn-x.png"] forState:UIControlStateNormal];
        
        [button addTarget:self action:@selector(accessoryButtonTapped:withEvent:)  forControlEvents:UIControlEventTouchUpInside];
        button.backgroundColor = [UIColor clearColor];
        cell.accessoryView = button;
    }
    
    if(tableView.tag==1){
        //accessory view
        UIView* accessoryView = [ [ [ UIView alloc ] initWithFrame:CGRectZero ] autorelease ];
        accessoryView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"gfx-tablecell-accessory.png"]];
        accessoryView.frame = CGRectMake(0.0, 0.0, 9.0, 14.0);
        accessoryView.opaque = NO;
        cell.accessoryView = accessoryView;
        
    }
    
    
    
    
    // UI configuration  ------------------------------------------------------------------------------------------------
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.highlightedTextColor = [UIColor blackColor];
    
    cell.detailTextLabel.textColor = [UIColor darkGrayColor];
    cell.detailTextLabel.highlightedTextColor = [UIColor darkGrayColor];
    
    //steps the make the cell background with alpha image
    UIView* backgroundView = [[[ UIView alloc ] initWithFrame:CGRectZero ] autorelease];
    backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tablecell-bg-stats-active.png"]];
    backgroundView.opaque = NO;
    cell.backgroundView = backgroundView;
    
    for ( UIView* view in cell.contentView.subviews ) 
    {
        view.backgroundColor = [UIColor clearColor];
    }
    
    
    UIView* selctedBackgroundView = [ [ [ UIView alloc ] initWithFrame:CGRectZero ] autorelease ];
    selctedBackgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"gfx-tablecell-bg.png"]];
    selctedBackgroundView.opaque = NO;
    cell.selectedBackgroundView = selctedBackgroundView;
    

    
    return cell;
}



#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    if(tableView.tag==1){
        UserVO * userVO = (UserVO *)[self.dataArr objectAtIndex:indexPath.row];
       
        if([model.usersArr indexOfObject:userVO]==NSNotFound){
            [model.usersArr addObject:userVO];
            
            [self.targetTableView reloadData];
        }
    }
    
}



- (void) accessoryButtonTapped: (UIControl *) button withEvent: (UIEvent *) event
{
    NSIndexPath * indexPath = [self.targetTableView indexPathForRowAtPoint: [[[event touchesForView: button] anyObject] locationInView: self.targetTableView]];
    if (indexPath == nil) return;
    
    //[self.tableView.delegate tableView: self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
     UserVO * userVO = (UserVO *)[model.usersArr objectAtIndex:indexPath.row];
    
    if([model.usersArr indexOfObject:userVO]!=NSNotFound){
        [model.usersArr removeObject:userVO];
        
        [self.targetTableView reloadData];
    }

}



#pragma mark textField delegate


- (BOOL) textFieldShouldClear:(UITextField *)textField{
    //
    
    NSMutableArray * searchArr = [dataModel userSearch:@"" uid:model.repVO.rep_uid];
    
    self.dataArr = searchArr;
    [self.sourceTableView reloadData];
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSMutableArray * searchArr;
    
    //the last charater won't clearup!
    if([textField.text length]>1){
        searchArr = [dataModel userSearch:textField.text uid:model.repVO.rep_uid];
    }else{
        searchArr = [dataModel userSearch:@"" uid:model.repVO.rep_uid];
    }
    
    self.dataArr = searchArr;
    [self.sourceTableView reloadData];
    
    return YES;
}



@end
