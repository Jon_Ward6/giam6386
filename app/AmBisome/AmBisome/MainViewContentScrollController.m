//
//  MainViewContentScrollController.m
//  AmBisome
//
//  Created by Jonathan Xie on 9/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainViewContentScrollController.h"
#import "Model.h"
#import "AppScrollView.h"
#import "MainViewPageController.h"

@interface MainViewContentScrollController ()
- (void) configScrollView;
- (void)loadScrollViewWithPage:(int)page;
@end

@implementation MainViewContentScrollController

@synthesize scrollView;
@synthesize pageControllers;
@synthesize currentPageIndex;
@synthesize pageArr;
@synthesize closeBtn,moduleID;


- (id)initWithDataArray:(NSArray *)array 
{
    self = [super init];
    if (self) {
        self.pageArr = array;
        pageControllers = [[NSMutableArray alloc] init];
        //currentPageIndex = 1;
    }
    return self;
}

- (id)initWithDataArray:(NSArray *)array moduleID:(NSString*)mid
{
    self = [super init];
    if (self) {
        self.pageArr = array;
        self.moduleID = mid;
        pageControllers = [[NSMutableArray alloc] init];
        //currentPageIndex = 1;
    }
    return self;
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    model = nil;
    [pageControllers release], pageControllers = nil;
    [pageArr release], pageArr = nil;
    [scrollView release], scrollView = nil;
    [closeBtn release], closeBtn=nil;
    [moduleID release], moduleID=nil;
    [super dealloc];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    
    model = [Model sharedModel];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-module.png"]];
    self.navigationController.navigationBarHidden = YES;
    
    [self configScrollView];
    
    //if we have module 2
    if(self.moduleID && [self.moduleID rangeOfString: @"module2" ].location != NSNotFound){
        [self shwoCloseButton];
    }
    
    //add listener
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationListener:) name:@"nextPageContentScroll" object:nil];
}


-(void)notificationListener:(NSNotification *)notification{
    NSDictionary *userInfo = [notification userInfo];
    NSString * action = [userInfo objectForKey:@"action"];
    
    if([action isEqualToString:@"nextPage"]){
        [self nextPage];
    }
    
}


- (void) configScrollView {
	
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    NSUInteger count = [self.pageArr count];
    
    
    for (unsigned i = 0; i < count; i++) {
        [controllers addObject:[NSNull null]];
    }
    self.pageControllers = controllers;
    [controllers release];
    //
    self.scrollView.pagingEnabled = YES;
    //scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * count, 1);
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * count, self.scrollView.frame.size.height);
    
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
	self.scrollView.contentMode = UIViewContentModeScaleAspectFit;
	self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.scrollView.minimumZoomScale = (self.scrollView.frame.size.width / self.scrollView.contentSize.width) / 1.00;//0.99;
    self.scrollView.bounces = NO;
    
    
    
    // cause it's at second level of the stack - the initial process is bit slow for whatever the reason
    [self performSelector:@selector(delayLoad:) withObject:nil afterDelay:0.1];
    
    //start tracking
    [self trackPage:0];
	
}

-(void)delayLoad:(id)sender{
    [self loadScrollViewWithPage:0];
	[self loadScrollViewWithPage:1];
}

- (void)loadScrollViewWithPage:(int)page {
	if(page < 0) return;
	if(page >=[self.pageArr count]) return;
    
    // replace the placeholder if necessary
    MainViewPageController *controller = [pageControllers objectAtIndex:page];
	
    if ((NSNull *)controller == [NSNull null]) {
		controller = [[MainViewPageController alloc] initWithPage:[self.pageArr objectAtIndex:page]];
        [pageControllers replaceObjectAtIndex:page withObject:controller];
		
        [controller release];
    }
    
	
    // add the controller's view to the scroll view
    if (nil == controller.view.superview) {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        
		
        [self.scrollView addSubview:controller.view];
		
    }
    
	controller.view.tag = page+300;
    
    //force update on every page! - MainViewPageController
    NSString * action = (page==currentPageIndex)? @"onScreen" : @"offScreen";
    
    if(page==currentPageIndex){
        [controller notifyPageAction:action];
    }
    
}



- (void)scrollViewDidScroll:(UIScrollView *)sender {
	
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView {
    
    //[self removeScrollViewPages];
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
	
    if(model.callVO != nil){//not for the demo!
        [self trackPage:page];
    }
    currentPageIndex = page;
    
	
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    
}


-(void)nextPage
{
    [self gotoPage:currentPageIndex + 1];
}

-(void)prevPage
{
    [self gotoPage:currentPageIndex - 1];
}



-(void)gotoPage:(int)page
{
    
    if(page<[self.pageControllers count] && page>=0){
        
        currentPageIndex = page;
        
        [self loadScrollViewWithPage:page - 1];
        [self loadScrollViewWithPage:page];
        [self loadScrollViewWithPage:page + 1];
        
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        
        [self.scrollView scrollRectToVisible:frame animated:YES];
        
        //currentPageIndex = page;
    }
    
}


//ADD::page time tracking
- (void)trackPage:(NSInteger)page {
    
}


-(void)shwoCloseButton
{
    if(self.closeBtn!=nil) return;//once
    //
    self.closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.closeBtn.frame = CGRectMake(1000-22, -10, 40, 70);
    [self.closeBtn setImage:[UIImage imageNamed:@"btn-close-quickacess.png"] forState:UIControlStateNormal];
    // add targets and actions
    [self.closeBtn addTarget:self action:@selector(closeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:self.closeBtn];
}


-(void)closeBtnClicked:sender
{
    //[self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
}






@end
