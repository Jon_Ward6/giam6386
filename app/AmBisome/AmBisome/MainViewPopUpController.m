//
//  MainViewPopUpController.m
//  W6eDetailer
//
//  Created by Jonathan Xie on 31/10/11.
//  Copyright (c) 2011 Ward6. All rights reserved.
//

#import "MainViewPopUpController.h"

@implementation MainViewPopUpController

@synthesize aWebView=aWebView_, type=type_, name=name_,fileStr=fileStr_;
@synthesize indicator;

- (void)dealloc
{
    [fileStr_ release],fileStr_=nil;
    //[aWebView_ release], aWebView_=nil;
    aWebView_=nil;
    [type_ release], type_=nil;
    [name_ release], name_=nil;
    [indicator release],indicator=nil;
    [super dealloc];
}




- (id)initWithPage:(NSString *)file
{
    self = [super initWithNibName:@"MainViewPopUpController" bundle:nil];
    if (self!=nil) {  
        
        //NSLog(@"self.fileStr:%@", file);
        self.fileStr = file;
        
        NSArray *fileArr = [file componentsSeparatedByString: @".pdf"];
        if([fileArr count]<2) return self;
        
        self.type = @"pdf";
        self.name = [fileArr objectAtIndex:0];
    
        
        
    }
    return self;
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    self.aWebView.scalesPageToFit = YES;
    
    //detect it's web pdf or local pdf
    NSRange range = [self.fileStr rangeOfString:@"/"];
    
    NSURLRequest *request = nil;
    //web URL
    if(range.length>0){
        //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@.%@", self.name, self.type]];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", self.fileStr]];
        request = [NSURLRequest requestWithURL:url];
        
        
        //
        self.indicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
        self.indicator.center = CGPointMake(1024/2, 768/2 - 40);
        self.indicator.hidesWhenStopped = YES;
        
        [self.view addSubview:self.indicator];
        
    }else{
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:self.name ofType:self.type];
        NSURL *baseURL = [NSURL fileURLWithPath:htmlFile];
        request = [NSURLRequest requestWithURL:baseURL];
        

    }

    [self.aWebView loadRequest:request];
    
    

}




-(IBAction)closePopup:(id)sender{
    [self.view removeFromSuperview];
}




-(IBAction)historyBack:(id)sender{
    if([self.aWebView canGoBack]){
        [self.aWebView goBack];
    }
}

-(IBAction)historyForward:(id)sender{
    if([self.aWebView canGoForward]){
        [self.aWebView goForward];
    }
    
}





#pragma mark -
#pragma mark webView

- (void)webViewDidStartLoad:(UIWebView *)webView {

	if(self.indicator!=nil) [self.indicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    if(self.indicator!=nil) [self.indicator stopAnimating];
    
}



- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    return YES; // Return YES to make sure regular navigation works as expected.
    
    
}



- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    if ( [error code] != 204 ) {
        
        // report the error inside the webview
        NSString* errorString = [NSString stringWithFormat:@"Failed to load page: %@ - %d",error.localizedDescription, [error code]];
        [self.aWebView loadHTMLString:errorString baseURL:nil];
        
    }
    
    
}




@end
