//
//  StatsViewSingleController.h
//  eDetailer
//
//  Created by Jonathan Xie on 27/04/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CallVO;

@interface StatsViewSingleController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    CallVO * callVO_;
    NSArray * pageArr_;
    UITableView * aTableView;
    
    UILabel * callTime;
    UILabel * totalDuration;
    UILabel * moduleViews;
    UILabel * pageViews;
    UIButton * groupButton_;
    
    NSString * callTitle_;
    UILabel * callLabel;
    
    NSDictionary *tableContents_;
}


@property(nonatomic, retain) CallVO * callVO;
@property(nonatomic, retain) NSArray * pageArr;
@property(nonatomic, retain) IBOutlet UILabel * callTime;
@property(nonatomic, retain) IBOutlet UILabel * totalDuration;
@property(nonatomic, retain) IBOutlet UILabel * moduleViews;
@property(nonatomic, retain) IBOutlet UILabel * pageViews;
@property(nonatomic, retain) IBOutlet UIButton * groupButton;
@property(nonatomic, retain) IBOutlet UILabel * callLabel;

@property(nonatomic, retain) UITableView * aTableView;
@property(nonatomic, retain) NSString * callTitle;

@property(nonatomic, retain) NSDictionary *tableContents;

- (id)initWithCallObject:(CallVO*)aCallVO label:(NSString *)labelString;

@end



@interface StatsViewSingleController(Private)
-(NSString *)timeFormat:(double)seconds;
-(NSArray *)sectionArray:(NSArray*)sourceArray;
-(double)sectionTotalTime:(NSInteger)section;
@end