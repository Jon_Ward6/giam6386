//
//  PageVO.m
//  Demo
//
//  Created by Jonathan Xie on 23/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "PageVO.h"

@implementation PageVO

@synthesize startTime=startTime_, endTime=endTime_, time_spent;
@synthesize rowid, page_uid, page_name, module_uid, filename, type;

-(void)setEndTime:(NSDate *)aEndTime{
    //endTime_ = [aEndTime retain];
    
    if (endTime_ != aEndTime)
    {
        [endTime_ release];
        endTime_ = [aEndTime retain];
    }
    
    self.time_spent = [endTime_ timeIntervalSinceDate:startTime_];
}

- (void)dealloc {
    [module_uid release],module_uid=nil;
    [page_uid release],page_uid=nil;
    [page_name release],page_name=nil;
    [filename release],filename=nil;
    [type release],type=nil;
    [startTime_ release],startTime_=nil;
    [endTime_ release],endTime_=nil;
    
    [super dealloc];
}


- (NSString *)description {
    NSString *desc = [NSString stringWithFormat:@"PageVO:{\n module_uid=%@\n page_uid=%@\n filename=%@\n type=%@\n time_spent=%0.1f}",
                      module_uid,
                      page_uid,    
                      filename, 
                      type,
                      time_spent];
	return desc;
}


@end
