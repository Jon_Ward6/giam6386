//
//  CallPageVO.m
//  W6eDetailer
//
//  Created by Jonathan Xie on 20/10/11.
//  Copyright (c) 2011 Ward6. All rights reserved.
//

#import "CallPageVO.h"

@implementation CallPageVO

@synthesize rowid, call_uid, page_uid, page_name, module_uid, user_uid, time_spent;

- (void)dealloc {
    [call_uid release];
    [module_uid release];
    [user_uid release];
    [page_uid release];
    [page_name release];
    [super dealloc];
}

- (NSString *)description {
    NSString *desc = [NSString stringWithFormat:@"CallPageVO:{\n call_uid=%@\n page_uid=%@\n module_uid=%@\n user_uid=%@\n time_spent=%0.1f\n page_name=%@}",
                      call_uid,
                      page_uid,    
                      module_uid, 
                      user_uid,
                      time_spent,
                      page_name];
	return desc;
}

@end
