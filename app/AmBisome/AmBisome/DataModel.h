//
//  DataModel.h
//  eDetailer
//
//  Created by Jonathan Xie on 11/04/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RepVO;
@class PracticeVO;
@class UserVO;
@class CallVO;
@class CallModuleVO;
@class DataModel;
@class PageVO;
@class ModuleVO;
@class CallPageVO;


@protocol DataModelDelegate
@optional
- (void)dataModelDidFinishLoading:(DataModel*)dataModel data:(id)data;
- (void)dataModel:(DataModel*)model didFailWithError:(NSError *)error;
@end



@interface DataModel : NSObject {
    NSString *databaseName;
	NSString *databasePath;
    
    id <NSObject, DataModelDelegate> delegate;
}

@property (nonatomic, retain) NSString *databasePath;

@property (nonatomic, assign) id <NSObject, DataModelDelegate> delegate;



+ (DataModel *)sharedDataModel;

- (void)serverRequestWithAction:(NSString*)action andData:(NSString *)jsonString;
- (void)checkAndCreateDatabase;

//call
//-(NSMutableArray * )callSelectAll;
-(NSMutableArray * )callSelectAll:(NSString *)rep_uid;
-(NSMutableArray * )callSelectByUserId:(NSString*)user_uid;
-(int)callAdd:(CallVO *)callVO;

//callModule
-(NSMutableArray * )callModuleSelectAll;
-(NSMutableArray * )callModuleSelectByCallId:(NSString*)call_uid;
-(int)callModuleAdd:(CallModuleVO *)callModuleVO;

//rep
-(int)repAdd:(RepVO *)repVO;
-(RepVO * )repSelectOne:(NSString *)username passwd:(NSString *)password;

//practice
-(NSMutableArray *)practiceSelectAll;
-(PracticeVO*)practiceSelectOne:(NSString*)uid;
-(int)practiceAdd:(PracticeVO *)practiceVO;

//user
-(int)userAdd:(UserVO *)userVO;
-(NSMutableArray * )userSelectAll:(NSString *)rep_uid;
-(NSMutableArray * )userSelectByProp:(NSString *)name andValue:(id)value;
-(NSMutableArray * )userSearch:(NSString *)word uid:(NSString *)rep_uid;
-(UserVO*)userSelectOne:(NSString *)uid;
-(BOOL)userOfflineStatusUpdate:(NSString*)status;
-(NSMutableArray * )getGroupCallByID:(NSString*)groupcall_uid;

//module
-(NSMutableArray * )moduleSelectAll;
-(ModuleVO * )moduleSelectByUid:(NSString*)module_uid;

//page
-(NSMutableArray * )pageSelectByModuleId:(NSString*)module_uid;
-(NSMutableArray * )pageQuickAccessSelectByModuleId:(NSString*)module_uid;
-(NSMutableArray * )quickAccessSelectAll;

-(void)savePageArray:(NSMutableArray * )pageArray;
-(void)PageAdd:(PageVO*)pageVO;
-(NSMutableArray * )callPageSelectByCallId:(NSString*)call_uid;
-(int)callPageAdd:(CallPageVO *)callPageVO;
- (void)downloadPages:(NSArray*)data;
-(PageVO*)getPageByFileName:(NSString*)fileName;

//syn up to server
-(void)uploadAllData;
-(void)downloadAllData:(NSString*)rep_uid;
-(NSMutableArray * )callSelectAllData;
-(NSMutableArray * )callModuleSelectAllData;
-(NSMutableArray * )callPageSelectAllData;
-(NSMutableArray * )userSelectAllData;
-(NSMutableArray * )practiceSelectAllData;

//keycode
-(void)keycodeLogin:(NSString*)keycode;

//heplers
-(NSDate *)StringToNSDate:(NSString *)dateString;


//Reachable
-(BOOL) isInternetReachable;
-(BOOL) isWebSiteReachable: (NSString *)host;

-(void)testOn;

@end
