//
//  CallPageVO.h
//  W6eDetailer
//
//  Created by Jonathan Xie on 20/10/11.
//  Copyright (c) 2011 Ward6. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CallPageVO : NSObject{
    NSInteger rowid;
    NSString * call_uid;
    NSString * page_uid;
    NSString * page_name;
    NSString * module_uid;
    NSString * user_uid;
    double time_spent;
}

@property (nonatomic, assign) NSInteger rowid;
@property (nonatomic, retain) NSString * call_uid;
@property (nonatomic, retain) NSString * page_uid;
@property (nonatomic, retain) NSString * page_name;
@property (nonatomic, retain) NSString * module_uid;
@property (nonatomic, retain) NSString * user_uid;
@property (nonatomic, assign) double time_spent;

@end
