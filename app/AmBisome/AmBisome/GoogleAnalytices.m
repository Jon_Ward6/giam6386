//
//  GoogleAnalytices.m
//  eDetailer
//
//  Created by Jonathan Xie on 19/05/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "GoogleAnalytices.h"
#import "GANTracker.h"

@implementation GoogleAnalytices

+(void)trackPageView:(NSString*)view;{
    NSError * error;
    
    if (![[GANTracker sharedTracker] trackPageview:view withError:&error]) {
        NSLog(@"error in trackPageview");
    }
}

+(void)trackCustomVariable:(NSString *)name value:(id)value index:(NSInteger)index{
    NSError *error;
    
    if (![[GANTracker sharedTracker] setCustomVariableAtIndex:index
                                                         name:name
                                                        value:value
                                                    withError:&error]) {
        NSLog(@"error in setCustomVariableAtIndex");
    }
}

@end
