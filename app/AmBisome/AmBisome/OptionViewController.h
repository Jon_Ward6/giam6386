//
//  OptionViewController.h
//  AtriplaED
//
//  Created by Jonathan Xie on 8/06/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Model;
@class DataModel;

@interface OptionViewController : UIViewController{
    Model * model;
    DataModel * dataModel;
}

@end
