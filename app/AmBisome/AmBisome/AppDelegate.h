//
//  AppDelegate.h
//  AmBisome
//
//  Created by Jonathan Xie on 3/04/12.
//  Copyright (c) 2012 Ward6. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
