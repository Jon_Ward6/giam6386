//
//  RepVO.m
//  eDetailer
//
//  Created by Jonathan Xie on 14/04/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "RepVO.h"
#import "FMDatabase.h"


@implementation RepVO

@synthesize rowid,rep_uid, username,password,email,regionCode,access_level;

- (void)dealloc {
    [rep_uid release];
    [username release];
    [password release];
    [email release];
    [regionCode release];
   
    [super dealloc];
}


-(RepVO *)dataSet:(NSDictionary*)dict{
    username = [dict valueForKey:@"username"];
    password = [dict valueForKey:@"password"];
    email = [dict valueForKey:@"email"];
    regionCode = [dict valueForKey:@"regionCode"];
    access_level = [[dict valueForKey:@"access_level"] intValue];
    
    return self;
}

- (NSString *)description {
    NSString *desc = [NSString stringWithFormat:@"RepVO:{\n username=%@\n password=%@\n email=%@\n regionCode=%@\n access_level=%d}",
                      username,
                      password,    
                      email, 
                      regionCode,
                      access_level];
	return desc;
}





@end
