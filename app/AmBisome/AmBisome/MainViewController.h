//
//  MainViewController.h
//  Nurofen
//
//  Created by Jonathan Xie on 4/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Model;
@class DataModel;
@class MainViewContentController;
@class MainViewPopUpController;
@class MainViewNavController;
@class MainViewQuickAccessController;

@interface MainViewController : UIViewController <UIGestureRecognizerDelegate>{
    Model * model;
    DataModel * dataModel;
    
    MainViewContentController * contentView;
    
    //
    MainViewNavController * navView;
    MainViewQuickAccessController * quickAccessView;
    //
    UITapGestureRecognizer *tapGR;
    UISwipeGestureRecognizer *swipeUpGR;
    UISwipeGestureRecognizer *swipeDownGR;
    
    //
    MainViewPopUpController * popup_;
}


@property(nonatomic, retain) MainViewContentController * contentView;

@property(nonatomic, retain) MainViewNavController * navView;
@property(nonatomic, retain) MainViewQuickAccessController * quickAccessView;

@property (nonatomic, retain) UITapGestureRecognizer *tapGR;
@property (nonatomic, retain) UISwipeGestureRecognizer *swipeUpGR;
@property (nonatomic, retain) UISwipeGestureRecognizer *swipeDownGR;
@property (nonatomic, retain) MainViewPopUpController * popup;

-(void)btnAction:sender;

-(void)showDashBoard:(BOOL)flag;
-(void)showQuickAccessView:(BOOL)flag;

@end


