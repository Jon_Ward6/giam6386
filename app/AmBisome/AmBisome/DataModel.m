//
//  DBModel.m
//  eDetailer
//
//  Created by Jonathan Xie on 11/04/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "DataModel.h"
#import "SynthesizeSingleton.h"
#import "Constants.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "JSON.h"
#import "Reachability.h"
#import "CallVO.h"
#import "CallModuleVO.h"
#import "ModuleVO.h"
#import "UserVO.h"
#import "RepVO.h"
#import "PracticeVO.h"
#import "PageVO.h"
#import "CallPageVO.h"
#import "UserVO.h"
#import "Utilities.h"
#import "Model.h"
#import "CallPageVO.h"


#define TBL_USER @"user"
#define TBL_MODULE @"module"
#define TBL_CALL_MODULE @"call_module"
#define TBL_CALL @"call"
#define TBL_PRACTICE @"practice" 
#define TBL_REGION @"region"
#define TBL_REP @"rep"
#define TBL_REP_REGION @"rep_region"



@interface DataModel(private)

-(FMDatabase *) openDataBase;
- (void)downloadUsers:(NSArray*)data;
- (void)downloadPractices:(NSArray*)data;
- (void)downloadRep:(NSDictionary*)data;
- (void)downloadCalls:(NSArray*)data;
- (void)downloadCallModules:(NSArray*)data;
@end



@implementation DataModel

@synthesize databasePath;
@synthesize delegate;

SYNTHESIZE_SINGLETON_FOR_CLASS(DataModel);

- (id) init
{
	self = [super init];
	if (self != nil) {
        
        databaseName = @"eDetailer.sqlite";
        
        // Get the path to the documents directory and append the databaseName
		NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDir = [documentPaths objectAtIndex:0];
		databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
        [databasePath retain];
        
        [self checkAndCreateDatabase];
        
        //test
        //NSString *myMD5String = [Utilities returnMD5Hash:@"BeyondHIV+Atripla"];
        //NSLog(@"myMD5String: %@", myMD5String);
        
    }
    
    return self;
}



- (void) dealloc
{
    [databasePath release];
    delegate = nil;
	[super dealloc];
}



// REMOTE DATABASE ---------------------------------------------------------------------------------------------------------

- (void)serverRequestWithAction:(NSString*)action andData:(NSString *)jsonString{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", WEBSERVICE_URL, action]];
	ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
	[request addPostValue:[Utilities returnMD5Hash:@"BeyondHIV+Atripla"] forKey:@"key"];
	[request addPostValue:action forKey:@"action"];
	[request addPostValue:jsonString forKey:@"data"];
    
    
	[request setDelegate:self];
	[request startAsynchronous];
    
}



- (void)requestFinished:(ASIHTTPRequest *)request
{
	// Use when fetching text data
	NSString *responseString = [request responseString];
	//NSLog(@"responseString: %@", responseString);
	
	// Use when fetching binary data
	//NSData *responseData = [request responseData];
	//NSLog(@"responseData: %@", responseData);
    
    //make sure we only call this bit only if it's in 'getusers'. otherwise you will get JSONValue error
    
    //getUsers
    NSRange range = [responseString rangeOfString:@"getusers"];
    if(range.length>0){
        NSDictionary * dict = [responseString JSONValue];
        NSString * type = [dict objectForKey:@"type"];
        
        //get users: reuturn values from server
        if([type isEqualToString:@"getusers"]){
            NSArray * users = [dict objectForKey:@"data"];
            if(users && [users count]>0){
                [self downloadUsers:users];
                return;
            }
        }
    }
    
    //get server rep
    NSRange rep_range = [responseString rangeOfString:@"getrep"];
    if(rep_range.length>0){
        
        NSDictionary * rep_dict = [responseString JSONValue];
        NSString * rep_type = [rep_dict objectForKey:@"type"];
        
        if([rep_type isEqualToString:@"getrep"]){
            NSDictionary * rep = [rep_dict objectForKey:@"data"];
            
            if(rep && [rep count]>0){
                [self downloadRep:rep];
                return;
            }
        }
        
        //notify the listener
        //if ([delegate respondsToSelector:@selector(dataModelDidFinishLoading:)]){
        [delegate dataModel:self didFailWithError:nil];
        //}
        
    }
    
    
    //get server rep
    NSRange calls_range = [responseString rangeOfString:@"getcalls"];
    if(calls_range.length>0){
        NSDictionary * dict = [responseString JSONValue];
        NSString * type = [dict objectForKey:@"type"];
        
        //get users: reuturn values from server
        if([type isEqualToString:@"getcalls"]){
            NSArray * calls = [dict objectForKey:@"data"];
            if(calls && [calls count]>0){
                [self downloadCalls:calls];
                return;
            }
        }
        
    }
    
    
    //getcallpages
    NSRange page_range = [responseString rangeOfString:@"getcallpages"];
    if(page_range.length>0){
        NSDictionary * dict = [responseString JSONValue];
        //NSLog(@"getcallpages: %@", dict);
        NSString * type = [dict objectForKey:@"type"];
        
        //get users: reuturn values from server
        if([type isEqualToString:@"getcallpages"]){
            NSArray * data = [dict objectForKey:@"data"];
            if(data && [data count]>0){
                [self downloadPages:data];
                return;
            }
        }
    }

    
    
    //get keycode
    NSRange keycode_range = [responseString rangeOfString:@"getkeycode"];
    if(keycode_range.length>0){
        NSDictionary * dict = [responseString JSONValue];
        NSString * type = [dict objectForKey:@"type"];
        
        if([type isEqualToString:@"getkeycode"]){
            NSString * data = [dict objectForKey:@"data"];
            [self.delegate dataModelDidFinishLoading:self data:data];
        }
    }
	
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
	NSError *error = [request error];
    
    NSLog(@"requestFailed: %@", error);
    //notify the listener
    //if ([delegate respondsToSelector:@selector(dataModelDidFinishLoading:)]){
    [delegate dataModel:self didFailWithError:error];
    //}
}

//callback handler
- (void)downloadUsers:(NSArray*)data{
    UserVO * userVO = nil;//[[[UserVO alloc] init] autorelease];
    //
    for(NSDictionary * user in data){
        userVO = [[[UserVO alloc] init] autorelease];
        userVO.user_uid = [user objectForKey:@"user_uid"];
        userVO.rep_uid = [user objectForKey:@"rep_uid"];
        userVO.practice_uid = [user objectForKey:@"practice_uid"];
        userVO.firstname = [user objectForKey:@"firstname"];
        userVO.lastname = [user objectForKey:@"lastname"];
        userVO.email = [user objectForKey:@"email"];
        
        //add practice which is associated with these users
        [self downloadPractices:[user objectForKey:@"practice_arr"]];
        
        //[self userAdd:userVO];
        //NOTICE:: we need to make the delay call to avoid choking sqlite!
        [self performSelector:@selector(downloadUsersDelay:) withObject:userVO afterDelay:0.0];
        
    }
    
}

- (void)downloadUsersDelay:(UserVO*)userVO
{
    [self userAdd:userVO];
}

//callback handler
- (void)downloadPractices:(NSArray*)data{
    PracticeVO * practiceVO = nil;//[[[PracticeVO alloc] init] autorelease];
    //
    for(NSDictionary * practice in data){
        practiceVO = [[[PracticeVO alloc] init] autorelease];
        practiceVO.name = [practice objectForKey:@"name"];
        practiceVO.practice_uid = [practice objectForKey:@"practice_uid"];
        practiceVO.street = [practice objectForKey:@"street"];
        practiceVO.suburb = [practice objectForKey:@"suburb"];
        practiceVO.state = [practice objectForKey:@"state"];
        practiceVO.postcode = [practice objectForKey:@"postcode"];
        practiceVO.region_code = [practice objectForKey:@"region_code"];
        
        //[self practiceAdd:practiceVO];
        //NOTICE:: we need to make the delay call to avoid choking sqlite!
        [self performSelector:@selector(downloadPracticesDelay:) withObject:practiceVO afterDelay:0.0];
    }
    
    
}

- (void)downloadPracticesDelay:(PracticeVO*)practiceVO{
    [self practiceAdd:practiceVO];
}

//callback handler
- (void)downloadRep:(NSDictionary*)data{
    RepVO * repVO = [[[RepVO alloc] init] autorelease];
    
    repVO.rep_uid = [data objectForKey:@"rep_uid"];
    repVO.username = [data objectForKey:@"username"];
    repVO.password = [data objectForKey:@"password"];
    repVO.email = [data objectForKey:@"email"];
    repVO.access_level = [[data objectForKey:@"access_level"] intValue];
    
    
    if(repVO.rep_uid){
        [self repAdd:repVO];
        //
        //if ([delegate respondsToSelector:@selector(dataModelDidFinishLoading:)]){
        [delegate dataModelDidFinishLoading:self data:repVO];
        //}
    }
}


- (void)downloadCalls:(NSArray*)data{
    CallVO * callVO = nil;
    
    NSDateFormatter *dateFormat = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    NSDateFormatter *dateFormat2 = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormat2 setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    //
    for(NSDictionary * call in data){
        callVO = [[[CallVO alloc] init] autorelease];
        //
        callVO.call_uid = [call objectForKey:@"call_uid"];
        callVO.user_uid = [call objectForKey:@"user_uid"];
        callVO.rep_uid = [call objectForKey:@"rep_uid"];
        callVO.groupcall_uid = [call objectForKey:@"groupcall_uid"];
        callVO.startTime = [dateFormat dateFromString:[call objectForKey:@"start_time"]];
        callVO.endTime = [dateFormat2 dateFromString:[call objectForKey:@"end_time"]];
        callVO.time_spent = [[call objectForKey:@"time_spent"] doubleValue];
        
        
        
        //[self callAdd:callVO];
        //NOTICE:: we need to make the delay call to avoid choking sqlite!
        [self performSelector:@selector(downloadCallsDelay:) withObject:callVO afterDelay:0.0];
        
        [self downloadCallModules:[call objectForKey:@"callmodules"]];
    }
    
    
}

- (void)downloadCallsDelay:(CallVO*)callVO{
    [self callAdd:callVO];
}

- (void)downloadCallModules:(NSArray*)data{
    
    CallModuleVO * callModuleVO = nil;
    
    //
    for(NSDictionary * callModule in data){
        callModuleVO = [[[CallModuleVO alloc] init] autorelease];
        callModuleVO.call_uid = [callModule objectForKey:@"call_uid"];
        callModuleVO.module_uid = [callModule objectForKey:@"module_uid"];
        callModuleVO.time_spent = [[callModule objectForKey:@"time_spent"] doubleValue];
        callModuleVO.user_uid = [callModule objectForKey:@"user_uid"];
        
        //[self callModuleAdd:callModuleVO];
        //NOTICE:: we need to make the delay call to avoid choking sqlite!
        [self performSelector:@selector(downloadCallModulesDelay:) withObject:callModuleVO afterDelay:0.0];
    }
}

- (void)downloadCallModulesDelay:(CallModuleVO*)callModuleVO{
    [self callModuleAdd:callModuleVO];
}


- (void)downloadPages:(NSArray*)data{
    CallPageVO * callPageVO = nil;
    
    //
    for(NSDictionary * callPage in data){
        callPageVO = [[[CallPageVO alloc] init] autorelease];
        callPageVO.call_uid = [callPage objectForKey:@"call_uid"];
        callPageVO.module_uid = [callPage objectForKey:@"module_uid"];
        callPageVO.user_uid = [callPage objectForKey:@"user_uid"];
        callPageVO.page_uid = [callPage objectForKey:@"page_uid"];
        callPageVO.time_spent = [[callPage objectForKey:@"time_spent"] doubleValue];
        callPageVO.page_name = [callPage objectForKey:@"page_name"];
        
        
        //[self callModuleAdd:callModuleVO];
        //NOTICE:: we need to make the delay call to avoid choking sqlite!
        [self performSelector:@selector(downloadPagesDelay:) withObject:callPageVO afterDelay:0.0];
    }

}

- (void)downloadPagesDelay:(CallPageVO*)callPageVO{
    [self callPageAdd:callPageVO];
}


// LOCAL DATABASE ---------------------------------------------------------------------------------------------------------

-(void) checkAndCreateDatabase{
	BOOL success;
    
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
    
	success = [fileManager fileExistsAtPath:databasePath];
	if(success) return;
	
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
	
	[fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
	
    // this release is getting the warning from Analyze!
    // fileManager is the singleton, so it wont need it.
	//[fileManager release]; 
}


-(FMDatabase *) openDataBase{
    FMDatabase * db = [FMDatabase databaseWithPath:databasePath];
	if (![db open]) {
        NSLog(@"Could not open db.");
        return nil;
    }
	[db setShouldCacheStatements:YES];
    
    return db;
}




// CALL ------------------------------------------------------------------------------------------------------

-(NSMutableArray * )callSelectAll:(NSString *)rep_uid{
	FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
    
    NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    CallVO * callVO = nil;
    
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM call WHERE rep_uid=?", rep_uid];
	
    while ([rs next]) {
        callVO = [[[CallVO alloc] init] autorelease];
        callVO.call_uid = [rs stringForColumn:@"call_uid"];
        callVO.user_uid = [rs stringForColumn:@"user_uid"];
        callVO.rep_uid = [rs stringForColumn:@"rep_uid"];
        callVO.groupcall_uid = [rs stringForColumn:@"groupcall_uid"];
        callVO.startTime = [rs dateForColumn:@"start_time"];
        callVO.endTime = [rs dateForColumn:@"end_time"];
        callVO.time_spent = [rs doubleForColumn:@"time_spent"];
        
		
		[dataArr addObject:callVO];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
    
}


//-(NSMutableArray * )callSelectOne:(NSInteger)uid{


//}

-(NSMutableArray * )callSelectByUserId:(NSString*)user_uid{
	FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
    
    NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    CallVO * callVO = nil;
    
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM call WHERE user_uid=?", user_uid];
	
    while ([rs next]) {
        callVO = [[[CallVO alloc] init] autorelease];
        callVO.call_uid = [rs stringForColumn:@"call_uid"];
        callVO.user_uid = [rs stringForColumn:@"user_uid"];
        callVO.rep_uid = [rs stringForColumn:@"rep_uid"];
        callVO.groupcall_uid = [rs stringForColumn:@"groupcall_uid"];
        callVO.startTime = [rs dateForColumn:@"start_time"];
        callVO.endTime = [rs dateForColumn:@"end_time"];
        callVO.time_spent = [rs doubleForColumn:@"time_spent"];
        
		
		[dataArr addObject:callVO];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
    
}



-(int)callAdd:(CallVO *)callVO{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return -1;
    
    [db beginTransaction];
    
    BOOL isMultUsers = (callVO.usersArr && [callVO.usersArr count]>1)? YES : NO; 
    
    
    if(isMultUsers){
        for (UserVO* userVO in callVO.usersArr) {
            [db executeUpdate:@"INSERT INTO call (call_uid,user_uid,rep_uid,groupcall_uid,start_time,end_time,time_spent) VALUES (?,?,?,?,?,?,?)" ,
             callVO.call_uid, userVO.user_uid, callVO.rep_uid, callVO.groupcall_uid, 
             callVO.startTime,
             callVO.endTime,
             [NSNumber numberWithDouble:callVO.time_spent]
             ];
        }
        
    }else{
        
        if(callVO.user_uid){
            [db executeUpdate:@"INSERT INTO call (call_uid,user_uid,rep_uid,groupcall_uid,start_time,end_time,time_spent) VALUES (?,?,?,?,?,?,?)" ,
             callVO.call_uid, callVO.user_uid, callVO.rep_uid, callVO.groupcall_uid,
             callVO.startTime,
             callVO.endTime,
             [NSNumber numberWithDouble:callVO.time_spent]
             ];
        }
        
    }
    
    [db commit];
    //[db close];
    return [db lastInsertRowId];
}


-(NSMutableArray * )getGroupCallByID:(NSString*)groupcall_uid{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
    
    NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    UserVO * userVO = nil;
    
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM call, user WHERE call.groupcall_uid=? AND user.user_uid=call.user_uid", groupcall_uid];
	
    while ([rs next]) {
        userVO = [[[UserVO alloc] init] autorelease];
        userVO.rowid = [rs intForColumn:@"id"];
        userVO.user_uid = [rs stringForColumn:@"user_uid"];
        userVO.rep_uid = [rs stringForColumn:@"rep_uid"];
        userVO.practice_uid = [rs stringForColumn:@"practice_uid"];
        userVO.firstname = [rs stringForColumn:@"firstname"];
        userVO.lastname = [rs stringForColumn:@"lastname"];
        userVO.email = [rs stringForColumn:@"email"];
        
		
		[dataArr addObject:userVO];
    }
    
    [rs close];
	[db close];
	
	return [dataArr autorelease];
    
}






// MODULE_CALL ------------------------------------------------------------------------------------------------------

-(NSMutableArray * )callModuleSelectAll{
	FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
    
	NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    
    CallModuleVO * callModuleVO = nil;
    
	
	//FMResultSet *rs = [db executeQuery:@"SELECT * FROM call_module, module WHERE module.id=call_module.module_id"];
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM call_module WHERE id>0"];
	
    while ([rs next]) {
        callModuleVO = [[[CallModuleVO alloc] init] autorelease];
        
        callModuleVO.call_uid = [rs stringForColumn:@"call_uid"];
        callModuleVO.module_uid = [rs stringForColumn:@"module_uid"];
        callModuleVO.user_uid = [rs stringForColumn:@"user_uid"];
        callModuleVO.time_spent = [rs doubleForColumn:@"time_spent"];
        
        //NSLog(@"callModuleVO.time_spent:%f", callModuleVO.time_spent);
		
		[dataArr addObject:callModuleVO];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
	
}


-(NSMutableArray * )callModuleSelectByCallId:(NSString*)call_uid{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
	NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    
    CallModuleVO * callModuleVO = nil;
    
	
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM call_module WHERE call_uid=?", call_uid];
	
    while ([rs next]) {
        callModuleVO = [[[CallModuleVO alloc] init] autorelease];
        
        callModuleVO.call_uid = [rs stringForColumn:@"call_uid"];
        callModuleVO.module_uid = [rs stringForColumn:@"module_uid"];
        callModuleVO.user_uid = [rs stringForColumn:@"user_uid"];
        callModuleVO.time_spent = [rs doubleForColumn:@"time_spent"];
		
		[dataArr addObject:callModuleVO];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
    
}


-(int)callModuleAdd:(CallModuleVO *)callModuleVO{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return -1;
    
    [db beginTransaction];
    
    [db executeUpdate:@"INSERT INTO call_module (call_uid,module_uid,user_uid,time_spent) VALUES (?, ?, ?, ?)" ,
     callModuleVO.call_uid, 
     callModuleVO.module_uid, 
     callModuleVO.user_uid, 
     [NSNumber numberWithDouble:callModuleVO.time_spent]
     ];
    
    [db commit];
    //[db close];
    return [db lastInsertRowId];
}


// MODULE ------------------------------------------------------------------------------------------------------

-(NSMutableArray * )moduleSelectAll{
	FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
    
	NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    ModuleVO * moduleVO = nil;
	
	FMResultSet *rs = [db executeQuery:@"SELECT * FROM module WHERE id>0"];
	//NSLog(@"test2:%@", rs);
    while ([rs next]) {
        
        moduleVO = [[[ModuleVO alloc] init] autorelease];
        moduleVO.rowid = [rs intForColumn:@"id"];
        moduleVO.module_uid = [rs stringForColumn:@"module_uid"];
        moduleVO.title = [rs stringForColumn:@"title"];
        moduleVO.description = [rs stringForColumn:@"description"];
        moduleVO.access_level = [rs intForColumn:@"access_level"];
        moduleVO.content_url = [rs stringForColumn:@"content_url"];
        moduleVO.downloaded = [rs stringForColumn:@"downloaded"];
        
		
		[dataArr addObject:moduleVO];
        
    }
	[rs close];
	[db close];
    
	
	return [dataArr autorelease];
	
}

-(ModuleVO * )moduleSelectByUid:(NSString*)module_uid{
	FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
    

    ModuleVO * moduleVO = [[[ModuleVO alloc] init] autorelease];
    
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM module WHERE module_uid=? LIMIT 1", module_uid];
    
    
    if ([rs next]) {
        //moduleVO = [[[ModuleVO alloc] init] autorelease];
        moduleVO.rowid = [rs intForColumn:@"id"];
        moduleVO.module_uid = [rs stringForColumn:@"module_uid"];
        moduleVO.title = [rs stringForColumn:@"title"];
        moduleVO.description = [rs stringForColumn:@"description"];
        moduleVO.access_level = [rs intForColumn:@"access_level"];
        moduleVO.content_url = [rs stringForColumn:@"content_url"];
        moduleVO.downloaded = [rs stringForColumn:@"downloaded"];

    }
	[rs close];
	[db close];
	
	return moduleVO;
    
}


// PAGE ------------------------------------------------------------------------------------------------------

-(NSMutableArray * )pageSelectByModuleId:(NSString*)module_uid{
	FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
    
	NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    PageVO * pageVO = nil;
	
	FMResultSet *rs = [db executeQuery:@"SELECT * FROM page WHERE module_uid=?", module_uid];
    
    //if([module_uid isEqualToString:@"module2-3"]){
        //rs = [db executeQuery:@"SELECT * FROM page WHERE module_uid=? AND page_uid=?", module_uid, @"amb_mod2_3_tissue_penetration_3"];
    //}
	
    while ([rs next]) {
        
        pageVO = [[[PageVO alloc] init] autorelease];
        pageVO.rowid = [rs intForColumn:@"id"];
        pageVO.module_uid = [rs stringForColumn:@"module_uid"];
        pageVO.page_uid = [rs stringForColumn:@"page_uid"];
        pageVO.filename = [rs stringForColumn:@"filename"];
        pageVO.type = [rs stringForColumn:@"type"];
        pageVO.time_spent = [rs doubleForColumn:@"time_spent"];
        pageVO.page_name = [rs stringForColumn:@"page_name"];
		
		[dataArr addObject:pageVO];
    }
	[rs close];
	[db close];
    
	
	return [dataArr autorelease];
	
}


-(void)savePageArray:(NSMutableArray * )pageArray{
	if (pageArray==nil || ![pageArray count]>0) return;
    
    for (PageVO* pageVO in pageArray){
        //NSLog(@"page: %.1f - %@", [pageVO time_spent], [pageVO filename]);
        [self PageAdd:pageVO];
    }
}

-(void)PageAdd:(PageVO*)pageVO{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return;
    
    [db beginTransaction];
    
    NSString * callUID = [[[Model sharedModel] callVO] call_uid];
    NSString * userUID = [[[Model sharedModel] userVO] user_uid];
    NSNumber * time_spent = [NSNumber numberWithDouble:pageVO.time_spent];
    
    [db executeUpdate:@"INSERT INTO call_page (call_uid,module_uid,user_uid,time_spent,page_uid, page_name) VALUES (?,?,?,?,?,?)",callUID,pageVO.module_uid, userUID, time_spent, pageVO.page_uid, pageVO.page_name];
    
    [db commit];
    [db close];
    
    //test
    //[self callPageSelectByCallId:callUID];
    
}


-(NSMutableArray * )callPageSelectByCallId:(NSString*)call_uid{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
    
	NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    CallPageVO *callPageVO = nil;
	
	FMResultSet *rs = [db executeQuery:@"SELECT * FROM call_page WHERE call_uid=?", call_uid];
    //FMResultSet *rs = [db executeQuery:@"SELECT * FROM call_page"];
    
    while ([rs next]) {
        
        callPageVO = [[[CallPageVO alloc] init] autorelease];
        callPageVO.call_uid = [rs stringForColumn:@"call_uid"];
        callPageVO.module_uid = [rs stringForColumn:@"module_uid"];
        callPageVO.user_uid = [rs stringForColumn:@"user_uid"];
        callPageVO.page_uid = [rs stringForColumn:@"page_uid"];
        callPageVO.time_spent = [rs doubleForColumn:@"time_spent"];
        callPageVO.page_name = [rs stringForColumn:@"page_name"];
        
        
        //NSLog(@"callPageVO.call_uid: %@", callPageVO.call_uid);
        //NSLog(@"callPageVO.module_uid: %@", callPageVO.module_uid);
        //NSLog(@"callPageVO.user_uid: %@", callPageVO.user_uid);
        //NSLog(@"callPageVO.page_uid: %@", callPageVO.page_uid);
        //NSLog(@"callPageVO.time_spent: %0.1f", callPageVO.time_spent);
        
		
		[dataArr addObject:callPageVO];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
}

-(int)callPageAdd:(CallPageVO *)callPageVO{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return -1;
    
    
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM call_page WHERE call_uid=? AND page_uid=?", callPageVO.call_uid, callPageVO.page_uid];
    if([rs next]) return 0;
    
    [db beginTransaction];
    
    [db executeUpdate:@"INSERT INTO call_page (call_uid,module_uid,user_uid,time_spent,page_uid,page_name) VALUES (?,?,?,?,?,?)" ,
     callPageVO.call_uid, 
     callPageVO.module_uid, 
     callPageVO.user_uid, 
     [NSNumber numberWithDouble:callPageVO.time_spent],
     callPageVO.page_uid,
     callPageVO.page_name
     ];
    
    [db commit];
    //[db close];

    
    return [db lastInsertRowId];
    
}

-(PageVO*)getPageByFileName:(NSString*)fileName{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
    PageVO * pageVO = [[[PageVO alloc] init] autorelease];
	
	FMResultSet *rs = [db executeQuery:@"SELECT * FROM page WHERE filename=? LIMIT 1", fileName];
	
    if ([rs next]) {
        pageVO = [[[PageVO alloc] init] autorelease];
        pageVO.rowid = [rs intForColumn:@"id"];
        pageVO.module_uid = [rs stringForColumn:@"module_uid"];
        pageVO.page_uid = [rs stringForColumn:@"page_uid"];
        pageVO.filename = [rs stringForColumn:@"filename"];
        pageVO.type = [rs stringForColumn:@"type"];
        pageVO.time_spent = [rs doubleForColumn:@"time_spent"];
        pageVO.page_name = [rs stringForColumn:@"page_name"];
	}	
    
	[rs close];
	[db close];
	
	return pageVO;
}



// PAGE QUICK ACCESS ------------------------------------------------------------------------------------------------------

-(NSMutableArray * )pageQuickAccessSelectByModuleId:(NSString*)module_uid{
	FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
    
	NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    PageVO * pageVO = nil;
	
	FMResultSet *rs = [db executeQuery:@"SELECT * FROM page_quickaccess WHERE module_uid=?", module_uid];
    
    while ([rs next]) {
        
        pageVO = [[[PageVO alloc] init] autorelease];
        pageVO.rowid = [rs intForColumn:@"id"];
        pageVO.module_uid = [rs stringForColumn:@"module_uid"];
        pageVO.page_uid = [rs stringForColumn:@"page_uid"];
        pageVO.filename = [rs stringForColumn:@"filename"];
        pageVO.type = [rs stringForColumn:@"type"];
        pageVO.time_spent = [rs doubleForColumn:@"time_spent"];
        pageVO.page_name = [rs stringForColumn:@"page_name"];
        
		
		[dataArr addObject:pageVO];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
	
}

-(NSMutableArray * )quickAccessSelectAll{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
    
	NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    PageVO * pageVO = nil;
	
	//FMResultSet *rs = [db executeQuery:@"SELECT * FROM page_quickaccess WHERE module_uid!=?", @""];
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM page_quickaccess GROUP BY module_uid ORDER BY id"];
    int counter = 0;
    
    while ([rs next]) {
        
        counter++;
        
        pageVO = [[[PageVO alloc] init] autorelease];
        pageVO.rowid = counter;//[rs intForColumn:@"id"];
        pageVO.module_uid = [rs stringForColumn:@"module_uid"];
        pageVO.page_uid = [rs stringForColumn:@"page_uid"];
        pageVO.filename = [rs stringForColumn:@"filename"];
        pageVO.type = [rs stringForColumn:@"type"];
        pageVO.time_spent = [rs doubleForColumn:@"time_spent"];
        pageVO.page_name = [rs stringForColumn:@"page_name"];
        
        
        
		
		[dataArr addObject:pageVO];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
}



// USER ------------------------------------------------------------------------------------------------------


-(NSMutableArray * )userSelectAll:(NSString *)rep_uid{
	FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
	NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    UserVO * userVO = nil;
    
	
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM user WHERE rep_uid=?", rep_uid];
	
    while ([rs next]) {
        
        userVO = [[[UserVO alloc] init] autorelease];
        userVO.rowid = [rs intForColumn:@"id"];
        userVO.user_uid = [rs stringForColumn:@"user_uid"];
        userVO.rep_uid = [rs stringForColumn:@"rep_uid"];
        userVO.practice_uid = [rs stringForColumn:@"practice_uid"];
        userVO.firstname = [rs stringForColumn:@"firstname"];
        userVO.lastname = [rs stringForColumn:@"lastname"];
        userVO.email = [rs stringForColumn:@"email"];
        userVO.phone = [rs stringForColumn:@"phone"];
        
		
		[dataArr addObject:userVO];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
	
}

-(NSMutableArray * )userSelectByProp:(NSString *)name andValue:(id)value{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
	NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    UserVO * userVO = nil;
    
	
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM user WHERE ?=?", name, value];
	
    while ([rs next]) {
        
        userVO = [[[UserVO alloc] init] autorelease];
        userVO.rowid = [rs intForColumn:@"id"];
        userVO.user_uid = [rs stringForColumn:@"user_uid"];
        userVO.rep_uid = [rs stringForColumn:@"rep_uid"];
        userVO.practice_uid = [rs stringForColumn:@"practice_uid"];
        userVO.firstname = [rs stringForColumn:@"firstname"];
        userVO.lastname = [rs stringForColumn:@"lastname"];
        userVO.email = [rs stringForColumn:@"email"];
        userVO.phone = [rs stringForColumn:@"phone"];
		
		[dataArr addObject:userVO];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
}


-(int)userAdd:(UserVO *)userVO{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return -1;
    
    FMResultSet *rs = [db executeQuery:@"SELECT * from user where email=?", userVO.email];
    if([rs next]) return [rs intForColumn:@"id"];//-2;
    
    
    [db beginTransaction];
    
    [db executeUpdate:@"INSERT INTO user (user_uid,rep_uid,practice_uid,firstname,lastname,email,phone,date_creation,date_update) VALUES (?, ?,?, ?, ?, ?, ?, DATETIME('NOW'), DATETIME('NOW'))" ,
     userVO.user_uid, userVO.rep_uid, userVO.practice_uid, userVO.firstname, userVO.lastname, userVO.email, userVO.phone];
    
    [db commit];
    //[db close];
    return [db lastInsertRowId];
}


-(NSMutableArray * )userSearch:(NSString *)word uid:(NSString *)rep_uid{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
    
    NSMutableArray *dataArr = [[NSMutableArray alloc] init];
	UserVO * userVO = nil;
    
    NSString * formattedWord = [NSString stringWithFormat:@"%%%@%%", word];
	
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM user, practice WHERE user.rep_uid=? AND practice.practice_uid=user.practice_uid AND (user.firstname LIKE ? or user.lastname LIKE ? or practice.name LIKE ?) ", rep_uid, formattedWord, formattedWord, formattedWord];
    
    
	
    while ([rs next]) {
		userVO = [[[UserVO alloc] init] autorelease];
        userVO.rowid = [rs intForColumn:@"id"];
        userVO.user_uid = [rs stringForColumn:@"user_uid"];
        userVO.rep_uid = [rs stringForColumn:@"rep_uid"];
        userVO.practice_uid = [rs stringForColumn:@"practice_uid"];
        userVO.firstname = [rs stringForColumn:@"firstname"];
        userVO.lastname = [rs stringForColumn:@"lastname"];
        userVO.email = [rs stringForColumn:@"email"];
        userVO.phone = [rs stringForColumn:@"phone"];
        
        [dataArr addObject:userVO];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
}

-(UserVO*)userSelectOne:(NSString *)uid{
    FMDatabase * db = [self openDataBase];
    if (db==nil) return nil;
    
    
    UserVO * userVO = [[[UserVO alloc] init] autorelease];
    
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM user WHERE user_uid=?", uid];
    
    
    if([rs next]) {
        userVO.rowid = [rs intForColumn:@"id"];
        userVO.rep_uid = [rs stringForColumn:@"rep_uid"];
        userVO.user_uid = [rs stringForColumn:@"user_uid"];
        userVO.practice_uid = [rs stringForColumn:@"practice_uid"];
        userVO.firstname = [rs stringForColumn:@"firstname"];
        userVO.lastname = [rs stringForColumn:@"lastname"];
        userVO.email = [rs stringForColumn:@"email"];
        userVO.phone = [rs stringForColumn:@"phone"];
        
    }
    [rs close];
    [db close];
    
    return userVO;
}


-(BOOL)userOfflineStatusUpdate:(NSString*)status{
    FMDatabase * db = [self openDataBase];
    if (db==nil) return NO;
    [db executeUpdate:@"update user set status = ? where status = '' OR status='offline'" , status];
    //
    return YES;
}



//PRACTICE ------------------------------------------------------------------------------------------------------

-(NSMutableArray *)practiceSelectAll{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
	NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    PracticeVO * practiceVO = nil;
    
	
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM practice WHERE id>0"];
	
    while ([rs next]) {
        practiceVO = [[[PracticeVO alloc] init] autorelease];
        practiceVO.rowid = [rs intForColumn:@"id"];
        practiceVO.practice_uid = [rs stringForColumn:@"practice_uid"];
        practiceVO.name = [rs stringForColumn:@"name"];
        practiceVO.street = [rs stringForColumn:@"street"];
        practiceVO.suburb = [rs stringForColumn:@"suburb"];
        practiceVO.postcode = [rs stringForColumn:@"postcode"];
        practiceVO.state = [rs stringForColumn:@"state"];
        practiceVO.region_code = [rs stringForColumn:@"region_code"];
        
		
		[dataArr addObject:practiceVO];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
    
}



-(PracticeVO*)practiceSelectOne:(NSString*)uid{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
    
    
    
    PracticeVO * practiceVO = [[[PracticeVO alloc] init] autorelease];
	
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM practice WHERE practice_uid=?", uid];
    //FMResultSet *rs = [db executeQuery:@"SELECT * FROM practice WHERE rowid>0"];
    
    if([rs next]) {
        
        practiceVO.rowid = [rs intForColumn:@"id"];
        practiceVO.practice_uid = [rs stringForColumn:@"practice_uid"];
        practiceVO.name = [rs stringForColumn:@"name"];
        practiceVO.street = [rs stringForColumn:@"street"];
        practiceVO.suburb = [rs stringForColumn:@"suburb"];
        practiceVO.postcode = [rs stringForColumn:@"postcode"];
        practiceVO.state = [rs stringForColumn:@"state"];
        practiceVO.region_code = [rs stringForColumn:@"region_code"];
        
		
    }
	[rs close];
	[db close];
	
	return practiceVO;
    
}

-(int)practiceAdd:(PracticeVO *)practiceVO{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return -1;
    
    FMResultSet *rs = [db executeQuery:@"SELECT * from practice where practice_uid=?", practiceVO.practice_uid];
    if([rs next]) return [rs intForColumn:@"id"];//-2;
    
    
    [db beginTransaction];
    
    [db executeUpdate:@"INSERT INTO practice (practice_uid,name,street,suburb,postcode,state,region_code) VALUES (?, ?, ?, ?, ?, ?, ?)" ,
     practiceVO.practice_uid, practiceVO.name, practiceVO.street,practiceVO.suburb,practiceVO.postcode,practiceVO.state,practiceVO.region_code];
    
    [db commit];
    //[db close];
    
    return [db lastInsertRowId];
}


//REP ------------------------------------------------------------------------------------------------------
-(int)repAdd:(RepVO *)repVO{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return -1;
    
    FMResultSet *rs = [db executeQuery:@"SELECT * from 'rep' where email=?", repVO.email];
    if([rs next]) return -2;
    
    if(!repVO || !repVO.rep_uid) return -1;
    
    
    [db beginTransaction];
    
    
    [db executeUpdate:@"INSERT INTO rep (rep_uid, username,password,email,access_level) VALUES (?, ?, ?, ?, ?)", 
     repVO.rep_uid, repVO.username, repVO.password, repVO.email, [NSNumber numberWithInt:repVO.access_level]];
    
    
    
    [db commit];
    //[db close];
    
    return [db lastInsertRowId];
}


//login!
-(RepVO * )repSelectOne:(NSString *)username passwd:(NSString *)password{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
    
    RepVO * repVO = [[[RepVO alloc] init] autorelease];
    
    //NOTICE: use the LIKE instead of = to make the login case insensitive
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM rep WHERE username LIKE ? AND password LIKE ? LIMIT 1", username, password];
	
    
    if([rs next]) {
        repVO.rowid = [rs intForColumn:@"id"];
        repVO.rep_uid = [rs stringForColumn:@"rep_uid"];
        repVO.username = [rs stringForColumn:@"username"];
        repVO.password = [rs stringForColumn:@"password"];
        repVO.email = [rs stringForColumn:@"email"];
        repVO.access_level = [rs intForColumn:@"access_level"];
		
    }
	[rs close];
	[db close];
    
    
    return repVO;
    
}



// sync up all the data to the server // ------------------------------------------------------------------------

-(void)uploadAllData{
    
    //It's important to get the call order right, cos the dependancy
    if([self isWebSiteReachable:SERVER_NAME]){
        
        //addpractice
        [self serverRequestWithAction:@"addpractice" andData:[[self practiceSelectAllData] JSONRepresentation]];
        
        //add users
        [self serverRequestWithAction:@"adduser" andData:[[self userSelectAllData] JSONRepresentation]];
        
        //add calls
        [self serverRequestWithAction:@"addcall" andData:[[self callSelectAllData] JSONRepresentation]];
        
        //add call modules
        [self serverRequestWithAction:@"addcallmodule" andData:[[self callModuleSelectAllData] JSONRepresentation]];
        
        //add call pages
        [self serverRequestWithAction:@"addcallpage" andData:[[self callPageSelectAllData] JSONRepresentation]];
        
        //NSLog(@"data:: %@", [self callPageSelectAllData]);
    }
    
}

-(void)downloadAllData:(NSString*)rep_uid{
    
    if([self isWebSiteReachable:SERVER_NAME]){
        
        //get all the reps - so everyone can login!
        //[self serverRequestWithAction:@"getallrep" andData:nil];
        
        
        //get all the users(doctors)
        NSDictionary * dict = [[[NSDictionary alloc] initWithObjectsAndKeys:rep_uid, @"rep_uid", nil] autorelease];
        [self serverRequestWithAction:@"getusers" andData:[dict JSONRepresentation]];
        
        //get calls + callModules
        NSDictionary * dict2 = [[[NSDictionary alloc] initWithObjectsAndKeys:rep_uid, @"rep_uid", nil] autorelease];
        [self serverRequestWithAction:@"getcalls" andData:[dict2 JSONRepresentation]];
        
        //get pageCalls
        NSDictionary * dict3 = [[[NSDictionary alloc] initWithObjectsAndKeys:rep_uid, @"rep_uid", nil] autorelease];
        [self serverRequestWithAction:@"getcallpages" andData:[dict3 JSONRepresentation]];
        
    }
    
}

-(NSMutableArray * )callSelectAllData{
	FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
    
    NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    NSMutableDictionary *callDict = nil;
    
    NSDateFormatter *dateFormat = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormat setDateFormat:@"dd-MM-yyyy h:mm:ss"];
    
    
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM call WHERE id>0"];
	
    while ([rs next]) {
        callDict = [[[NSMutableDictionary alloc] init] autorelease];
        [callDict setValue:[rs stringForColumn:@"call_uid"] forKey:@"call_uid"];
        [callDict setValue:[rs stringForColumn:@"user_uid"] forKey:@"user_uid"];
        [callDict setValue:[rs stringForColumn:@"rep_uid"] forKey:@"rep_uid"];
        [callDict setValue:[rs stringForColumn:@"groupcall_uid"]? [rs stringForColumn:@"groupcall_uid"] : @"" forKey:@"groupcall_uid"];
        [callDict setValue:[dateFormat stringFromDate:[rs dateForColumn:@"start_time"]] forKey:@"start_time"];
        [callDict setValue:[dateFormat stringFromDate:[rs dateForColumn:@"end_time"]] forKey:@"end_time"];
        [callDict setValue:[NSNumber numberWithDouble: [rs doubleForColumn:@"time_spent"]] forKey:@"time_spent"];
        
        //NSLog(@"endtime: %@",[dateFormat stringFromDate:[rs dateForColumn:@"end_time"]]);
		
		[dataArr addObject:callDict];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
    
}




-(NSMutableArray * )callModuleSelectAllData{
	FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
    
	NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *callModuleDict = nil;   
    
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM call_module WHERE id>0"];
	
    while ([rs next]) {
        callModuleDict = [[[NSMutableDictionary alloc] init] autorelease];
        [callModuleDict setValue:[rs stringForColumn:@"call_uid"] forKey:@"call_uid"];
        [callModuleDict setValue:[rs stringForColumn:@"module_uid"] forKey:@"module_uid"];
        [callModuleDict setValue:[rs stringForColumn:@"user_uid"] forKey:@"user_uid"];
        [callModuleDict setValue:[NSNumber numberWithDouble: [rs doubleForColumn:@"time_spent"]] forKey:@"time_spent"];
        
		
		[dataArr addObject:callModuleDict];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
	
}


-(NSMutableArray * )callPageSelectAllData{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
	NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *callPageDict = nil; 
    
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM call_page WHERE id>0"];
	
    while ([rs next]) {
        
        callPageDict = [[[NSMutableDictionary alloc] init] autorelease];
        [callPageDict setValue:[rs stringForColumn:@"call_uid"] forKey:@"call_uid"];
        [callPageDict setValue:[rs stringForColumn:@"module_uid"] forKey:@"module_uid"];
        [callPageDict setValue:[rs stringForColumn:@"user_uid"] forKey:@"user_uid"];
        [callPageDict setValue:[NSNumber numberWithDouble: [rs doubleForColumn:@"time_spent"]] forKey:@"time_spent"];
        [callPageDict setValue:[rs stringForColumn:@"page_uid"] forKey:@"page_uid"];
        [callPageDict setValue:[rs stringForColumn:@"page_name"]? [rs stringForColumn:@"page_name"] : @"" forKey:@"page_name"];
        
		
		[dataArr addObject:callPageDict];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
    
    
    
}




-(NSMutableArray * )userSelectAllData{
	FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
	NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    NSMutableDictionary *userDict = nil;
    
	
    //FMResultSet *rs = [db executeQuery:@"SELECT * FROM user WHERE id>0"];
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM user WHERE user_uid NOT LIKE 'default-%'"];
    
	
    while ([rs next]) {
        userDict = [[[NSMutableDictionary alloc] init] autorelease];
        [userDict setValue:[NSNumber numberWithInt:[rs intForColumn:@"id"]] forKey:@"id"];
        [userDict setValue:[rs stringForColumn:@"user_uid"] forKey:@"user_uid"];
        [userDict setValue:[rs stringForColumn:@"rep_uid"] forKey:@"rep_uid"];
        [userDict setValue:[rs stringForColumn:@"practice_uid"] forKey:@"practice_uid"];
        [userDict setValue:[rs stringForColumn:@"firstname"] forKey:@"firstname"];
        [userDict setValue:[rs stringForColumn:@"lastname"] forKey:@"lastname"];
        [userDict setValue:[rs stringForColumn:@"email"] forKey:@"email"];
		
		[dataArr addObject:userDict];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
	
}


-(NSMutableArray * )practiceSelectAllData{
    FMDatabase * db = [self openDataBase];
	if (db==nil) return nil;
	
    
	NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    NSMutableDictionary *practiceDict = nil;
    
	
    //FMResultSet *rs = [db executeQuery:@"SELECT * FROM practice WHERE id>0"];
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM practice WHERE practice_uid NOT LIKE 'default-%'"];
	
    while ([rs next]) {
        
        practiceDict = [[[NSMutableDictionary alloc] init] autorelease];
        [practiceDict setValue:[NSNumber numberWithInt:[rs intForColumn:@"id"]] forKey:@"id"];
        [practiceDict setValue:[rs stringForColumn:@"practice_uid"] forKey:@"practice_uid"];
        [practiceDict setValue:[rs stringForColumn:@"name"] forKey:@"name"];
        [practiceDict setValue:[rs stringForColumn:@"street"] forKey:@"street"];
        [practiceDict setValue:[rs stringForColumn:@"suburb"] forKey:@"suburb"];
        [practiceDict setValue:[rs stringForColumn:@"postcode"] forKey:@"postcode"];
        [practiceDict setValue:[rs stringForColumn:@"state"] forKey:@"state"];
        [practiceDict setValue:[rs stringForColumn:@"region_code"] forKey:@"region_code"];
		
		[dataArr addObject:practiceDict];
    }
	[rs close];
	[db close];
	
	return [dataArr autorelease];
    
}



-(void)keycodeLogin:(NSString*)keycode{ 

    
    if([self isWebSiteReachable:SERVER_NAME]){

        NSDictionary * dict = [[[NSDictionary alloc] initWithObjectsAndKeys:keycode, @"keycode", nil] autorelease];
        [self serverRequestWithAction:@"getkeycode" andData:[dict JSONRepresentation]];

    }
    
}



















// end of sync up all the data to the server // -------------------------------------------------------



/*
 
 -(void)removeLocalClientData{
 FMDatabase * db = [FMDatabase databaseWithPath:databasePath];
 if (![db open]) {
 NSLog(@"Could not open db.");
 return;
 }
 //clean up all
 [db beginTransaction];
 [db executeUpdate:@"delete from 'client' where id > 0"];
 [db commit];
 [db close];
 }
 
 */


// REACHABILITY ---------------------------------------------------------------------------------------------------------

-(BOOL) isInternetReachable {
	return ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable);
}

-(BOOL) isWebSiteReachable: (NSString *)host {
	return ([[Reachability reachabilityWithHostName: host] currentReachabilityStatus] != NotReachable);
}


// HELPER FUNCTIONS
-(NSDate *)StringToNSDate:(NSString *)dateString
{
    //NSString *dateString = @"2010-01-19";
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    //NSDate *dateFromString = [[NSDate alloc] init];
    // ta-daaa!
    NSDate * dateFromString = [dateFormatter dateFromString:dateString];
    
    return dateFromString;
    
}


//TESTING FUNCTION
-(void)testOn{
    //add call pages
    //Log(@"callPageSelectAllData:%@", [[self callPageSelectAllData] JSONRepresentation]);
    //[self serverRequestWithAction:@"addcallpage" andData:[[self callPageSelectAllData] JSONRepresentation]];
}



@end
