//
//  MainViewQuickAccessMenuController.m
//  AtriplaED
//
//  Created by Jonathan Xie on 27/06/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import "MainViewQuickAccessMenuController.h"
#import "MainViewQuickAccessController.h"
#import "MainViewController.h"
#import "ModuleVO.h"
#import "PageVO.h"

#define TITLELABEL_TAG 1
#define NUMBERLABEL_TAG 2
#define TAGIMAGE_TAG 3


@implementation MainViewQuickAccessMenuController

@synthesize tableView=tableView_;
@synthesize tableArr=tableArr_;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [tableArr_ release], tableArr_=nil;
    [tableView_ release],tableView_=nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    model = [Model sharedModel];
    dataModel = [DataModel sharedDataModel];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"quickaccess-bg.png"]];
    
    //tableView
    self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0.0,238.0,905,70*7.0) style:UITableViewStylePlain] autorelease];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.rowHeight = 70.0;
    self.tableView.backgroundColor = [UIColor clearColor];
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorColor = [UIColor colorWithWhite:0.8 alpha:0.5];
    
    [self.view addSubview:self.tableView];
    
    //data
    self.tableArr = [dataModel quickAccessSelectAll];
    [self.tableView reloadData];
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

/*
-(IBAction)btnAction:sender{
    int tag = [(UIButton*)sender tag];
    
    
    NSArray * pages = [dataModel pageQuickAccessSelectByModuleId:[NSString stringWithFormat:@"quickaccess%d", tag]];
    
    //NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithObject:pages forKey:@"pageArray"];
    NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:pages,@"pageArray",@"quickaccess",@"action", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"launchQuickAccessView" object:nil userInfo:dict];
    
    //MainViewQuickAccessController * accessViewController = (MainViewQuickAccessController *)model.mainViewQuickAccessController;
    //[accessViewController showContent:pages scrollEnabled:YES];
    
}
*/



#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableArr count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UILabel *titleLabel, *numberLabel;
    UIImageView * tagImage;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        numberLabel = [[[UILabel alloc] initWithFrame:CGRectMake(122.0, 12.0, 60.0, 44.0)] autorelease];
        numberLabel.tag = NUMBERLABEL_TAG;
        numberLabel.font = [UIFont fontWithName:@"DINCond-Bold" size:35];
        numberLabel.textAlignment = UITextAlignmentLeft;
        numberLabel.textColor = [UIColor colorWithWhite:0.0 alpha:0.2];
        numberLabel.backgroundColor = [UIColor clearColor];
        //numberLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cell.contentView addSubview:numberLabel];
        
        titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(201.0, 11.0, 380.0, 44.0)] autorelease];
        titleLabel.tag = TITLELABEL_TAG;
        titleLabel.font = [UIFont fontWithName:@"DINCond-Bold" size:23];
        titleLabel.textAlignment = UITextAlignmentLeft;
        titleLabel.textColor = [UIColor colorWithRed:210.0/255.0 green:220.0/255.0 blue:228.0/255.0 alpha:0.9];
        titleLabel.backgroundColor = [UIColor clearColor];
        //titleLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cell.contentView addSubview:titleLabel];
        
        //tagImage
        tagImage = [[[UIImageView alloc] initWithFrame:CGRectMake(905.0-206-16, 12.0, 206.0, 52.0)] autorelease];
        tagImage.tag = TAGIMAGE_TAG;
        //tagImage.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cell.contentView addSubview:tagImage];
       
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:indexPath.row%2? 0.4:0.2];
        
    }else {
        titleLabel = (UILabel *)[cell.contentView viewWithTag:TITLELABEL_TAG];
        numberLabel = (UILabel *)[cell.contentView viewWithTag:NUMBERLABEL_TAG];
        tagImage = (UIImageView *)[cell.contentView viewWithTag:TAGIMAGE_TAG];
    }
    
    titleLabel.text = [(PageVO*)[self.tableArr objectAtIndex:indexPath.row] page_name];
    numberLabel.text = [NSString stringWithFormat:@"0%d", [(PageVO*)[self.tableArr objectAtIndex:indexPath.row] rowid]];
    
    UIImage *theImage = [UIImage imageNamed:@"quickaccess-btn-showdata.png"];
    tagImage.image = theImage;
    
    
    //NSLog(@"test: %@", [self.tableArr objectAtIndex:indexPath.row]);

    
    return cell;
}



#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    

    NSInteger index = [(PageVO*)[self.tableArr objectAtIndex:indexPath.row] rowid];
    NSArray * pages = [dataModel pageQuickAccessSelectByModuleId:[NSString stringWithFormat:@"quickaccess%d", index]];
    
    //NSLog(@"pages: %@", pages);
    
    //NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithObject:pages forKey:@"pageArray"];
    NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:pages,@"pageArray",@"quickaccess",@"action", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"launchQuickAccessView" object:nil userInfo:dict];
}








@end
