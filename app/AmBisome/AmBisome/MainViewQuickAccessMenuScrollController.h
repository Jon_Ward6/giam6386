//
//  MainViewQuickAccessMenuScrollController.h
//  BeyondHIV
//
//  Created by Jonathan Xie on 8/02/12.
//  Copyright (c) 2012 Ward6. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewQuickAccessMenuScrollController : UIViewController<UIScrollViewDelegate>{
    UIScrollView * scrollView_;
}

@property(nonatomic,retain)IBOutlet UIScrollView * scrollView;

@end
