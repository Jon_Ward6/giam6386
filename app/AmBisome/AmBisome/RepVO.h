//
//  RepVO.h
//  eDetailer
//
//  Created by Jonathan Xie on 14/04/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RepVO : NSObject {
    NSInteger rowid;
    NSString * rep_uid;
    NSString * username;
    NSString * password;
    NSString * email;
    NSString * regionCode;
    NSInteger access_level;
}

@property (nonatomic, assign) NSInteger rowid;
@property (nonatomic, retain) NSString * rep_uid;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * regionCode;
@property (nonatomic, readwrite) NSInteger access_level;


-(RepVO *)dataSet:(NSDictionary*)dict;

@end
