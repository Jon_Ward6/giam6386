//
//  NewClientViewController.h
//  AtriplaED
//
//  Created by Jonathan Xie on 8/06/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import <UIKit/UIKit.h>


@class Model;
@class DataModel;


@interface NewClientViewController : UIViewController<UITextFieldDelegate>  {
    UITextField * firstname;
    UITextField * lastname;
    UITextField * practiceName;
    UITextField * address;
    UITextField * suburb;
    UITextField * postcode;
    UITextField * phone;
    UITextField * email;
    
    UIControl *currentControl;
    
    Model * model;
    DataModel * dataModel;
    
}

@property(nonatomic, retain) IBOutlet UITextField * firstname;
@property(nonatomic, retain) IBOutlet UITextField * lastname;
@property(nonatomic, retain) IBOutlet UITextField * practiceName;
@property(nonatomic, retain) IBOutlet UITextField * address;
@property(nonatomic, retain) IBOutlet UITextField * suburb;
@property(nonatomic, retain) IBOutlet UITextField * postcode;
@property(nonatomic, retain) IBOutlet UITextField * phone;
@property(nonatomic, retain) IBOutlet UITextField * email;

@property (nonatomic, assign) UIControl *currentControl;


@end


@interface NewClientViewController(private)
-(void)resignAllResponder;
@end