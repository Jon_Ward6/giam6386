//
//  ViewController.m
//  AmBisome
//
//  Created by Jonathan Xie on 3/04/12.
//  Copyright (c) 2012 Ward6. All rights reserved.
//

#import "ViewController.h"
#import "MainViewController.h"
#import "LoginViewController.h"

#import "ELCUIApplication.h"

#import "DataTester.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize navController=navController_, loginNavController = loginNavController_;
@synthesize mainView,loginView;

- (void)dealloc
{
    [model release];
	[navController_ release], navController_=nil;
    [loginNavController_ release], loginNavController_ = nil;
    [mainView release], mainView = nil;
    [loginView release], loginView = nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    model = [Model sharedModel];
	model.appView = self;
    
    
	self.view.backgroundColor = [UIColor blackColor];
	
    //main view panel ------------------------------------------------------------------------------------
	mainView = [[MainViewController alloc] init];
	
	self.navController = [[[UINavigationController alloc] initWithRootViewController:mainView] autorelease];
	self.navController.navigationBarHidden = YES;
    [self.view addSubview:self.navController.view];
	
	[mainView release];
	
	//move up 20 pixel
	//CGRect frame = self.navController.view.frame;
	//frame.origin.y -= 20;
	//self.navController.view.frame = frame;
    
    
    //shell: login panel ----------------------------------------------------------------------------------------
    loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    
    self.loginNavController = [[[UINavigationController alloc] initWithRootViewController:loginView] autorelease];
	self.loginNavController.navigationBarHidden = YES;
    [self.view addSubview:self.loginNavController.view];
	
    self.loginNavController.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-module.png"]];
	[loginView release];
	
	//move up 20 pixel
	//CGRect loginFrame = self.loginNavController.view.frame;
	//loginFrame.origin.y -= 20;
    //loginFrame.size.height += 30;
	//self.loginNavController.view.frame = loginFrame;
    
    
    //testing    -------------------------------------------------------------------------------------------
    DataTester * tester = [[[DataTester alloc] init] autorelease];
    [tester test];
    
    
    //
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(applicationDidTimeout:) 
                                                 name:kApplicationDidTimeoutNotification 
                                               object:nil];
    
}


- (void)applicationDidTimeout:(NSNotification *) notif {
    //pop back to initial screen
    [self.loginNavController popToRootViewControllerAnimated:NO];
    [self.navController popToRootViewControllerAnimated:NO];
    
    //make sure default the login nav!
    CGRect frame = self.loginNavController.view.frame;
    frame.origin.y = 0;
    self.loginNavController.view.frame = frame;
    
    //
    model.repVO = nil;
}



- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    
}


@end
