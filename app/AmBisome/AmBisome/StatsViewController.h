//
//  StatsViewController.h
//  Demo
//
//  Created by Jonathan Xie on 23/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Model;
@class DataModel;
@class StatsViewScrollController;
@class StatsViewAllController;

@interface StatsViewController : UIViewController <UITableViewDelegate,UITextFieldDelegate>{
    Model * model;
    //
    NSMutableArray * callArray;
    NSMutableArray * callModuleArray;
    NSMutableArray * userArray;
    
    UITableView * aTableView;
    
    StatsViewScrollController * scrollView;
    StatsViewAllController * allView;
    
    UITextField * searchText;
}

@property(nonatomic, retain) IBOutlet UITableView * aTableView;
@property(nonatomic, retain) StatsViewScrollController * scrollView;
@property(nonatomic, retain) StatsViewAllController * allView;

@property(nonatomic, retain) NSMutableArray * callArray;
@property(nonatomic, retain) NSMutableArray * callModuleArray;
@property(nonatomic, retain) NSMutableArray * userArray;

@property(nonatomic, retain) IBOutlet UITextField * searchText;

-(IBAction)showAll:sender;

@end
