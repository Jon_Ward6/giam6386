//
//  LaunchViewController.h
//  BeyondHIV
//
//  Created by Jonathan Xie on 8/08/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Model;
@class UserVO;

@interface LaunchViewController : UIViewController {
    Model * model;
    
    UserVO * userVO_;
}

@property(nonatomic, retain) UserVO * userVO;

@end