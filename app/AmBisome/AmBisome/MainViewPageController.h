//
//  MainViewPageController.h
//  Demo
//
//  Created by Jonathan Xie on 14/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Model;
@class DataModel;

@interface MainViewPageController : UIViewController <UIWebViewDelegate>{
	UIWebView * aWebView;
	NSString * type;
    NSString * name;
    
    
    NSInteger pageIndex;
    NSArray * dataArray;
    BOOL viewNeedsUpdate;
    
    Model * model;
    DataModel * dataModel;
    
    
    
    UIActivityIndicatorView * indicator;
}

@property (nonatomic, retain) IBOutlet UIWebView * aWebView;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * name;

@property (nonatomic, retain) UIActivityIndicatorView * indicator;

@property(nonatomic, assign) NSInteger pageIndex;
@property(nonatomic, retain) NSArray * dataArray;

- (id)initWithPage:(NSDictionary *)pageDict;
- (id)initWithFileName:(NSString *)fileName;
- (void)notifyPageAction:(NSString *)action;
- (void)callJavascriptFunction:(NSString*)functionName;

- (void)update:(NSInteger)newPageIndex data:(NSArray*)anArray;

- (BOOL)isPageType:(NSString*)pageType;

@end


@interface MainViewPageController ()

-(void)gotoPage;

@end
