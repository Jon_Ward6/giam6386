//
//  StatsViewSingleController.m
//  eDetailer
//
//  Created by Jonathan Xie on 27/04/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "StatsViewSingleController.h"
#import "UserVO.h"
#import "CallVO.h"
#import "CallModuleVO.h"
#import "DataModel.h"
#import "PracticeVO.h"
#import "CallPageVO.h"


@implementation StatsViewSingleController


@synthesize callVO=callVO_, pageArr=pageArr_;
//@synthesize userName, practiceName, address, suburb, email;
@synthesize callTime, totalDuration, moduleViews, pageViews;
@synthesize aTableView, groupButton=groupButton_;
@synthesize callTitle=callTitle_, callLabel, tableContents=tableContents_;


- (id)initWithCallObject:(CallVO*)aCallVO label:(NSString *)labelString
{
    self = [super initWithNibName:@"StatsViewSingleController" bundle:nil];
    if (self!=nil) {
        self.callVO = aCallVO;
        self.callTitle = labelString;
    }
    return self;
}


- (void)dealloc
{    
    [tableContents_ release],tableContents_=nil;
    [groupButton_ release],groupButton_=nil;
    [pageArr_ release],pageArr_=nil;
    [callVO_ release],callVO_=nil;
    [aTableView release];
    [callTime release],aTableView=nil;
    [totalDuration release],totalDuration=nil;
    [moduleViews release],moduleViews=nil;
    [pageViews release],pageViews=nil;
    [callTitle_ release],callTitle_=nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    
    
    self.totalDuration.text = [self timeFormat:self.callVO.time_spent];
    //
    NSMutableArray * callModules = [[DataModel sharedDataModel] callModuleSelectByCallId:self.callVO.call_uid];
    
    //get the correct number of unique modules
    NSInteger modules = [[callModules valueForKeyPath:@"@distinctUnionOfObjects.module_uid"] count]; 
    self.moduleViews.text = [NSString stringWithFormat:@"%d", modules];
    
    //
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy 'at' HH:mm"];
    
    self.callTime.text = [dateFormat stringFromDate:self.callVO.endTime];
    
    [dateFormat release];
    
    self.aTableView = [[[UITableView alloc] initWithFrame:CGRectMake(59,120,910,368) style:UITableViewStylePlain] autorelease];
    self.aTableView.dataSource = self;
    self.aTableView.delegate = self;
    
    [self.view addSubview:self.aTableView];
    
    self.aTableView.pagingEnabled = YES;
    self.aTableView.backgroundColor = [UIColor clearColor];
    self.aTableView.rowHeight = 32;
    
    //
    NSArray * pageArrbyID = [[DataModel sharedDataModel] callPageSelectByCallId:self.callVO.call_uid];
   // self.pageArr = [[self sectionArray:pageArrbyID] retain];//[[DataModel sharedDataModel] callPageSelectByCallId:self.callVO.call_uid];
    self.pageArr = [self sectionArray:pageArrbyID];
    
    //[self.aTableView reloadData];
    
    if([self.pageArr count]>0){
        [self.aTableView setAlpha:1.0];
        [self.aTableView reloadData];
    }else{
        [self.aTableView setAlpha:0.0];
    }
    

    //group button
    [self.groupButton setAlpha:[self.callVO.groupcall_uid length]>0? 1.0 : 0.0];
    
    //Call Label
    self.callLabel.text = self.callTitle;
    //
    self.pageViews.text = [NSString stringWithFormat:@"%d", [pageArrbyID count]];
}


-(NSString *)timeFormat:(double)seconds{
    NSString * timeStr = @"0";
    
    if(seconds>0){
        int secs = (int)seconds % 60;
        int mins = (int)seconds / 60;
        int hrs  = (int)mins / 60;
        
        timeStr = [NSString stringWithFormat:@"%02d : %02d : %02ds", hrs, mins, secs];
    }
    return timeStr;
}





#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.pageArr count];
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *listData = [self.tableContents objectForKey:[self.pageArr objectAtIndex:section]];
    return [listData count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSArray *listData = [self.tableContents objectForKey:[self.pageArr objectAtIndex:indexPath.section]];
    
    
    CallPageVO * callPageVO = [listData objectAtIndex:indexPath.row];//[self.pageArr objectAtIndex:indexPath.row];
    
    NSString * pageName = [callPageVO.page_name stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    
    cell.textLabel.text = pageName? pageName : callPageVO.page_uid;
    cell.textLabel.textColor = [UIColor colorWithWhite:0.0 alpha:0.7];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    
    
    cell.detailTextLabel.text  = [self timeFormat:callPageVO.time_spent];
    cell.detailTextLabel.textColor = [UIColor darkGrayColor];//[UIColor colorWithRed:204.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:14.0];
    
    
    UIView *myView = [[UIView alloc] init];
    if ((indexPath.row % 2) == 0)
        myView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.6];
    else
        myView.backgroundColor = [UIColor clearColor];
    
    cell.backgroundView = myView;
    [myView release];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return [self.pageArr objectAtIndex:section];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSString* moduleUid = [self.pageArr objectAtIndex:section];
    
    ModuleVO * moduleVO;
    
    //this is not the ideal set of quick access module but as we have asked to add this so late then the mixin method is good!
    if([moduleUid isEqualToString:@"quickaccess"]){
        moduleVO = [[[ModuleVO alloc] init] autorelease];
        moduleVO.title = @"Quick Access";
    }else{
        moduleVO = [[DataModel sharedDataModel] moduleSelectByUid:moduleUid];
    }
    
    
    
    UIView * sectionView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.aTableView.frame.size.width, 60)] autorelease];
    sectionView.backgroundColor = [UIColor colorWithWhite:0.91 alpha:0.91];//[UIColor colorWithWhite:0.7 alpha:0.9];
    //
    UILabel * headerText = [[[UILabel alloc] initWithFrame:CGRectMake(0, 15, self.aTableView.frame.size.width/2, 32)] autorelease];
    headerText.text = [NSString stringWithFormat:@"  %@", moduleVO.title]; //moduleVO.title;
    headerText.backgroundColor = [UIColor clearColor];
    headerText.font = [UIFont boldSystemFontOfSize:19];
    [sectionView addSubview:headerText];
    
    /*
    UILabel * timeText = [[[UILabel alloc] initWithFrame:CGRectMake(self.aTableView.frame.size.width - 150, 20, 140, 32)] autorelease];
    timeText.text = @"hours : minutes : seconds";
    timeText.textColor = [UIColor grayColor];
    timeText.backgroundColor = [UIColor clearColor];
    timeText.font = [UIFont systemFontOfSize:12];
    [sectionView addSubview:timeText];
     */
    
    UILabel * labelText = [[[UILabel alloc] initWithFrame:CGRectMake(self.aTableView.frame.size.width - 145, 17, 80, 32)] autorelease];
    labelText.text = @"Total";
    labelText.textColor = [UIColor grayColor];
    labelText.backgroundColor = [UIColor clearColor];
    labelText.font = [UIFont boldSystemFontOfSize:14];
    [sectionView addSubview:labelText];
    
    
    UILabel * timeText = [[[UILabel alloc] initWithFrame:CGRectMake(self.aTableView.frame.size.width - 87, 17, 100, 32)] autorelease];
    timeText.text = [self timeFormat:[self sectionTotalTime:section]];
    timeText.textColor = [UIColor colorWithRed:204.0f/255.0f green:102.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
    timeText.backgroundColor = [UIColor clearColor];
    timeText.font = [UIFont systemFontOfSize:14];
    [sectionView addSubview:timeText];
    
    
	return sectionView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 60;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView * footerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.aTableView.frame.size.width, 60)] autorelease];
    /*
    UILabel * labelText = [[[UILabel alloc] initWithFrame:CGRectMake(self.aTableView.frame.size.width - 160, 0, 80, 32)] autorelease];
    labelText.text = @"Total";
    labelText.textColor = [UIColor grayColor];
    labelText.backgroundColor = [UIColor clearColor];
    labelText.font = [UIFont boldSystemFontOfSize:14];
    [footerView addSubview:labelText];
    
    
    UILabel * timeText = [[[UILabel alloc] initWithFrame:CGRectMake(self.aTableView.frame.size.width - 87, 0, 100, 32)] autorelease];
    timeText.text = [self timeFormat:[self sectionTotalTime:section]];
    timeText.textColor = [UIColor colorWithRed:204.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
    timeText.backgroundColor = [UIColor clearColor];
    timeText.font = [UIFont systemFontOfSize:14];
    [footerView addSubview:timeText];
     */
    
	return footerView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
	return 40;
}



#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES]; 
}


-(IBAction)groupButtonClick:(id)sender
{
    
    
    NSMutableArray * userArr = [[DataModel sharedDataModel] getGroupCallByID:self.callVO.groupcall_uid];
    NSString * usersName = @"";
    for (UserVO * user in userArr) {
        usersName = [NSString stringWithFormat:@"%@\n %@ %@", usersName, user.firstname, user.lastname];
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Users in this Group:" 
                                                    message: usersName 
                                                   delegate:self 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
}



-(NSArray*)sectionArray:(NSArray*)sourceArray{
    //NSMutableArray * _pageArr;
    NSMutableArray * sections;
    NSMutableArray * sectionArr;
    NSMutableDictionary * dict = [[[NSMutableDictionary alloc] init] autorelease];
    
    sections = [sourceArray valueForKeyPath:@"@distinctUnionOfObjects.module_uid"];
    //NSLog(@"sections:%@", sections);
    //NSLog(@"sourceArray:%@", sourceArray);
    
    for(NSString * section in sections){
        sectionArr = [[[NSMutableArray alloc] init] autorelease];
        for(CallPageVO * pageVO in sourceArray){
            if([pageVO.module_uid isEqualToString:section]){
                [sectionArr addObject:pageVO];
            }
        }        
        
        [dict setObject:sectionArr forKey:section];
        //[_pageArr addObject:dict];
    }
    
    self.tableContents = dict;
    
    
    return [[self.tableContents allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
}

-(double)sectionTotalTime:(NSInteger)section{
    double duration = 0.0f;
    NSArray *listData = [self.tableContents objectForKey:[self.pageArr objectAtIndex:section]];
    for(CallPageVO * pageVO in listData){
        duration += pageVO.time_spent;
    }
    
    return duration;
}




@end