//
//  MainViewNavController.h
//  Demo
//
//  Created by Jonathan Xie on 16/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Model;
@class DataModel;

@interface MainViewNavController : UIViewController{
    Model * model;
    DataModel * dataModel;
    
    UIView * demoview;
    
    
    
}


@property(nonatomic, retain) IBOutlet UIView * demoview;


-(void)showDemoView:(BOOL)flag;
-(void)resetAll;
-(IBAction)onEndCall:sender;

@end
