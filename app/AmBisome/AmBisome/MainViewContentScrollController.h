//
//  MainViewContentScrollController.h
//  AmBisome
//
//  Created by Jonathan Xie on 9/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AppScrollView;
@class Model;

@interface MainViewContentScrollController : UIViewController<UIScrollViewDelegate>{
    Model * model;
    NSArray * pageArr;
    NSMutableArray *pageControllers;
    NSInteger currentPageIndex;
    
    AppScrollView * scrollView;
}

@property(nonatomic, retain) IBOutlet AppScrollView * scrollView;
@property(nonatomic, retain) NSArray * pageArr;
@property(nonatomic, retain) NSMutableArray * pageControllers;
@property (nonatomic, assign) NSInteger currentPageIndex;

@property (nonatomic, retain) UIButton * closeBtn;
@property (nonatomic, retain) NSString * moduleID;


- (id)initWithDataArray:(NSArray *)array;
- (id)initWithDataArray:(NSArray *)array moduleID:(NSString*)mid;

-(void)nextPage;
-(void)nextPage;
@end


