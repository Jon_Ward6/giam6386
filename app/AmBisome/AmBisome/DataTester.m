//
//  DataTester.m
//  eDetailer
//
//  Created by Jonathan Xie on 19/05/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "DataTester.h"
#import "Model.h"
#import "JSON.h"
#import "Constants.h"
#import "DataModel.h"
#import "UserVO.h"
#import "RepVO.h"
#import "PracticeVO.h"

@implementation DataTester


-(void)test{
    model = [Model sharedModel];
    dataModel = [DataModel sharedDataModel];
    
    
    //NSLog(@"quickaccess: %@", [dataModel pageQuickAccessSelectByModuleId:@"quickaccess17"]);
    
    //[dataModel testOn];
    
    //NSLog(@"data::%@", [dataModel callPageSelectAllData]);
    
    //get pageCalls
    //NSDictionary * dict3 = [[[NSDictionary alloc] initWithObjectsAndKeys:@"Atripla-Ward6-Digital1", @"rep_uid", nil] autorelease];
    //[ dataModel serverRequestWithAction:@"getcallpages" andData:[dict3 JSONRepresentation]];
    
    
    //Testing for dataModel -------------------------------------------------------------------
    
    //NSLog(@"moduleSelectByUid: %@", [[dataModel moduleSelectByUid:@"casestudy1"] title]);
    
    
    //NSString *uuid = [[NSProcessInfo processInfo] globallyUniqueString];
    //NSLog(@"UUID: %@", uuid);
    
    
    //page
    //NSLog(@"moduleSelectAll:%@", [dataModel moduleSelectAll]);
    
    //user
    /*
     NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
     [dict setValue:[NSNumber numberWithInt:1] forKey:@"rep_id"];
     [dict setValue:[NSNumber numberWithInt:1] forKey:@"practice_id"];
     [dict setValue:@"John" forKey:@"firstname"];
     [dict setValue:@"Smith" forKey:@"lastname"];
     [dict setValue:@"john.smith@gmail.com" forKey:@"email"];
     
     
     NSLog(@"userAdd:%d", [dataModel userAdd:dict]);
     
     NSLog(@"userSelectAll:%@", [dataModel userSelectAll]);
     */
    
    /*
     //practice
     NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
     [dict setValue:@"Sydney Medical Center" forKey:@"name"];
     [dict setValue:@"123 Macquaire street" forKey:@"street"];
     [dict setValue:@"city" forKey:@"suburb"];
     [dict setValue:@"2000" forKey:@"postcode"];
     [dict setValue:@"nsw" forKey:@"state"];
     [dict setValue:@"2000" forKey:@"region_code"];
     
     
     
     NSLog(@"practiceAdd:%d", [dataModel practiceAdd:dict]);
     
     NSLog(@"practiceSelectOne:%@", [dataModel practiceSelectOne:1]);
     */
    
    //PracticeVO * practiceVO = [dataModel practiceSelectOne:@""];
    
   // NSLog(@"practiceSelectOne:%@", practiceVO.name);

    
	/*
     NSMutableDictionary * rep = [[NSMutableDictionary alloc] init];
     [rep setValue:@"demo" forKey:@"username"];
     [rep setValue:@"demo" forKey:@"password"];
     [rep setValue:@"demo@gmail.com" forKey:@"email"];
     [rep setValue:@"1" forKey:@"access_level"];
     */
    //[dataModel repAdd:rep];
    // NSLog(@"repAdd:%d", [dataModel repAdd:rep]);
    
    // NSLog(@"login:%@", [dataModel repSelectOne:@"demo" passwd:@"demo"]);
	
    
    //test data
    //[model removeLocalClientData];
    
    //NSLog(@"localClientData: %@", [model localClientData]);
    //NSLog(@"localClientData: %@", [model searchLocalClientData:@"North Shore"]);
    //NSLog(@"test:%@", [NSString stringWithFormat:@"%%%@%%", @"John"]);
    /*
     //1. clear up the local data
     
     [model postData:[[model localData] JSONRepresentation]];
     [model removeLocalData];
     
     
     //2. compose the new data
     NSMutableDictionary *myDictionary = [[[NSMutableDictionary alloc] init]autorelease];
     [myDictionary setValue:@"Jono5" forKey:@"firstname"];
     [myDictionary setValue:@"Xie5" forKey:@"lastname"];
     [myDictionary setValue:@"jonoxie4@gmail.com" forKey:@"email"];
     [myDictionary setValue:@"100" forKey:@"module1"];
     [myDictionary setValue:@"200" forKey:@"module2"];
     [myDictionary setValue:@"300" forKey:@"module3"];
     [myDictionary setValue:@"400" forKey:@"module4"];
     [myDictionary setValue:@"500" forKey:@"module5"];
     
     //build the array
     NSMutableArray *dataArr = [[NSMutableArray alloc] initWithObjects:myDictionary, nil];
     
     //jSON
     //NSString* jsonString = [dataArr JSONRepresentation];
     //[model postData:jsonString];
     
     if([model isWebSiteReachable:SERVER_NAME]){
     // NSString* jsonString = [dataArr JSONRepresentation];
     // [model postData:jsonString];
     }else{
     //save to local db
     // [model setLocalData:dataArr];
     
     }
     
     [model setLocalData:dataArr];
     
     
     NSLog(@"data: %@", [model localData]);
     */
    
    
    //NSDictionary * dict = [[NSDictionary alloc] initWithObjectsAndKeys:@"jon-ward6", @"rep_uid", nil];
    //[dataModel serverRequestWithAction:@"getcalls" andData:[dict JSONRepresentation]];
    
    
    /*
     //test2
     //rep_id, practice_id,firstname, lastname, email, date_creation, date_update
     NSMutableDictionary *myDictionary = [[[NSMutableDictionary alloc] init]autorelease];
     [myDictionary setValue:@"Jono5" forKey:@"firstname"];
     [myDictionary setValue:@"Xie5" forKey:@"lastname"];
     [myDictionary setValue:@"jonoxie4@gmail.com" forKey:@"email"];
     [myDictionary setValue:[NSNumber numberWithInt:10] forKey:@"rep_id"];
     [myDictionary setValue:[NSNumber numberWithInt:3] forKey:@"practice_id"];
     
     //build the array
     NSMutableArray *dataArr = [[NSMutableArray alloc] initWithObjects:myDictionary, nil];
     
     //jSON
     NSString* jsonString = [dataArr JSONRepresentation];
     [dataModel serverRequestWithAction:@"adduser" andData:jsonString];
     */
    /*
     NSDictionary * dict = [[NSDictionary alloc] initWithObjectsAndKeys:@"jon-ward6", @"rep_uid", nil];
     
     //NSMutableArray *dataArr = [[[NSMutableArray alloc] initWithObjects:dict, nil] autorelease];
     
     [dataModel serverRequestWithAction:@"getusers" andData:[dict JSONRepresentation]];
     
     //NSLog(@"dict: %@", [dict JSONRepresentation]);
     
     */
    
    /*
     //test server rep
     NSMutableDictionary *myDictionary = [[[NSMutableDictionary alloc] init]autorelease];
     [myDictionary setValue:@"jon" forKey:@"username"];
     [myDictionary setValue:@"ward6" forKey:@"password"];
     
     
     [dataModel serverRequestWithAction:@"getrep" andData:[myDictionary JSONRepresentation]];
     
     NSLog(@"dict: %@", [myDictionary JSONRepresentation]);
     */
    /*
     //we should have the delegate to call back. for now, just try to login again!
     NSMutableDictionary *repDict = [[[NSMutableDictionary alloc] init]autorelease];
     [repDict setValue:@"rep6" forKey:@"username"];
     [repDict setValue:@"ward6" forKey:@"password"];
     
     //[dataModel serverRequestWithAction:@"getrep" andData:[repDict JSONRepresentation]];
     
     
     
     
     RepVO * rep1 = [[RepVO alloc] init];
     rep1.rep_uid = @"ward6-rep6";
     rep1.username = @"rep6";
     rep1.password = @"ward6";
     rep1.email = @"rep6@gmail.com.au";
     rep1.access_level = 10;
     
     //[dataModel repAdd:rep];
     NSLog(@"repAdd:%d", [dataModel repAdd:rep1]);
     
     // NSLog(@"login:%@", [dataModel repSelectOne:@"demo" passwd:@"demo"]);
     */

    
}

- (void)dealloc {
    [model release], model = nil;
    [dataModel release], dataModel=nil;
    [super dealloc];
}

@end
