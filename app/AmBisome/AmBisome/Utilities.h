//
//  Utilities.h
//  AtriplaED
//
//  Created by Jonathan Xie on 28/06/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface Utilities : NSObject {
    
}

//generates md5 hash from a string
+ (NSString *) returnMD5Hash:(NSString*)concat;

@end
