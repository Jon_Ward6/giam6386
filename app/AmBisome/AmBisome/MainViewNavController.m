//
//  MainViewNavController.m
//  Demo
//
//  Created by Jonathan Xie on 16/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "MainViewNavController.h"
#import "Model.h"
#import "DataModel.h"
#import "ViewController.h"
#import "OptionViewController.h"
#import "CallVO.h"
#import "ModuleVO.h"
#import "CallModuleVO.h"
#import "RepVO.h"
#import "PageVO.h"
#import "MainViewContentController.h"
#import "JSON.h"
#import "Constants.h"
#import "MainViewController.h"

/*
 Important Note:
 We are using this Nav controller for both MainViewController and MainViewContentController
 to detect where it's at, we assign the tag value:
 
 100 is the MainViewController. if(self.view.tag==100)
 101 is the MainViewContentController. if(self.view.tag==101)
 
 */

@implementation MainViewNavController

@synthesize demoview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"restartModule" object:nil];
    [demoview release], demoview=nil;
    [dataModel release], dataModel = nil;
    //[model release], model = nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    self.view.frame = CGRectMake(0.0, 0.0, 1024.0, 50.0);
    
    model = [Model sharedModel];
    dataModel = [DataModel sharedDataModel];
    
    [model retain];
    
    //self.demoview.alpha = 0;????
    
    
    self.demoview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-demo.png"]];
    [self showDemoView:NO];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(goBackHome:) 
                                                 name:@"restartModule"
                                               object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}


//calling from the end of presentation (restart button)
-(void)goBackHome:(NSNotification*)notification{
    [self onBackHome:nil];
}


-(IBAction)onBackHome:sender{
    //reset the session back to default 0
    [model.moduleSessionData setValue:[NSNumber numberWithInt:0] forKey:@"casestudy"];
    
    
    if(self.view.tag==101){
        UINavigationController * navController = model.appView.navController;
        
        [UIView beginAnimations:@"animation" context:nil];
        [UIView setAnimationDuration:1.00];
        [model.appView.navController popToRootViewControllerAnimated:NO]; 
        [[navController.viewControllers objectAtIndex:0] viewDidAppear:YES];
        
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:navController.view cache:NO]; 
        [UIView commitAnimations];
        
        //end the module and add to the call object
        if(model.callVO != nil){
            model.moduleVO.endTime = [NSDate date];
            [model.callVO addModule:model.moduleVO];
        } 
        
    }
    
    
    if(self.view.tag==100){
        //if its on MainViewController, we should end call here
        //diabled! don't need to go home...
        //[self onEndCall:nil];
        
        
        /*
         UINavigationController * navController = model.appView.loginNavController;
         
         //
         [UIView animateWithDuration:0.9
         delay:0.0
         options: UIViewAnimationCurveEaseOut
         animations:^{
         CGRect frame = navController.view.frame;
         frame.origin.y = -20;
         navController.view.frame = frame;
         } 
         completion:^(BOOL finished){
         [model.appView.navController popToRootViewControllerAnimated:YES];
         }
         ];
         
         //reset - hide all the layers
         [self resetAll];
         
         */
    }
    
}



-(IBAction)onEndCall:sender{
    //reset the session back to default 0
    [model.moduleSessionData setValue:[NSNumber numberWithInt:0] forKey:@"casestudy"];
    
    BOOL isOnline = [model isWebSiteReachable:SERVER_NAME];
    
    
    if(model.callVO != nil && model.callVO.call_uid!=nil){
        //end quickaccess page time tracking
        if(model.quickAccessCurrentPageVO!=nil){
            model.quickAccessCurrentPageVO.endTime = [NSDate date];
            [model.quickAccessModuleVO addPage:model.quickAccessCurrentPageVO];
        }
        
        
        
        //end quickaccess module
        model.quickAccessModuleVO.endTime = [NSDate date];
        if([model.quickAccessModuleVO.pageArray count]>0){
            [model.callVO addModule:model.quickAccessModuleVO];
        }
        
        //end page time tracking
        if(model.currentPageVO){
            model.currentPageVO.endTime = [NSDate date];
            [model.moduleVO addPage:model.currentPageVO];
        }
        
        //end module
        if(model.moduleVO){
            model.moduleVO.endTime = [NSDate date];
            [model.callVO addModule:model.moduleVO];
        }
        
        //end call / insert to DB
        model.callVO.endTime = [NSDate date];
        int insertID = [dataModel callAdd:model.callVO];
        
        
        
        //CallModule
        if(insertID>0){
            CallModuleVO * callModuleVO;
            
            for(ModuleVO * module in model.callVO.moduleArray){
                callModuleVO = [[[CallModuleVO alloc] init] autorelease];
                callModuleVO.call_uid = model.callVO.call_uid;
                callModuleVO.module_uid = module.module_uid;
                callModuleVO.user_uid = model.userVO.user_uid;
                callModuleVO.time_spent = module.time_spent;
                //
                [dataModel callModuleAdd:callModuleVO];
                
                //save page tracking data
                [dataModel savePageArray:module.pageArray];
            }
            
        }
        
        
        //Sync up all the data to the server
        if(isOnline){
            [dataModel uploadAllData];
        }
        
        
    }
    
    
    //
    [self resetAll];
    
    
    //try to delay the animation to give more time for data saving
    [self performSelector:@selector(delayCloseAnimation) withObject:nil afterDelay:0.2];
    
    
}


-(void)delayCloseAnimation{
    //nil all cache data
    model.callVO = nil;
    model.quickAccessCurrentPageVO = nil;
    model.currentPageVO = nil;
    model.moduleVO = nil;
    
    
    //now call animation
    UINavigationController * navController = model.appView.loginNavController;
    
    OptionViewController * optionView = (OptionViewController *)[navController.viewControllers objectAtIndex:1 ];
    
    [navController popToViewController:optionView animated:YES];
    
    
    //
    [UIView animateWithDuration:0.9
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         CGRect frame = navController.view.frame;
                         frame.origin.y = 0;//-20;
                         navController.view.frame = frame;
                     } 
                     completion:^(BOOL finished){
                         [model.appView.navController popToRootViewControllerAnimated:YES];
                     }
     ];
}



-(IBAction)onDemoAction:sender{
    if(self.demoview.alpha==0){
        [self showDemoView:YES];
    }else{
        [self showDemoView:NO];
    }
}


-(void)showDemoView:(BOOL)flag
{
    if(flag==YES){
        [UIView beginAnimations:@"animation" context:nil];
        [UIView setAnimationDuration:0.8];
        
        //demoView
        CGRect demoFrame = self.demoview.frame;
        demoFrame.origin.y = 0;
        self.demoview.frame = demoFrame;
        self.demoview.alpha = 1.0;
        
        
        [UIView commitAnimations];
    }else{
        
        [UIView beginAnimations:@"animation" context:nil];
        [UIView setAnimationDuration:0.80];
        
        //demoView
        CGRect demoFrame = self.demoview.frame;
        demoFrame.origin.y = -(768 + 60);
        self.demoview.frame = demoFrame;
        self.demoview.alpha = 0.0;
        
        
        [UIView commitAnimations];
    }
}



-(IBAction)onHelpAction:sender{
    
    if(self.view.tag==101){
        [model.mainViewContentController showQuickAccessView:YES];
    }
    
    if(self.view.tag==100){
        [model.mainViewController showQuickAccessView:YES];
    }
}



-(void)resetAll{
    if(self.view.tag==100){
        [model.mainViewController showQuickAccessView:NO];
        [self showDemoView:NO];
        [self.view setAlpha:0];
    }
}


@end
