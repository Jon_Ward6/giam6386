//
//  UIView+Extensions.m
//  iDeal
//
//  Created by casserd on 23/03/2010.
//  Copyright 2010 devedup.com. All rights reserved.
//

#import "UIView+Extensions.h"

@implementation UIView (Extensions)

- (void) maintainVisibityOfControl:(UIControl *)control toView:(UIView*)view offset:(float)offset {
	static const float deviceHeight = 768;//460;//
	static const float keyboardHeight = 216;//352;//216//iphone;
	static const float gap = 5; //gap between the top of keyboard and the control
	
	//Find the controls absolute position in the 320*480 window - it could be nested in other views
	CGPoint absolute = [control.superview convertPoint:control.frame.origin toView:view];
    
	//If it would be hidden behind the keyboard....
	if (absolute.y > (keyboardHeight + gap)) {
		//Shift the view
		float shiftBy = (deviceHeight - absolute.y) - (deviceHeight - keyboardHeight - gap - offset);
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.3f]; //this is speed of keyboard
		CGAffineTransform slideTransform = CGAffineTransformMakeTranslation(0.0, shiftBy);
		self.transform = slideTransform;			
		[UIView commitAnimations];
	}	
}

- (void) resetViewToIdentityTransform {		
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3f]; //this is speed of keyboard
	CGAffineTransform slideTransform = CGAffineTransformIdentity;
	self.transform = slideTransform;			
	[UIView commitAnimations];
}


@end
