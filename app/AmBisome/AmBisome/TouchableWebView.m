//
//  TouchableWebView.m
//  Demo
//
//  Created by Jonathan Xie on 15/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "TouchableWebView.h"


@implementation TouchableWebView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// Could override any of the touches events here

- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event {
    
    // Do your custom handling here then call the superclass
	[super touchesEnded: touches withEvent: event];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [super dealloc];
}

@end
