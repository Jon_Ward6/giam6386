//
//  KeyCodeLoginViewController.h
//  BeyondHIV
//
//  Created by Jonathan Xie on 6/01/12.
//  Copyright (c) 2012 Ward6. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataModel.h"

@interface KeyCodeLoginViewController : UIViewController<UITextFieldDelegate, DataModelDelegate>{
    UITextField * keycodeText;
    UIControl *currentControl;
    
    DataModel * dataModel;
}

@property(nonatomic, retain) IBOutlet UITextField * keycodeText;
@property (nonatomic, assign) UIControl *currentControl;

@end
