//
//  CallVO.m
//  Demo
//
//  Created by Jonathan Xie on 23/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import "CallVO.h"
#import "ModuleVO.h"

@implementation CallVO

@synthesize startTime=startTime_, endTime=endTime_, moduleArray=moduleArray_, time_spent;
@synthesize rowid, call_uid, user_uid, rep_uid, usersArr, groupcall_uid;


-(void)setEndTime:(NSDate *)aEndTime{
    //endTime_ = [aEndTime retain];
    
    if (endTime_ != aEndTime)
    {
        [endTime_ release];
        endTime_ = [aEndTime retain];
    }
    
    self.time_spent = [endTime_ timeIntervalSinceDate:startTime_];
}

- (void)dealloc {
    [groupcall_uid release];
    [usersArr release];
    [rep_uid release];
    [user_uid release];
    [call_uid release];
    [startTime_ release];
    [endTime_ release];
    [moduleArray_ release], moduleArray_=nil;
    [super dealloc];
}

- (void)addModule:(ModuleVO*)moduleVO{
    
    if(self.moduleArray==nil){
        self.moduleArray = [[[NSMutableArray alloc] init] autorelease]; 
    }
    
    //
    for (ModuleVO* module in self.moduleArray){
        if([moduleVO.module_uid isEqualToString:module.module_uid]){
            
            if(moduleVO.time_spent>0){
                module.time_spent += moduleVO.time_spent;
                //merge pages here!
                // NSLog(@"module.totalTime2:%d", module.totalTime);
            }
            return;
        }
    }
    //if it's new module
    if(moduleVO) [self.moduleArray addObject:moduleVO];
}



@end
