//
//  UIView+Extensions.h
//  iDeal
//
//  Created by casserd on 23/03/2010.
//  Copyright 2010 devedup.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/*
	Generally its bad practice to use categories on Cocoa classes as
	if a later API is released with the same method as these, then there
	would be name clashes, and problems of devestating proportions !
 
	So only add stuff here if you're sure it won't clash ! (or you don't care)
 
 */
@interface UIView (Extensions)

// This will scroll up the view so the textfield is always in view when the keyboard
// appears and then there is another method to reset it
- (void) maintainVisibityOfControl:(UIControl *)control toView:(UIView*)view offset:(float)offset; 
- (void) resetViewToIdentityTransform;

@end
