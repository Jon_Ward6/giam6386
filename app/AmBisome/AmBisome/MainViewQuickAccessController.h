//
//  MainViewQuickAccessController.h
//  AtriplaED
//
//  Created by Jonathan Xie on 27/06/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppScrollView.h"

@class Model;

@class MainViewQuickAccessMenuController;

@interface MainViewQuickAccessController : UIViewController <UIScrollViewDelegate>{
    Model * model;

    //data
    //screens for ecach module
    NSArray * contentArr;
    NSMutableArray *pageControllers;
    
    //UIs
    AppScrollView * scrollView;
    MainViewQuickAccessMenuController * menuViewController;
    UIButton * closeBtn;
    
    //Bool
    BOOL isScrollableContent;
    NSInteger currentPageIndex;
}

@property(nonatomic, retain) NSArray * contentArr;
@property(nonatomic, retain) IBOutlet AppScrollView * scrollView;
@property(nonatomic, retain) NSMutableArray * pageControllers;
@property(nonatomic, retain) MainViewQuickAccessMenuController * menuViewController;
@property(nonatomic, retain) UIButton * closeBtn;
@property (nonatomic, assign) NSInteger currentPageIndex;

- (id)initWithContent:(NSArray *)anArray scrollEnabled:(BOOL)scroll;
- (void)showMenu:(BOOL)flag animated:(BOOL)animated;
- (void)showContent:(NSArray *)anArray scrollEnabled:(BOOL)scroll;

-(void)gotoPage:(int)page;
-(void)nextPage;
-(void)prevPage;

- (void)trackPage:(NSInteger)page;


@end




@interface MainViewQuickAccessController(private)

- (void)resetScrollView;
- (void)configScrollView;
- (void)loadScrollViewWithPage:(int)page;

@end