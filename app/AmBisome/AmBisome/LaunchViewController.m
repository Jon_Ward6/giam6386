//
//  LaunchViewController.m
//  BeyondHIV
//
//  Created by Jonathan Xie on 8/08/11.
//  Copyright 2011 Ward6. All rights reserved.
//

#import "LaunchViewController.h"
#import "Model.h"
#import "UserVO.h"
#import "CallVO.h"
#import "RepVO.h"

@implementation LaunchViewController

@synthesize userVO=userVO_;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [userVO_ release], userVO_=nil;
    [model release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    model = [Model sharedModel];
    
}


-(IBAction)canelAction:sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)startAction:sender{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.60];
    
    CGRect frame = self.navigationController.view.frame;
    frame.origin.y = -(768+30);
    self.navigationController.view.frame = frame;
    
    [UIView commitAnimations];
    
    
    
    //safety
    model.currentPageVO = nil;
    model.quickAccessModuleVO=nil;
    model.quickAccessModuleVO = [[ModuleVO alloc] init];
    
    //set up Call new data here
    if(self.userVO!=nil){
        model.userVO = self.userVO;
        
        model.callVO = [[[CallVO alloc] init] autorelease];
        model.callVO.call_uid = [model uniqueString];
        model.callVO.user_uid = self.userVO.user_uid;
        model.callVO.usersArr = model.usersArr;
        model.callVO.groupcall_uid = (model.usersArr!=nil && [model.usersArr count]>1)? [model uniqueString]: nil;
        model.callVO.rep_uid = model.repVO.rep_uid;
        model.callVO.time_spent = 0.0;
        model.callVO.startTime = [NSDate date];
        model.callVO.moduleArray = [[[NSMutableArray alloc] init] autorelease];
        
    }
}




@end
