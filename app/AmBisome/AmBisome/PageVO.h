//
//  PageVO.h
//  Demo
//
//  Created by Jonathan Xie on 23/03/11.
//  Copyright 2011 XIEngine System. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PageVO : NSObject {
    NSInteger rowid;
    NSString * page_uid;
    NSString * page_name;
    NSString * module_uid;
    NSString * filename;
    NSString * type;
    NSDate * startTime;
    NSDate * endTime;
    double time_spent;
}

@property (nonatomic, assign) NSInteger rowid;
@property (nonatomic, retain) NSString * page_uid;
@property (nonatomic, retain) NSString * page_name;
@property (nonatomic, retain) NSString * module_uid;
@property (nonatomic, retain) NSString * filename;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSDate * startTime;
@property (nonatomic, retain) NSDate * endTime;
@property (nonatomic, assign) double time_spent;

@end
